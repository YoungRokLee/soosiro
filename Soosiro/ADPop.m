//
//  ADPop.m
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 7. 29..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import "ADPop.h"

@implementation ADPop
@synthesize index;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
		UIView *back = [[UIView alloc] initWithFrame:frame];
		back.backgroundColor = [UIColor blackColor];
		back.alpha = 0.7f;
		[self addSubview:back];
		
        UIImageView *popup = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iphone_popup.png"]];
		popup.center = self.center;
		[self addSubview:popup];
		
		title = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 40)];
		title.center = CGPointMake(160, 130);
		title.text = @"화상컨설팅서비스";
		title.backgroundColor = [UIColor clearColor];
		title.textAlignment = UITextAlignmentCenter;
//		title.textColor = [UIColor colorWithRed:504 green:50 blue:0 alpha:1];
		title.textColor = [UIColor orangeColor];
		[self addSubview:title];
		
		text = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, 250, 140)];
		text.center = self.center;
		text.backgroundColor = [UIColor clearColor];
		text.font = [UIFont systemFontOfSize:13];
		text.text = @"입학사정관과 일반적으로 이용할 수 있는 자기소개서를 컨설팅 받는 서비스 입니다. 전문 컨설턴트 선생님과 2회 통화를 통하여 컨설팅이 이루어 집니다. 최고의 선생님과 최고의 자기소개서를 준비해 보세요";
		[text setUserInteractionEnabled:NO];
		[self addSubview:text];
		
		url = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 140, 40)];
		url.center = CGPointMake(200, 330);
		url.text = @"> 컨설팅받기";
		url.font = [UIFont systemFontOfSize:15];
		url.textColor = [UIColor grayColor];
		url.backgroundColor = [UIColor clearColor];
		url.textAlignment = UITextAlignmentRight;
		[self addSubview:url];
		
		UIButton *btn = [[UIButton alloc] init];
						  
		[btn setFrame:CGRectMake(0, 0, 40, 40)];
		[btn addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
		[btn setCenter:CGPointMake(270, 110)];
		[self addSubview:btn];
		
		UIButton *btnGo = [[UIButton alloc] init];
		
		[btnGo setFrame:CGRectMake(0, 0, 100, 40)];
		[btnGo addTarget:self action:@selector(goWeb) forControlEvents:UIControlEventTouchUpInside];
		[btnGo setCenter:CGPointMake(220, 330)];
		[self addSubview:btnGo];

    }
    return self;
}

- (void)close {
	[self removeFromSuperview];
}

- (void)goWeb {
	NSURL *sooseero = [[NSURL alloc] initWithString: @"http://www.sooseero.com"];
				  [[UIApplication sharedApplication] openURL:sooseero];
	[self removeFromSuperview];
}

- (void)setIndex:(int)val{
	switch (val) {
		case 0:{
			title.text = @"화상 컨설팅 서비스";
			text.text =@"최고의 입시컨설턴트 선생님과 1:1화상대회를 통하여 합격전략을 세우는 서비스입니다. 여러분이 입력하신 내신성적, 모의고사성적, 특기사항을 바탕으로 수시컨설팅이 진행됩니다. 최고의 컨설턴트 선생님과 최고의 합격전략을 세워보세요!";
			break;
		}
		case 1:{
			title.text = @"논술 첨삭 서비스";
			text.text =@"최고의 논술 첨삭선생님에게 논술지도와 테스트를 받아보세요. 여러분이 올려주신 논술원고를 1회 수정하여 다시 보내드리는 과정으로 진행됩니다. 최고의 논술 선생님과 논술전형을 준비해 보세요!";
			break;
		}
		case 2:{
			title.text = @"자기소개서 컨설팅 서비스";
			text.text =@"입학사정관과 일반적으로 이용할 수 있는 자기소개서를 컨설팅 받는 서비스 입니다. 전문 컨설턴트 선생님과 2회 통화를 통하여 컨설팅이 이루어 집니다. 최고의 선생님과 최고의 자기소개서를 준비해 보세요";
			break;
		}
		default:
			break;
	}
}

@end
