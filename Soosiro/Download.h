//
//  Download.h
//  SampleDownload
//
//  Created by Myounggyu JANG on 11. 1. 16..
//  Copyright 2011 귀여운 아들. All rights reserved.
//

#import <Foundation/Foundation.h>

//MainViewController와 통신(Protocol)
@protocol DownloadDelegate <NSObject>

@optional
- (void) didReceiveData:(NSData *)theData;
- (void) didReceiveFilename:(NSString *)aName;
- (void) dataDownloadFailed:(NSString *)reason;
- (void) dataDownloadAtPercent:(NSNumber *) aPercent;
- (void) dismissActionSheet;
@end

//DownLoad Class
@interface Download : NSObject {

	id<DownloadDelegate>delegate;
	NSURLResponse * response;
	NSMutableData * data;
	NSString	  * urlString;
	NSURLConnection * urlconnection;
	BOOL isDownloading;
}

@property (nonatomic, retain) id delegate;
@property (nonatomic, retain) NSURLResponse * response;
@property (nonatomic, retain) NSMutableData * data;
@property (nonatomic, retain) NSString	 * urlString;
@property (nonatomic, retain) NSURLConnection *urlconnection;
@property (assign) BOOL isDownloading;

+ (Download *) sharedInstance;
+ (void) download:(NSString *) aURLString;
+ (void) cancel;
@end
