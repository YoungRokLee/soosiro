//
//  TypeViewController.h
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 6. 18..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TypeViewController : UIViewController
@property (nonatomic) int menu;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSString *univCode;
@property (strong, nonatomic) NSString *unit;
@property (strong, nonatomic) NSString *univ;
@property (strong, nonatomic) NSString *series03;
@property (strong, nonatomic) NSString *series02;
@property (strong, nonatomic) NSString *series01;
@property (strong, nonatomic) IBOutlet UILabel *labelMainTitle;
@property (strong, nonatomic) IBOutlet UIImageView *imageAD;
- (IBAction)actionBack:(id)sender;


@end
