//
//  SpecialityViewController.m
//  Soosiro
//
//  Created by 이영록 on 12. 6. 20..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import "SpecialityViewController.h"
#import "Language.h"
#import "Special.h"
#import "EnglishGradeViewController.h"
#import "User.h"
#import "TouchXML.h"
@interface SpecialityViewController (){
	UITapGestureRecognizer *tapGesture;
	int idx;
	int oldidx;
	NSArray *adArray;
	NSTimer *timer;
}
@property (strong, nonatomic) NSArray *tabList;
@property (strong, nonatomic) NSArray *langList;
@property (strong, nonatomic) NSDictionary *englishList;
@property (strong, nonatomic) NSArray *specialList;
@property (nonatomic, retain) NSMutableData *receivedData;
@property (nonatomic, retain) NSURLResponse *response;
@property (nonatomic, retain) NSString *rdata;
@end

@implementation SpecialityViewController
@synthesize tabImage01;
@synthesize tabImage02;
@synthesize tabImage03;
@synthesize tableView=_tableView;
@synthesize tabList;
@synthesize langList;
@synthesize englishList;
@synthesize specialList;
@synthesize ID;
@synthesize scrollView;
@synthesize check1;
@synthesize check2;
@synthesize check3;
@synthesize check4;
@synthesize check5;
@synthesize check6;
@synthesize check7;
@synthesize check8;
@synthesize check9;
@synthesize check10;

@synthesize check_1;
@synthesize check_2;
@synthesize check_3;
@synthesize check_4;
@synthesize check_5;
@synthesize check_6;
@synthesize check_7;
@synthesize check_8;
@synthesize check_9;
@synthesize check_10;
@synthesize check_11;
@synthesize check_12;
@synthesize check_13;
@synthesize check_14;
@synthesize check_15;
@synthesize check_16;
@synthesize check_17;
@synthesize check_18;
@synthesize check_19;
@synthesize check_20;
@synthesize check_21;
@synthesize check_22;
@synthesize check_23;
@synthesize check_24;
@synthesize check_25;
@synthesize check_26;
@synthesize check_27;
@synthesize check_28;
@synthesize check_29;
@synthesize check_30;
@synthesize labelMainTitle;
@synthesize imageAD;
@synthesize scrollView1;
@synthesize receivedData;
@synthesize response;
@synthesize rdata;

- (void)visibleImage:(int)index {
	[self update:oldidx];
	idx = index;
	oldidx = index;
	for (UIImageView *imageView in tabList) {
		[imageView setHidden:YES];
	}
	UIImageView *imageView = [tabList objectAtIndex:index];
	[imageView setHidden:NO];
	if (index==0) {
		[scrollView setHidden:YES];
		[self.tableView setHidden:NO];
		[scrollView1 setHidden:YES];
		langList =[Language languageRecordWithEmail:ID];
		
		if (langList.count==0) {
			langList = [NSArray arrayWithObjects:
						[NSMutableDictionary dictionaryWithObjectsAndKeys:@"영어",@"content",@"",@"grade", nil],
						[NSMutableDictionary dictionaryWithObjectsAndKeys:@"중국어",@"content",@"",@"grade", nil],
						[NSMutableDictionary dictionaryWithObjectsAndKeys:@"독일어",@"content",@"",@"grade", nil],
						[NSMutableDictionary dictionaryWithObjectsAndKeys:@"프랑스어",@"content",@"",@"grade", nil],
						[NSMutableDictionary dictionaryWithObjectsAndKeys:@"일본어",@"content",@"",@"grade", nil],
						[NSMutableDictionary dictionaryWithObjectsAndKeys:@"러시아어",@"content",@"",@"grade", nil],
						[NSMutableDictionary dictionaryWithObjectsAndKeys:@"스페인어",@"content",@"",@"grade", nil], 
						[NSMutableDictionary dictionaryWithObjectsAndKeys:@"한자",@"content",@"",@"grade", nil],
						nil];
		}
		[self.tableView reloadData];
	} else if (index==1) {
		[scrollView setHidden:NO];
		[self.tableView setHidden:YES];
		[scrollView1 setHidden:YES];
		specialList = [Special specialRecordWithEmail:ID];
		if (specialList==nil || specialList.count==0 ) {
			check1.highlighted=NO;
			check2.highlighted=NO;
			check3.highlighted=NO;
			check4.highlighted=NO;
			check5.highlighted=NO;
			check6.highlighted=NO;
			check7.highlighted=NO;
			check8.highlighted=NO;
			check9.highlighted=NO;
			check10.highlighted=NO;
			
		}
	} else{
		[scrollView setHidden:YES];
		[self.tableView setHidden:YES];
		[scrollView1 setHidden:NO];
		
		NSDictionary  *dic =[User infoWithID:ID];
		if (dic==nil) return;
		if ([[dic objectForKey:@"P_INFO01"] isEqualToString:@"Y"])  [check_1 setHighlighted:YES]; else  [check_1 setHighlighted:NO];
		if ([[dic objectForKey:@"P_INFO02"] isEqualToString:@"Y"])  [check_2 setHighlighted:YES]; else  [check_2 setHighlighted:NO];
		if ([[dic objectForKey:@"P_INFO03"] isEqualToString:@"Y"])  [check_3 setHighlighted:YES]; else  [check_3 setHighlighted:NO];
		if ([[dic objectForKey:@"P_INFO04"] isEqualToString:@"Y"])  [check_4 setHighlighted:YES]; else  [check_4 setHighlighted:NO];
		if ([[dic objectForKey:@"P_INFO05"] isEqualToString:@"Y"])  [check_5 setHighlighted:YES]; else  [check_5 setHighlighted:NO];
		if ([[dic objectForKey:@"P_INFO06"] isEqualToString:@"Y"])  [check_6 setHighlighted:YES]; else  [check_6 setHighlighted:NO];
		if ([[dic objectForKey:@"P_INFO07"] isEqualToString:@"Y"])  [check_7 setHighlighted:YES]; else  [check_7 setHighlighted:NO];
		if ([[dic objectForKey:@"P_INFO08"] isEqualToString:@"Y"])  [check_8 setHighlighted:YES]; else  [check_8 setHighlighted:NO];
		if ([[dic objectForKey:@"P_INFO09"] isEqualToString:@"Y"])  [check_9 setHighlighted:YES]; else [ check_9 setHighlighted:NO];
		if ([[dic objectForKey:@"P_INFO10"] isEqualToString:@"Y"])  [check_10 setHighlighted:YES]; else [check_10 setHighlighted:NO];
		if ([[dic objectForKey:@"P_INFO11"] isEqualToString:@"Y"])  [check_11 setHighlighted:YES]; else [check_11 setHighlighted:NO];
		if ([[dic objectForKey:@"P_INFO12"] isEqualToString:@"Y"])  [check_12 setHighlighted:YES]; else [check_12 setHighlighted:NO];
		if ([[dic objectForKey:@"P_INFO13"] isEqualToString:@"Y"])  [check_13 setHighlighted:YES]; else [check_13 setHighlighted:NO];
		if ([[dic objectForKey:@"P_INFO14"] isEqualToString:@"Y"])  [check_14 setHighlighted:YES]; else [check_14 setHighlighted:NO];
		if ([[dic objectForKey:@"P_INFO15"] isEqualToString:@"Y"])  [check_15 setHighlighted:YES]; else [check_15 setHighlighted:NO];
		if ([[dic objectForKey:@"P_INFO16"] isEqualToString:@"Y"])  [check_16 setHighlighted:YES]; else [check_16 setHighlighted:NO];
		if ([[dic objectForKey:@"P_INFO17"] isEqualToString:@"Y"])  [check_17 setHighlighted:YES]; else [check_17 setHighlighted:NO];
		if ([[dic objectForKey:@"P_INFO18"] isEqualToString:@"Y"])  [check_18 setHighlighted:YES]; else [check_18 setHighlighted:NO];
		if ([[dic objectForKey:@"P_INFO19"] isEqualToString:@"Y"])  [check_19 setHighlighted:YES]; else [check_19 setHighlighted:NO];
		if ([[dic objectForKey:@"P_INFO20"] isEqualToString:@"Y"])  [check_20 setHighlighted:YES]; else [check_20 setHighlighted:NO];
		if ([[dic objectForKey:@"P_INFO21"] isEqualToString:@"Y"])  [check_21 setHighlighted:YES]; else [check_21 setHighlighted:NO];
		if ([[dic objectForKey:@"P_INFO22"] isEqualToString:@"Y"])  [check_22 setHighlighted:YES]; else [check_22 setHighlighted:NO];
		if ([[dic objectForKey:@"P_INFO23"] isEqualToString:@"Y"])  [check_23 setHighlighted:YES]; else [check_23 setHighlighted:NO];
		if ([[dic objectForKey:@"P_INFO24"] isEqualToString:@"Y"])  [check_24 setHighlighted:YES]; else [check_24 setHighlighted:NO];
		if ([[dic objectForKey:@"P_INFO25"] isEqualToString:@"Y"])  [check_25 setHighlighted:YES]; else [check_25 setHighlighted:NO];
		if ([[dic objectForKey:@"P_INFO26"] isEqualToString:@"Y"])  [check_26 setHighlighted:YES]; else [check_26 setHighlighted:NO];
		if ([[dic objectForKey:@"P_INFO27"] isEqualToString:@"Y"])  [check_27 setHighlighted:YES]; else [check_27 setHighlighted:NO];
		if ([[dic objectForKey:@"P_INFO28"] isEqualToString:@"Y"])  [check_28 setHighlighted:YES]; else [check_28 setHighlighted:NO];
		if ([[dic objectForKey:@"P_INFO29"] isEqualToString:@"Y"])  [check_29 setHighlighted:YES]; else [check_29 setHighlighted:NO];
		if ([[dic objectForKey:@"P_INFO30"] isEqualToString:@"Y"])  [check_30 setHighlighted:YES]; else [check_30 setHighlighted:NO];
	}
}

- (void)viewDidAppear:(BOOL)animated {
	englishList = [Language englishRecordWithEmail:ID];
	specialList = [Special specialRecordWithEmail:ID];
	if (specialList.count>0) {
		if ([[[specialList objectAtIndex:0] objectForKey:@"grade"] isEqualToString:@"Y"])  check1.highlighted=YES;
		if ([[[specialList objectAtIndex:1] objectForKey:@"grade"] isEqualToString:@"Y"])  check2.highlighted=YES;
		if ([[[specialList objectAtIndex:2] objectForKey:@"grade"] isEqualToString:@"Y"])  check3.highlighted=YES;
		if ([[[specialList objectAtIndex:3] objectForKey:@"grade"] isEqualToString:@"Y"])  check4.highlighted=YES;
		if ([[[specialList objectAtIndex:4] objectForKey:@"grade"] isEqualToString:@"Y"])  check5.highlighted=YES;
		if ([[[specialList objectAtIndex:5] objectForKey:@"grade"] isEqualToString:@"Y"])  check6.highlighted=YES;
		if ([[[specialList objectAtIndex:6] objectForKey:@"grade"]isEqualToString:@"Y"])  check7.highlighted=YES;
		if ([[[specialList objectAtIndex:7] objectForKey:@"grade"] isEqualToString:@"Y"])  check8.highlighted=YES;
		if ([[[specialList objectAtIndex:8] objectForKey:@"grade"] isEqualToString:@"Y"])  check9.highlighted=YES;
		if ([[[specialList objectAtIndex:9] objectForKey:@"grade"] isEqualToString:@"Y"])  check10.highlighted=YES;
	}
	[self.tableView reloadData];
	
}

- (void)onTimer{
	int index = arc4random()%3;
	imageAD.image = [adArray objectAtIndex:index];
	imageAD.tag = index;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	UIImage *image1 = [UIImage imageNamed:@"iphone_ad_1.png"];
	UIImage *image2 = [UIImage imageNamed:@"iphone_ad_2.png"];
	UIImage *image3 = [UIImage imageNamed:@"iphone_ad_3.png"];
	adArray = [NSArray arrayWithObjects:image1, image2, image3, nil];
	oldidx = -1;
	timer= [NSTimer scheduledTimerWithTimeInterval:15 target:self selector:@selector(onTimer) userInfo:nil repeats:YES];
	
	[self onTimer];

	labelMainTitle.text = [Title mainTilteWithMenu:4];
	tapGesture = [[UITapGestureRecognizer alloc] init];
	tapGesture.delegate = self;
	tapGesture.numberOfTapsRequired = 1;
	[scrollView addGestureRecognizer:tapGesture];
	
	UITapGestureRecognizer *tapGesture1 = [[UITapGestureRecognizer alloc] init];
	tapGesture1.delegate = self;
	tapGesture1.numberOfTapsRequired = 1;
	[scrollView1 addGestureRecognizer:tapGesture1];

	
   
	langList =[Language languageRecordWithEmail:ID];
	
	if (langList.count==0) {
		langList = [NSArray arrayWithObjects:
					 [NSMutableDictionary dictionaryWithObjectsAndKeys:@"영어",@"content",@"",@"grade", nil],
					 [NSMutableDictionary dictionaryWithObjectsAndKeys:@"중국어",@"content",@"",@"grade", nil],
					 [NSMutableDictionary dictionaryWithObjectsAndKeys:@"독일어",@"content",@"",@"grade", nil],
					 [NSMutableDictionary dictionaryWithObjectsAndKeys:@"프랑스어",@"content",@"",@"grade", nil],
					 [NSMutableDictionary dictionaryWithObjectsAndKeys:@"일본어",@"content",@"",@"grade", nil],
					 [NSMutableDictionary dictionaryWithObjectsAndKeys:@"러시아어",@"content",@"",@"grade", nil],
					 [NSMutableDictionary dictionaryWithObjectsAndKeys:@"스페인어",@"content",@"",@"grade", nil], 
					 [NSMutableDictionary dictionaryWithObjectsAndKeys:@"한자",@"content",@"",@"grade", nil],
					 nil];
	}
	tabList = [NSArray arrayWithObjects:tabImage01, tabImage02, tabImage03, nil];
	[self visibleImage:0];
	
	for (UIView *view in [scrollView1 subviews]) {
		if (view.tag>0) {
			if ([view isKindOfClass:[UIImageView class]]) {
				[view setFrame:CGRectMake(20, (view.tag-1)*30+10, 24, 24)];
			} else if ([view isKindOfClass:[UILabel class]]) {
				[view setFrame:CGRectMake(50, (view.tag-1)*30+10, 250, 24)];
			} 
			
		}		
	}
	scrollView1.contentSize = CGSizeMake(320,320);
	[self.tableView reloadData];
}

- (void)viewDidUnload
{
	[self setTabImage01:nil];
	[self setTabImage02:nil];
	[self setTableView:nil];
	[self setCheck1:nil];
	[self setCheck2:nil];
	[self setCheck3:nil];
	[self setCheck4:nil];
	[self setCheck4:nil];
	[self setCheck5:nil];
	[self setCheck6:nil];
	[self setCheck7:nil];
	[self setCheck8:nil];
	[self setCheck9:nil];
	[self setCheck10:nil];
	[self setScrollView:nil];
	[self setScrollView1:nil];
	[self setTabImage03:nil];
	[self setLabelMainTitle:nil];
	[self setImageAD:nil];
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)selectTab:(id)sender {
	UIButton *btn = (UIButton *)sender;
	int index = btn.tag;
	if (index==1) {
		
	} else {
		
	}
}

- (IBAction)touchTab:(id)sender {
	UIButton *btn = (UIButton *)sender;
	int index = btn.tag;
	[self visibleImage:index];
}

- (IBAction)actionBack:(id)sender {
	//[self update:idx];
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)update:(int)index {
    switch (index) {
		case 0:{
			NSString *engtest;
			if([englishList objectForKey:@"title"]==nil) engtest = @"";
			else engtest= [englishList objectForKey:@"title"];
			
			Language *lang = [[Language alloc] init];
			lang.P_ID =ID;
			lang.ENG_TEST       = engtest;
			lang.ENG_SCORE      = [[englishList objectForKey:@"score"] intValue];
			
			lang.CHI			= [[[langList objectAtIndex:1] objectForKey:@"grade"] intValue];
			lang.DEU			= [[[langList objectAtIndex:2] objectForKey:@"grade"] intValue];
			lang.FRA			= [[[langList objectAtIndex:3] objectForKey:@"grade"] intValue];
			lang.JAP			= [[[langList objectAtIndex:4] objectForKey:@"grade"] intValue];
			lang.RUS			= [[[langList objectAtIndex:5] objectForKey:@"grade"] intValue];
			lang.SPA			= [[[langList objectAtIndex:6] objectForKey:@"grade"] intValue];
			lang.HAN			= [[[langList objectAtIndex:7] objectForKey:@"grade"] intValue];
			[lang addLanguage];
			if ([Language chkLangWithID:ID]) [User updateInfoLang:ID value:@"Y"];
			else [User updateInfoLang:ID value:@"N"];
			[self sendSpecial:index];
			break;
		}
		case 1:{
			Special *special = [[Special alloc] init];
			special.P_ID = ID;
			if (check1.highlighted) special.SHSCHOOL = @"Y"; else special.SHSCHOOL= @"N";
			if (check2.highlighted) special.INOLIMP  = @"Y"; else special.INOLIMP = @"N";
			if (check3.highlighted) special.MOLIMP   = @"Y"; else special.MOLIMP = @"N";
			if (check4.highlighted) special.POLIMP   = @"Y"; else special.POLIMP = @"N";
			if (check5.highlighted) special.COLIMP   = @"Y"; else special.COLIMP = @"N";
			if (check6.highlighted) special.OOLIMP   = @"Y"; else special.OOLIMP = @"N";
			if (check7.highlighted) special.EOLIMP   = @"Y"; else special.EOLIMP = @"N";
			if (check8.highlighted) special.AOLIMP   = @"Y"; else special.AOLIMP = @"N";
			if (check9.highlighted) special.NOLIMP   = @"Y"; else special.NOLIMP = @"N";
			if (check10.highlighted) special.ROBOT    = @"Y"; else special.ROBOT  = @"N";
			[special addSpecial];
			
			if ([Special chkSpecialWithID:ID]) [User updateInfoMath:ID value:@"Y"];
			else [User updateInfoMath:ID value:@"N"];
			[self sendSpecial:index];
			break;
		}
		case 2:{
			NSMutableArray *array = [NSMutableArray array];
			if(check_1.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//입학사정관 포트폴리오
			if(check_2.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//공인외국어자격
			if(check_3.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//수학과학경력
			if(check_4.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//농어촌지역
			if(check_5.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//임원경력여부
			
			if(check_6.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//종교추천여부
			if(check_7.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//학교추천여부
			if(check_8.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//특성화고졸재직자
			if(check_9.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//장애인여부
			if(check_10.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//서해5도
			
			if(check_11.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//기초생활수급및차상위
			if(check_12.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//다문화가정
			if(check_13.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//장애인자녀
			if(check_14.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//소년소녀가장
			if(check_15.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//새터민
			
			if(check_16.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//독립유공자
			if(check_17.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//국가유공자
			if(check_18.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//민주화유공자
			if(check_19.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//5.18민주화유공자
			if(check_20.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//고엽제후유증
			
			if(check_21.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//특수임무수행자
			if(check_22.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//군자녀
			if(check_23.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//경찰자녀
			if(check_24.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//소방자녀
			if(check_25.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//환경미화자녀
			
			if(check_26.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//6.18자유상이자
			if(check_27.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//지원공상자녀
			if(check_28.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//교정공무원
			if(check_29.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//다자녀
			if(check_30.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//백일장수상경력
			
			User *user = [[User alloc] init];
			user.P_ID = ID;
			user.info = array;
			[user addInfo];
			if(!check_2.highlighted) [Language removeLanguageWithID:ID];
			if(!check_3.highlighted) [Special removeSpecialWithID:ID];
			[self sendSpecial:index];
			break;
		}
		default:
			break;
	}
}

- (IBAction)actionDone:(id)sender {
	[self update:idx];
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return langList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
	}
	NSDictionary *dic =[langList objectAtIndex:indexPath.row]; 
	cell.textLabel.text = [dic objectForKey:@"content"];
	cell.textLabel.backgroundColor =[UIColor clearColor];
	if (indexPath.row>0) {
		int number =[[dic objectForKey:@"grade"] intValue];
		if (number>0) {
			cell.accessoryType = UITableViewCellAccessoryCheckmark;
		} else {
			cell.accessoryType = UITableViewCellAccessoryNone;
		}
	} else {
		if ([Language chkEngWithID:ID])  {
			cell.accessoryType = UITableViewCellAccessoryCheckmark;
		} else {
			cell.accessoryType = UITableViewCellAccessoryNone;
		}
	}

	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.row==0) {
		EnglishGradeViewController *viewController = [[EnglishGradeViewController alloc] init];
		viewController.ID = ID;
		[self.navigationController pushViewController:viewController animated:YES];
	} else {
		NSMutableDictionary *dic = [langList objectAtIndex:indexPath.row];
		int chk = [[dic objectForKey:@"grade"] intValue];
		if (chk) {
			[dic setValue:[NSNumber numberWithInt:0]  forKey:@"grade"];
		} else {
			[dic setValue:[NSNumber numberWithInt:1] forKey:@"grade"];
		}
		[self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
		
//		NSString *engtest;
//		if([englishList objectForKey:@"title"]==nil) engtest = @"";
//		else engtest= [englishList objectForKey:@"title"];
//		
//		Language *lang = [[Language alloc] init];
//		lang.P_ID =ID;
//		lang.ENG_TEST       = engtest;
//		lang.ENG_SCORE      = [[englishList objectForKey:@"score"] intValue];
//		
//		lang.CHI			= [[[langList objectAtIndex:1] objectForKey:@"grade"] intValue];
//		lang.DEU			= [[[langList objectAtIndex:2] objectForKey:@"grade"] intValue];
//		lang.FRA			= [[[langList objectAtIndex:3] objectForKey:@"grade"] intValue];
//		lang.JAP			= [[[langList objectAtIndex:4] objectForKey:@"grade"] intValue];
//		lang.RUS			= [[[langList objectAtIndex:5] objectForKey:@"grade"] intValue];
//		lang.SPA			= [[[langList objectAtIndex:6] objectForKey:@"grade"] intValue];
//		lang.HAN			= [[[langList objectAtIndex:7] objectForKey:@"grade"] intValue];
//		[lang addLanguage];
//		if ([Language chkLangWithID:ID]) [User updateInfoLang:ID value:@"Y"];
//		else [User updateInfoLang:ID value:@"N"];
//		[self sendSpecial];
	}
	
}
#pragma mark - UIGestureRecognizer Delegate 
- (BOOL) gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
	if (gestureRecognizer == tapGesture){
		if ([touch.view isKindOfClass:[UIImageView class]]) {
			
			UIImageView *imageView = (UIImageView *)[touch view];
			[imageView setHighlighted:![imageView isHighlighted]];
		}
	
	} else {
		if ([touch.view isKindOfClass:[UIImageView class]]) {
			
			UIImageView *imageView = (UIImageView *)[touch view];
			[imageView setHighlighted:![imageView isHighlighted]];

		}

	}
	return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	UITouch *touch = [touches anyObject];
	if (touch.view == imageAD ) {
		
		ADPop *adPop = [[ADPop alloc] initWithFrame:CGRectMake(0, 0, 320, 460)];
		[self.view addSubview:adPop];
		adPop.index = touch.view.tag;
	}
}

- (void)sendSpecial:(int)index {
	NSMutableString *str;	
	if (index==0) {
		NSMutableString *lang = [NSMutableString string];
				for (int i=1; i<8; i++) {
			[lang appendString:[NSString stringWithFormat:@"%d", [[[langList objectAtIndex:i] objectForKey:@"grade"] intValue]]] ;
		}
		NSString *engtest;
		if([englishList objectForKey:@"title"]==nil) engtest = @"";
		else engtest= [englishList objectForKey:@"title"];
		
		NSString *engscore;
		if([englishList objectForKey:@"score"]==nil) engscore = @"0";
		else engscore= [englishList objectForKey:@"score"];
		
		NSLog(@"lang = %@", lang);
		str=[NSMutableString stringWithFormat:@"http://www.sooseero.com/mobile/member/information2_1.mgo?id=%@&engTest=%@&engScore=%@&lang=%@", ID, engtest, engscore , lang];
		
		
	} else if (index==1) {
		NSMutableString *math = [NSMutableString string];
		if (check1.highlighted) [math appendString:@"1"]; else  [math appendString:@"0"];
		if (check2.highlighted) [math appendString:@"1"]; else  [math appendString:@"0"];
		if (check3.highlighted) [math appendString:@"1"]; else  [math appendString:@"0"];
		if (check4.highlighted) [math appendString:@"1"]; else  [math appendString:@"0"];
		if (check5.highlighted) [math appendString:@"1"]; else  [math appendString:@"0"];
		if (check6.highlighted) [math appendString:@"1"]; else  [math appendString:@"0"];
		if (check7.highlighted) [math appendString:@"1"]; else  [math appendString:@"0"];
		if (check8.highlighted) [math appendString:@"1"]; else  [math appendString:@"0"];
		if (check9.highlighted) [math appendString:@"1"]; else  [math appendString:@"0"];
		if (check10.highlighted) [math appendString:@"1"]; else  [math appendString:@"0"];
		
		str=[NSMutableString stringWithFormat:@"http://www.sooseero.com/mobile/member/information2_2.mgo?id=%@&math=%@", ID, math];
	}else if (index==2) {
		NSMutableString *info = [NSMutableString string];
		if (check_1.highlighted) [info appendString:@"1"]; else  [info appendString:@"0"];
		if (check_2.highlighted) [info appendString:@"1"]; else  [info appendString:@"0"];
		if (check_3.highlighted) [info appendString:@"1"]; else  [info appendString:@"0"];
		if (check_4.highlighted) [info appendString:@"1"]; else  [info appendString:@"0"];
		if (check_5.highlighted) [info appendString:@"1"]; else  [info appendString:@"0"];
		if (check_6.highlighted) [info appendString:@"1"]; else  [info appendString:@"0"];
		if (check_7.highlighted) [info appendString:@"1"]; else  [info appendString:@"0"];
		if (check_8.highlighted) [info appendString:@"1"]; else  [info appendString:@"0"];
		if (check_9.highlighted) [info appendString:@"1"]; else  [info appendString:@"0"];
		if (check_10.highlighted) [info appendString:@"1"]; else  [info appendString:@"0"];
		if (check_11.highlighted) [info appendString:@"1"]; else  [info appendString:@"0"];
		if (check_12.highlighted) [info appendString:@"1"]; else  [info appendString:@"0"];
		if (check_13.highlighted) [info appendString:@"1"]; else  [info appendString:@"0"];
		if (check_14.highlighted) [info appendString:@"1"]; else  [info appendString:@"0"];
		if (check_15.highlighted) [info appendString:@"1"]; else  [info appendString:@"0"];
		if (check_16.highlighted) [info appendString:@"1"]; else  [info appendString:@"0"];
		if (check_17.highlighted) [info appendString:@"1"]; else  [info appendString:@"0"];
		if (check_18.highlighted) [info appendString:@"1"]; else  [info appendString:@"0"];
		if (check_19.highlighted) [info appendString:@"1"]; else  [info appendString:@"0"];
		if (check_20.highlighted) [info appendString:@"1"]; else  [info appendString:@"0"];
		if (check_21.highlighted) [info appendString:@"1"]; else  [info appendString:@"0"];
		if (check_22.highlighted) [info appendString:@"1"]; else  [info appendString:@"0"];
		if (check_23.highlighted) [info appendString:@"1"]; else  [info appendString:@"0"];
		if (check_24.highlighted) [info appendString:@"1"]; else  [info appendString:@"0"];
		if (check_25.highlighted) [info appendString:@"1"]; else  [info appendString:@"0"];
		if (check_26.highlighted) [info appendString:@"1"]; else  [info appendString:@"0"];
		if (check_27.highlighted) [info appendString:@"1"]; else  [info appendString:@"0"];
		if (check_28.highlighted) [info appendString:@"1"]; else  [info appendString:@"0"];
		if (check_29.highlighted) [info appendString:@"1"]; else  [info appendString:@"0"];
		if (check_30.highlighted) [info appendString:@"1"]; else  [info appendString:@"0"];
		
		
		str=[NSMutableString stringWithFormat:@"http://www.sooseero.com/mobile/member/information2.mgo?id=%@&info=%@", ID, info];
	}
	
	
	NSString *url=	[str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	
	NSLog(@"%@", url);
	[self requestUrl:url];
}



- (BOOL)requestUrl:(NSString *)url {
	
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] 
														   cachePolicy:NSURLRequestUseProtocolCachePolicy 
													   timeoutInterval:30.0]; 
	[request setHTTPMethod:@"GET"];
	NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
	if (connection) { 
		self.receivedData = [NSMutableData data]; // 수신할 데이터를 받을 공간을 마련
		return YES;
	}
	return NO;
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)aResponse {
	[receivedData setLength:0];
	self.response = aResponse;
	
	
	
	NSArray *cookies;
    NSDictionary *responseHeaderFields;
	
    // 받은 header들을 dictionary형태로 받고
    responseHeaderFields = [(NSHTTPURLResponse *)aResponse allHeaderFields];
	
    if(responseHeaderFields != nil)
    {
        // responseHeaderFields에 포함되어 있는 항목들 출력
        for(NSString *key in responseHeaderFields)
        {
            NSLog(@"Header: %@ = %@", key, [responseHeaderFields objectForKey:key]);
        }
		
        // cookies에 포함되어 있는 항목들 출력
        cookies = [NSHTTPCookie cookiesWithResponseHeaderFields:responseHeaderFields forURL:[aResponse URL]];
		
        if(cookies != nil)
        {
            for(NSHTTPCookie *a in cookies)
            {
                NSLog(@"Cookie: %@ = %@", [a name], [a value]);
            }
        }
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	//NSString *returnString = [[NSString alloc] initWithData:data encoding:EUCKRStringEncoding];
	[receivedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
	
	rdata = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
	NSLog(@"%@", rdata);
	
	CXMLDocument *rssParser = [[CXMLDocument alloc] initWithXMLString:rdata options:0 error:nil];
	NSArray *xmlNodes  = [rssParser nodesForXPath:@"//result" error:nil];
	
	for (CXMLElement *resultElement in xmlNodes) {
		
		int result = [[[[resultElement children] objectAtIndex:0] stringValue] intValue];
		
		switch (result) {
			case 0:{
				UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"성적전송실패" message:@"성적전송에 실패하였습니다." delegate:self cancelButtonTitle:@"확인" otherButtonTitles:nil]; 
				[alert show];
				break;
			}
				//			case 1:{
				//				UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"회원가입성공" message:@"회원가입에 성공하였습니다." delegate:self cancelButtonTitle:@"확인" otherButtonTitles:nil]; 
				//				[alert show];
				//				break;			   
				//			}
			default:
				break;
		}
	}
}


@end
