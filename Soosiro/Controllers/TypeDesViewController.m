//
//  SeriesDesViewController.m
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 6. 18..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import "TypeDesViewController.h"
#import "UnivInfo.h"
@interface TypeDesViewController (){
	UIActivityIndicatorView *indicator;
	NSArray *adArray;
	NSTimer *timer;

}

@end

@implementation TypeDesViewController
@synthesize menu;

@synthesize type;
@synthesize tclass;
@synthesize scrollView;
@synthesize labelMainTitle;
@synthesize imageAD;
@synthesize series03;
@synthesize series02;
@synthesize series01;

- (void)initView
{
    NSDictionary *dic  = [UnivInfo typeInfoWithType:type];
	int org = 10;
	UIImageView *imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake(10, org, 178, 47)];
	imageView1.image = [UIImage imageNamed:@"title_1.png"];
	[scrollView addSubview:imageView1];
	
	UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(50, 8 , 130, 40)];
	label1.text =@"전형개요";
	label1.backgroundColor =[UIColor clearColor];
	[imageView1 addSubview:label1];
	
	UITextView *textView1 = [[UITextView alloc] initWithFrame:CGRectMake(10, org+imageView1.frame.size.height +10 , 300, 40)];
	textView1.text = [dic objectForKey:@"T_intro"];
	[textView1 setUserInteractionEnabled:NO];
	textView1.backgroundColor =[UIColor clearColor];
	[scrollView addSubview:textView1];
	[textView1 setFrame:CGRectMake(10, org+imageView1.frame.size.height +10, 300, textView1.contentSize.height)];
	
	org = textView1.frame.origin.y+textView1.frame.size.height +10;
	
	NSString *cata = [dic objectForKey:@"cata"];
	if (cata !=nil &&  ![cata isEqualToString:@""]) {
		UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(10, org, 178, 47)];
		imageView2.image = [UIImage imageNamed:@"title_2.png"];
		[scrollView addSubview:imageView2];
		
		UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(50, 8 , 130, 40)];
		label2.text =@"주요대학일람표";
		label2.backgroundColor =[UIColor clearColor];
		[imageView2 addSubview:label2];
		
		UITextView *textView2 = [[UITextView alloc] initWithFrame:CGRectMake(10, imageView2.frame.origin.y+imageView2.frame.size.height +10 , 300, 40)];
		textView2.text = cata;
		[textView2 setUserInteractionEnabled:NO];
		textView2.backgroundColor =[UIColor clearColor];
		[scrollView addSubview:textView2];
		[textView2 setFrame:CGRectMake(10, imageView2.frame.origin.y+imageView2.frame.size.height +10, 300, textView2.contentSize.height)];
		
		org = textView2.frame.origin.y+textView2.frame.size.height +10;
	}
	
	UIImageView *imageView3 = [[UIImageView alloc] initWithFrame:CGRectMake(10, org, 178, 47)];
	imageView3.image = [UIImage imageNamed:@"title_3.png"];
	[scrollView addSubview:imageView3];
	
	UILabel *label3 = [[UILabel alloc] initWithFrame:CGRectMake(50, 8 , 130, 40)];
	label3.text =@"전형대비법";
	label3.backgroundColor =[UIColor clearColor];
	[imageView3 addSubview:label3];
	
	UITextView *textView3 = [[UITextView alloc] initWithFrame:CGRectMake(10, imageView3.frame.origin.y+imageView3.frame.size.height +10 , 300, 40)];
	textView3.text = [dic objectForKey:@"prep"];
	[textView3 setUserInteractionEnabled:NO];
	textView3.backgroundColor =[UIColor clearColor];
	[scrollView addSubview:textView3];
	[textView3 setFrame:CGRectMake(10, imageView3.frame.origin.y+imageView3.frame.size.height +10, 300, textView3.contentSize.height)];
	org = textView3.frame.origin.y+textView3.frame.size.height +10;	
	
	scrollView.contentSize = CGSizeMake(320, org);
	[indicator stopAnimating];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	labelMainTitle.text = [Title mainTilteWithMenu:menu];
	UIImage *image1 = [UIImage imageNamed:@"iphone_ad_1.png"];
	UIImage *image2 = [UIImage imageNamed:@"iphone_ad_2.png"];
	UIImage *image3 = [UIImage imageNamed:@"iphone_ad_3.png"];
	adArray = [NSArray arrayWithObjects:image1, image2, image3, nil];
	
	timer= [NSTimer scheduledTimerWithTimeInterval:15 target:self selector:@selector(onTimer) userInfo:nil repeats:YES];
	
	[self onTimer];

    indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 64.0f, 64.0f)];
	[indicator setCenter:self.view.center];
    [indicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
	[self.view addSubview : indicator];	
	[indicator startAnimating];
	
	[self performSelector:@selector(initView) withObject:nil afterDelay:0];

	
}

- (void)onTimer{
	int index = arc4random()%3;
	imageAD.image = [adArray objectAtIndex:index];
	imageAD.tag = index;
}

- (void)viewDidUnload
{

    [self setLabelMainTitle:nil];
    [self setImageAD:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)actionBack:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)actionNext:(id)sender {
	AreaViewController *viewController =[[AreaViewController alloc] init];
	viewController.menu = menu;
	viewController.type = type;
	viewController.tclass = tclass;
	[self.navigationController pushViewController:viewController animated:YES];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	UITouch *touch = [touches anyObject];
	if (touch.view == imageAD ) {
		
		ADPop *adPop = [[ADPop alloc] initWithFrame:CGRectMake(0, 0, 320, 460)];
		[self.view addSubview:adPop];
		adPop.index = touch.view.tag;
	}
}

@end
