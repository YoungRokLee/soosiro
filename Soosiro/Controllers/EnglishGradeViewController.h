//
//  EnglishGradeViewController.h
//  Soosiro
//
//  Created by 이영록 on 12. 7. 6..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EnglishGradeViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIView *viewBar;
@property (strong, nonatomic) IBOutlet UITextField *textGrade;
@property (strong, nonatomic) NSString *ID;
@property (strong, nonatomic) IBOutlet UILabel *labelMainTitle;
@property (strong, nonatomic) IBOutlet UIImageView *imageAD;
- (IBAction)actionEditingEnd:(id)sender;
- (IBAction)actionBack:(id)sender;
- (IBAction)actionDone:(id)sender;
@end
