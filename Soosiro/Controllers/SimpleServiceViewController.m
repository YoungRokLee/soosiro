//
//  SimpleServiceViewController.m
//  Soosiro
//
//  Created by 이영록 on 12. 8. 3..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import "SimpleServiceViewController.h"

@interface SimpleServiceViewController (){
	NSArray *adArray;
	NSTimer *timer;
}

@end

@implementation SimpleServiceViewController

@synthesize scrollView;
@synthesize imageAD;
@synthesize labelTitle;
@synthesize uname;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	labelTitle.text = uname;
	scrollView.contentSize = CGSizeMake(320,100);
	UIImage *image1 = [UIImage imageNamed:@"iphone_ad_1.png"];
	UIImage *image2 = [UIImage imageNamed:@"iphone_ad_2.png"];
	UIImage *image3 = [UIImage imageNamed:@"iphone_ad_3.png"];
	adArray = [NSArray arrayWithObjects:image1, image2, image3, nil];
	timer= [NSTimer scheduledTimerWithTimeInterval:15 target:self selector:@selector(onTimer) userInfo:nil repeats:YES];
	
	[self onTimer];
    // Do any additional setup after loading the view from its nib.
}

- (void)onTimer{
	int index = arc4random()%3;
	imageAD.image = [adArray objectAtIndex:index];
	imageAD.tag = index;
}

- (void)viewDidUnload
{
	[self setScrollView:nil];
	[self setImageAD:nil];

	[self setLabelTitle:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	UITouch *touch = [touches anyObject];
	if (touch.view == imageAD ) {
		
		ADPop *adPop = [[ADPop alloc] initWithFrame:CGRectMake(0, 0, 320, 460)];
		[self.view addSubview:adPop];
		adPop.index = touch.view.tag;
	}
}

- (IBAction)actionHome:(id)sender {
	[self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
}
- (IBAction)actionBack:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}
@end
