//
//  UnivViewControllerViewController.m
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 6. 16..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import "UnivViewController.h"
#import "RecResultViewController.h"
#import "User.h"
#import "TypeBViewController.h"
#import "UnivInfo.h"
#import "Title.h"
#import "ADPop.h"

@interface UnivViewController (){
	UIActivityIndicatorView *indicator;
	NSArray *adArray;
	NSTimer *timer;
}
@end

@implementation UnivViewController
@synthesize tableView=_tableView;
@synthesize list;
@synthesize area;
@synthesize menu;
@synthesize type;
@synthesize unit;
@synthesize tclass;
@synthesize series03;
@synthesize series02;
@synthesize series01;
@synthesize textSearch;
@synthesize mf;
@synthesize serarchBar;
@synthesize imageAD;
@synthesize labelMainTitle;
@synthesize ID;

- (BOOL)isExist:(NSString *)univName {
	BOOL result = NO;
	for (int i=0; i<list.count; i++) {
		NSDictionary *dic = [list objectAtIndex:i];
		NSString *name = [dic objectForKey:@"U_NM"];
		if ([name isEqualToString:univName]) {
			result = YES;
			break;
		}
	}
	return result;
}

- (void)initView
{
	if (menu==4) {
		[self.tableView setFrame:CGRectMake(0, 44, 320, 367)];
		list = [NSMutableArray array];
		NSMutableArray *grades= [Record gradeArrayWithEmail:ID];
		NSNumber* avg = [Record avgValueWithID:ID];
		NSArray *array =[UnivInfo univInfoWithType:type avg:avg series03:series03 series02:series02 series01:series01 mf:mf];	
		
		for (int i=0; i<array.count; i++) {
			NSDictionary *dic = [array objectAtIndex:i];
			NSLog(@" %@ %@ %@ %@ %@ %@ %f ", [dic objectForKey:@"U_CODE"], [dic objectForKey:@"U_NM"],[dic objectForKey:@"T_NM"],[dic objectForKey:@"U_UNIT"],[dic objectForKey:@"U_PROVISO"],[dic objectForKey:@"TU_NM"],[[dic objectForKey:@"U_JUST"] doubleValue]);
		}
		
		for (int i=0;i<array.count;i++){
			NSDictionary *dic = [array objectAtIndex:i];
			NSLog(@"%@  %@  %@   --%@--  %@  %@ %@ ", [dic objectForKey:@"U_NM"], [dic objectForKey:@"TU_NM"], [dic objectForKey:@"U_FIXNUM"],[dic objectForKey:@"U_PROVISO"], [dic objectForKey:@"TU_SEQ"], [dic objectForKey:@"TU_CODE"], grades);
			
			if (![self chekProvisoWithGrades:grades proviso:[dic objectForKey:@"U_PROVISO"]] || [self isExist:[dic objectForKey:@"U_NM"]]) continue;
			[list addObject:dic];
			
			if (list.count>=10) break;
		}
	} else {
        
		list = [UnivInfo univInfoWithMenu:menu area:area unit:unit series03:series03 series02:series02 series01:series01 type:type tclass:tclass mf:mf];
	}
	[self.tableView reloadData];
	[indicator stopAnimating];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	UIImage *image1 = [UIImage imageNamed:@"iphone_ad_1.png"];
	UIImage *image2 = [UIImage imageNamed:@"iphone_ad_2.png"];
	UIImage *image3 = [UIImage imageNamed:@"iphone_ad_3.png"];
	adArray = [NSArray arrayWithObjects:image1, image2, image3, nil];
	
	timer= [NSTimer scheduledTimerWithTimeInterval:15 target:self selector:@selector(onTimer) userInfo:nil repeats:YES];
				
	[self onTimer];
	
	if (menu==4) {
		[serarchBar setHidden:YES];
		[textSearch setHidden:YES];
	}
	[self.tableView setBackgroundColor:[UIColor clearColor]];
	labelMainTitle.text = [Title mainTilteWithMenu:menu];
	indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 64.0f, 64.0f)];
	[indicator setCenter:self.view.center];
    [indicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
    [self.view addSubview : indicator];	
	
	indicator.hidden= FALSE;
	[indicator startAnimating];
		
	[self performSelector:@selector(initView) withObject:nil afterDelay:0];
	
}

- (void)onTimer{
	int index = arc4random()%3;
	imageAD.image = [adArray objectAtIndex:index];
	imageAD.tag = index;
}

- (void)viewDidUnload
{
	[self setTableView:nil];
	[self setTextSearch:nil];
    [self setSerarchBar:nil];
	[self setImageAD:nil];
	[self setLabelMainTitle:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)actionBack:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)actionEndEditing:(id)sender {
	list = [UnivInfo univInfoWithMenu:menu area:area unit:unit series03:series03 series02:series02 series01:series01 type:type mf:mf search:textSearch.text];
	
	[self.tableView reloadData];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return list.count;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
	}
	
	NSDictionary *dic = [list objectAtIndex:indexPath.row];
	if (menu==4) {
		cell.textLabel.text = [dic objectForKey:@"U_NM"];
	} else {
	cell.textLabel.text = [dic objectForKey:@"name"];
	}
	//cell.textLabel.font = [UIFont fontWithName:@"NanumGothicOTF" size:15];
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	switch (menu) {
		case 0:{
			TypeBViewController *viewController = [[TypeBViewController alloc] init];
//			viewController.menu = menu;
			NSDictionary *dic = [list objectAtIndex:indexPath.row];
			viewController.univCode = [dic objectForKey:@"code"];
			viewController.univ = [dic objectForKey:@"name"];
		
			[self.navigationController pushViewController:viewController animated:YES];
			break;
		}
		case 1:{
			SeriesViewController *viewController = [[SeriesViewController alloc] init];
			viewController.menu = menu;
			viewController.type = type;
			viewController.tclass = tclass;
			NSDictionary *dic = [list objectAtIndex:indexPath.row];
			viewController.univCode = [dic objectForKey:@"code"];
			
			[self.navigationController pushViewController:viewController animated:YES];
			break;
		}
		case 2:{
			TypeBViewController *viewController = [[TypeBViewController alloc] init];
			
			NSDictionary *dic = [list objectAtIndex:indexPath.row];
			viewController.menu = menu;
			viewController.univCode = [dic objectForKey:@"code"];
			viewController.univ = [dic objectForKey:@"name"];
			viewController.series03 = series03;
			viewController.series02 = series02;
			viewController.series01 = series01;
			viewController.unit = unit;
			[self.navigationController pushViewController:viewController animated:YES];
			break;
		}
		case 4:{
			RecResultViewController *viewController = [[RecResultViewController alloc] init];
			viewController.type	= type;
			NSDictionary *dic = [list objectAtIndex:indexPath.row];
			viewController.univ = [dic objectForKey:@"U_NM"];
			viewController.univCode = [dic objectForKey:@"U_CODE"];
			
			//viewController.unit = unit;
			viewController.ID = ID;
			
			viewController.series03 = series03;
			viewController.series02 = series02;
			viewController.series01 = series01;
			[self.navigationController pushViewController:viewController animated:YES];
			break;
		}
			
		default:{
			SeriesViewController *viewController = [[SeriesViewController alloc] init];
			viewController.menu = menu;
			viewController.type = type;
			NSDictionary *dic = [list objectAtIndex:indexPath.row];
			viewController.univCode = [dic objectForKey:@"code"];
			NSLog(@"univCode = %@", viewController.univCode);
			[self.navigationController pushViewController:viewController animated:YES];
			break;
		}
	}
	
}

- (NSDictionary *)findWithArea:(NSString*)area1 array:(NSArray*)arr {
for (NSDictionary *dic in arr) {
	if ([[dic objectForKey:@"area"] isEqualToString:[area1 lowercaseString]])
		return dic;
}
return nil;
}

- (BOOL)chekProvisoWithGrades:(NSArray*)grades proviso:(NSString*)proviso{
	NSDictionary *userDic = [User userInfoWithID:ID];
	NSString *hCate = [userDic objectForKey:@"P_Cate"];
	
	BOOL isSuccess=YES;
	double study;
	double study1=[[grades objectAtIndex:3] doubleValue];
	double study2=[[grades objectAtIndex:4] doubleValue];
	
	
	if ([proviso isEqualToString:@""]) return isSuccess;
	
	NSString *a = [proviso substringWithRange:NSMakeRange(0, 1)];
	NSString *b = [proviso substringWithRange:NSMakeRange(1, 1)];
	NSString *c = [proviso substringWithRange:NSMakeRange(2, 1)];
	NSString *d = [proviso substringWithRange:NSMakeRange(3, 1)];
	int scount =  [[proviso substringWithRange:NSMakeRange(4, 1)] intValue];
	NSString *gradeType = [proviso substringWithRange:NSMakeRange(5, 1)];
	int acount =  [[proviso substringWithRange:NSMakeRange(7, 1)] intValue];
	NSString *gradeType1 =[proviso substringWithRange:NSMakeRange(8, 1)]; 
	int calcResult =  [[proviso substringWithRange:NSMakeRange(9, 2)] intValue];
	
	NSString *r1 = [proviso substringWithRange:NSMakeRange(12, 1)];
	NSString *r2 = [proviso substringWithRange:NSMakeRange(13, 1)];
	NSString *r3 = [proviso substringWithRange:NSMakeRange(14, 1)];
	
	if (scount==0) {
		study = 0;
	}
	if (scount==1) {
		if (study1<=study2) study = study1;
		else study = study2;
	} else if (scount ==2) {
		if ([gradeType isEqualToString:@"p"]) {
			study = (study1+study2)/2;
		} else {
			if (study1<=study2) study = study2;
			else study = study1;
		}
	}
	
	NSMutableArray *arrGrades = [NSMutableArray array];
	if (![a isEqualToString:@"n"]) [arrGrades addObject: [NSDictionary dictionaryWithObjectsAndKeys: [grades objectAtIndex:0],@"value", @"a", @"area", nil]];
	if (![b isEqualToString:@"n"]) [arrGrades addObject:[NSDictionary dictionaryWithObjectsAndKeys: [grades objectAtIndex:1],@"value", @"b", @"area", nil]];
	if (![c isEqualToString:@"n"]) [arrGrades addObject:[NSDictionary dictionaryWithObjectsAndKeys: [grades objectAtIndex:2],@"value", @"c", @"area", nil]];
	if (![d isEqualToString:@"n"]) [arrGrades addObject:[NSDictionary dictionaryWithObjectsAndKeys: [NSNumber numberWithDouble:study],@"value", @"d", @"area", nil]];
	
	NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"value" ascending:YES];
	NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
	NSMutableArray *sortedArray = [NSMutableArray arrayWithArray:[arrGrades sortedArrayUsingDescriptors:sortDescriptors]];
	
	
	if ([hCate isEqualToString:@"문과"] &&([b isEqualToString:@"B"] || [d isEqualToString:@"D"] )) return NO;
	
	if ([gradeType1 isEqualToString:@"g"]) {
		int passCount=0;
		for (int i = 0; i<sortedArray.count; i++) {
			if(calcResult >= [[[sortedArray objectAtIndex:i] objectForKey:@"value"] doubleValue]) passCount+=1;
		}
		if (passCount<acount) isSuccess = NO;				 
		
		if (![r1 isEqualToString:@"n"] && [r2 isEqualToString:@"n"]) {
			NSDictionary *dic=[self findWithArea:r1 array:sortedArray];
			if ([[dic objectForKey:@"value"] doubleValue]>calcResult) isSuccess = NO;
		} else if(![r1 isEqualToString:@"n"] && ![r2 isEqualToString:@"n"]) {
			if ([r3 isEqualToString:@"1"]) {
				NSDictionary *dic1=[self findWithArea:r1 array:sortedArray];
				NSDictionary *dic2=[self findWithArea:r2 array:sortedArray];
				if (!([[dic1 objectForKey:@"value"] doubleValue]<=calcResult || [[dic2 objectForKey:@"value"] doubleValue]<=calcResult)) isSuccess = NO;
			} else 	if ([r3 isEqualToString:@"2"]) {
				NSDictionary *dic1=[self findWithArea:r1 array:sortedArray];
				NSDictionary *dic2=[self findWithArea:r2 array:sortedArray];
				if (!([[dic1 objectForKey:@"value"] doubleValue]<=calcResult && [[dic2 objectForKey:@"value"] doubleValue]<=calcResult)) isSuccess = NO;
			}			 
		}
	} else if([gradeType1 isEqualToString:@"p"]) {
		if ([r1 isEqualToString:@"n"]) {
			double sum=0;
			for (int i=0; i<acount; i++) {
				sum = sum + [[[sortedArray objectAtIndex:i] objectForKey:@"value"] doubleValue];
			}
			double avg = sum/acount;
			if (avg>calcResult) isSuccess = NO;
		} if (![r1 isEqualToString:@"n"] && [r2 isEqualToString:@"n"]){
			NSDictionary *dic=[self findWithArea:r1 array:sortedArray]; 
			[sortedArray removeObject:dic];
			double sum=0;
			for (int i=0; i<acount-1; i++) {
				sum = sum + [[[sortedArray objectAtIndex:i] objectForKey:@"value"] doubleValue];
			}
			sum = sum+[[dic objectForKey:@"value"] doubleValue];
			double avg = sum/acount;
			if (avg>calcResult) isSuccess = NO;
		} if (![r1 isEqualToString:@"n"] && ![r2 isEqualToString:@"n"]){		
			if ([r3 isEqualToString:@"1"]) {
				BOOL tempCheck = NO;
				
				NSMutableArray *temp = [sortedArray copy];
				
				NSDictionary *dic=[self findWithArea:r1 array:temp]; 
				[temp removeObject:dic];
				double sum=0;
				for (int i=0; i<acount-1; i++) {
					sum = sum + [[[temp objectAtIndex:i] objectForKey:@"value"] doubleValue];
				}
				sum = sum+[[dic objectForKey:@"value"] doubleValue];
				double avg = sum/acount;
				if (avg<=calcResult) tempCheck = YES;
				
				if (!tempCheck) {
					NSMutableArray *temp = [sortedArray copy];
					
					NSDictionary *dic=[self findWithArea:r2 array:temp]; 
					[temp removeObject:dic];
					double sum=0;
					for (int i=0; i<acount-1; i++) {
						sum = sum + [[[temp objectAtIndex:i] objectForKey:@"value"] doubleValue];
					}
					sum = sum+[[dic objectForKey:@"value"] doubleValue];
					avg = sum/acount;
					if (avg<=calcResult) tempCheck = YES;
				}
				if (!tempCheck) isSuccess = NO;
			} else 	if ([r3 isEqualToString:@"2"]) {
				NSDictionary *dic=[self findWithArea:r1 array:sortedArray]; 
				[sortedArray removeObject:dic];
				double sum=[[dic objectForKey:@"value"] doubleValue];
				
				dic=[self findWithArea:r2 array:sortedArray]; 
				[sortedArray removeObject:dic];
				sum=sum+[[dic objectForKey:@"value"] doubleValue];
				
				for (int i=0; i<acount-2; i++) {
					sum = sum + [[[sortedArray objectAtIndex:i] objectForKey:@"value"] doubleValue];
				}
				double avg = sum/acount;
				if (avg>calcResult) isSuccess = NO;
			}	
		}
	} else if([gradeType1 isEqualToString:@"s"]) {
		if ([r1 isEqualToString:@"n"]) {
			double sum=0;
			for (int i=0; i<acount; i++) {
				sum = sum + [[[sortedArray objectAtIndex:i] objectForKey:@"value"] doubleValue];
			}
			if (sum>calcResult) isSuccess = NO;
		} if (![r1 isEqualToString:@"n"] && [r2 isEqualToString:@"n"]){
			NSDictionary *dic=[self findWithArea:r1 array:sortedArray]; 
			[sortedArray removeObject:dic];
			double sum=0;
			for (int i=0; i<acount-1; i++) {
				sum = sum + [[[sortedArray objectAtIndex:i] objectForKey:@"value"] doubleValue];
			}
			sum = sum+[[dic objectForKey:@"value"] doubleValue];
			if (sum>calcResult) isSuccess = NO;
		} if (![r1 isEqualToString:@"n"] && ![r2 isEqualToString:@"n"]){		
			if ([r3 isEqualToString:@"1"]) {
				BOOL tempCheck = NO;
				NSMutableArray *temp = [sortedArray copy];
				NSDictionary *dic=[self findWithArea:r1 array:temp]; 
				[temp removeObject:dic];
				double sum=0;
				for (int i=0; i<acount-1; i++) {
					sum = sum + [[[temp objectAtIndex:i] objectForKey:@"value"] doubleValue];
				}
				sum = sum+[[dic objectForKey:@"value"] doubleValue];
				
				if (sum<=calcResult) tempCheck = YES;
				
				if (!tempCheck) {
					NSMutableArray *temp = [sortedArray copy];
					
					NSDictionary *dic=[self findWithArea:r2 array:temp]; 
					[temp removeObject:dic];
					sum=0;
					for (int i=0; i<acount-1; i++) {
						sum = sum + [[[temp objectAtIndex:i] objectForKey:@"value"] doubleValue];
					}
					sum = sum+[[dic objectForKey:@"value"] doubleValue];
					if (sum<=calcResult) tempCheck = YES;
				}
				if (!tempCheck) isSuccess = NO;
			} else 	if ([r3 isEqualToString:@"2"]) {
				NSDictionary *dic=[self findWithArea:r1 array:sortedArray]; 
				[sortedArray removeObject:dic];
				double sum=[[dic objectForKey:@"value"] doubleValue];
				dic=[self findWithArea:r2 array:sortedArray]; 
				[sortedArray removeObject:dic];
				sum=sum+[[dic objectForKey:@"value"] doubleValue];
				
				for (int i=0; i<acount-2; i++) {
					sum = sum + [[[sortedArray objectAtIndex:i] objectForKey:@"value"] doubleValue];
				}
				if (sum>calcResult) isSuccess = NO;
			}	
		}
		
	}
	return isSuccess;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	UITouch *touch = [touches anyObject];
	if (touch.view == imageAD ) {
		ADPop *adPop = [[ADPop alloc] initWithFrame:CGRectMake(0, 0, 320, 460)];
		[self.view addSubview:adPop];
		adPop.index = touch.view.tag;
	}
}

@end
