//
//  ResultViewController.m
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 7. 20..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import "ResultViewController.h"
#import "UnivInfo.h"

@interface ResultViewController (){
	UIActivityIndicatorView *indicator;
	NSArray *adArray;
	NSTimer *timer;

}

@end

@implementation ResultViewController
@synthesize scrollView;
@synthesize tCode;
@synthesize uCode;
@synthesize univ;
@synthesize barTitle;
@synthesize imageAD;
- (void)initView
{
    NSDictionary *dic = [UnivInfo typeDetailWithUniv:uCode type:[self tableName:tCode] type1:tCode];
	NSLog(@"%@", dic);
	UIImageView *imageView2;
	UIImageView *imageView3;
	UIImageView *imageView4;
	UIImageView *imageView5;
	
	UITextView *textView2;
	UITextView *textView3;
	UITextView *textView4;
	UITextView *textView5;
	
	NSString *sum = [dic objectForKey:@"SUM"];
	NSString *poss  = [dic objectForKey:@"POSS"];
	NSString *typ   = [[dic objectForKey:@"TYPE"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
	NSString *sub   = [[dic objectForKey:@"SUB"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
	NSString *chr  = [[dic objectForKey:@"CHAR"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
	int org = 10;
    
	if ( sum != nil && ![sum isEqualToString:@""] ) {
		UIImageView *imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake(10, org, 178, 47)];
		imageView1.image = [UIImage imageNamed:@"title_1.png"];
		[scrollView addSubview:imageView1];
        
		UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(50, 8 , 130, 40)];
		label1.text =@"개괄";
		label1.backgroundColor =[UIColor clearColor];
		[imageView1 addSubview:label1];
        
		UITextView *textView1 = [[UITextView alloc] initWithFrame:CGRectMake(10, org+imageView1.frame.size.height +10 , 300, 40)];
		textView1.text = sum;
		[textView1 setUserInteractionEnabled:NO];
		textView1.backgroundColor =[UIColor clearColor];
		[scrollView addSubview:textView1];
		[textView1 setFrame:CGRectMake(10, org+imageView1.frame.size.height +10, 300, textView1.contentSize.height)];
		
		org = textView1.frame.origin.y+textView1.frame.size.height +10;
	}
	if ( poss!=nil && ![poss isEqualToString:@""]) {
		imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(10, org, 178, 47)];
		imageView2.image = [UIImage imageNamed:@"title_2.png"];
		[scrollView addSubview:imageView2];
		
		UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(50, 8 , 130, 40)];
		label2.text =@"지원가능선";
		label2.backgroundColor =[UIColor clearColor];
		[imageView2 addSubview:label2];
		
		textView2 = [[UITextView alloc] initWithFrame:CGRectMake(10, imageView2.frame.origin.y+imageView2.frame.size.height +10 , 300, 40)];
		textView2.text = poss;
		[textView2 setUserInteractionEnabled:NO];
		textView2.backgroundColor =[UIColor clearColor];
		[scrollView addSubview:textView2];
		[textView2 setFrame:CGRectMake(10, imageView2.frame.origin.y+imageView2.frame.size.height +10, 300, textView2.contentSize.height)];
        
		org = textView2.frame.origin.y+textView2.frame.size.height +10;
	}
	
	if ( typ!=nil && ![typ  isEqualToString:@""]) {
		imageView3 = [[UIImageView alloc] initWithFrame:CGRectMake(10, org, 178, 47)];
		imageView3.image = [UIImage imageNamed:@"title_3.png"];
		[scrollView addSubview:imageView3];
		
		UILabel *label3 = [[UILabel alloc] initWithFrame:CGRectMake(50, 8 , 130, 40)];
		label3.text =@"시험유형";
		label3.backgroundColor =[UIColor clearColor];
		[imageView3 addSubview:label3];
		
		textView3 = [[UITextView alloc] initWithFrame:CGRectMake(10, imageView3.frame.origin.y+imageView3.frame.size.height +10 , 300, 40)];
		textView3.text = typ;
		[textView3 setUserInteractionEnabled:NO];
		textView3.backgroundColor =[UIColor clearColor];
		[scrollView addSubview:textView3];
		[textView3 setFrame:CGRectMake(10, imageView3.frame.origin.y+imageView3.frame.size.height +10, 300, textView3.contentSize.height)];
		org = textView3.frame.origin.y+textView3.frame.size.height +10;	
	}
	if (sub !=nil && ![sub  isEqualToString:@""]) {
		imageView4 = [[UIImageView alloc] initWithFrame:CGRectMake(10, org, 178, 47)];
		imageView4.image = [UIImage imageNamed:@"title_4.png"];
		[scrollView addSubview:imageView4];
		
		UILabel *label4 = [[UILabel alloc] initWithFrame:CGRectMake(50, 8 , 130, 40)];
		label4.text =@"최근출제된시험주제";
		label4.backgroundColor =[UIColor clearColor];
		[imageView4 addSubview:label4];
		
		textView4 = [[UITextView alloc] initWithFrame:CGRectMake(10, imageView4.frame.origin.y+imageView4.frame.size.height +10 , 300, 40)];
		textView4.text = sub;
		[textView4 setUserInteractionEnabled:NO];
		textView4.backgroundColor =[UIColor clearColor];
		[scrollView addSubview:textView4];
		[textView4 setFrame:CGRectMake(10, imageView4.frame.origin.y+imageView4.frame.size.height +10, 300, textView4.contentSize.height)];
		org = textView4.frame.origin.y+textView4.frame.size.height +10;
	}
	if ( chr !=nil && ![chr isEqualToString:@""]) {
		imageView5 = [[UIImageView alloc] initWithFrame:CGRectMake(10, org, 178, 47)];
		imageView5.image = [UIImage imageNamed:@"title_5.png"];
		[scrollView addSubview:imageView5];
		
		UILabel *label5 = [[UILabel alloc] initWithFrame:CGRectMake(50, 8 , 130, 40)];
		label5.text =@"특징";
		label5.backgroundColor =[UIColor clearColor];
		[imageView5 addSubview:label5];
		
		textView5 = [[UITextView alloc] initWithFrame:CGRectMake(10, imageView5.frame.origin.y+imageView5.frame.size.height +10 , 300, 40)];
		textView5.text = chr;
		[textView5 setUserInteractionEnabled:NO];
		textView5.backgroundColor =[UIColor clearColor];
		[scrollView addSubview:textView5];
		[textView5 setFrame:CGRectMake(10, imageView5.frame.origin.y+imageView5.frame.size.height +10, 300, textView5.contentSize.height)];
		org = textView5.frame.origin.y+textView5.frame.size.height +10;
	}
    //	scrollView.contentSize = CGSizeMake(320,textView5.frame.origin.y+textView5.contentSize.height+20);
	
	scrollView.contentSize = CGSizeMake(320, org);
	[indicator stopAnimating];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	UIImage *image1 = [UIImage imageNamed:@"iphone_ad_1.png"];
	UIImage *image2 = [UIImage imageNamed:@"iphone_ad_2.png"];
	UIImage *image3 = [UIImage imageNamed:@"iphone_ad_3.png"];
	adArray = [NSArray arrayWithObjects:image1, image2, image3, nil];
	
	timer= [NSTimer scheduledTimerWithTimeInterval:15 target:self selector:@selector(onTimer) userInfo:nil repeats:YES];
	
	[self onTimer];

	barTitle.text  =univ;
	scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 44 , 320, 369)];
	[self.view addSubview:scrollView];
	NSLog(@"univ = %@ tCode = %@", uCode, [self tableName:tCode]);
	indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 64.0f, 64.0f)];
	[indicator setCenter:self.view.center];
    [indicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
    [self.view addSubview : indicator];	
	
	indicator.hidden= FALSE;
	[indicator startAnimating];
	
	[self performSelector:@selector(initView) withObject:nil afterDelay:0];
}

- (void)onTimer{
	int index = arc4random()%3;
	imageAD.image = [adArray objectAtIndex:index];
	imageAD.tag = index;
}
- (void)viewDidUnload
{
    [self setImageAD:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (NSString *)tableName:(NSString*)code {
	
	if ([code isEqualToString:@"TN01"] ) return @"12_TN01";
	else if ([code isEqualToString:@"TN02"] ) return @"13_TN02";
	else if ([code isEqualToString:@"TN03"] ) return @"14_TN03";
	else if ([code isEqualToString:@"TN04"] ) return @"15_TN04";
	else if ([code isEqualToString:@"TN05"] ) return @"16_TN05";
	else if ([code isEqualToString:@"TS01"] ) return @"17_TS01";
	else if ([code isEqualToString:@"TS02"] ) return @"18_TS02";
	else if ([code isEqualToString:@"TS03"] ) return @"19_TS03";
	else if ([code isEqualToString:@"TS04"] ) return @"20_TS04";
	else if ([code isEqualToString:@"TS05"] ) return @"21_TS05";
	else if ([code isEqualToString:@"TS06"] ) return @"22_TS06";
	else if ([code isEqualToString:@"TS07"] ) return @"23_TS07";
	else if ([code isEqualToString:@"TS08"] ) return @"24_TS08";
	else if ([code isEqualToString:@"TS09"] ) return @"25_TS09";
	else if ([code isEqualToString:@"TS10"] ) return @"26_TS10";
	else if ([code isEqualToString:@"TS12"] ) return @"27_TS12";
	else if ([code isEqualToString:@"TS13"] ) return @"28_TS13";
	else if ([code isEqualToString:@"TS14"] ) return @"29_TS14";
	else if ([code isEqualToString:@"TS15"] ) return @"30_TS15";
	else if ([code isEqualToString:@"TS16"] ) return @"31_TS16";
	else if ([code isEqualToString:@"TS17"] ) return @"32_TS17";
	else return @"12_TN01";
	
	
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)actionBack:(id)Sender {
	[self.navigationController popViewControllerAnimated:YES];
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	UITouch *touch = [touches anyObject];
	if (touch.view == imageAD ) {
		
		ADPop *adPop = [[ADPop alloc] initWithFrame:CGRectMake(0, 0, 320, 460)];
		[self.view addSubview:adPop];
		adPop.index = touch.view.tag;
	}
}

@end
