//
//  RecResultViewController.m
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 7. 19..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import "RecResultViewController.h"
#import "ResultViewController.h"
#import "User.h"
#import "SimpleServiceViewController.h"
#import "Test.h"
@interface RecResultViewController (){
	UIScrollView *scrollView;
	NSArray *array;
	
	NSString *hCate;
	NSDictionary *userDic;
	NSDictionary *infoDic;
	UIActivityIndicatorView *indicator;
	NSArray *adArray;
	NSTimer *timer;
}
@end

@implementation RecResultViewController
@synthesize univ;
@synthesize univCode;
@synthesize unit;
@synthesize type;
@synthesize series01;
@synthesize series02;
@synthesize series03;
@synthesize labelUniv;
@synthesize imageAD;
@synthesize ID;
@synthesize menu;

- (void)btnClick:(id)sender {
	if (menu==3) {
		
		SimpleServiceViewController *viewController = [[SimpleServiceViewController alloc] init];
		viewController.uname = univ;
		[self.navigationController pushViewController:viewController animated:YES];
	} else {
		
		UIButton *btn = (UIButton *)sender;
		
		ResultViewController *viewController = [[ResultViewController alloc] init];
		viewController.uCode = univCode;
		viewController.univ = univ;
		viewController.tCode = [[array objectAtIndex:btn.tag] objectForKey:@"T_CODE"];
		[self.navigationController pushViewController:viewController animated:YES];
	}
}

- (void)initView
{
    NSNumber* avg;
	NSMutableArray *grades;
	if (menu==3) {
		avg = [Test avgValue];
		grades= [Test gradeArray];
	} else {
		avg = [Record avgValueWithID:ID];
		grades= [Record gradeArrayWithEmail:ID];
	}
	NSLog(@"%f  ", [avg doubleValue]);
	
	array = [UnivInfo typesDescriptionWithUniv:univCode avg:avg series03:series03 series02:series02 series01:series01 type:type unit:unit];
	NSLog(@"%@", array);
	
	scrollView.contentSize = CGSizeMake(320,450*array.count);
	
	userDic = [User userInfoWithID:ID];
	infoDic = [User infoWithID:ID];
	
	hCate = [userDic objectForKey:@"P_Cate"];
	
	int org = 10;	
	for (int i=0;i<array.count;i++){
		
		NSDictionary *dic = [array objectAtIndex:i];
		if ([avg doubleValue] > [[dic objectForKey:@"U_JUST"] doubleValue]) {
			continue;
		}
		
		if (![self chekProvisoWithGrades:grades proviso:[dic objectForKey:@"U_PROVISO"]]) continue;
		
		NSArray *tuessArray = [UnivInfo typeDetailWithTUCode:[dic objectForKey:@"TU_CODE"]];
		NSString *tunm =[NSString stringWithFormat:@"%@ (%d차)", [[tuessArray objectAtIndex:0] objectForKey:@"TU_NM"],[[dic objectForKey:@"TU_SEQ"] intValue]] ;
		
		NSMutableString *ratio = [[NSMutableString alloc] init];
		for (NSDictionary *dic in tuessArray) {
			[ratio appendString:[NSString stringWithFormat:@"%@ %@ \n",[dic objectForKey:@"TU_STEP"], [dic objectForKey:@"TU_RATIO"]]]; 
		}
        
		UIImageView *imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake(10, org, 178, 47)];
		imageView1.image = [UIImage imageNamed:@"title_1.png"];
		[scrollView addSubview:imageView1];
		
		UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(50, 8 , 130, 40)];
		label1.text =@"대학명";
		label1.backgroundColor =[UIColor clearColor];
		[imageView1 addSubview:label1];
		
		UITextView *textView1 = [[UITextView alloc] initWithFrame:CGRectMake(10, org+imageView1.frame.size.height +10 , 300, 40)];
		textView1.text =  [dic objectForKey:@"U_NM"];
		[textView1 setUserInteractionEnabled:NO];
		textView1.backgroundColor =[UIColor clearColor];
		[scrollView addSubview:textView1];
		[textView1 setFrame:CGRectMake(10, org+imageView1.frame.size.height +10, 300, textView1.contentSize.height)];
		
		org = textView1.frame.origin.y+textView1.frame.size.height;
		
		UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(10, org, 178, 47)];
		imageView2.image = [UIImage imageNamed:@"title_2.png"];
		[scrollView addSubview:imageView2];
        
		UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(45, 8 , 130, 40)];
		label2.text =@"모집단위명";
		label2.backgroundColor =[UIColor clearColor];
		[imageView2 addSubview:label2];
		
		UITextView *textView2 = [[UITextView alloc] initWithFrame:CGRectMake(10, org+imageView2.frame.size.height +10 , 300, 40)];
		textView2.text =  [dic objectForKey:@"U_UNIT"];;
		[textView2 setUserInteractionEnabled:NO];
		textView2.backgroundColor =[UIColor clearColor];
		[scrollView addSubview:textView2];
		[textView2 setFrame:CGRectMake(10, org+imageView2.frame.size.height +10, 300, textView2.contentSize.height)];
		
		org = textView2.frame.origin.y+textView2.frame.size.height;
		
		UIImageView *imageView3 = [[UIImageView alloc] initWithFrame:CGRectMake(10, org, 178, 47)];
		imageView3.image = [UIImage imageNamed:@"title_3.png"];
		[scrollView addSubview:imageView3];
		
		UILabel *label3 = [[UILabel alloc] initWithFrame:CGRectMake(45, 8 , 130, 40)];
		label3.text = @"전형명";
		label3.backgroundColor =[UIColor clearColor];
		[imageView3 addSubview:label3];
		
		UITextView *textView3 = [[UITextView alloc] initWithFrame:CGRectMake(10, org+imageView3.frame.size.height +10 , 300, 40)];
		textView3.text = tunm;
		[textView3 setUserInteractionEnabled:NO];
		textView3.backgroundColor =[UIColor clearColor];
		[scrollView addSubview:textView3];
		[textView3 setFrame:CGRectMake(10, org+imageView3.frame.size.height +10, 300, textView3.contentSize.height)];
		
		org = textView3.frame.origin.y+textView3.frame.size.height;
		
		UIImageView *imageView4 = [[UIImageView alloc] initWithFrame:CGRectMake(10, org, 178, 47)];
		imageView4.image = [UIImage imageNamed:@"title_4.png"];
		[scrollView addSubview:imageView4];
        
		UILabel *label4 = [[UILabel alloc] initWithFrame:CGRectMake(45, 8 , 130, 40)];
		label4.text = @"선발인원";
		label4.backgroundColor =[UIColor clearColor];
		[imageView4 addSubview:label4];
		
		UITextView *textView4 = [[UITextView alloc] initWithFrame:CGRectMake(10, org+imageView4.frame.size.height +10 , 300, 40)];
		int num = [[dic objectForKey:@"U_FIXNUM"] intValue];
		if(num>0) textView4.text = [NSString stringWithFormat:@"%d명 선발",[[dic objectForKey:@"U_FIXNUM"] intValue]];
		else  textView4.text = [dic objectForKey:@"U_NOTE"];
		[textView4 setUserInteractionEnabled:NO];
		textView4.backgroundColor =[UIColor clearColor];
		[scrollView addSubview:textView4];
		[textView4 setFrame:CGRectMake(10, org+imageView4.frame.size.height +10, 300, textView4.contentSize.height)];
        
		org = textView4.frame.origin.y+textView4.frame.size.height;
		
		UIImageView *imageView5 = [[UIImageView alloc] initWithFrame:CGRectMake(10, org, 178, 47)];
		imageView5.image = [UIImage imageNamed:@"title_5.png"];
		[scrollView addSubview:imageView5];
		
		UILabel *label5 = [[UILabel alloc] initWithFrame:CGRectMake(45, 8 , 130, 40)];
		label5.text = @"수능최저등급";
		label5.backgroundColor =[UIColor clearColor];
		[imageView5 addSubview:label5];
		
		UITextView *textView5 = [[UITextView alloc] initWithFrame:CGRectMake(10, org+imageView5.frame.size.height +10 , 300, 40)];
		NSString *proviso = [dic objectForKey:@"U_PROVISO"];
		if([proviso isEqualToString:@""] || proviso==nil) textView5.text = @"수능최저등급 미적용";
		else textView5.text = [dic objectForKey:@"U_MEA"];;
		//else textView5.text = [UnivInfo provisoWithCode:[dic objectForKey:@"U_PROVISO"]];
		[textView5 setUserInteractionEnabled:NO];
		textView5.backgroundColor =[UIColor clearColor];
		[scrollView addSubview:textView5];
		[textView5 setFrame:CGRectMake(10, org+imageView4.frame.size.height +10, 300, textView5.contentSize.height)];
		
		org = textView5.frame.origin.y+textView5.frame.size.height+10;
		
		UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(20,org, 70, 31)];
		[btn setImage:[UIImage imageNamed:@"detail.png"] forState:UIControlStateNormal];
		[btn setImage:[UIImage imageNamed:@"detail_selected.png"] forState:UIControlStateSelected];
		[scrollView addSubview:btn];
		btn.tag=i;
		[btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
		org = btn.frame.origin.y+btn.frame.size.height+10;
		
		if (i<array.count-1) {
			UIImageView *imageSep = [[UIImageView alloc] initWithFrame:CGRectMake(0, org, 320, 3)];
			imageSep.image = [UIImage imageNamed:@"line_shadow.png"];
			[scrollView addSubview:imageSep];
			org = btn.frame.origin.y+btn.frame.size.height+20;
		}
	}
	scrollView.contentSize = CGSizeMake(320,org);
	[indicator stopAnimating];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	UIImage *image1 = [UIImage imageNamed:@"iphone_ad_1.png"];
	UIImage *image2 = [UIImage imageNamed:@"iphone_ad_2.png"];
	UIImage *image3 = [UIImage imageNamed:@"iphone_ad_3.png"];
	adArray = [NSArray arrayWithObjects:image1, image2, image3, nil];
	
	timer= [NSTimer scheduledTimerWithTimeInterval:15 target:self selector:@selector(onTimer) userInfo:nil repeats:YES];
	
	[self onTimer];

	labelUniv.text = univ;
	scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 44 , 320, 369)];
	[self.view addSubview:scrollView];
	
	indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 64.0f, 64.0f)];
	[indicator setCenter:self.view.center];
    [indicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
    [self.view addSubview : indicator];	
	
	indicator.hidden= FALSE;
	[indicator startAnimating];
	
	[self performSelector:@selector(initView) withObject:nil afterDelay:0];
}

- (void)onTimer{
	int index = arc4random()%3;
	imageAD.image = [adArray objectAtIndex:index];
	imageAD.tag = index;
}


- (NSDictionary *)findWithArea:(NSString*)area array:(NSArray*)arr {
	for (NSDictionary *dic in arr) {
		if ([[dic objectForKey:@"area"] isEqualToString:[area lowercaseString]])
			return dic;
	}
	return nil;
}


- (BOOL)chekProvisoWithGrades:(NSArray*)grades proviso:(NSString*)proviso{
	BOOL isSuccess=YES;
	double study;
	double study1=[[grades objectAtIndex:3] doubleValue];
	double study2=[[grades objectAtIndex:4] doubleValue];
	
		
	if ([proviso isEqualToString:@""]) return isSuccess;
	
	NSString *a = [proviso substringWithRange:NSMakeRange(0, 1)];
	NSString *b = [proviso substringWithRange:NSMakeRange(1, 1)];
	NSString *c = [proviso substringWithRange:NSMakeRange(2, 1)];
	NSString *d = [proviso substringWithRange:NSMakeRange(3, 1)];
	int scount =  [[proviso substringWithRange:NSMakeRange(4, 1)] intValue];
	NSString *gradeType = [proviso substringWithRange:NSMakeRange(5, 1)];
	int acount =  [[proviso substringWithRange:NSMakeRange(7, 1)] intValue];
	NSString *gradeType1 =[proviso substringWithRange:NSMakeRange(8, 1)]; 
	int calcResult =  [[proviso substringWithRange:NSMakeRange(9, 2)] intValue];
	
	NSString *r1 = [proviso substringWithRange:NSMakeRange(12, 1)];
	NSString *r2 = [proviso substringWithRange:NSMakeRange(13, 1)];
	NSString *r3 = [proviso substringWithRange:NSMakeRange(14, 1)];
	
	if (scount==0) {
		study = 0;
	}
	if (scount==1) {
		if (study1<=study2) study = study1;
		else study = study2;
	} else if (scount ==2) {
		if ([gradeType isEqualToString:@"p"]) {
			study = (study1+study2)/2;
		} else {
			if (study1<=study2) study = study2;
			else study = study1;
		}
	}

	NSMutableArray *arrGrades = [NSMutableArray array];
	if (![a isEqualToString:@"n"]) [arrGrades addObject: [NSDictionary dictionaryWithObjectsAndKeys: [grades objectAtIndex:0],@"value", @"a", @"area", nil]];
	if (![b isEqualToString:@"n"]) [arrGrades addObject:[NSDictionary dictionaryWithObjectsAndKeys: [grades objectAtIndex:1],@"value", @"b", @"area", nil]];
	if (![c isEqualToString:@"n"]) [arrGrades addObject:[NSDictionary dictionaryWithObjectsAndKeys: [grades objectAtIndex:2],@"value", @"c", @"area", nil]];
	if (![d isEqualToString:@"n"]) [arrGrades addObject:[NSDictionary dictionaryWithObjectsAndKeys: [NSNumber numberWithDouble:study],@"value", @"d", @"area", nil]];
	
	NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"value" ascending:YES];
	NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
	NSMutableArray *sortedArray = [NSMutableArray arrayWithArray:[arrGrades sortedArrayUsingDescriptors:sortDescriptors]];

	
	if ([hCate isEqualToString:@"문과"] &&([b isEqualToString:@"B"] || [d isEqualToString:@"D"] )) return NO;
	
	if ([gradeType1 isEqualToString:@"g"]) {
		int passCount=0;
		for (int i = 0; i<sortedArray.count; i++) {
			if(calcResult >= [[[sortedArray objectAtIndex:i] objectForKey:@"value"] doubleValue]) passCount+=1;
		}
		if (passCount<acount) isSuccess = NO;				 
		
		if (![r1 isEqualToString:@"n"] && [r2 isEqualToString:@"n"]) {
			NSDictionary *dic=[self findWithArea:r1 array:sortedArray];
			if ([[dic objectForKey:@"value"] doubleValue]>calcResult) isSuccess = NO;
		} else if(![r1 isEqualToString:@"n"] && ![r2 isEqualToString:@"n"]) {
			if ([r3 isEqualToString:@"1"]) {
				NSDictionary *dic1=[self findWithArea:r1 array:sortedArray];
				NSDictionary *dic2=[self findWithArea:r2 array:sortedArray];
				if (!([[dic1 objectForKey:@"value"] doubleValue]<=calcResult || [[dic2 objectForKey:@"value"] doubleValue]<=calcResult)) isSuccess = NO;
			} else 	if ([r3 isEqualToString:@"2"]) {
				NSDictionary *dic1=[self findWithArea:r1 array:sortedArray];
				NSDictionary *dic2=[self findWithArea:r2 array:sortedArray];
				if (!([[dic1 objectForKey:@"value"] doubleValue]<=calcResult && [[dic2 objectForKey:@"value"] doubleValue]<=calcResult)) isSuccess = NO;
			}			 
		}
	} else if([gradeType1 isEqualToString:@"p"]) {
		if ([r1 isEqualToString:@"n"]) {
			double sum=0;
			for (int i=0; i<acount; i++) {
				sum = sum + [[[sortedArray objectAtIndex:i] objectForKey:@"value"] doubleValue];
			}
			double avg = sum/acount;
			if (avg>calcResult) isSuccess = NO;
		} if (![r1 isEqualToString:@"n"] && [r2 isEqualToString:@"n"]){
			NSDictionary *dic=[self findWithArea:r1 array:sortedArray]; 
			[sortedArray removeObject:dic];
			double sum=0;
			for (int i=0; i<acount-1; i++) {
				sum = sum + [[[sortedArray objectAtIndex:i] objectForKey:@"value"] doubleValue];
			}
			sum = sum+[[dic objectForKey:@"value"] doubleValue];
			double avg = sum/acount;
			if (avg>calcResult) isSuccess = NO;
		} if (![r1 isEqualToString:@"n"] && ![r2 isEqualToString:@"n"]){		
			if ([r3 isEqualToString:@"1"]) {
				BOOL tempCheck = NO;
				
				NSMutableArray *temp = [sortedArray copy];
				
				NSDictionary *dic=[self findWithArea:r1 array:temp]; 
				[temp removeObject:dic];
				double sum=0;
				for (int i=0; i<acount-1; i++) {
					sum = sum + [[[temp objectAtIndex:i] objectForKey:@"value"] doubleValue];
				}
				sum = sum+[[dic objectForKey:@"value"] doubleValue];
				double avg = sum/acount;
				if (avg<=calcResult) tempCheck = YES;
				
				if (!tempCheck) {
					NSMutableArray *temp = [sortedArray copy];
					
					NSDictionary *dic=[self findWithArea:r2 array:temp]; 
					[temp removeObject:dic];
					double sum=0;
					for (int i=0; i<acount-1; i++) {
						sum = sum + [[[temp objectAtIndex:i] objectForKey:@"value"] doubleValue];
					}
					sum = sum+[[dic objectForKey:@"value"] doubleValue];
					avg = sum/acount;
					if (avg<=calcResult) tempCheck = YES;
				}
				if (!tempCheck) isSuccess = NO;
			} else 	if ([r3 isEqualToString:@"2"]) {
				NSDictionary *dic=[self findWithArea:r1 array:sortedArray]; 
				[sortedArray removeObject:dic];
				double sum=[[dic objectForKey:@"value"] doubleValue];
				
				dic=[self findWithArea:r2 array:sortedArray]; 
				[sortedArray removeObject:dic];
				sum=sum+[[dic objectForKey:@"value"] doubleValue];
				
				for (int i=0; i<acount-2; i++) {
					sum = sum + [[[sortedArray objectAtIndex:i] objectForKey:@"value"] doubleValue];
				}
				double avg = sum/acount;
				if (avg>calcResult) isSuccess = NO;
			}	
		}
	} else if([gradeType1 isEqualToString:@"s"]) {
		if ([r1 isEqualToString:@"n"]) {
			double sum=0;
			for (int i=0; i<acount; i++) {
				sum = sum + [[[sortedArray objectAtIndex:i] objectForKey:@"value"] doubleValue];
			}
			if (sum>calcResult) isSuccess = NO;
		} if (![r1 isEqualToString:@"n"] && [r2 isEqualToString:@"n"]){
			NSDictionary *dic=[self findWithArea:r1 array:sortedArray]; 
			[sortedArray removeObject:dic];
			double sum=0;
			for (int i=0; i<acount-1; i++) {
				sum = sum + [[[sortedArray objectAtIndex:i] objectForKey:@"value"] doubleValue];
			}
			sum = sum+[[dic objectForKey:@"value"] doubleValue];
			if (sum>calcResult) isSuccess = NO;
		} if (![r1 isEqualToString:@"n"] && ![r2 isEqualToString:@"n"]){		
			if ([r3 isEqualToString:@"1"]) {
				BOOL tempCheck = NO;
				NSMutableArray *temp = [sortedArray copy];
				NSDictionary *dic=[self findWithArea:r1 array:temp]; 
				[temp removeObject:dic];
				double sum=0;
				for (int i=0; i<acount-1; i++) {
					sum = sum + [[[temp objectAtIndex:i] objectForKey:@"value"] doubleValue];
				}
				sum = sum+[[dic objectForKey:@"value"] doubleValue];
				
				if (sum<=calcResult) tempCheck = YES;
				
				if (!tempCheck) {
					NSMutableArray *temp = [sortedArray copy];
					
					NSDictionary *dic=[self findWithArea:r2 array:temp]; 
					[temp removeObject:dic];
					sum=0;
					for (int i=0; i<acount-1; i++) {
						sum = sum + [[[temp objectAtIndex:i] objectForKey:@"value"] doubleValue];
					}
					sum = sum+[[dic objectForKey:@"value"] doubleValue];
					if (sum<=calcResult) tempCheck = YES;
				}
				if (!tempCheck) isSuccess = NO;
			} else 	if ([r3 isEqualToString:@"2"]) {
				NSDictionary *dic=[self findWithArea:r1 array:sortedArray]; 
				[sortedArray removeObject:dic];
				double sum=[[dic objectForKey:@"value"] doubleValue];
				dic=[self findWithArea:r2 array:sortedArray]; 
				[sortedArray removeObject:dic];
				sum=sum+[[dic objectForKey:@"value"] doubleValue];
				
				for (int i=0; i<acount-2; i++) {
					sum = sum + [[[sortedArray objectAtIndex:i] objectForKey:@"value"] doubleValue];
				}
				if (sum>calcResult) isSuccess = NO;
			}	
		}
	
	}
	return isSuccess;
}

- (void)viewDidUnload
{
    [self setImageAD:nil];
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)goBack:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)goHome:(id)sender {
	[self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	UITouch *touch = [touches anyObject];
	if (touch.view == imageAD ) {
		
		ADPop *adPop = [[ADPop alloc] initWithFrame:CGRectMake(0, 0, 320, 460)];
		[self.view addSubview:adPop];
		adPop.index = touch.view.tag;
	}
}


@end
