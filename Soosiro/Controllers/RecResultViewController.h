//
//  RecResultViewController.h
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 7. 19..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecResultViewController : UIViewController
@property (strong, nonatomic) NSString *univ;
@property (strong, nonatomic) NSString *univCode;
@property (strong, nonatomic) NSString *unit;
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSString *series01;
@property (strong, nonatomic) NSString *series02;
@property (strong, nonatomic) NSString *series03;
@property (strong, nonatomic) NSString *ID;
@property (strong, nonatomic) IBOutlet UILabel *labelUniv;
@property (strong, nonatomic) IBOutlet UIImageView *imageAD;
@property (nonatomic) int menu;
- (IBAction)goBack:(id)sender;
- (IBAction)goHome:(id)sender;
@end
