//
//  EnglishGradeViewController.m
//  Soosiro
//
//  Created by 이영록 on 12. 7. 6..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import "EnglishGradeViewController.h"
#import "Language.h"
#import "User.h"
#import "TouchXML.h"
@interface EnglishGradeViewController (){
	NSIndexPath *selectedIndex;
	BOOL isNeedSave;
	NSMutableDictionary *engDic;
	NSArray *adArray;
	NSTimer *timer;
}
@property (strong, nonatomic) NSArray *langList;
@property (strong, nonatomic) NSArray *englishList;
@property (nonatomic, retain) NSMutableData *receivedData;
@property (nonatomic, retain) NSURLResponse *response;
@property (nonatomic, retain) NSString *rdata;
@end

@implementation EnglishGradeViewController
@synthesize tableView=_tableView;
@synthesize viewBar;
@synthesize textGrade;
@synthesize englishList;
@synthesize langList;
@synthesize ID;
@synthesize labelMainTitle;
@synthesize imageAD;
@synthesize receivedData;
@synthesize response;
@synthesize rdata;
- (void)viewDidLoad
{
    [super viewDidLoad];
    labelMainTitle.text = [Title mainTilteWithMenu:4];
	UIImage *image1 = [UIImage imageNamed:@"iphone_ad_1.png"];
	UIImage *image2 = [UIImage imageNamed:@"iphone_ad_2.png"];
	UIImage *image3 = [UIImage imageNamed:@"iphone_ad_3.png"];
	adArray = [NSArray arrayWithObjects:image1, image2, image3, nil];
	
	timer= [NSTimer scheduledTimerWithTimeInterval:15 target:self selector:@selector(onTimer) userInfo:nil repeats:YES];
	
	[self onTimer];
	

	langList =[Language languageRecordWithEmail:ID];
	
	if (langList.count==0) {
		langList = [NSArray arrayWithObjects:
					[NSMutableDictionary dictionaryWithObjectsAndKeys:@"Eng",@"content",[NSNumber numberWithInt:0],@"grade", nil],
					[NSMutableDictionary dictionaryWithObjectsAndKeys:@"CHI",@"content",[NSNumber numberWithInt:0],@"grade", nil],
					[NSMutableDictionary dictionaryWithObjectsAndKeys:@"DEU",@"content",[NSNumber numberWithInt:0],@"grade", nil],
					[NSMutableDictionary dictionaryWithObjectsAndKeys:@"FRA",@"content",[NSNumber numberWithInt:0],@"grade", nil],
					[NSMutableDictionary dictionaryWithObjectsAndKeys:@"JAP",@"content",[NSNumber numberWithInt:0],@"grade", nil],
					[NSMutableDictionary dictionaryWithObjectsAndKeys:@"RUS",@"content",[NSNumber numberWithInt:0],@"grade", nil],
					[NSMutableDictionary dictionaryWithObjectsAndKeys:@"SPA",@"content",[NSNumber numberWithInt:0],@"grade", nil], 
					[NSMutableDictionary dictionaryWithObjectsAndKeys:@"HAN",@"content",[NSNumber numberWithInt:0],@"grade", nil],
					nil];
	}


	englishList = [NSArray arrayWithObjects:
				[NSMutableDictionary dictionaryWithObjectsAndKeys:@"G-TELP",@"content",[NSNumber numberWithInt:0],@"grade", nil],
				[NSMutableDictionary dictionaryWithObjectsAndKeys:@"TEPS",@"content",[NSNumber numberWithInt:0],@"grade", nil],
				[NSMutableDictionary dictionaryWithObjectsAndKeys:@"TOEFL-CBT",@"content",[NSNumber numberWithInt:0],@"grade", nil],
				[NSMutableDictionary dictionaryWithObjectsAndKeys:@"TOEFL-IBT",@"content",[NSNumber numberWithInt:0],@"grade", nil],
				[NSMutableDictionary dictionaryWithObjectsAndKeys:@"TOEFL-PBT",@"content",[NSNumber numberWithInt:0],@"grade", nil],
				[NSMutableDictionary dictionaryWithObjectsAndKeys:@"TOEIC",@"content",[NSNumber numberWithInt:0],@"grade", nil],
				nil];
	
	engDic = [Language englishRecordWithEmail:ID];
	
	[self.tableView reloadData];
	
	[viewBar setFrame:CGRectMake(0, 480, 320, 44)];

	[[NSNotificationCenter defaultCenter] addObserver:self 
											 selector:@selector(keyboardWillShow:) 
												 name:UIKeyboardWillShowNotification 
											   object:nil]; 
	
	[[NSNotificationCenter defaultCenter] addObserver:self 
											 selector:@selector(keyboardWillHide:) 
												 name:UIKeyboardWillHideNotification
											   object:nil];
	

}

- (void)onTimer{
	int index = arc4random()%3;
	imageAD.image = [adArray objectAtIndex:index];
	imageAD.tag = index;
}

- (void)keyboardWillShow:(NSNotification *)notif {
	[UIView beginAnimations:@"ViewAnimation" context:nil];
    [UIView setAnimationDuration:0.25];
	[viewBar setFrame:CGRectMake(0, 200, 320, 50)];
    [UIView commitAnimations];
}

- (void)keyboardWillHide:(NSNotification *)notif {
	[UIView beginAnimations:@"ViewAnimation" context:nil];
    [UIView setAnimationDuration:0.25];
	[viewBar setFrame:CGRectMake(0, 480, 320, 50)];
	[textGrade setText:@""];
    [UIView commitAnimations];
}

- (IBAction)actionDone:(id)sender {
	[self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:2] animated:YES];
}
- (void)viewDidUnload
{
    [self setTableView:nil];
    [self setViewBar:nil];
    [self setTextGrade:nil];
    [self setLabelMainTitle:nil];
    [self setImageAD:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return englishList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
	}
	NSDictionary *dic =[englishList objectAtIndex:indexPath.row]; 
	NSString *content = [dic objectForKey:@"content"];
	cell.textLabel.text = content;
	cell.textLabel.backgroundColor =[UIColor clearColor];
	NSString *title = [engDic objectForKey:@"title"];
	
	NSLog(@" %@ %@", content, title);
	if (title !=nil &&[content isEqualToString:title]) {
		int score = [[engDic objectForKey:@"score"] intValue];
		if (score>0) {
				cell.detailTextLabel.text = [NSString stringWithFormat:@"%d", [[engDic objectForKey:@"score"] intValue]];
		} else cell.detailTextLabel.text = @"";
	} else cell.detailTextLabel.text = @"";

	cell.detailTextLabel.backgroundColor = [UIColor clearColor];
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	selectedIndex = indexPath;
	[textGrade becomeFirstResponder];
	[tableView setUserInteractionEnabled:NO];
	
}

- (IBAction)actionEditingEnd:(id)sender {
	NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
	NSNumber * number = [numberFormatter numberFromString:textGrade.text];
	if (!number) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"입력값 오류" message:@"입력값이 잘못되었습니다." delegate:self cancelButtonTitle:@"취소" otherButtonTitles:@"재입력", nil];
		[alert show];
		[self.tableView setUserInteractionEnabled:YES];
		[textGrade resignFirstResponder];
	    return;
	}
	isNeedSave = YES;
	
	engDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:[[englishList objectAtIndex:selectedIndex.row] objectForKey:@"content"], @"title", number, @"score", nil];

	[self.tableView reloadData];
	[self.tableView setUserInteractionEnabled:YES];

	Language *lang = [[Language alloc] init];
	lang.P_ID =ID;
	lang.ENG_TEST		= [engDic objectForKey:@"title"];
	lang.ENG_SCORE		= [[engDic objectForKey:@"score"] intValue];
	lang.CHI			= [[[langList objectAtIndex:1] objectForKey:@"grade"] intValue];
	lang.DEU			= [[[langList objectAtIndex:2] objectForKey:@"grade"] intValue];
	lang.FRA			= [[[langList objectAtIndex:3] objectForKey:@"grade"] intValue];
	lang.JAP			= [[[langList objectAtIndex:4] objectForKey:@"grade"] intValue];
	lang.RUS			= [[[langList objectAtIndex:5] objectForKey:@"grade"] intValue];
	lang.SPA			= [[[langList objectAtIndex:6] objectForKey:@"grade"] intValue];
	lang.HAN			= [[[langList objectAtIndex:7] objectForKey:@"grade"] intValue];
	[lang addLanguage];
	if ([Language chkLangWithID:ID]) [User updateInfoLang:ID value:@"Y"];
	else [User updateInfoLang:ID value:@"N"];
	[self sendLang];
	[textGrade resignFirstResponder];
	//englishList =[Language englishRecordWithEmail:ID];
	//[self.tableView reloadData];
}

- (IBAction)actionBack:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	UITouch *touch = [touches anyObject];
	if (touch.view == imageAD ) {
		
		ADPop *adPop = [[ADPop alloc] initWithFrame:CGRectMake(0, 0, 320, 460)];
		[self.view addSubview:adPop];
		adPop.index = touch.view.tag;
	}
}

- (void)sendLang {
	NSMutableString *str;	

	NSMutableString *lang = [NSMutableString string];
	for (int i=1; i<8; i++) {
		[lang appendString: [[[langList objectAtIndex:i] objectForKey:@"grade"] stringValue]] ;
	}
	NSString *engtest;
	if([engDic objectForKey:@"title"]==nil) engtest = @"";
	else engtest= [engDic objectForKey:@"title"];
	
	NSString *engscore;
	if([engDic objectForKey:@"score"]==nil) engscore = @"0";
	else engscore= [engDic objectForKey:@"score"];
	
	NSLog(@"lang = %@", lang);
	str=[NSMutableString stringWithFormat:@"http://www.sooseero.com/mobile/member/information2_1.mgo?id=%@&engTest=%@&engScore=%@&lang=%@", ID, engtest, engscore , lang];
		
		
		
	
	NSString *url=	[str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	
	NSLog(@"%@", url);
	[self requestUrl:url];
}



- (BOOL)requestUrl:(NSString *)url {
	
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] 
														   cachePolicy:NSURLRequestUseProtocolCachePolicy 
													   timeoutInterval:30.0]; 
	[request setHTTPMethod:@"GET"];
	NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
	if (connection) { 
		self.receivedData = [NSMutableData data]; // 수신할 데이터를 받을 공간을 마련
		return YES;
	}
	return NO;
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)aResponse {
	[receivedData setLength:0];
	self.response = aResponse;
	
	
	
	NSArray *cookies;
    NSDictionary *responseHeaderFields;
	
    // 받은 header들을 dictionary형태로 받고
    responseHeaderFields = [(NSHTTPURLResponse *)aResponse allHeaderFields];
	
    if(responseHeaderFields != nil)
    {
        // responseHeaderFields에 포함되어 있는 항목들 출력
        for(NSString *key in responseHeaderFields)
        {
            NSLog(@"Header: %@ = %@", key, [responseHeaderFields objectForKey:key]);
        }
		
        // cookies에 포함되어 있는 항목들 출력
        cookies = [NSHTTPCookie cookiesWithResponseHeaderFields:responseHeaderFields forURL:[aResponse URL]];
		
        if(cookies != nil)
        {
            for(NSHTTPCookie *a in cookies)
            {
                NSLog(@"Cookie: %@ = %@", [a name], [a value]);
            }
        }
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	//NSString *returnString = [[NSString alloc] initWithData:data encoding:EUCKRStringEncoding];
	[receivedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
	
	rdata = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
	NSLog(@"%@", rdata);
	
	CXMLDocument *rssParser = [[CXMLDocument alloc] initWithXMLString:rdata options:0 error:nil];
	NSArray *xmlNodes  = [rssParser nodesForXPath:@"//result" error:nil];
	
	for (CXMLElement *resultElement in xmlNodes) {
		
		int result = [[[[resultElement children] objectAtIndex:0] stringValue] intValue];
		
		switch (result) {
			case 0:{
				UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"성적전송실패" message:@"성적전송에 실패하였습니다." delegate:self cancelButtonTitle:@"확인" otherButtonTitles:nil]; 
				[alert show];
				break;
			}
			default:
				break;
		}
	}
}




@end
