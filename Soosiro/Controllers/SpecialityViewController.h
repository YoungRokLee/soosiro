//
//  SpecialityViewController.h
//  Soosiro
//
//  Created by 이영록 on 12. 6. 20..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpecialityViewController : UIViewController<UIGestureRecognizerDelegate>
@property (strong, nonatomic) IBOutlet UIImageView *tabImage01;
@property (strong, nonatomic) IBOutlet UIImageView *tabImage02;
@property (strong, nonatomic) IBOutlet UIImageView *tabImage03;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet NSString *ID;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) IBOutlet UIImageView *check1;
@property (strong, nonatomic) IBOutlet UIImageView *check2;
@property (strong, nonatomic) IBOutlet UIImageView *check3;
@property (strong, nonatomic) IBOutlet UIImageView *check4;
@property (strong, nonatomic) IBOutlet UIImageView *check5;
@property (strong, nonatomic) IBOutlet UIImageView *check6;
@property (strong, nonatomic) IBOutlet UIImageView *check7;
@property (strong, nonatomic) IBOutlet UIImageView *check8;
@property (strong, nonatomic) IBOutlet UIImageView *check9;
@property (strong, nonatomic) IBOutlet UIImageView *check10;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView1;

@property (strong, nonatomic) IBOutlet UIImageView *check_1;
@property (strong, nonatomic) IBOutlet UIImageView *check_2;
@property (strong, nonatomic) IBOutlet UIImageView *check_3;
@property (strong, nonatomic) IBOutlet UIImageView *check_4;
@property (strong, nonatomic) IBOutlet UIImageView *check_5;
@property (strong, nonatomic) IBOutlet UIImageView *check_6;
@property (strong, nonatomic) IBOutlet UIImageView *check_7;
@property (strong, nonatomic) IBOutlet UIImageView *check_8;
@property (strong, nonatomic) IBOutlet UIImageView *check_9;
@property (strong, nonatomic) IBOutlet UIImageView *check_10;
@property (strong, nonatomic) IBOutlet UIImageView *check_11;
@property (strong, nonatomic) IBOutlet UIImageView *check_12;
@property (strong, nonatomic) IBOutlet UIImageView *check_13;
@property (strong, nonatomic) IBOutlet UIImageView *check_14;
@property (strong, nonatomic) IBOutlet UIImageView *check_15;
@property (strong, nonatomic) IBOutlet UIImageView *check_16;
@property (strong, nonatomic) IBOutlet UIImageView *check_17;
@property (strong, nonatomic) IBOutlet UIImageView *check_18;
@property (strong, nonatomic) IBOutlet UIImageView *check_19;
@property (strong, nonatomic) IBOutlet UIImageView *check_20;
@property (strong, nonatomic) IBOutlet UIImageView *check_21;
@property (strong, nonatomic) IBOutlet UIImageView *check_22;
@property (strong, nonatomic) IBOutlet UIImageView *check_23;
@property (strong, nonatomic) IBOutlet UIImageView *check_24;
@property (strong, nonatomic) IBOutlet UIImageView *check_25;
@property (strong, nonatomic) IBOutlet UIImageView *check_26;
@property (strong, nonatomic) IBOutlet UIImageView *check_27;
@property (strong, nonatomic) IBOutlet UIImageView *check_28;
@property (strong, nonatomic) IBOutlet UIImageView *check_29;
@property (strong, nonatomic) IBOutlet UIImageView *check_30;
@property (strong, nonatomic) IBOutlet UILabel *labelMainTitle;
@property (strong, nonatomic) IBOutlet UIImageView *imageAD;

- (IBAction)selectTab:(id)sender;
- (IBAction)touchTab:(id)sender;
- (IBAction)actionBack:(id)sender;
- (IBAction)actionDone:(id)sender;
@end
