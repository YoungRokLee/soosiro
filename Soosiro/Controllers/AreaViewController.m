//
//  AreaViewController.m
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 6. 7..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import "AreaViewController.h"
#import "UnivInfo.h"
#import "Title.h"
@interface AreaViewController (){
	NSArray *list;
	UIActivityIndicatorView *indicator;
}

@end

@implementation AreaViewController
@synthesize map01ImageView;
@synthesize map02ImageView;
@synthesize map03ImageView;
@synthesize map04ImageView;
@synthesize map05ImageView;
@synthesize map06ImageView;
@synthesize labelBack;
@synthesize imageViewList;
@synthesize menu;
@synthesize type;
@synthesize tclass;
@synthesize unit;
@synthesize series03;
@synthesize series02;
@synthesize series01;
@synthesize labelMainTitle;


#pragma mark - View lifecycle

- (int)indexForLCode:(NSString*)code{
	if([code isEqualToString:@"L01"]) return 0;
	else if([code isEqualToString:@"L03"] || [code isEqualToString:@"L08"]) return 1;
	else if([code isEqualToString:@"L15"]) return 2;
	else if([code isEqualToString:@"L04"] || [code isEqualToString:@"L09"] || [code isEqualToString:@"L10"]) return 3;
	else if([code isEqualToString:@"L02"] || [code isEqualToString:@"L05"] || [code isEqualToString:@"L07"] || [code isEqualToString:@"L11"] || [code isEqualToString:@"L12"]) return 4;
	else if([code isEqualToString:@"L06"] || [code isEqualToString:@"L13"] || [code isEqualToString:@"L14"] || [code isEqualToString:@"L16"]) return 5;
	else return 0;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	labelMainTitle.text = [Title mainTilteWithMenu:menu];
    imageViewList = [NSMutableArray arrayWithObjects:map01ImageView,map02ImageView, map03ImageView, map04ImageView, map05ImageView, map06ImageView, nil];
	for (id imageView in imageViewList) {
		[(UIImageView*)imageView setHidden:YES]; 
	}
	
	list = [UnivInfo lcodeWithMenu:menu type:type tclass:tclass series03:series03 series02:series02 series01:series01];
	
	for (NSString *lcode in list) {
		int index = [self indexForLCode:lcode];
		for (UIButton *btn in [self.view subviews]) {
			if (btn.tag == index)
				btn.enabled = YES;
		}	
	}
	
	
}

- (void)viewDidUnload
{
    [self setMap02ImageView:nil];
    [self setMap01ImageView:nil];
    [self setMap03ImageView:nil];
    [self setMap04ImageView:nil];
    [self setMap05ImageView:nil];
    [self setMap06ImageView:nil];

	[self setLabelBack:nil];
    [self setLabelMainTitle:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)back:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)areaSelected:(id)sender {
	UIButton *button = (UIButton *)sender;
	int area = button.tag;
	
	UnivViewController *viewController = [[UnivViewController alloc] init];
	viewController.menu = menu;
	viewController.area = area;
	viewController.type = type;
	viewController.tclass = tclass;
	viewController.unit = unit;
	viewController.series03 = series03;
	viewController.series02 = series02;
	viewController.series01 = series01;
	NSLog(@"unit name = %@", unit);
	UIImageView *imageView = [imageViewList objectAtIndex:area];
	[imageView setHidden:YES];
	[self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)touchInArea:(id)sender {
	for (id imageView in imageViewList) {
		[(UIImageView*)imageView setHidden:YES]; 
	}
	UIButton *button = (UIButton *)sender;
	int index = button.tag;
	UIImageView *imageView = [imageViewList objectAtIndex:index];
	[imageView setHidden:NO];
}


@end
