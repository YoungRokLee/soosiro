//
//  SeriesDesViewController.h
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 6. 18..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TypeDesViewController : UIViewController
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSString *tclass;
@property (nonatomic) int menu;

@property (strong, nonatomic) NSString *series03;
@property (strong, nonatomic) NSString *series02;
@property (strong, nonatomic) NSString *series01;
@property (strong, nonatomic)IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UILabel *labelMainTitle;
@property (strong, nonatomic) IBOutlet UIImageView *imageAD;


- (IBAction)actionBack:(id)sender;

- (IBAction)actionNext:(id)sender;
@end
