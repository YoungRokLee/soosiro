//
//  GradeViewController.h
//  Soosiro
//
//  Created by 이영록 on 12. 6. 20..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GradeViewController : UIViewController<UITextFieldDelegate>
@property (nonatomic) int menu;
@property (nonatomic) int style;
@property (nonatomic) int seq;
@property (nonatomic) int mf;
@property (strong, nonatomic) NSString *term;
@property (strong, nonatomic) NSString *ID;
@property (strong, nonatomic) IBOutlet UILabel *labelTitle;

@property (strong, nonatomic) IBOutlet UILabel *labelAlert;
@property (strong, nonatomic) IBOutlet UILabel *labelInfo;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIView *viewBar;
@property (strong, nonatomic) IBOutlet UIView *viewInfo;
@property (strong, nonatomic) IBOutlet UITextField *textGrade;
@property (strong, nonatomic) IBOutlet UIButton *btnDone;
@property (strong, nonatomic) IBOutlet UILabel *labelArea;
@property (strong, nonatomic) IBOutlet UILabel *labelMainTitle;
@property (strong, nonatomic) IBOutlet UIImageView *imageAD;



- (IBAction)actionBack:(id)sender;
- (IBAction)actionNext:(id)sender;
//- (IBAction)actionEditingEnd:(id)sender;

@end
