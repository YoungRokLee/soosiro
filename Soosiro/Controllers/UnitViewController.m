//
//  UnitViewController.m
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 6. 16..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import "UnitViewController.h"
#import "UnivInfo.h"
#import "TypeViewController.h"
#import "StrategyViewController.h"
#import "User.h"
#import "UnivBViewController.h"
@interface UnitViewController (){
	int selectedRow;
	NSMutableArray *list;
	NSString *series03;
	UIActivityIndicatorView *indicator;
	NSArray *adArray;
	NSTimer *timer;
}
@end

@implementation UnitViewController
@synthesize tableView=_tableView;
@synthesize labelMainTitle;
@synthesize univCode;
@synthesize series02;
@synthesize series01;
@synthesize imageAD;
@synthesize menu;
@synthesize type;
@synthesize labelTitle;
@synthesize mf;
@synthesize ID;
@synthesize typeString;
@synthesize tclass;
- (void)initView
{
	if (menu==5) {
		list = [UnivInfo series03InfoWithMenu:menu univ:univCode seriese02:series02 series01:series01 type:typeString tclass:tclass mf:mf];
	} else 
		list = [UnivInfo series03InfoWithMenu:menu univ:univCode seriese02:series02 series01:series01 type:type tclass:tclass mf:mf];
	
	[self.tableView reloadData];
	[indicator stopAnimating];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	UIImage *image1 = [UIImage imageNamed:@"iphone_ad_1.png"];
	UIImage *image2 = [UIImage imageNamed:@"iphone_ad_2.png"];
	UIImage *image3 = [UIImage imageNamed:@"iphone_ad_3.png"];
	adArray = [NSArray arrayWithObjects:image1, image2, image3, nil];
	
	timer= [NSTimer scheduledTimerWithTimeInterval:15 target:self selector:@selector(onTimer) userInfo:nil repeats:YES];
	
	[self onTimer];

	
	labelMainTitle.text = [Title mainTilteWithMenu:menu];
	selectedRow = -1;
	//[labelTitle setFont:[UIFont fontWithName:@"NanumGothicOTF" size:20]];
    [self.tableView setBackgroundColor:[UIColor clearColor]];
	
	indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 64.0f, 64.0f)];
	[indicator setCenter:self.view.center];
    [indicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
	[self.view addSubview : indicator];	
	[indicator startAnimating];
	
	[self performSelector:@selector(initView) withObject:nil afterDelay:0];
}

- (void)onTimer{
	int index = arc4random()%3;
	imageAD.image = [adArray objectAtIndex:index];
	imageAD.tag = index;
}


- (void)viewDidUnload
{
	[self setTableView:nil];
	[self setLabelTitle:nil];
    [self setLabelMainTitle:nil];
    [self setImageAD:nil];
    [super viewDidUnload];

}

- (IBAction)actionBack:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return list.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
	}
	NSDictionary *dic = [list objectAtIndex:indexPath.row];

	cell.textLabel.text = [dic objectForKey:@"content"];
	cell.textLabel.backgroundColor =[UIColor clearColor];
	
	if ([dic objectForKey:@"type"]==@"master") {
		cell.contentView.backgroundColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"list_bar_yellow_title.png"]];
		//tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
		//	tableView.separatorColor=[UIColor colorWithRed:128 green:128 blue:128 alpha:255];
		cell.indentationLevel = 0;
	} else {
		cell.contentView.backgroundColor = [UIColor clearColor];
		cell.indentationLevel = 1;
	}

	return cell;
}

- (void)loadUnit:(NSDictionary *)dic
{
    NSArray *detailList;
	
    
	
	if (menu==5) {
		detailList = [UnivInfo unitInfoWithMenu:menu univ:univCode series03:[dic objectForKey:@"content"] series02:series02 series01:series01  type:typeString mf:mf];
	} else
		detailList = [UnivInfo unitInfoWithMenu:menu univ:univCode series03:[dic objectForKey:@"content"] series02:series02 series01:series01  type:type mf:mf];

		
    int order = [[dic objectForKey:@"order"] intValue];
    
    for (int i=1;i<=detailList.count;i++) {
        NSString *string =[[detailList objectAtIndex:i-1] objectForKey:@"content"];
        NSDictionary *dic1 = [NSDictionary dictionaryWithObjectsAndKeys:string, @"content",@"detail",@"type", nil];
        
        [list insertObject:dic1 atIndex:order+i];
    }
    NSLog(@"%@", list);
    [self.tableView reloadData];
	[indicator stopAnimating];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	

	NSDictionary *dic = [list objectAtIndex:indexPath.row];
	
	if ([[dic objectForKey:@"type"] isEqual:@"master"]) {
		series03 = [[list objectAtIndex:indexPath.row] objectForKey:@"content"];
		NSMutableArray *array = [NSMutableArray array];
		
		for (NSDictionary *dic in list){
			if (![[dic objectForKey:@"type"] isEqual:@"master"]) {
				[array addObject:dic];
			}
		}
		
		for (id object in array) {
			[list removeObject:object];
		}
		
		NSLog(@"selectedRow = %d indexPath.row = %d ", selectedRow, indexPath.row);
		if (selectedRow != indexPath.row) {
			[indicator startAnimating];
			//[self loadUnit:dic];
			[self performSelector:@selector(loadUnit:) withObject:dic afterDelay:0];
			selectedRow = indexPath.row;
		}else {
			selectedRow = -1;
			[tableView reloadData];
		}
	} else {
		switch (menu) {
			case 0:{
				TypeViewController *viewController = [[TypeViewController alloc] init];
				viewController.menu = menu;
				viewController.univCode = univCode;
				viewController.unit = [[list objectAtIndex:indexPath.row] objectForKey:@"content"];
				[self.navigationController pushViewController:viewController animated:YES];
				break;
			}
			case 1:{
				TypeDetailViewController *viewController = [[TypeDetailViewController alloc] init];
				viewController.type	= type;
				viewController.univCode = univCode;
				viewController.unit = [[list objectAtIndex:indexPath.row] objectForKey:@"content"];
				viewController.menu = menu;
				viewController.series01 = series01;
				viewController.series02 = series02;
				viewController.series03 = series03;
				[self.navigationController pushViewController:viewController animated:YES];
				break;
			}
			case 2:{
				AreaViewController *viewController = [[AreaViewController alloc] init];
				viewController.menu = menu;
				viewController.series03 = series03;
				viewController.series02 = series02;
				viewController.series01 = series01;
				viewController.unit = [[list objectAtIndex:indexPath.row] objectForKey:@"content"];
				[self.navigationController pushViewController:viewController animated:YES];
				break;
			}
			case 3:{
				StrategyViewController *viewController = [[StrategyViewController alloc] init];
				viewController.ID = ID;
				viewController.menu = menu;
				viewController.series03 = series03;
				viewController.series02 = series02;
				viewController.series01 = series01;
				
				viewController.unit = [[list objectAtIndex:indexPath.row] objectForKey:@"content"];
				[self.navigationController pushViewController:viewController animated:YES];

				break;
			}
			case 4:{
				UnivBViewController *viewController = [[UnivBViewController alloc] init];
				viewController.menu = menu;
				viewController.ID = ID;
				viewController.series03 = series03;
				viewController.series02 = series02;
				viewController.series01 = series01;
				viewController.mf = mf;
				viewController.unit = [[list objectAtIndex:indexPath.row] objectForKey:@"content"];
				viewController.type = type;
				[self.navigationController pushViewController:viewController animated:YES];
				break;
			}	
			case 5:{
//				[self performSelector:@selector(showIndicator) onThread:[NSThread mainThread] withObject:nil waitUntilDone:YES];
				StrategyViewController *viewController = [[StrategyViewController alloc] init];
				viewController.ID = ID;
				viewController.menu = menu;
				viewController.series03 = series03;
				viewController.series02 = series02;
				viewController.series01 = series01;
				
				viewController.unit = [[list objectAtIndex:indexPath.row] objectForKey:@"content"];
				[self.navigationController pushViewController:viewController animated:YES];
				break;
			}	

			default:
				break;
		}
		
	}
	
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	UITouch *touch = [touches anyObject];
	if (touch.view == imageAD ) {
		
		ADPop *adPop = [[ADPop alloc] initWithFrame:CGRectMake(0, 0, 320, 460)];
		[self.view addSubview:adPop];
		adPop.index = touch.view.tag;
	}
}


@end
