//
//  GradeMainViewController.h
//  Soosiro
//
//  Created by 이영록 on 12. 6. 20..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GradeMainViewController : UIViewController
@property (nonatomic) int menu;
@property (strong, nonatomic) IBOutlet UIButton *btnRecommend;
@property (strong, nonatomic) IBOutlet UIButton *btnStrategy;
@property (strong, nonatomic) NSString *ID;
@property (strong, nonatomic) IBOutlet UILabel *labelMainTitle;
- (IBAction)actionRecommend:(id)sender;
- (IBAction)actionStrategy:(id)sender;
- (IBAction)actionInput1:(id)sender;
- (IBAction)actionInput2:(id)sender;
- (IBAction)actionInput3:(id)sender;
- (IBAction)actionBack:(id)sender;


@end
