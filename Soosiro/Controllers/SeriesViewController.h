//
//  SeriesViewController.h
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 6. 16..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SeriesViewController : UIViewController{
	UITableView *_tableView;
}
@property (nonatomic) int menu;
@property (strong, nonatomic) NSString *univCode;
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSString *tclass;
@property (strong, nonatomic) NSArray *list;
@property (strong, nonatomic) NSString *ID;
@property (strong, nonatomic) NSString *mf;
@property (strong, nonatomic) IBOutlet UIImageView *tabImage01;
@property (strong, nonatomic) IBOutlet UIImageView *tabImage02;
@property (strong, nonatomic) IBOutlet UILabel *labelMainTitle;

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIImageView *imageAD;

- (IBAction)selectTab:(id)sender;
- (IBAction)touchTab:(id)sender;
- (IBAction)actionBack:(id)sender;

@end
