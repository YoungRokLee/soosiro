//
//  GradeMainViewController.m
//  Soosiro
//
//  Created by 이영록 on 12. 6. 20..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import "GradeMainViewController.h"
#import "RecommendTyepeViewController.h"
#import "StrategyViewController.h"
#import "Record.h"
@interface GradeMainViewController ()

@end

@implementation GradeMainViewController
@synthesize btnRecommend;
@synthesize btnStrategy;
@synthesize menu;
@synthesize ID;
@synthesize labelMainTitle;

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	if(!([Record chkGradeWithID:ID] && [Record chkRecordWithID:ID])) {
		[btnRecommend setEnabled:NO];
		[btnStrategy setEnabled:NO];
	} else {
		[btnRecommend setEnabled:YES];
		[btnStrategy setEnabled:YES];
	}

}

- (void)viewDidLoad
{
    [super viewDidLoad];
	labelMainTitle.text = [Title mainTilteWithMenu:menu];
		
}

- (void)viewDidUnload
{
	[self setBtnRecommend:nil];
    [self setBtnStrategy:nil];
    [self setLabelMainTitle:nil];
	[super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)actionRecommend:(id)sender {
	RecommendTyepeViewController *viewController = [[RecommendTyepeViewController alloc] init];
	viewController.ID = ID;
	viewController.menu = menu;
	[self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)actionStrategy:(id)sender {
	SeriesViewController *viewController = [[SeriesViewController alloc] init];
	viewController.ID = ID;
	viewController.menu = 5;
	[self.navigationController pushViewController:viewController animated:YES];
//	StrategyViewController *viewController = [[StrategyViewController alloc] init];
//	viewController.ID = ID;
//	[self.navigationController pushViewController:viewController animated:YES];
}
- (IBAction)actionInput1:(id)sender {
	YearViewController *viewController =[[YearViewController alloc] init];
	viewController.ID = ID;
	viewController.style = 0;
	viewController.menu = menu;
	
	[self.navigationController pushViewController:viewController animated:YES];	
}
- (IBAction)actionInput2:(id)sender {
	SpecialityViewController *viewController = [[SpecialityViewController alloc] init];
	viewController.ID = ID;
	[self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)actionInput3:(id)sender {
	YearViewController *viewController =[[YearViewController alloc] init];
	viewController.ID = ID;
	viewController.style = 1;
	viewController.menu = menu;

	[self.navigationController pushViewController:viewController animated:YES];
	
//	GradeViewController *viewController =[[GradeViewController alloc] init];
//	viewController.style = 1;
//	[self.navigationController pushViewController:viewController animated:YES];	
}

- (IBAction)actionBack:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
	
}
@end
