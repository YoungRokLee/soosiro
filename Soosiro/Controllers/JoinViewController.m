//
//  JoinViewController.m
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 6. 26..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import "JoinViewController.h"
#import "JoinDetailViewController.h"
#import "User.h"
#import "TouchXML.h"
#define UIColorFromRGB(rgbValue) [UIColor \
		colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
		green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
		blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


enum  {
	EUCKRStringEncoding = -2147481280,
	KSC5601StringEncoding = -2147481290,
};


@interface JoinViewController (){
	NSArray *areaList;
	UIAlertView *areaAlertView;
	UIPickerView *areaPickerView;

	NSArray *schoolList;
	NSArray *codeList;
	
	UIPickerView *schoolPickerView;
	UIAlertView *schoolAlertView;

	NSArray *yearList;
	UIPickerView *yearPickerView;
	UIAlertView *yearAlertView;

	NSString *sClass;
	NSString *sCode;
	NSString *lCode;
	
	NSArray *schList;
	UIPickerView *schPickerView;
	UIAlertView *schAlertView;
	
	BOOL isHidden;
	BOOL isIDCheck;
	int yCode;
	float version;
}
@property (nonatomic, retain) NSMutableData *receivedData;
@property (nonatomic, retain) NSURLResponse *response;
@property (nonatomic, retain) NSString *rdata;
@end

@implementation JoinViewController
@synthesize labelAreaContent;
@synthesize chkMale;
@synthesize chkFemale;
@synthesize scrollView;
@synthesize labelSType;
@synthesize chkLiberal;
@synthesize chkNatural;
@synthesize textPW1;
@synthesize labelYear;
@synthesize labelSchool;
@synthesize labelTitle;
@synthesize labelID;
@synthesize labelPW;
@synthesize labelName;
@synthesize labelGender;
@synthesize labelMale;
@synthesize labelFemaile;
@synthesize labelIdentity;
@synthesize labelArea;
@synthesize textID;
@synthesize textPW;
@synthesize textName;
@synthesize receivedData;
@synthesize response;
@synthesize rdata;

- (void)viewDidLoad
{
    [super viewDidLoad];
	isHidden = NO;
	//scrollView.frame = CGRectMake(0, -100, 320, 420);
	UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] init];
	tapGesture.delegate = self;
	tapGesture.numberOfTapsRequired = 1;
	[scrollView addGestureRecognizer:tapGesture];

	lCode  = [[NSMutableString alloc] init];
	sClass = [[NSMutableString alloc] init];
	areaList = [User localLists];
	NSLog(@"Local =%@", areaList);
	
	version = [[[UIDevice currentDevice] systemVersion] floatValue];
	
	if (version<5.0) 
		areaPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(-20,100,100,100)];
	else 
		areaPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(30,120,220,130)];
	
	areaPickerView.delegate  =self;
	areaPickerView.dataSource = self;
	areaPickerView.showsSelectionIndicator = YES;
	areaAlertView= [[UIAlertView alloc] initWithTitle:@"지 역 선 택" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"확인",@"취소",nil];
	areaAlertView.delegate = self;
	[areaAlertView addSubview:areaPickerView];
	[areaAlertView setTransform:CGAffineTransformMakeScale(1.0, 0.35)];

	

	
	schoolList = [User hClassLists];
	if (version<5.0) 
		schoolPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(-20,100,100,100)];
	else 
		schoolPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(30,120,220,130)];	
	
	schoolPickerView.delegate  =self;
	schoolPickerView.dataSource = self;
	schoolPickerView.showsSelectionIndicator = YES;
	
	schoolAlertView= [[UIAlertView alloc] initWithTitle:@"학 교 분 류" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"확인",@"취소",nil];
	[schoolAlertView setTransform:CGAffineTransformMakeScale(1.0, 0.35)];
	schoolAlertView.delegate = self;
	[schoolAlertView addSubview:schoolPickerView];
	
	yearList = [NSArray arrayWithObjects:@"1학년",@"2학년",@"3학년",@"재수생",@"n수생",nil];
	
	if (version<5.0) 
		yearPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(-20,100,100,100)];
	else 
		yearPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(30,120,220,130)];	
	
	yearPickerView.delegate  =self;
	yearPickerView.dataSource = self;
	yearPickerView.showsSelectionIndicator = YES;
	//[yearPickerView setTransform:CGAffineTransformMakeScale(0.7f, 0.8f)];
	
	yearAlertView= [[UIAlertView alloc] initWithTitle:@"학 년 선 택" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"확인",@"취소",nil];
	yearAlertView.delegate = self;
	[yearAlertView setTransform:CGAffineTransformMakeScale(1.0, 0.35)];	
	[yearAlertView addSubview:yearPickerView];
	
	if (version<5.0) 
		schPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(-20,100,100,100)];
	else 
		schPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(30,120,220,130)];
	
	schPickerView.delegate  =self;
	schPickerView.dataSource = self;
	schPickerView.showsSelectionIndicator = YES;
	
	schAlertView= [[UIAlertView alloc] initWithTitle:@"학 교 선 택" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"확인",@"취소",nil];
	[schAlertView setTransform:CGAffineTransformMakeScale(1.0, 0.35)];
	schAlertView.delegate = self;
	[schAlertView addSubview:schPickerView];

	[chkMale setHighlighted:YES];
	NSLog(@"FONTS: %@", [UIFont fontNamesForFamilyName:@"NanumGothicOTF"]);

//	labelTitle.font = [UIFont fontWithName:@"NanumGothicBoldOTFBold" size:22];
	labelTitle.textColor = [UIColor whiteColor];
//	labelBack.font =[UIFont fontWithName:@"NanumGothicOTF" size:15]; 
	labelTitle.textColor = [UIColor whiteColor];
//	labelOK.font = [UIFont fontWithName:@"NanumGothicOTF" size:15];
	labelTitle.textColor = [UIColor whiteColor];
	
//	labelID.font = [UIFont fontWithName:@"NanumGothicOTF" size:20]; 
	
	[chkLiberal setHighlighted:YES];

	[[NSNotificationCenter defaultCenter] addObserver:self 
											 selector:@selector(keyboardWillShow:) 
												 name:UIKeyboardWillShowNotification 
											   object:nil]; 
	
	[[NSNotificationCenter defaultCenter] addObserver:self 
											 selector:@selector(keyboardWillHide:) 
												 name:UIKeyboardWillHideNotification
											   object:nil];
}

- (void)keyboardWillShow:(NSNotification *)notif {
	if (isHidden) {
	
		[UIView beginAnimations:@"ViewAnimation" context:nil];
		[UIView setAnimationDuration:0.25];
		[scrollView setFrame:CGRectMake(0, -45, 320, 416)];
		
		[UIView commitAnimations];
	}
}

- (void)keyboardWillHide:(NSNotification *)notif {
	[UIView beginAnimations:@"ViewAnimation" context:nil];
    [UIView setAnimationDuration:0.25];
	[scrollView setFrame:CGRectMake(0, 44, 320, 416)];
	isHidden = NO;
    [UIView commitAnimations];
}

- (void)viewDidUnload
{
    [self setLabelID:nil];
    [self setLabelPW:nil];
    [self setLabelName:nil];
    [self setLabelGender:nil];
    [self setLabelMale:nil];
    [self setLabelFemaile:nil];
    [self setLabelIdentity:nil];
    [self setLabelArea:nil];
	[self setLabelTitle:nil];
	[self setTextID:nil];
	[self setTextPW:nil];
	[self setTextName:nil];
	[self setLabelIdentity:nil];
	[self setLabelAreaContent:nil];
	[self setChkMale:nil];
	[self setChkFemale:nil];
	[self setScrollView:nil];
	[self setLabelSType:nil];
	[self setChkLiberal:nil];
	[self setChkNatural:nil];
	[self setTextPW1:nil];
	[self setLabelYear:nil];
	[self setLabelSchool:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)idDidEnd:(id)sender {
	NSLog(@"check");
	
	NSMutableString *url=[NSMutableString stringWithFormat:@"http://www.sooseero.com/mobile/member/idCheck.mgo?id=%@", textID.text];
	
	[url stringByAddingPercentEscapesUsingEncoding:EUCKRStringEncoding];
	
	[self requestUrl:url];

}

- (IBAction)goBack:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)goNext:(id)sender {
	if ([textID.text isEqualToString:@""]) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"아이디오류" message:@"아이디가 잘못되었습니다." delegate:self cancelButtonTitle:@"확인" otherButtonTitles:nil]; 
		[alert show];
		return;
	}
	
	if ([textPW.text isEqualToString:@""]||![textPW.text isEqualToString:textPW1.text]) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"비밀번호오류" message:@"비밀번호가 잘못되었습니다." delegate:self cancelButtonTitle:@"확인" otherButtonTitles:nil]; 
		[alert show];
		return;
	}

	if ([textName.text isEqualToString:@""]) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"이름오류" message:@"이름을 입력하십시요." delegate:self cancelButtonTitle:@"확인" otherButtonTitles:nil]; 
		[alert show];
		return;
	}
	
	if ([labelAreaContent.text isEqualToString:@""]) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"지역선택" message:@"지역을 선택하십시요." delegate:self cancelButtonTitle:@"확인" otherButtonTitles:nil]; 
		[alert show];
		return;
	}
	
	if ([labelSchool.text isEqualToString:@""]) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"학교선택" message:@"학교을 선택하십시요." delegate:self cancelButtonTitle:@"확인" otherButtonTitles:nil]; 
		[alert show];
		return;
	}
	if ([labelSType.text isEqualToString:@""]) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"학교분류" message:@"학교분류를 선택하십시요." delegate:self cancelButtonTitle:@"확인" otherButtonTitles:nil]; 
		[alert show];
		return;
	}
	if ([labelYear.text isEqualToString:@""]) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"학년선택" message:@"학년을 선택하십시요." delegate:self cancelButtonTitle:@"확인" otherButtonTitles:nil]; 
		[alert show];
		return;
	}

	
	User *user = [[User alloc] init];
	user.P_ID = textID.text;
	user.P_Nm = textName.text;
	user.P_PWD = textPW.text;
	if (chkMale.highlighted) user.P_MF = @"M";
	else if (chkFemale.highlighted) user.P_MF = @"F";
	user.L_code = lCode;
	user.H_Nm = sCode;
	user.P_Syear = yCode;
	user.H_Class =sClass;
	if (chkLiberal.highlighted) user.P_Cate =@"문과";
	else if (chkNatural.highlighted) user.P_Cate =@"이과";
	
	[user add];
	
	JoinDetailViewController *viewController = [[JoinDetailViewController alloc] init];
	viewController.ID = textID.text;
	viewController.user = user;
	[self.navigationController pushViewController:viewController animated:YES];
	
}

- (IBAction)actionEndEditing:(id)sender {
	
}

#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	[textField resignFirstResponder];
	return YES;
}

#pragma mark - UIPickerViewDatasource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
	if (version<5.0) {
		pickerView.transform = CGAffineTransformIdentity;
		pickerView.transform = CGAffineTransformMakeScale(0.75, 0.75);
	}

	if([pickerView isEqual:areaPickerView])
	{
		return [areaList count];
	} else if([pickerView isEqual:schoolPickerView])
	{
		return [schoolList count];
	} else if([pickerView isEqual:yearPickerView])
	{
		return [yearList count];
	}else if([pickerView isEqual:schPickerView])
	{
		return [schList count];
	}else return 0;
	
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
	if([pickerView isEqual:areaPickerView])
	{
		return [[areaList objectAtIndex:row] objectForKey:@"L_NM"];
	} else if([pickerView isEqual:schoolPickerView])
	{
		return [[schoolList objectAtIndex:row] objectForKey:@"H_CLASS"];
	} else if([pickerView isEqual:yearPickerView])
	{
		return [yearList objectAtIndex:row];
	} else if([pickerView isEqual:schPickerView])
	{
		return [[schList objectAtIndex:row] objectForKey:@"H_NM"];
	} else return nil;
	
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component{
	if (version<5.0) return 280;
	else return 200;
}

#pragma mark UIAlertViewDelegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if([alertView isEqual:schoolAlertView]){
		int index = [schoolPickerView selectedRowInComponent:0];
		
		switch (buttonIndex) {
			case 0:
				labelSType.text = [NSString stringWithFormat:@"%@",[[schoolList objectAtIndex:index] objectForKey:@"H_CLASS"]];
				sClass = [[schoolList objectAtIndex:index] objectForKey:@"H_CLASSCODE"];
				break;
			case 1:
				break;
				
		}
	} else	if([alertView isEqual:areaAlertView])
	{
		int index = [areaPickerView selectedRowInComponent:0];
		
		switch(buttonIndex)
		{
			case 0:{
				labelAreaContent.text = [NSString stringWithFormat:@"%@",[[areaList objectAtIndex:index]  objectForKey:@"L_NM"]]; 
				lCode =[[areaList objectAtIndex:index]  objectForKey:@"L_CODE"];
				schList = [User schoolList:lCode];
				[schPickerView reloadAllComponents];
				break;
			}
			case 1:
				break;
		}
	} else	if([alertView isEqual:yearAlertView])
	{
		int index = [yearPickerView selectedRowInComponent:0];
		
		switch(buttonIndex)
		{
			case 0:{
				labelYear.text = [yearList objectAtIndex:index]; 
				yCode =index+1;
				break;
			}
			case 1:
				break;
		}
	} else	if([alertView isEqual:schAlertView])
	{
		int index = [schPickerView selectedRowInComponent:0];
		
		switch(buttonIndex)
		{
			case 0:{
				labelSchool.text = [[schList objectAtIndex:index] objectForKey:@"H_NM"];; 
				sCode =[[schList objectAtIndex:index] objectForKey:@"H_CODE"];
				break;
			}
			case 1:
				break;
		}
	}
	else {
		
	}
}

#pragma mark - UIGestureRecognizer Delegate 
- (BOOL) gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
	if (touch.view ==labelSchool) {
		if ([labelAreaContent.text isEqualToString:@""]) {
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"지역선택" message:@"지역을 먼저 선택하십시요." delegate:self cancelButtonTitle:@"확인" otherButtonTitles:nil]; 
			[alert show];
			return NO;
		}

		[schAlertView setTransform:CGAffineTransformIdentity];
		[schAlertView setTransform:CGAffineTransformMakeScale(1.0, 0.35)];
		[schAlertView show];
	}  else if (touch.view ==labelSType) {
		[schoolAlertView setTransform:CGAffineTransformIdentity];
		[schoolAlertView setTransform:CGAffineTransformMakeScale(1.0, 0.35)];
		[schoolAlertView show];
	}  else	if (touch.view == labelAreaContent) {
		[areaAlertView setTransform:CGAffineTransformIdentity];
		[areaAlertView setTransform:CGAffineTransformMakeScale(1.0, 0.35)];
		[areaAlertView show];
	} else if (touch.view == labelYear) {
		[yearAlertView setTransform:CGAffineTransformIdentity];
		[yearAlertView setTransform:CGAffineTransformMakeScale(1.0, 0.35)];
		[yearAlertView show];
	} else  if (touch.view ==chkMale) {
		[chkMale setHighlighted:YES];
		[chkFemale setHighlighted:NO];
	} else if (touch.view ==chkFemale) {
		[chkMale setHighlighted:NO];
		[chkFemale setHighlighted:YES];
	} else  if (touch.view ==chkLiberal) {
		[chkLiberal setHighlighted:YES];
		[chkNatural setHighlighted:NO];
	} else if (touch.view ==chkNatural) {
		[chkLiberal setHighlighted:NO];
		[chkNatural setHighlighted:YES];
	} else {
		
	}
	
	return YES;
}




- (BOOL)requestUrl:(NSString *)url {
	
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] 
														   cachePolicy:NSURLRequestUseProtocolCachePolicy 
													   timeoutInterval:30.0]; 
	[request setHTTPMethod:@"GET"];
	NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
	if (connection) { 
		self.receivedData = [NSMutableData data]; // 수신할 데이터를 받을 공간을 마련
		return YES;
	}
	return NO;
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)aResponse {
	[receivedData setLength:0];
	self.response = aResponse;
	
	
	
	NSArray *cookies;
    NSDictionary *responseHeaderFields;
	
    // 받은 header들을 dictionary형태로 받고
    responseHeaderFields = [(NSHTTPURLResponse *)aResponse allHeaderFields];
	
    if(responseHeaderFields != nil)
    {
        // responseHeaderFields에 포함되어 있는 항목들 출력
        for(NSString *key in responseHeaderFields)
        {
            NSLog(@"Header: %@ = %@", key, [responseHeaderFields objectForKey:key]);
        }
		
        // cookies에 포함되어 있는 항목들 출력
        cookies = [NSHTTPCookie cookiesWithResponseHeaderFields:responseHeaderFields forURL:[aResponse URL]];
		
        if(cookies != nil)
        {
            for(NSHTTPCookie *a in cookies)
            {
                NSLog(@"Cookie: %@ = %@", [a name], [a value]);
            }
        }
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	//NSString *returnString = [[NSString alloc] initWithData:data encoding:EUCKRStringEncoding];
	[receivedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
	
	rdata = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
	NSLog(@"%@", rdata);
	
	CXMLDocument *rssParser = [[CXMLDocument alloc] initWithXMLString:rdata options:0 error:nil];
	NSArray *xmlNodes  = [rssParser nodesForXPath:@"//idCheck" error:nil];
	
    for (CXMLElement *resultElement in xmlNodes) {
		
		isIDCheck = [[[[resultElement children] objectAtIndex:0] stringValue] boolValue];
		if (!isIDCheck) {
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"아이디오류" message:@"사용할수 없는 아이디입니다." delegate:self cancelButtonTitle:@"확인" otherButtonTitles:nil]; 
			[alert show];
			return;

		}
	}
	
//	xmlNodes  = [rssParser nodesForXPath:@"//result" error:nil];
//	
//    for (CXMLElement *resultElement in xmlNodes) {
//		
//		BOOL isSuccess = [[[[resultElement children] objectAtIndex:0] stringValue] boolValue];
//		if (!isSuccess) {
//			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"회원가입실패" message:@"회원가입에 실패하였습니다." delegate:self cancelButtonTitle:@"확인" otherButtonTitles:nil]; 
//			[alert show];
//			return;
//			
//		}
//	}
//
}



@end
