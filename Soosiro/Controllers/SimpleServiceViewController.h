//
//  SimpleServiceViewController.h
//  Soosiro
//
//  Created by 이영록 on 12. 8. 3..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SimpleServiceViewController : UIViewController
@property (strong, nonatomic) NSString *uname;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIImageView *imageAD;
@property (strong, nonatomic) IBOutlet UILabel *labelTitle;
- (IBAction)actionHome:(id)sender;
- (IBAction)actionBack:(id)sender;

@end
