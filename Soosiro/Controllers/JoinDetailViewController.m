//
//  JoinDetailViewController.m
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 6. 26..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.


#import "JoinDetailViewController.h"
#import "User.h"
#import "TouchXML.h"
#import <CommonCrypto/CommonDigest.h>
#import "NSDataAdditions.h"
enum  {
	EUCKRStringEncoding = -2147481280,
	KSC5601StringEncoding = -2147481290,
};

@interface JoinDetailViewController (){
	NSArray *schoolList;
	NSArray *codeList;;
	UIPickerView *schoolPickerView;
	UIAlertView *schoolAlertView;
	NSString *SClass;
}
@property (nonatomic, retain) NSMutableData *receivedData;
@property (nonatomic, retain) NSURLResponse *response;
@property (nonatomic, retain) NSString *rdata;
@end

@implementation JoinDetailViewController

@synthesize scrollView;

@synthesize ID;
@synthesize user;
@synthesize check1;
@synthesize check2;
@synthesize check3;
@synthesize check4;
@synthesize check5;
@synthesize check6;
@synthesize check7;
@synthesize check8;
@synthesize check9;
@synthesize check10;
@synthesize check11;
@synthesize check12;
@synthesize check13;
@synthesize check14;
@synthesize check15;
@synthesize check16;
@synthesize check17;
@synthesize check18;
@synthesize check19;
@synthesize check20;
@synthesize check21;
@synthesize check22;
@synthesize check23;
@synthesize check24;
@synthesize check25;
@synthesize check26;
@synthesize check27;
@synthesize check28;
@synthesize check29;
@synthesize check30;
@synthesize receivedData;
@synthesize response;
@synthesize rdata;
@synthesize pw;


- (void)viewDidLoad
{
    [super viewDidLoad];
	scrollView.contentSize = CGSizeMake(320,950);
	UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] init];
	tapGesture.delegate = self;
	tapGesture.numberOfTapsRequired = 1;
	[scrollView addGestureRecognizer:tapGesture];
	
	schoolList = [NSArray arrayWithObjects:@"과학고등학교", @"외국어고등학교", @"영재고등학교", @"대안학교", @"자율형공립고", @"자율형사립고", @"특성화고",@"일반고등학교",  @"전문계고", @"검정고시", nil];
	codeList = [NSArray arrayWithObjects:@"HA", @"HB", @"HC", @"HD", @"HE", @"HF",  @"HG", @"HI", @"HJ", @"HZ", nil];
	schoolPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(30,120,220,130)];
	schoolPickerView.delegate  =self;
	schoolPickerView.dataSource = self;
	schoolPickerView.showsSelectionIndicator = YES;
	
	schoolAlertView= [[UIAlertView alloc] initWithTitle:@"학 교 분 류지 역 선 택" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"확인",@"취소",nil];
	[schoolAlertView setTransform:CGAffineTransformMakeScale(1.0, 0.35)];
	schoolAlertView.delegate = self;
	[schoolAlertView addSubview:schoolPickerView];

	
	for (UIView *view in [scrollView subviews]) {
		if (view.tag>0) {
			if ([view isKindOfClass:[UIImageView class]]) {
				[view setFrame:CGRectMake(20, (view.tag)*30, 24, 24)];
			} else if ([view isKindOfClass:[UILabel class]]) {
				[view setFrame:CGRectMake(50, (view.tag)*30, 250, 24)];
			} 
			
		}		
	}
}


- (void)viewDidUnload
{
	[self setScrollView:nil];
	[self setCheck1:nil];
	[self setCheck2:nil];
	[self setCheck3:nil];
	[self setCheck4:nil];
	[self setCheck4:nil];
	[self setCheck5:nil];
	[self setCheck6:nil];
	[self setCheck7:nil];
	[self setCheck8:nil];
	[self setCheck9:nil];
	[self setCheck10:nil];
	[self setCheck11:nil];
	[self setCheck12:nil];
	[self setCheck13:nil];
	[self setCheck14:nil];
	[self setCheck15:nil];
	[self setCheck16:nil];
	[self setCheck17:nil];
	[self setCheck18:nil];
	[self setCheck19:nil];
	[self setCheck20:nil];
	[self setCheck21:nil];
	[self setCheck22:nil];
	[self setCheck23:nil];
	[self setCheck24:nil];
	[self setCheck25:nil];
	[self setCheck26:nil];
	[self setCheck27:nil];
	[self setCheck28:nil];
	[self setCheck29:nil];
	[self setCheck30:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
#pragma mark - UIPickerViewDatasource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	return 1;
}

- (NSString*)encode:(NSString*)val {
	unsigned char md[16];
    const char *jjj =[val UTF8String ];
    CC_MD5(jjj, val.length, md);
    
	return [[NSData dataWithBytes:md length:16] base64Encoding];
}

-(NSString*) md5:(NSString*)srcStr
{
	const char *cStr = [srcStr UTF8String];
	unsigned char result[CC_MD5_DIGEST_LENGTH];
	CC_MD5(cStr, strlen(cStr), result);
	return [NSString stringWithFormat:
			@"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
			result[0],result[1],result[2],result[3],result[4],result[5],result[6],result[7],result[8],result[9],result[10],result[11],result[12],result[13],result[14],result[15]];
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
	return schoolList.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
	return [schoolList objectAtIndex:row];
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component{
	return 200;
}

#pragma mark UIAlertViewDelegate




#pragma mark - UIGestureRecognizer Delegate 
- (BOOL) gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
	if ([touch.view isKindOfClass:[UIImageView class]]) {
	
		UIImageView *imageView = (UIImageView *)[touch view];
		[imageView setHighlighted:![imageView isHighlighted]];
		
		if (touch.view  == check18 ) {
			check19.highlighted = !check18.highlighted;
		} else if (touch.view  == check19 ) {
			check18.highlighted = !check19.highlighted;
		} else if (touch.view  == check20 ) {
			check21.highlighted = !check20.highlighted;
		} else if (touch.view  == check21 ) {
			check20.highlighted = !check21.highlighted;
		}
	}
	
	return YES;
}

- (IBAction)goBack:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)actionDone:(id)sender {
	NSMutableArray *array = [NSMutableArray array];
	if(check1.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//입학사정관 포트폴리오
	if(check2.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//공인외국어자격
	if(check3.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//수학과학경력
	if(check4.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//농어촌지역
	if(check5.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//임원경력여부
	
	if(check6.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//종교추천여부
	if(check7.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//학교추천여부
	if(check8.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//특성화고졸재직자
	if(check9.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//장애인여부
	if(check10.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//서해5도
	
	if(check11.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//기초생활수급및차상위
	if(check12.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//다문화가정
	if(check13.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//장애인자녀
	if(check14.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//소년소녀가장
	if(check15.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//새터민
	
	if(check16.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//독립유공자
	if(check17.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//국가유공자
	if(check18.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//민주화유공자
	if(check19.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//5.18민주화유공자
	if(check20.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//고엽제후유증
	
	if(check21.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//특수임무수행자
	if(check22.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//군자녀
	if(check23.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//경찰자녀
	if(check24.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//소방자녀
	if(check25.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//환경미화자녀
	
	if(check26.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//6.18자유상이자
	if(check27.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//지원공상자녀
	if(check28.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//교정공무원
	if(check29.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//다자녀
	if(check30.highlighted) [array addObject:@"Y"]; else [array addObject:@"N"];//백일장수상경력

	user.info = array;
	[user addInfo];
	
	NSMutableString *info = [NSMutableString string];
	for (int i=0; i<array.count; i++) {
		if ([[array objectAtIndex:i] isEqualToString:@"Y"]) {
			[info appendString:@"1"];
		} else {
			[info appendString:@"0"];
		}
	}
	
		
	
	
	NSMutableString *url=[NSMutableString stringWithFormat:@"http://www.sooseero.com/mobile/member/join.mgo?id=%@&pw=%@&pNm=%@&pMf=%@&lCode=%@&hCode=%@&hClass=%@&pCate=%@&pSyear=%d&info=%@", user.P_ID, [self encode:user.P_PWD], user.P_Nm, user.P_MF, user.L_code, user.H_Nm, user.H_Class, user.P_Cate, user.P_Syear, info ];
	
	//NSString *url1=	[url stringByAddingPercentEscapesUsingEncoding:EUCKRStringEncoding];
	NSString *url1=	[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	
	NSLog(@"%@", url1);
	[self requestUrl:url1];
	
	[self.navigationController popToRootViewControllerAnimated:YES];
}



- (BOOL)requestUrl:(NSString *)url {
	
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] 
														   cachePolicy:NSURLRequestUseProtocolCachePolicy 
													   timeoutInterval:30.0]; 
	[request setHTTPMethod:@"GET"];
	NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
	if (connection) { 
		self.receivedData = [NSMutableData data]; // 수신할 데이터를 받을 공간을 마련
		return YES;
	}
	return NO;
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)aResponse {
	[receivedData setLength:0];
	self.response = aResponse;
	
	
	
	NSArray *cookies;
    NSDictionary *responseHeaderFields;
	
    // 받은 header들을 dictionary형태로 받고
    responseHeaderFields = [(NSHTTPURLResponse *)aResponse allHeaderFields];
	
    if(responseHeaderFields != nil)
    {
        // responseHeaderFields에 포함되어 있는 항목들 출력
        for(NSString *key in responseHeaderFields)
        {
            NSLog(@"Header: %@ = %@", key, [responseHeaderFields objectForKey:key]);
        }
		
        // cookies에 포함되어 있는 항목들 출력
        cookies = [NSHTTPCookie cookiesWithResponseHeaderFields:responseHeaderFields forURL:[aResponse URL]];
		
        if(cookies != nil)
        {
            for(NSHTTPCookie *a in cookies)
            {
                NSLog(@"Cookie: %@ = %@", [a name], [a value]);
            }
        }
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	//NSString *returnString = [[NSString alloc] initWithData:data encoding:EUCKRStringEncoding];
	[receivedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
	
	rdata = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
	NSLog(@"%@", rdata);
	
	CXMLDocument *rssParser = [[CXMLDocument alloc] initWithXMLString:rdata options:0 error:nil];
	NSArray *xmlNodes  = [rssParser nodesForXPath:@"//result" error:nil];
		
   for (CXMLElement *resultElement in xmlNodes) {
		
	   int result = [[[[resultElement children] objectAtIndex:0] stringValue] intValue];
	  
	   switch (result) {
		   case 0:{
			   UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"회원가입실패" message:@"회원가입에 실패하였습니다." delegate:self cancelButtonTitle:@"확인" otherButtonTitles:nil]; 
			   [alert show];
			   break;
		   }
		   case 1:{
			   UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"회원가입성공" message:@"회원가입에 성공하였습니다." delegate:self cancelButtonTitle:@"확인" otherButtonTitles:nil]; 
			   [alert show];
			   break;			   }
		   case 2:{
			   UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"회원가입실패" message:@"아이디가 중복되었습니다." delegate:self cancelButtonTitle:@"확인" otherButtonTitles:nil]; 
			   [alert show];
			   break;			   }

		   default:
			   break;
	   }
	}
}


@end
