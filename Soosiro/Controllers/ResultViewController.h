//
//  ResultViewController.h
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 7. 20..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResultViewController : UIViewController
@property (strong, nonatomic) NSString *tCode;
@property (strong, nonatomic) NSString *uCode;
@property (strong, nonatomic) NSString *univ;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UILabel *barTitle;
@property (strong, nonatomic) IBOutlet UIImageView *imageAD;
- (IBAction)actionBack:(id)Sender;
@end
