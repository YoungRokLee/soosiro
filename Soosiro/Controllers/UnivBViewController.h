//
//  UnivViewControllerViewController.h
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 6. 16..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UnivBViewController : UIViewController{
	UITableView *_tableView;
}
@property (nonatomic) int menu;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *list;
@property (nonatomic) int area;
@property (strong, nonatomic) NSString *ID;
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSString *tclass;
@property (strong, nonatomic) NSString *unit;
@property (strong, nonatomic) NSString *series03;
@property (strong, nonatomic) NSString *series02;
@property (strong, nonatomic) NSString *series01;

@property (strong, nonatomic) NSString *mf;

@property (strong, nonatomic) IBOutlet UIImageView *imageAD;
@property (strong, nonatomic) IBOutlet UILabel *labelMainTitle;
- (IBAction)actionBack:(id)sender;
//- (IBAction)actionEndEditing:(id)sender;

@end
