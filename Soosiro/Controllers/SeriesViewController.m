//
//  SeriesViewController.m
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 6. 16..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import "SeriesViewController.h"
#import "UnivInfo.h"
#import "UnitViewController.h"
#import "User.h"
#import "Title.h"
#import "Proviso.h"

@interface SeriesViewController (){
	NSString *series;
	UIActivityIndicatorView *indicator;
	NSMutableString *typeString;
	NSArray *adArray;
	NSTimer *timer;
}
@property (strong, nonatomic) NSArray *tabList;
@end

@implementation SeriesViewController
@synthesize tabImage01;
@synthesize tabImage02;
@synthesize labelMainTitle;
@synthesize tabList;
@synthesize univCode;
@synthesize tableView=_tableView;
@synthesize imageAD;
@synthesize list;
@synthesize menu;
@synthesize type;
@synthesize ID;
@synthesize mf;
@synthesize tclass;

- (void)visibleImage:(int)index {
	for (UIImageView *imageView in tabList) {
		[imageView setHidden:YES];
	}
	UIImageView *imageView = [tabList objectAtIndex:index];
	[imageView setHidden:NO];
}

- (void)initView
{
	typeString=[NSMutableString string];
	if (menu==5) {
		NSArray *types = [Proviso arrayOfPossibleType:ID];
		for (int i=0; i<types.count; i++) {
			if (i==types.count-1) [typeString appendFormat:@"'%@'",[types objectAtIndex:i]];
			else [typeString appendFormat:@"'%@',",[types objectAtIndex:i]];
		}
		list = [UnivInfo series02InfoWithMenu:menu univ:univCode seriese01:series type:typeString tclass:tclass mf:mf];
	} else {
		list = [UnivInfo series02InfoWithMenu:menu univ:univCode seriese01:series type:type tclass:tclass mf:mf];
	}
	NSLog(@"%@", list);
	[self.tableView reloadData];
	[indicator stopAnimating];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self.tableView setBackgroundColor:[UIColor clearColor]];
	UIImage *image1 = [UIImage imageNamed:@"iphone_ad_1.png"];
	UIImage *image2 = [UIImage imageNamed:@"iphone_ad_2.png"];
	UIImage *image3 = [UIImage imageNamed:@"iphone_ad_3.png"];
	adArray = [NSArray arrayWithObjects:image1, image2, image3, nil];
	
	timer= [NSTimer scheduledTimerWithTimeInterval:15 target:self selector:@selector(onTimer) userInfo:nil repeats:YES];
	
	[self onTimer];

	
	labelMainTitle.text = [Title mainTilteWithMenu:menu];
	mf = [User userMF:ID];
    tabList = [NSArray arrayWithObjects:tabImage01, tabImage02, nil];
	[self visibleImage:0];
	[self getSeries:0];
	
	indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 64.0f, 64.0f)];
	[indicator setCenter:self.view.center];
    [indicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
    [self.view addSubview : indicator];	
	
	indicator.hidden= FALSE;
	[indicator startAnimating];
	
	[self performSelector:@selector(initView) withObject:nil afterDelay:0];
}

- (void)onTimer{
	int index = arc4random()%3;
	imageAD.image = [adArray objectAtIndex:index];
	imageAD.tag = index;
}

- (void)viewDidUnload
{
	[self setTabImage01:nil];
	[self setTabImage02:nil];
	
	[self setUnivCode:nil];
	[self setTableView:nil];
	[self setLabelMainTitle:nil];
	[self setImageAD:nil];
    [super viewDidUnload];
}

- (void)getSeries:(int)index {
    
	switch (index) {
		case 0:{
			series = @"인문계";
			break;
		}
		case 1:{
			series = @"자연계";
			break;
		}
		default:
			break;
	}
	
}

- (void)loadSer {
	if (menu==5) {
		list = [UnivInfo series02InfoWithMenu:menu univ:univCode seriese01:series type:typeString tclass:tclass mf:mf];
	} else {
		list = [UnivInfo series02InfoWithMenu:menu univ:univCode seriese01:series type:type tclass:tclass mf:mf];
	}
	[self.tableView reloadData];
	[indicator stopAnimating];
}

- (IBAction)selectTab:(id)sender {
	UIButton *btn = (UIButton *)sender;
	int index = btn.tag;
	[self getSeries:index];
	[indicator startAnimating];
	[self performSelector:@selector(loadSer) withObject:nil afterDelay:0];
	
}

- (IBAction)touchTab:(id)sender {
	UIButton *btn = (UIButton *)sender;
	int index = btn.tag;
	[self visibleImage:index];
}

- (IBAction)actionBack:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return list.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
	}
	cell.textLabel.text = [list objectAtIndex:indexPath.row];
	cell.textLabel.backgroundColor =[UIColor clearColor];
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	
	UnitViewController *viewController = [[UnitViewController alloc] init];
	viewController.mf = mf;
	viewController.ID = ID;
	viewController.menu = menu;
	viewController.univCode =  univCode;
	viewController.type = type;
	viewController.typeString = typeString;
	viewController.tclass = tclass;
	viewController.series01 = series;
	viewController.series02 = [list objectAtIndex:indexPath.row];
	[self.navigationController pushViewController:viewController animated:YES];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	UITouch *touch = [touches anyObject];
	if (touch.view == imageAD ) {
		
		ADPop *adPop = [[ADPop alloc] initWithFrame:CGRectMake(0, 0, 320, 460)];
		[self.view addSubview:adPop];
		adPop.index = touch.view.tag;
	}
}


@end
