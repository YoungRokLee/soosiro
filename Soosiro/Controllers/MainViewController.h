//
//  MainViewController.h
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 6. 7..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController<UIAlertViewDelegate>{
	NSArray *imageViewList;
}
@property (nonatomic) BOOL isPremium;
@property (strong, nonatomic) IBOutlet UIImageView *sel01ImageView;
@property (strong, nonatomic) IBOutlet UIImageView *sel02ImageView;
@property (strong, nonatomic) IBOutlet UIImageView *sel03ImageView;
@property (strong, nonatomic) IBOutlet UIImageView *sel04ImageView;
@property (strong, nonatomic) IBOutlet UIImageView *sel05ImageView;
@property (strong, nonatomic) NSString *ID;
- (IBAction)touchInButton:(id)sender;
- (IBAction)menuSelected:(id)sender;
- (IBAction)cancelSelect:(id)sender;

@end
