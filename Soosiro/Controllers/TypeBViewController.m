//
//  TypeBViewController.m
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 7. 23..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import "TypeBViewController.h"
#import "UnivInfo.h"

@interface TypeBViewController (){
	NSArray *list;
	UIActivityIndicatorView *indicator;
	NSArray *adArray;
	NSTimer *timer;
}

@end

@implementation TypeBViewController
@synthesize univ;
@synthesize series01;
@synthesize tableView=_tableView;
@synthesize imageAD;
@synthesize series02;
@synthesize series03;
@synthesize univCode;
@synthesize unit;
@synthesize menu;
@synthesize labelMainTitle;

- (void)initView
{
    if (menu==0) list = [UnivInfo tuNameWithUniv:univCode ];
	else list = [UnivInfo tuNameWithUniv:univCode series03:series03 series02:series02 series01:series01];
	[self.tableView reloadData];
	[indicator stopAnimating];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	UIImage *image1 = [UIImage imageNamed:@"iphone_ad_1.png"];
	UIImage *image2 = [UIImage imageNamed:@"iphone_ad_2.png"];
	UIImage *image3 = [UIImage imageNamed:@"iphone_ad_3.png"];
	adArray = [NSArray arrayWithObjects:image1, image2, image3, nil];
	
	timer= [NSTimer scheduledTimerWithTimeInterval:15 target:self selector:@selector(onTimer) userInfo:nil repeats:YES];
	
	[self onTimer];

	labelMainTitle.text = [Title mainTilteWithMenu:menu];
	indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 64.0f, 64.0f)];
	[indicator setCenter:self.view.center];
    [indicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
    [self.view addSubview : indicator];	
	
	indicator.hidden= FALSE;
	[indicator startAnimating];
	
	[self performSelector:@selector(initView) withObject:nil afterDelay:0];
}

- (void)onTimer{
	int index = arc4random()%3;
	imageAD.image = [adArray objectAtIndex:index];
	imageAD.tag = index;
}

- (void)viewDidUnload
{
	[self setTableView:nil];
    [self setLabelMainTitle:nil];
    [self setImageAD:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [list count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
	}
	NSDictionary *dic =[list objectAtIndex:indexPath.row]; 
	cell.textLabel.text =[dic objectForKey:@"TU_NM"];
	
	cell.textLabel.backgroundColor =[UIColor clearColor];
	//	cell.detailTextLabel.text =[NSString stringWithFormat:@"%f", [[dic objectForKey:@"U_JUST"] doubleValue] ];
	
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	
	NSDictionary *dic = [list objectAtIndex:indexPath.row];
	TypeDetailViewController *viewController = [[TypeDetailViewController alloc] init];
	viewController.univCode = univCode;
	viewController.unit = unit;
	viewController.menu = menu;
	viewController.series03=series03;
	viewController.series02=series02;
	viewController.series01=series01;

	//viewController.type = [dic objectForKey:@"TU_NM"];
	
	if (menu==2||menu==0) 
		viewController.type  = [dic objectForKey:@"TU_NM"];
	else viewController.type = [dic objectForKey:@"T_NM"];
	
	[self.navigationController pushViewController:viewController animated:YES];
}


- (IBAction)actionBack:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	UITouch *touch = [touches anyObject];
	if (touch.view == imageAD ) {
		
		ADPop *adPop = [[ADPop alloc] initWithFrame:CGRectMake(0, 0, 320, 460)];
		[self.view addSubview:adPop];
		adPop.index = touch.view.tag;
	}
}

@end
