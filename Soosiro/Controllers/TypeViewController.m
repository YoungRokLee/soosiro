//
//  TypeViewController.m
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 6. 18..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import "TypeViewController.h"
#import "UnivInfo.h"
#import "ResultViewController.h"

@interface TypeViewController (){
	int selectedRow;
	NSMutableArray *list;
	UIActivityIndicatorView *indicator;
	NSArray *adArray;
	NSTimer *timer;
	NSString *tclass;
}
@end

@implementation TypeViewController
@synthesize tableView=_tableView;
@synthesize univCode;
@synthesize unit;
@synthesize menu;
@synthesize univ;
@synthesize series03;
@synthesize series02;
@synthesize series01;
@synthesize labelMainTitle;
@synthesize imageAD;

- (void)initView
{
    switch (menu) {
			
		case 0:
			list = [UnivInfo tclassInfoWithMenu:menu univ:univCode series03:@"" series02:@"" series01:@""];
			break;
		case 1:
			list = [UnivInfo tclassInfoWithMenu:menu univ:@"" series03:@"" series02:@"" series01:@""];
			break;	
		case 2:
			list = [UnivInfo tclassInfoWithMenu:menu univ:univCode series03:series03 series02:series02 series01:series01];
			break;	
		default:
			break;
	}
	
	[self.tableView reloadData];
	[indicator stopAnimating];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	UIImage *image1 = [UIImage imageNamed:@"iphone_ad_1.png"];
	UIImage *image2 = [UIImage imageNamed:@"iphone_ad_2.png"];
	UIImage *image3 = [UIImage imageNamed:@"iphone_ad_3.png"];
	adArray = [NSArray arrayWithObjects:image1, image2, image3, nil];
	
	timer= [NSTimer scheduledTimerWithTimeInterval:15 target:self selector:@selector(onTimer) userInfo:nil repeats:YES];
	
	[self onTimer];

	
	labelMainTitle.text =[Title mainTilteWithMenu:menu];
	[self.tableView setBackgroundColor:[UIColor clearColor]];
	selectedRow=-1;
	indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 64.0f, 64.0f)];
	[indicator setCenter:self.view.center];
    [indicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
	[self.view addSubview : indicator];	
	[indicator startAnimating];
	
	[self performSelector:@selector(initView) withObject:nil afterDelay:0];

}

- (void)onTimer{
	int index = arc4random()%3;
	imageAD.image = [adArray objectAtIndex:index];
	imageAD.tag = index;
}

- (void)viewDidUnload
{
    [self setTableView:nil];
    [self setLabelMainTitle:nil];
    [self setImageAD:nil];
	[super viewDidUnload];
	
}

- (IBAction)actionBack:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return list.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
	}
	NSDictionary *dic = [list objectAtIndex:indexPath.row];
	
	cell.textLabel.text = [dic objectForKey:@"content"];
	cell.textLabel.backgroundColor =[UIColor clearColor];
	
	if ([dic objectForKey:@"type"]==@"master") {
		cell.contentView.backgroundColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"list_bar_yellow_title.png"]];
		
		cell.indentationLevel = 0;
	} else {
		cell.contentView.backgroundColor = [UIColor clearColor];
		cell.indentationLevel = 1;
	}
	
	return cell;
}

- (void)loadType:(NSDictionary *)dic
{
    NSArray *detailList;
    tclass = [dic objectForKey:@"content"];
    detailList= [UnivInfo tnameInfoWithMenu:menu univ:univCode tclass:tclass];
    
    int order = [[dic objectForKey:@"order"] intValue];
    
    for (int i=1;i<=detailList.count;i++) {
        NSString *string =[[detailList objectAtIndex:i-1] objectForKey:@"content"];
        NSString *tcode =[[detailList objectAtIndex:i-1] objectForKey:@"tcode"];
        NSDictionary *dic1 = [NSDictionary dictionaryWithObjectsAndKeys:string, @"content",tcode, @"tcode",@"detail",@"type", nil];
        
        [list insertObject:dic1 atIndex:order+i];
    }
    NSLog(@"%@", list);
	[self.tableView reloadData];
	[indicator stopAnimating];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	
	NSDictionary *dic = [list objectAtIndex:indexPath.row];
	if ([[dic objectForKey:@"type"] isEqual:@"master"]) {
		tclass = [[list objectAtIndex:indexPath.row] objectForKey:@"content"];
		NSMutableArray *array = [NSMutableArray array];
		
		for (NSDictionary *dic in list){
			if (![[dic objectForKey:@"type"] isEqual:@"master"]) {
				[array addObject:dic];
			}
		}
		
		for (id object in array) {
			[list removeObject:object];
		}
		
		
		if (selectedRow != indexPath.row) {
			
			[indicator startAnimating];
			
			[self performSelector:@selector(loadType:) withObject:dic afterDelay:0];
	
			
		//	[self loadType:dic];
			
			selectedRow = indexPath.row;
		}else {
			selectedRow = -1;
			[tableView reloadData];
		}
	} else {
		switch (menu) {
			case 0:{
				TypeDetailViewController *viewController = [[TypeDetailViewController alloc] init];
				viewController.menu = menu;
				viewController.univCode = univCode;
				viewController.unit = unit;
				viewController.type = [dic objectForKey:@"content"];
				[self.navigationController pushViewController:viewController animated:YES];

				
//				ResultViewController *viewController = [[ResultViewController alloc] init];
//				viewController.uCode = univCode;
//				viewController.univ = univ;
//				viewController.tCode = [dic objectForKey:@"tcode"];
//				NSLog (@"dic = %@",  [dic objectForKey:@"tcode"]);
//				[self.navigationController pushViewController:viewController animated:YES];
				break;
			}
			case 1:{
				TypeDesViewController *viewController =[[TypeDesViewController alloc] init];
				viewController.menu = menu;
				viewController.tclass = tclass;
				viewController.type =[dic objectForKey:@"content"];
				
				
				[self.navigationController pushViewController:viewController animated:YES];
				
//				AreaViewController *viewController =[[AreaViewController alloc] init];
//				viewController.menu = menu;
//				viewController.type = [dic objectForKey:@"content"];
//				[self.navigationController pushViewController:viewController animated:YES];
				break;
			}
			default:{
				TypeDetailViewController *viewController = [[TypeDetailViewController alloc] init];
				viewController.univCode = univCode;
				viewController.unit = unit;
				viewController.series01 = series01;
				viewController.series02 = series02;
				viewController.series03 = series03;
				viewController.menu = menu;
				viewController.type = [dic objectForKey:@"content"];
				[self.navigationController pushViewController:viewController animated:YES];
				break;
			}
			
		}
	}
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	UITouch *touch = [touches anyObject];
	if (touch.view == imageAD ) {
		
		ADPop *adPop = [[ADPop alloc] initWithFrame:CGRectMake(0, 0, 320, 460)];
		[self.view addSubview:adPop];
		adPop.index = touch.view.tag;
	}
}

@end
