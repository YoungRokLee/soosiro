//
//  AreaViewController.h
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 6. 7..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AreaViewController : UIViewController
@property (nonatomic) int menu;
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSString *tclass;
@property (strong, nonatomic) NSString *unit;
@property (strong, nonatomic) NSMutableArray *imageViewList;
@property (strong, nonatomic) IBOutlet UIImageView *map01ImageView;
@property (strong, nonatomic) IBOutlet UIImageView *map02ImageView;
@property (strong, nonatomic) IBOutlet UIImageView *map03ImageView;
@property (strong, nonatomic) IBOutlet UIImageView *map04ImageView;
@property (strong, nonatomic) IBOutlet UIImageView *map05ImageView;
@property (strong, nonatomic) IBOutlet UIImageView *map06ImageView;
@property (strong, nonatomic) IBOutlet UILabel *labelBack;
@property (strong, nonatomic) NSString *series03;
@property (strong, nonatomic) NSString *series02;
@property (strong, nonatomic) NSString *series01;
@property (strong, nonatomic) IBOutlet UILabel *labelMainTitle;

- (IBAction)touchInArea:(id)sender;
- (IBAction)areaSelected:(id)sender;
- (IBAction)back:(id)sender;
@end
