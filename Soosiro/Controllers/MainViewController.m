//
//  MainViewController.m
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 6. 7..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import "MainViewController.h"

@interface MainViewController (){
	UIAlertView *alertPremium;
}
@end

@implementation MainViewController
@synthesize sel01ImageView;
@synthesize sel02ImageView;
@synthesize sel03ImageView;
@synthesize sel04ImageView;
@synthesize sel05ImageView;
@synthesize ID;
@synthesize isPremium;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    imageViewList = [NSArray arrayWithObjects:sel01ImageView, sel02ImageView, sel03ImageView,sel04ImageView,sel05ImageView, nil];
	[self hiddenAll];
}

- (void)viewDidUnload
{
    [self setSel01ImageView:nil];
    [self setSel02ImageView:nil];
    [self setSel03ImageView:nil];
    [self setSel04ImageView:nil];
    [self setSel05ImageView:nil];
    [super viewDidUnload];
}

- (void)hiddenAll {
	for (id imageView in imageViewList) {
		[imageView setHidden:YES];
	}

}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)touchInButton:(id)sender {
	[self hiddenAll];
	UIButton *button = (UIButton*)sender;
	int index = button.tag;
	UIImageView *imageView = [imageViewList objectAtIndex:index];
	[imageView setHidden:NO];
}

- (IBAction)menuSelected:(id)sender {
	[self hiddenAll];
	UIButton *button = (UIButton*)sender;
	int index = button.tag;
	switch (index) {
		case 0:{
			AreaViewController *viewController =[[AreaViewController alloc] init];
			viewController.menu = index;
			[self.navigationController pushViewController:viewController animated:YES];
			break;
		}
		case 1:{
			TypeViewController *viewController =[[TypeViewController alloc] init];
			viewController.menu = index;
			[self.navigationController pushViewController:viewController animated:YES];
			break;
		}	
		case 2:{
			SeriesViewController *viewController =[[SeriesViewController alloc] init];
			viewController.menu = index;
			[self.navigationController pushViewController:viewController animated:YES];
			break;
		}		
		case 3:{
			GradeViewController *viewController =[[GradeViewController alloc] init];
			viewController.menu = index;
			viewController.iD = ID;
			viewController.style = 0;
			[self.navigationController pushViewController:viewController animated:YES];
			break;
		}
		case 4:{
			if (!isPremium) {
				 alertPremium = [[UIAlertView alloc] initWithTitle:@"유료회원전용메뉴" message:@"유료회원으로 전환하시겠습니까?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes",nil];
				[alertPremium show];
				return;
			}
			GradeMainViewController *viewController =[[GradeMainViewController alloc] init];
			viewController.menu = index;
			viewController.ID = ID;
			[self.navigationController pushViewController:viewController animated:YES];
			break;
		}

		default:
			break;
	}
	
}

- (IBAction)cancelSelect:(id)sender {
	[self hiddenAll];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if([alertView isEqual:alertPremium]){
		switch (buttonIndex) {
			case 0:
				break;
			case 1:{
				NSURL *sooseero = [[NSURL alloc] initWithString: @"http://www.sooseero.com"];
				[[UIApplication sharedApplication] openURL:sooseero];
				break;
			}
				
		}
	} 
}

@end
