//
//  GradeViewController.m
//  Soosiro
//
//  Created by 이영록 on 12. 6. 20..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import "GradeViewController.h"
#import "Record.h"
#import "Test.h"
#import "Title.h"
#import "TouchXML.h"
#import "User.h"

@interface GradeViewController (){
	BOOL isNeedSave;
	NSArray *slist;
	NSString *smallTitle;
	NSString *bterm;
	
	Record *record;
	NSArray *adArray;
	NSTimer *timer;
	int syear;
	int range;
	int brange;
}
@property (strong, nonatomic) NSArray *list;
@property (nonatomic) NSIndexPath *selectedIndex;
@property (nonatomic, retain) NSMutableData *receivedData;
@property (nonatomic, retain) NSURLResponse *response;
@property (nonatomic, retain) NSString *rdata;
@end

@implementation GradeViewController
@synthesize labelAlert;
@synthesize labelInfo;
@synthesize tableView=_tableView;
@synthesize viewBar;
@synthesize viewInfo;
@synthesize textGrade;
@synthesize btnDone;
@synthesize labelArea;
@synthesize labelMainTitle;
@synthesize imageAD;
@synthesize list;
@synthesize menu;
@synthesize style;
@synthesize selectedIndex;
@synthesize seq;
@synthesize labelTitle;
@synthesize term;
@synthesize ID;
@synthesize mf;
@synthesize receivedData;
@synthesize response;
@synthesize rdata;

- (void)setMenu:(int)val{
	menu = val;
}

- (void)onTimer{
	int index = arc4random()%3;
	imageAD.image = [adArray objectAtIndex:index];
	imageAD.tag = index;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	labelMainTitle.text = [Title mainTilteWithMenu:menu];
	UIImage *image1 = [UIImage imageNamed:@"iphone_ad_1.png"];
	UIImage *image2 = [UIImage imageNamed:@"iphone_ad_2.png"];
	UIImage *image3 = [UIImage imageNamed:@"iphone_ad_3.png"];
	adArray = [NSArray arrayWithObjects:image1, image2, image3, nil];
	
	syear = [User userYear:ID];

	if (syear<1) {
		range = -1;
	} else if (syear == 1) {
		range = 1;
		brange = 0;
		bterm = @"1-1";
		
	} else if (syear == 2) {
		range = 3;
		brange = 2;
		bterm = @"2-1";

	} else {
		range = 7;
		brange = 4;
		bterm = @"3-1";
	} 
	
	

	
	timer= [NSTimer scheduledTimerWithTimeInterval:15 target:self selector:@selector(onTimer) userInfo:nil repeats:YES];
	
	[self onTimer];

	
	[self.tableView setBackgroundColor:[UIColor clearColor]];
	isNeedSave = NO;
	
	switch (style) {
		case 0:{
			
			self.tableView.frame = CGRectMake(0, 86, 320, 254);
			[labelAlert setHidden:NO];
			labelInfo.text = @"등급을 입력하세요";
			
			if (menu==3) {
				if(![Test chkRecord]) [btnDone setHidden:YES];
				[labelAlert setNumberOfLines:0];
				[labelAlert setLineBreakMode:UILineBreakModeWordWrap];
				labelAlert.text = @"내신성적\n특기자 전형은 제외"; 
				list = [Test record];
			} else if (menu==4){
				record = [[Record alloc] init];
				labelAlert.text = smallTitle;

				[btnDone setImage:[UIImage imageNamed:@"next_semester.png"] forState:UIControlStateNormal];
				[btnDone setImage:[UIImage imageNamed:@"next_semester_selected.png"] forState:UIControlStateSelected];
				//labelAlert.text = @"내신성적"; 
				//list = [Record  recordWithEmail:ID term:term];
			}
			break;
		}
		case 1:{
			labelAlert.text = @"모의고사"; 
			self.tableView.frame = CGRectMake(0, 86, 320, 254);
			[labelAlert setHidden:NO];
			labelInfo.text = @"등급을 입력하세요";
			if (menu==3) {
				if(![Test chkGrade]) [btnDone setHidden:YES];
				list = [Test grade];
			} else if (menu==4){
				record = [[Record alloc] init];
				labelAlert.text = smallTitle;
				[btnDone setImage:[UIImage imageNamed:@"next_term.png"] forState:UIControlStateNormal];
				[btnDone setImage:[UIImage imageNamed:@"next_term_selected.png"] forState:UIControlStateSelected];
				//list = [Record  gradeWithEmail:ID seq:seq];
			}
			
			NSLog(@"list %@", list);
			
			break;
		}
			
		default:
			break;
	}
			
	if (menu==3) {
		if ([list count]==0) {
			list = [NSMutableArray arrayWithObjects:
					[NSMutableDictionary dictionaryWithObjectsAndKeys:@"언어", @"content", @"", @"grade", nil],
					[NSMutableDictionary dictionaryWithObjectsAndKeys:@"수리", @"content", @"", @"grade", nil],
					[NSMutableDictionary dictionaryWithObjectsAndKeys:@"외국어", @"content", @"", @"grade", nil],
					[NSMutableDictionary dictionaryWithObjectsAndKeys:@"탐구-1", @"content", @"", @"grade", nil],
					[NSMutableDictionary dictionaryWithObjectsAndKeys:@"탐구-2", @"content", @"", @"grade", nil],
					nil];
		}
	}


	[self.tableView reloadData];

		
	NSLog(@"list %@", list);
	[viewBar setFrame:CGRectMake(0, 480, 320, 44)];
	[viewInfo setFrame:CGRectMake(0, -44, 320, 44)];
	[[NSNotificationCenter defaultCenter] addObserver:self 
											 selector:@selector(keyboardWillShow:) 
												 name:UIKeyboardWillShowNotification 
											   object:nil]; 
	
	[[NSNotificationCenter defaultCenter] addObserver:self 
											 selector:@selector(keyboardWillHide:) 
												 name:UIKeyboardWillHideNotification
											   object:nil];
	
	
}
- (void)setSeq:(int)val{
	seq = val;
	if (slist == nil) {
		slist = [NSArray arrayWithObjects:@"1-1", @"1-2", @"2-1", @"2-2", @"3-1", @"3-2", @"3-3", @"3-4", nil];

	}
	term = [slist objectAtIndex:seq];
	if (style==0) {
		smallTitle = [NSString stringWithFormat:@"내신성적 %@",  term];
		list = [Record  recordWithEmail:ID term:term];
	} else {
		smallTitle = [NSString stringWithFormat:@"모의고사 %@",  term]; 
		list = [Record  gradeWithEmail:ID seq:seq+1];
	}
	labelAlert.text = smallTitle;
	if ([list count]==0) {
		list = [NSMutableArray arrayWithObjects:
				[NSMutableDictionary dictionaryWithObjectsAndKeys:@"언어", @"content", [NSNumber numberWithInt:0], @"grade", nil],
				[NSMutableDictionary dictionaryWithObjectsAndKeys:@"수리", @"content", [NSNumber numberWithInt:0], @"grade", nil],
				[NSMutableDictionary dictionaryWithObjectsAndKeys:@"외국어", @"content", [NSNumber numberWithInt:0], @"grade", nil],
				[NSMutableDictionary dictionaryWithObjectsAndKeys:@"탐구-1", @"content", [NSNumber numberWithInt:0], @"grade", nil],
				[NSMutableDictionary dictionaryWithObjectsAndKeys:@"탐구-2", @"content", [NSNumber numberWithInt:0], @"grade", nil],
				nil];
	}
	[self.tableView reloadData];
	
}

- (void)viewDidUnload
{
    [self setLabelAlert:nil];
    [self setTableView:nil];
	[self setViewBar:nil];
	[self setTextGrade:nil];
	[self setViewInfo:nil];
	[self setLabelInfo:nil];
	[self setLabelTitle:nil];
    [self setBtnDone:nil];
	[self setLabelArea:nil];
	[self setLabelMainTitle:nil];
    [self setImageAD:nil];
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)actionBack:(id)sender {
	if (menu==4) {
		[self sendRecord];
	}[self saveRecord];
	
	[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)actionNext:(id)sender {
	if (menu==3) {
		if (style==0) {
			GradeViewController *viewController = [[GradeViewController alloc] init];
			viewController.menu = menu;
			viewController.mf=mf;
			viewController.ID = ID;
			viewController.style = 1;
			[self.navigationController pushViewController:viewController animated:YES];
		} else if(style==1) {
			SeriesViewController *viewController =[[SeriesViewController alloc] init];
			viewController.ID = ID;
			viewController.menu = menu;
			[self.navigationController pushViewController:viewController animated:YES];
		}
	} else if (menu==4) {
		[self saveRecord];
		[self sendRecord];

		if (style==0) {
			if (range>5) {
				range=5;
			}
			if (seq<range) {
				[self setSeq:seq+1];
			} else [self.navigationController popViewControllerAnimated:YES];
			
		} else if(style==1) {
			if (seq<range) {
				[self setSeq:seq+1];
			} else [self.navigationController popViewControllerAnimated:YES];
		}
		
	}
}

- (void)saveRecord {
	switch (style) {
		case 0:{
			if (menu ==3 ) {
				Test *test = [[Test alloc] init];
				test.lang  = [[[list objectAtIndex:0] objectForKey:@"grade"] intValue];
				test.math  = [[[list objectAtIndex:1] objectForKey:@"grade"] intValue];
				test.fore  = [[[list objectAtIndex:2] objectForKey:@"grade"] intValue];
				test.inqu1 = [[[list objectAtIndex:3] objectForKey:@"grade"] intValue];
				test.inqu2 = [[[list objectAtIndex:4] objectForKey:@"grade"] intValue];
				[test addRecord];
				
			} else {
				record.ID = ID;
				record.term   = term;
				record.lang  = [[[list objectAtIndex:0] objectForKey:@"grade"] intValue];
				record.math  = [[[list objectAtIndex:1] objectForKey:@"grade"] intValue];
				record.fore  = [[[list objectAtIndex:2] objectForKey:@"grade"] intValue];
				record.inqu1 = [[[list objectAtIndex:3] objectForKey:@"grade"] intValue];
				record.inqu2 = [[[list objectAtIndex:4] objectForKey:@"grade"] intValue];
				
				[record addRecord];
			}
			break;
		}
		case 1:{
			if (menu==3) {
				Test *test = [[Test alloc] init];
				test.lang  = [[[list objectAtIndex:0] objectForKey:@"grade"] intValue];
				test.math  = [[[list objectAtIndex:1] objectForKey:@"grade"] intValue];
				test.fore  = [[[list objectAtIndex:2] objectForKey:@"grade"] intValue];
				test.inqu1 = [[[list objectAtIndex:3] objectForKey:@"grade"] intValue];
				test.inqu2 = [[[list objectAtIndex:4] objectForKey:@"grade"] intValue];
				[test addGrade];
				
			} else {
				record.ID = ID;
				record.seq   = seq+1;
				record.lang  = [[[list objectAtIndex:0] objectForKey:@"grade"] intValue];
				record.math  = [[[list objectAtIndex:1] objectForKey:@"grade"] intValue];
				record.fore  = [[[list objectAtIndex:2] objectForKey:@"grade"] intValue];
				record.inqu1 = [[[list objectAtIndex:3] objectForKey:@"grade"] intValue];
				record.inqu2 = [[[list objectAtIndex:4] objectForKey:@"grade"] intValue];
				[record addGrade];
			}
			break;
		}
		default:
			break;
	}
}

- (BOOL)textFieldShouldReturn:(UITextField *)theTextField
{
	NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
	NSNumber * number = [numberFormatter numberFromString:textGrade.text];
	if (!number || !(number.intValue>0 && number.intValue<10)) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"입력값 오류" message:@"입력값이 잘못되었습니다." delegate:self cancelButtonTitle:@"취소" otherButtonTitles:@"재입력", nil];
		[alert show];
		[self.tableView setUserInteractionEnabled:YES];
		[textGrade resignFirstResponder];
	    return NO;
	}
	NSMutableDictionary *dic = [list objectAtIndex:selectedIndex.row];
	[dic setValue:textGrade.text forKey:@"grade"];
	[self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:selectedIndex] withRowAnimation:UITableViewRowAnimationFade];
	[self.tableView setUserInteractionEnabled:YES];
	textGrade.text =@"";
	
	if (selectedIndex.row==4) {
		if (menu==3) [btnDone setHidden:NO];
		[self saveRecord];
		[textGrade resignFirstResponder];
	} else if (selectedIndex.row<4) {
		NSIndexPath *index = [NSIndexPath indexPathForRow:selectedIndex.row+1 inSection:0];
		selectedIndex=index;
		labelArea.text = [[list objectAtIndex:selectedIndex.row] objectForKey:@"content"];
	}
	return YES;
}


- (void)keyboardWillShow:(NSNotification *)notif {
	[UIView beginAnimations:@"ViewAnimation" context:nil];
    [UIView setAnimationDuration:0.25];
	[viewBar setFrame:CGRectMake(0, 200, 320, 50)];
	[viewInfo setFrame:CGRectMake(0, 0, 320, 44)];
    [UIView commitAnimations];
}

- (void)keyboardWillHide:(NSNotification *)notif {
	[UIView beginAnimations:@"ViewAnimation" context:nil];
    [UIView setAnimationDuration:0.25];
	[viewBar setFrame:CGRectMake(0, 480, 320, 50)];
	[viewInfo setFrame:CGRectMake(0, -44, 320, 44)];
	[textGrade setText:@""];
    [UIView commitAnimations];
}

#pragma mark - Alert View
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	NSLog(@"buttonIndex = %d", buttonIndex);
	if (buttonIndex==1) {
		[textGrade becomeFirstResponder];
	} else {
		
	}
	
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return list.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
	}
	NSDictionary *dic =[list objectAtIndex:indexPath.row]; 
	cell.textLabel.text = [dic objectForKey:@"content"];
	cell.textLabel.backgroundColor =[UIColor clearColor];
	int grade = [[dic objectForKey:@"grade"] intValue];
	if (grade>0) {
		cell.detailTextLabel.text = [NSString stringWithFormat:@"%d", grade];
	} else {
		cell.detailTextLabel.text = @"";
	}
	
	cell.detailTextLabel.textColor = [UIColor blackColor];	
	cell.detailTextLabel.backgroundColor = [UIColor clearColor];
	
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	
	if (menu>3 && style==0) {
		if([Record chkRecordWithID:ID term:term] && term<bterm) {
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"성적입력" message:@"내신성적을 이미 입력하셨습니다." delegate:self cancelButtonTitle:@"확인" otherButtonTitles:nil]; 
			[alert show];
			return;
		}
	} else if (menu>3 && style==1) {
		if([Record chkGradeWithID:ID seq:seq+1] && seq<brange) {
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"성적입력" message:@"모의고사성적을 이미 입력하셨습니다." delegate:self cancelButtonTitle:@"확인" otherButtonTitles:nil]; 
			[alert show];
			return;
		}
	}
	
	
	selectedIndex = indexPath;
	labelArea.text = [[list objectAtIndex:selectedIndex.row] objectForKey:@"content"];
	[textGrade becomeFirstResponder];
	[tableView setUserInteractionEnabled:NO];
	

}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	UITouch *touch = [touches anyObject];
	if (touch.view == imageAD ) {
		
		ADPop *adPop = [[ADPop alloc] initWithFrame:CGRectMake(0, 0, 320, 460)];
		[self.view addSubview:adPop];
		adPop.index = touch.view.tag;
	}
}

- (void)sendRecord {
	NSMutableString *str;	
	if (style==0) {
		str=[NSMutableString stringWithFormat:@"http://www.sooseero.com/mobile/member/user3.mgo?id=%@&term=%@&rlang=%@&rmath=%@&rfore=%@&rinqu1=%@&rinqu2=%@", ID, term, [[list objectAtIndex:0] objectForKey:@"grade"], [[list objectAtIndex:1] objectForKey:@"grade"], [[list objectAtIndex:2] objectForKey:@"grade"], [[list objectAtIndex:3] objectForKey:@"grade"], [[list objectAtIndex:4] objectForKey:@"grade"]];
	} else if (style==1) {
		str=[NSMutableString stringWithFormat:@"http://www.sooseero.com/mobile/member/user4.mgo?id=%@&gseq=%@&glang=%@&gmath=%@&gfore=%@&ginqu1=%@&ginqu2=%@", ID, term, [[list objectAtIndex:0] objectForKey:@"grade"], [[list objectAtIndex:1] objectForKey:@"grade"], [[list objectAtIndex:2] objectForKey:@"grade"], [[list objectAtIndex:3] objectForKey:@"grade"], [[list objectAtIndex:4] objectForKey:@"grade"]];
	}
		
	//NSString *url1=	[url stringByAddingPercentEscapesUsingEncoding:EUCKRStringEncoding];
	NSString *url=	[str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	
	NSLog(@"%@", url);
	[self requestUrl:url];
}



- (BOOL)requestUrl:(NSString *)url {
	
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] 
														   cachePolicy:NSURLRequestUseProtocolCachePolicy 
													   timeoutInterval:30.0]; 
	[request setHTTPMethod:@"GET"];
	NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
	if (connection) { 
		self.receivedData = [NSMutableData data]; // 수신할 데이터를 받을 공간을 마련
		return YES;
	}
	return NO;
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)aResponse {
	[receivedData setLength:0];
	self.response = aResponse;
	
	
	
	NSArray *cookies;
    NSDictionary *responseHeaderFields;
	
    // 받은 header들을 dictionary형태로 받고
    responseHeaderFields = [(NSHTTPURLResponse *)aResponse allHeaderFields];
	
    if(responseHeaderFields != nil)
    {
        // responseHeaderFields에 포함되어 있는 항목들 출력
        for(NSString *key in responseHeaderFields)
        {
            NSLog(@"Header: %@ = %@", key, [responseHeaderFields objectForKey:key]);
        }
		
        // cookies에 포함되어 있는 항목들 출력
        cookies = [NSHTTPCookie cookiesWithResponseHeaderFields:responseHeaderFields forURL:[aResponse URL]];
		
        if(cookies != nil)
        {
            for(NSHTTPCookie *a in cookies)
            {
                NSLog(@"Cookie: %@ = %@", [a name], [a value]);
            }
        }
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	//NSString *returnString = [[NSString alloc] initWithData:data encoding:EUCKRStringEncoding];
	[receivedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
	
	rdata = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
	NSLog(@"%@", rdata);
	
	CXMLDocument *rssParser = [[CXMLDocument alloc] initWithXMLString:rdata options:0 error:nil];
	NSArray *xmlNodes  = [rssParser nodesForXPath:@"//result" error:nil];
	
	for (CXMLElement *resultElement in xmlNodes) {
		
		int result = [[[[resultElement children] objectAtIndex:0] stringValue] intValue];
		
		switch (result) {
			case 0:{
				UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"성적전송실패" message:@"성적전송에 실패하였습니다." delegate:self cancelButtonTitle:@"확인" otherButtonTitles:nil]; 
				[alert show];
				break;
			}
//			case 1:{
//				UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"회원가입성공" message:@"회원가입에 성공하였습니다." delegate:self cancelButtonTitle:@"확인" otherButtonTitles:nil]; 
//				[alert show];
//				break;			   
//			}
					default:
				break;
		}
	}
}


@end
