//
//  JoinViewController.h
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 6. 26..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JoinViewController : UIViewController<UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, UIGestureRecognizerDelegate, UIAlertViewDelegate>
@property (strong, nonatomic) IBOutlet UILabel *labelTitle;
@property (strong, nonatomic) IBOutlet UILabel *labelID;
@property (strong, nonatomic) IBOutlet UILabel *labelPW;

@property (strong, nonatomic) IBOutlet UILabel *labelName;
@property (strong, nonatomic) IBOutlet UILabel *labelGender;
@property (strong, nonatomic) IBOutlet UILabel *labelMale;
@property (strong, nonatomic) IBOutlet UILabel *labelFemaile;
@property (strong, nonatomic) IBOutlet UILabel *labelIdentity;
@property (strong, nonatomic) IBOutlet UILabel *labelArea;
@property (strong, nonatomic) IBOutlet UITextField *textID;
@property (strong, nonatomic) IBOutlet UITextField *textPW;
@property (strong, nonatomic) IBOutlet UITextField *textName;
@property (strong, nonatomic) IBOutlet UILabel *labelAreaContent;
@property (strong, nonatomic) IBOutlet UIImageView *chkMale;
@property (strong, nonatomic) IBOutlet UIImageView *chkFemale;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UILabel *labelSType;
@property (strong, nonatomic) IBOutlet UIImageView *chkLiberal;
@property (strong, nonatomic) IBOutlet UIImageView *chkNatural;
@property (strong, nonatomic) IBOutlet UITextField *textPW1;
@property (strong, nonatomic) IBOutlet UILabel *labelYear;
@property (strong, nonatomic) IBOutlet UILabel *labelSchool;
- (IBAction)idDidEnd:(id)sender;



- (IBAction)goBack:(id)sender;

- (IBAction)goNext:(id)sender;
- (IBAction)actionEndEditing:(id)sender;

@end
