//
//  JoinDetailViewController.h
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 6. 26..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"
@interface JoinDetailViewController : UIViewController<UIGestureRecognizerDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UIAlertViewDelegate>
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;


@property (strong, nonatomic) NSString *ID;
@property (strong, nonatomic) NSString *pw;
@property (strong, nonatomic) User *user;
@property (strong, nonatomic) IBOutlet UIImageView *check1;
@property (strong, nonatomic) IBOutlet UIImageView *check2;
@property (strong, nonatomic) IBOutlet UIImageView *check3;
@property (strong, nonatomic) IBOutlet UIImageView *check4;
@property (strong, nonatomic) IBOutlet UIImageView *check5;
@property (strong, nonatomic) IBOutlet UIImageView *check6;
@property (strong, nonatomic) IBOutlet UIImageView *check7;
@property (strong, nonatomic) IBOutlet UIImageView *check8;
@property (strong, nonatomic) IBOutlet UIImageView *check9;
@property (strong, nonatomic) IBOutlet UIImageView *check10;
@property (strong, nonatomic) IBOutlet UIImageView *check11;
@property (strong, nonatomic) IBOutlet UIImageView *check12;
@property (strong, nonatomic) IBOutlet UIImageView *check13;
@property (strong, nonatomic) IBOutlet UIImageView *check14;
@property (strong, nonatomic) IBOutlet UIImageView *check15;
@property (strong, nonatomic) IBOutlet UIImageView *check16;
@property (strong, nonatomic) IBOutlet UIImageView *check17;
@property (strong, nonatomic) IBOutlet UIImageView *check18;
@property (strong, nonatomic) IBOutlet UIImageView *check19;
@property (strong, nonatomic) IBOutlet UIImageView *check20;
@property (strong, nonatomic) IBOutlet UIImageView *check21;
@property (strong, nonatomic) IBOutlet UIImageView *check22;
@property (strong, nonatomic) IBOutlet UIImageView *check23;
@property (strong, nonatomic) IBOutlet UIImageView *check24;
@property (strong, nonatomic) IBOutlet UIImageView *check25;
@property (strong, nonatomic) IBOutlet UIImageView *check26;
@property (strong, nonatomic) IBOutlet UIImageView *check27;
@property (strong, nonatomic) IBOutlet UIImageView *check28;
@property (strong, nonatomic) IBOutlet UIImageView *check29;
@property (strong, nonatomic) IBOutlet UIImageView *check30;



- (IBAction)goBack:(id)sender;
- (IBAction)actionDone:(id)sender;

@end
