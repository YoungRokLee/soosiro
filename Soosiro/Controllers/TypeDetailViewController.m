//
//  TypeDetailViewController.m
//  Soosiro
//
//  Created by 이영록 on 12. 6. 19..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import "TypeDetailViewController.h"
#import "UnivInfo.h"
@interface TypeDetailViewController (){
	UIScrollView *scrollView;
	UIActivityIndicatorView *indicator;
	NSArray *adArray;
	NSTimer *timer;

}
@end

@implementation TypeDetailViewController

@synthesize univCode;
@synthesize unit;
@synthesize type;
@synthesize menu;
@synthesize imageAD;
@synthesize series01;
@synthesize series02;
@synthesize series03;
@synthesize labelMainTitle;


- (void)initView
{
    NSArray *array;
	if (menu==0) {
        array = [UnivInfo typesDescriptionWithUniv:univCode  type:type];
	} else {
        array = [UnivInfo typesDescriptionWithMenu:menu univ:univCode type:type series03:series03 series02:series02 series01:series01 ];
	}
	
	 	int org = 10;
	for (int i=0;i<array.count;i++){
		
		NSDictionary *dic = [array objectAtIndex:i];
		NSLog(@"%@  %@  %@  %@  %@", [dic objectForKey:@"U_NM"], [dic objectForKey:@"TU_NM"], [dic objectForKey:@"U_FIXNUM"], [dic objectForKey:@"TU_SEQ"], [dic objectForKey:@"TU_CODE"]);
		
		NSArray *tuessArray = [UnivInfo typeDetailWithTUCode:[dic objectForKey:@"TU_CODE"]];
		NSMutableString *ratio = [[NSMutableString alloc] init];
		for (NSDictionary *dic in tuessArray) {
			[ratio appendString:[NSString stringWithFormat:@"%@ %@ \n",[dic objectForKey:@"TU_STEP"], [dic objectForKey:@"TU_RATIO"]]]; 
		}
		
		
		UIImageView *imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake(10, org, 178, 47)];
		imageView1.image = [UIImage imageNamed:@"title_1.png"];
		[scrollView addSubview:imageView1];
		
		UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(50, 8 , 130, 40)];
		label1.text =@"대학명";
		label1.backgroundColor =[UIColor clearColor];
		[imageView1 addSubview:label1];
		
		UITextView *textView1 = [[UITextView alloc] initWithFrame:CGRectMake(10, org+imageView1.frame.size.height +10 , 300, 40)];
		textView1.text =  [dic objectForKey:@"U_NM"];
		[textView1 setUserInteractionEnabled:NO];
		textView1.backgroundColor =[UIColor clearColor];
		[scrollView addSubview:textView1];
		[textView1 setFrame:CGRectMake(10, org+imageView1.frame.size.height +10, 300, textView1.contentSize.height)];
		
		org = textView1.frame.origin.y+textView1.frame.size.height;
		if (menu>0) {
            
			UIImageView *imageView1_1 = [[UIImageView alloc] initWithFrame:CGRectMake(10, org, 178, 47)];
			imageView1_1.image = [UIImage imageNamed:@"title_2.png"];
			[scrollView addSubview:imageView1_1];
			
			UILabel *label1_1 = [[UILabel alloc] initWithFrame:CGRectMake(50, 8 , 130, 40)];
			label1_1.text =@"모집단위";
			label1_1.backgroundColor =[UIColor clearColor];
			[imageView1_1 addSubview:label1_1];
			
			UITextView *textView1_1 = [[UITextView alloc] initWithFrame:CGRectMake(10, org+imageView1_1.frame.size.height +10 , 300, 40)];
			textView1_1.text =  [dic objectForKey:@"U_UNIT"];
			[textView1_1 setUserInteractionEnabled:NO];
			textView1_1.backgroundColor =[UIColor clearColor];
			[scrollView addSubview:textView1_1];
			[textView1_1 setFrame:CGRectMake(10, org+imageView1_1.frame.size.height +10, 300, textView1_1.contentSize.height)];
			
			org = textView1_1.frame.origin.y+textView1_1.frame.size.height;
		}
		
		UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(10, org, 178, 47)];
		imageView2.image = [UIImage imageNamed:@"title_3.png"];
		[scrollView addSubview:imageView2];
		
		UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(50, 8 , 130, 40)];
		label2.text =@"전형명";
		label2.backgroundColor =[UIColor clearColor];
		[imageView2 addSubview:label2];
		
		UITextView *textView2 = [[UITextView alloc] initWithFrame:CGRectMake(10, imageView2.frame.origin.y+imageView2.frame.size.height +10 , 300, 40)];
		textView2.text = [NSString stringWithFormat:@"%@ (%d차)",[dic objectForKey:@"TU_NM"], [[dic objectForKey:@"TU_SEQ"] intValue]];
		[textView2 setUserInteractionEnabled:NO];
		textView2.backgroundColor =[UIColor clearColor];
		[scrollView addSubview:textView2];
		[textView2 setFrame:CGRectMake(10, imageView2.frame.origin.y+imageView2.frame.size.height +10, 300, textView2.contentSize.height)];
		
		org = textView2.frame.origin.y+textView2.frame.size.height ;
		
		
		UIImageView *imageView3 = [[UIImageView alloc] initWithFrame:CGRectMake(10, org, 178, 47)];
		imageView3.image = [UIImage imageNamed:@"title_4.png"];
		[scrollView addSubview:imageView3];
		
		UILabel *label3 = [[UILabel alloc] initWithFrame:CGRectMake(50, 8 , 130, 40)];
		label3.text =@"선발인원";
		label3.backgroundColor =[UIColor clearColor];
		[imageView3 addSubview:label3];
		
		UITextView *textView3 = [[UITextView alloc] initWithFrame:CGRectMake(10, imageView3.frame.origin.y+imageView3.frame.size.height +10 , 300, 40)];
		
		int num = [[dic objectForKey:@"U_FIXNUM"] intValue];
		if(num>0) textView3.text = [NSString stringWithFormat:@"%d명 선발",[[dic objectForKey:@"U_FIXNUM"] intValue]];
		else  textView3.text = [dic objectForKey:@"U_NOTE"];
		[textView3 setUserInteractionEnabled:NO];
		textView3.backgroundColor =[UIColor clearColor];
		[scrollView addSubview:textView3];
		[textView3 setFrame:CGRectMake(10, imageView3.frame.origin.y+imageView3.frame.size.height +10, 300, textView3.contentSize.height)];
		org = textView3.frame.origin.y+textView3.frame.size.height;	
		
		if (menu>0) {
			UIImageView *imageView4 = [[UIImageView alloc] initWithFrame:CGRectMake(10, org, 178, 47)];
			imageView4.image = [UIImage imageNamed:@"title_4.png"];
			[scrollView addSubview:imageView4];
			
			UILabel *label4 = [[UILabel alloc] initWithFrame:CGRectMake(50, 8 , 130, 40)];
			label4.text =@"수능최저등급";
			label4.backgroundColor =[UIColor clearColor];
			[imageView4 addSubview:label4];
			
			UITextView *textView4 = [[UITextView alloc] initWithFrame:CGRectMake(10, imageView4.frame.origin.y+imageView4.frame.size.height +10 , 300, 40)];
			
			NSLog(@" proviso == %@", [dic objectForKey:@"U_PROVISO"]);
			NSString *proviso = [dic objectForKey:@"U_PROVISO"];
			if([proviso isEqualToString:@""] || proviso == nil) textView4.text = @"수능최저등급 미적용";
			else textView4.text = @"수능최저등급 적용";
			[textView4 setUserInteractionEnabled:NO];
			textView4.backgroundColor =[UIColor clearColor];
			[scrollView addSubview:textView4];
			[textView4 setFrame:CGRectMake(10, imageView4.frame.origin.y+imageView4.frame.size.height +10, 300, textView4.contentSize.height)];
			org = textView4.frame.origin.y+textView4.frame.size.height +10;	
		}
		UIImageView *imageView5 = [[UIImageView alloc] initWithFrame:CGRectMake(10, org, 178, 47)];
		imageView5.image = [UIImage imageNamed:@"title_5.png"];
		[scrollView addSubview:imageView5];
		
		UILabel *label5 = [[UILabel alloc] initWithFrame:CGRectMake(50, 8 , 130, 40)];
		label5.text =@"전형요소반영비율";
		label5.backgroundColor =[UIColor clearColor];
		[imageView5 addSubview:label5];
		
		UITextView *textView5 = [[UITextView alloc] initWithFrame:CGRectMake(10, imageView5.frame.origin.y+imageView5.frame.size.height +10 , 300, 40)];
		textView5.text = ratio;
		[textView5 setUserInteractionEnabled:NO];
		textView5.backgroundColor =[UIColor clearColor];
		[scrollView addSubview:textView5];
		[textView5 setFrame:CGRectMake(10, imageView5.frame.origin.y+imageView5.frame.size.height +10, 300, textView5.contentSize.height)];
		org = textView5.frame.origin.y+textView5.frame.size.height +10;	
		
		if (i<array.count-1) {
			UIImageView *imageSep = [[UIImageView alloc] initWithFrame:CGRectMake(0, org, 320, 3)];
			imageSep.image = [UIImage imageNamed:@"line_shadow.png"];
			[scrollView addSubview:imageSep];
			org = org+20;
		}

		
	}
	
	
	
	scrollView.contentSize = CGSizeMake(320, org);
	[indicator stopAnimating];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	labelMainTitle.text =[Title mainTilteWithMenu:menu];
	UIImage *image1 = [UIImage imageNamed:@"iphone_ad_1.png"];
	UIImage *image2 = [UIImage imageNamed:@"iphone_ad_2.png"];
	UIImage *image3 = [UIImage imageNamed:@"iphone_ad_3.png"];
	adArray = [NSArray arrayWithObjects:image1, image2, image3, nil];
	
	timer= [NSTimer scheduledTimerWithTimeInterval:15 target:self selector:@selector(onTimer) userInfo:nil repeats:YES];
	
	[self onTimer];

	scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 44 , 320, 367)];
	[self.view addSubview:scrollView];

	indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 64.0f, 64.0f)];
	[indicator setCenter:self.view.center];
    [indicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
    [self.view addSubview : indicator];	
	
	indicator.hidden= FALSE;
	[indicator startAnimating];
	
	[self performSelector:@selector(initView) withObject:nil afterDelay:0];
}

- (void)onTimer{
	int index = arc4random()%3;
	imageAD.image = [adArray objectAtIndex:index];
	imageAD.tag = index;
}

- (void)viewDidUnload
{
	[self setLabelMainTitle:nil];
    [self setImageAD:nil];
	[super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (IBAction)goBack:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)goHome:(id)sender {
	[self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	UITouch *touch = [touches anyObject];
	if (touch.view == imageAD ) {
		
		ADPop *adPop = [[ADPop alloc] initWithFrame:CGRectMake(0, 0, 320, 460)];
		[self.view addSubview:adPop];
		adPop.index = touch.view.tag;
	}
}


@end
