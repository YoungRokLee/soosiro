//
//  TypeDetailViewController.h
//  Soosiro
//
//  Created by 이영록 on 12. 6. 19..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TypeDetailViewController : UIViewController
@property (strong, nonatomic) NSString *univCode;
@property (strong, nonatomic) NSString *unit;
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSString *series01;
@property (strong, nonatomic) NSString *series02;
@property (strong, nonatomic) NSString *series03;
@property (strong, nonatomic) IBOutlet UILabel *labelMainTitle;
@property (nonatomic) int menu;
@property (strong, nonatomic) IBOutlet UIImageView *imageAD;

- (IBAction)goBack:(id)sender;
- (IBAction)goHome:(id)sender;


@end
