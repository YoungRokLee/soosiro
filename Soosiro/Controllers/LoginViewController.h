//
//  LoginViewController.h
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 6. 6..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import <UIKit/UIKit.h>

#define DOCSFOLDER		[NSHomeDirectory() stringByAppendingString:@"/Documents/"]
@interface LoginViewController : UIViewController<UITextFieldDelegate, UIActionSheetDelegate>
@property (strong, nonatomic) IBOutlet UITextField *idText;
@property (strong, nonatomic) IBOutlet UIButton *joinButton;
@property (strong, nonatomic) IBOutlet UITextField *pwText;
- (IBAction)login:(id)sender;
- (IBAction)join:(id)sender;
- (IBAction)done:(id)sender;


@end
