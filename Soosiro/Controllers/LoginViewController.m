//
//  LoginViewController.m
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 6. 6..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import "LoginViewController.h"
#import "MainViewController.h"
#import "JoinViewController.h"
#import "Download.h"
#import "TouchXML.h"
#import "User.h"
#import "Record.h"
#import "Test.h"
#import "Language.h"
#import "Special.h"
#import "NSDataAdditions.h"
#import <CommonCrypto/CommonDigest.h>

enum  {
	EUCKRStringEncoding = -2147481280,
	KSC5601StringEncoding = -2147481290,
};

@interface LoginViewController (){
	UIButton * button;
	UIActionSheet	* baseSheet;
	UIProgressView * progress;
	
	UIAlertView *alertPremium;
	
	NSString * savePath;
	NSString * fileName;
	NSString *dbVersion;
	BOOL isPremium;
	NSString *dburl;
}
@property (nonatomic, retain) NSString * savePath;
@property (nonatomic, retain) NSString * fileName;
@property (nonatomic, retain) NSMutableData *receivedData;
@property (nonatomic, retain) NSURLResponse *response;
@property (nonatomic, retain) NSString *rdata;
@end



@implementation LoginViewController

@synthesize idText;
@synthesize joinButton;
@synthesize pwText;
@synthesize savePath;
@synthesize fileName;
@synthesize receivedData;
@synthesize response;
@synthesize rdata;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self.navigationController setNavigationBarHidden:YES];
	isPremium = NO;
	pwText.secureTextEntry = YES;
}

- (void)viewDidUnload
{
    [self setIdText:nil];
    [self setPwText:nil];
   
	[self setJoinButton:nil];
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)login:(id)sender {
	if ([idText.text isEqualToString:@""]) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"아이디오류" message:@"아이디가 잘못되었습니다." delegate:self cancelButtonTitle:@"확인" otherButtonTitles:nil]; 
		[alert show];
		return;
	}
	
	if ([pwText.text isEqualToString:@""]) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"비밀번호오류" message:@"비밀번호가 잘못되었습니다." delegate:self cancelButtonTitle:@"확인" otherButtonTitles:nil]; 
		[alert show];
		return;
	}

	[self getData ];
}

- (IBAction)join:(id)sender {
	JoinViewController *viewController =[[JoinViewController alloc] init];
	[self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)done:(id)sender {
	UITextField *text = (UITextField *)sender;
	
	if (text==idText) {
		[text resignFirstResponder];
		[pwText becomeFirstResponder];
	} else [pwText resignFirstResponder];
	
}

-(NSString*) md5:(NSString*)srcStr
{
	const char *cStr = [srcStr UTF8String];
	unsigned char result[CC_MD5_DIGEST_LENGTH];
	CC_MD5(cStr, strlen(cStr), result);
	return [NSString stringWithFormat:
			@"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
			result[0],result[1],result[2],result[3],result[4],result[5],result[6],result[7],result[8],result[9],result[10],result[11],result[12],result[13],result[14],result[15]];
}

- (void)onStartDownload {
	
	NSLog(@"click");
	NSString * fileAddress = dburl;
	
	[self presentSheet];// 1) 액션 시트 
	[Download sharedInstance].delegate = self; // 2) 다운로드 클래스에 델리게이트 지정
	[Download download:fileAddress]; // 3) 파일을 다운로드 합니다.
}

//액션시트를 화면에 출력합니다.
//다운로드 게이지를 보여줍니다.
- (void) presentSheet {
	if(!baseSheet) {
		baseSheet = [[UIActionSheet alloc] initWithTitle:@"\n\n DB를 다운로드 중입니다. \n\n\n\n" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
		progress = [[UIProgressView alloc] initWithFrame:CGRectMake(50, 70, 220, 90)];
		progress.tag = 999;
		[progress setProgressViewStyle:UIProgressViewStyleDefault];
		[baseSheet addSubview:progress];
	}
	
	[progress setProgress:(0.0f)];
	[baseSheet showInView:self.view];
}

#pragma mark Download delegate
//게이지 값을 받습니다.
- (void) dataDownloadAtPercent:(NSNumber *)aPercent {
	[progress setHidden:NO];
	[progress setProgress:[aPercent floatValue]];
}

//액션시트를 해제합니다.
- (void) dismissActionSheet {
	[baseSheet dismissWithClickedButtonIndex:0 animated:YES];
	
	baseSheet = nil;
}

//다운로드를 실패했을 때 알림창 출력합니다.
- (void) dataDownloadFailed:(NSString *) reason {
	NSLog(@"다운로드 실패 : %@", reason);
	[self dismissActionSheet];
	[self messageView:reason];
}

//다운로드 완료 후에 사용 할 샌드박스경로와 이름으로 지정한다.(완료전 실행되는 메소드입니다.)
- (void)didReceiveFilename:(NSString *) aName
{
	self.fileName = aName;
	self.savePath = [DOCSFOLDER stringByAppendingString:aName];
	
	
}

//다운로드 완료후 파일을 지정된 이름으로 샌드박스에 저장하기
- (void) didReceiveData:(NSData *)theData {
	
//	long filesize;
	
	NSLog(@"저장파일 경로 %@ ", self.savePath);
	
	if(![theData writeToFile:savePath atomically:YES]) {
		NSLog(@"Error writing data to file");
		return;
	} else {
		[self dismissActionSheet];
		NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
		[userDefaults setObject:dbVersion forKey:@"dbversion"];
        [userDefaults synchronize];
		NSLog(@"dbversion=%@  ", dbVersion);
		if([[NSFileManager defaultManager] fileExistsAtPath:self.savePath]) {

		}
	}
}


//AlertView용 메소드입니다.
- (void) messageView:(NSString *)message {
	UIAlertView * alertView =  [[UIAlertView alloc] initWithTitle:@"알림" message:message delegate:self cancelButtonTitle:@"확인" otherButtonTitles:nil];
	[alertView show];
	
}

- (NSString*)encode:(NSString*)val {
	unsigned char md[16];
    const char *jjj =[val UTF8String ];
    CC_MD5(jjj, val.length, md);
    
   return [[NSData dataWithBytes:md length:16] base64Encoding];
}
- (NSString*) base64:(NSString*)srcStr
{
	NSLog(@"Original string: %@", srcStr);  
	NSData *sourceData = [srcStr dataUsingEncoding:NSUTF8StringEncoding];  
	
	NSString *base64EncodedString = [sourceData base64Encoding];  
	NSLog(@"Encoded form: %@", base64EncodedString);
	
	return base64EncodedString;
}

- (void)getData{
	
	NSMutableString *url=[NSMutableString stringWithFormat: @"http://www.sooseero.com/mobile/member/login.mgo?id=%@&pwd=%@", idText.text, [self encode:pwText.text]];
	
	NSLog(@"%@", url);
	[url stringByAddingPercentEscapesUsingEncoding:EUCKRStringEncoding];

	[self requestUrl:url];
}	


- (BOOL)requestUrl:(NSString *)url {
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] 
														   cachePolicy:NSURLRequestUseProtocolCachePolicy 
													   timeoutInterval:20.0]; 
	[request setHTTPMethod:@"GET"];
	NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
	if (connection) { 
		self.receivedData = [NSMutableData data]; // 수신할 데이터를 받을 공간을 마련
		return YES;
	}
	return NO;
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
	
	UIAlertView *alert  = [[UIAlertView alloc] initWithTitle:@"로그인실패" message:@"네트워크상태를 확인바랍니다." delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil];
	[alert show];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)aResponse {
	[receivedData setLength:0];
	self.response = aResponse;
	
	
	
	NSArray *cookies;
    NSDictionary *responseHeaderFields;
	
    // 받은 header들을 dictionary형태로 받고
    responseHeaderFields = [(NSHTTPURLResponse *)aResponse allHeaderFields];
	
    if(responseHeaderFields != nil)
    {
        // responseHeaderFields에 포함되어 있는 항목들 출력
        for(NSString *key in responseHeaderFields)
        {
            NSLog(@"Header: %@ = %@", key, [responseHeaderFields objectForKey:key]);
        }
		
        // cookies에 포함되어 있는 항목들 출력
        cookies = [NSHTTPCookie cookiesWithResponseHeaderFields:responseHeaderFields forURL:[aResponse URL]];
		
        if(cookies != nil)
        {
            for(NSHTTPCookie *a in cookies)
            {
                NSLog(@"Cookie: %@ = %@", [a name], [a value]);
            }
        }
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	//NSString *returnString = [[NSString alloc] initWithData:data encoding:EUCKRStringEncoding];
	[receivedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
	
	rdata = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
	NSLog(@"%@", rdata);
	
	CXMLDocument *rssParser = [[CXMLDocument alloc] initWithXMLString:rdata options:0 error:nil];
	NSArray *xmlNodes  = [rssParser nodesForXPath:@"/root" error:nil];
		
    for (CXMLElement *resultElement in xmlNodes) {
		
		NSMutableDictionary *dataItem = [[NSMutableDictionary alloc] init];
		
		int count = 18;
		if ([[resultElement children] count]<18) {
			count=[[resultElement children] count];
		}
		
		for(int i = 0; i < count; i++) {
			[dataItem setValue:[[[resultElement children] objectAtIndex:i] stringValue] forKey:[[[resultElement children] objectAtIndex:i] name]];
			NSLog(@"**%@",[[[resultElement children] objectAtIndex:i] stringValue] );
			
		}
		NSLog(@" dic = %@", dataItem);
		if ([dataItem objectForKey:@"login"]==nil ) {
			UIAlertView *alert  = [[UIAlertView alloc] initWithTitle:@"로그인실패" message:@"로그인에 실패했습니다." delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil];
			[alert show];
			return;
		}
		if (![[dataItem objectForKey:@"login"] isEqualToString:@"1"]) {
			UIAlertView *alert  = [[UIAlertView alloc] initWithTitle:@"로그인실패" message:@"아이디나 비밀번호를 잘못되었습니다" delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil];
			[alert show];
			return;
		}
		
		if ([[dataItem objectForKey:@"preminum"] isEqualToString:@"1"]) {
			isPremium = YES;
		} else if ([[dataItem objectForKey:@"preminum"] isEqualToString:@"0"]) {
			isPremium = NO;
		} 
		isPremium = YES;
		dburl = [dataItem objectForKey:@"db_url"];
		User *user = [[User alloc] init];
		user.P_ID = idText.text;
		user.P_Nm = [dataItem objectForKey:@"pNm"];
		user.P_PWD = pwText.text;
		user.P_MF = @"M" ;
		user.P_Syear = [[dataItem objectForKey:@"pSyear"] intValue];
		NSLog(@"syear = %d", user.P_Syear);
		user.L_code = [dataItem objectForKey:@"lCode"];
		user.H_Nm = [dataItem objectForKey:@"hCode"];
		user.H_Class =[dataItem objectForKey:@"hClass"];
		user.P_Cate =[dataItem objectForKey:@"pCate"];
		[user add];
		
		NSString *info = [dataItem objectForKey:@"info"];
		if (info!=nil) {
			NSMutableArray *array = [NSMutableArray array];
			for (int i=0; i<30; i++) {
				if ([[info substringWithRange:NSMakeRange(i, 1)] isEqualToString:@"1"]) {
				
					[array addObject:@"Y"];
				} else {
					[array addObject:@"N"];
				}
			}
			User *user = [[User alloc] init];
			user.P_ID = idText.text;
			user.info = array;
			[user addInfo];
		}
		
		
		NSString *lang = [dataItem objectForKey:@"lang"];
		NSString *engTest = [dataItem objectForKey:@"engTest"];
		if (engTest==nil) engTest=@"";
		
		int engScore = [[dataItem objectForKey:@"engScore"] intValue];
		
		if (lang!=nil) {
			NSMutableArray *array = [NSMutableArray array];
			for (int i=0; i<7; i++) {
				[array addObject:[lang substringWithRange:NSMakeRange(i, 1)]];
			}
			Language *lang = [[Language alloc] init];
			lang.P_ID =idText.text;
			lang.ENG_TEST		= engTest;
			lang.ENG_SCORE		= engScore;
			lang.CHI			= [[array objectAtIndex:0] intValue];
			lang.DEU			= [[array objectAtIndex:1] intValue];
			lang.FRA			= [[array objectAtIndex:2] intValue];
			lang.JAP			= [[array objectAtIndex:3] intValue];
			lang.RUS			= [[array objectAtIndex:4] intValue];
			lang.SPA			= [[array objectAtIndex:5] intValue];
			lang.HAN			= [[array objectAtIndex:6] intValue];
			[lang addLanguage];
		}
		
		NSString *math = [dataItem objectForKey:@"math"];
		if (math!=nil) {
			NSMutableArray *array = [NSMutableArray array];
			for (int i=0; i<10; i++) {
				[array addObject:[math substringWithRange:NSMakeRange(i, 1)]];
				NSLog(@"%d번째 %d", i,[[array objectAtIndex:i] intValue] );
			}
			Special *special = [[Special alloc] init];
			special.P_ID = idText.text;
			if ([[array objectAtIndex:0] intValue]==1) special.SHSCHOOL = @"Y"; else special.SHSCHOOL= @"N";
			if ([[array objectAtIndex:1] intValue]==1) special.INOLIMP  = @"Y"; else special.INOLIMP = @"N";
			if ([[array objectAtIndex:2] intValue]==1) special.MOLIMP   = @"Y"; else special.MOLIMP = @"N";
			if ([[array objectAtIndex:3] intValue]==1) special.POLIMP   = @"Y"; else special.POLIMP = @"N";
			if ([[array objectAtIndex:4] intValue]==1) special.COLIMP   = @"Y"; else special.COLIMP = @"N";
			if ([[array objectAtIndex:5] intValue]==1) special.OOLIMP   = @"Y"; else special.OOLIMP = @"N";
			if ([[array objectAtIndex:6] intValue]==1) special.EOLIMP   = @"Y"; else special.EOLIMP = @"N";
			if ([[array objectAtIndex:7] intValue]==1) special.AOLIMP   = @"Y"; else special.AOLIMP = @"N";
			if ([[array objectAtIndex:8] intValue]==1) special.NOLIMP   = @"Y"; else special.NOLIMP = @"N";
			if ([[array objectAtIndex:9] intValue]==1) special.ROBOT    = @"Y"; else special.ROBOT  = @"N";
			[special addSpecial];
		}

		
		NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
		
		if ([userDefaults objectForKey:@"dbversion"]==nil) {
			dbVersion = @"0";
		} else 	dbVersion = [NSString stringWithString:[userDefaults objectForKey:@"dbversion"]];
		
		NSString *serverVerson = [dataItem objectForKey:@"dbversion"];
		
		NSLog(@"dbversion=%@  serverVersion=%@", dbVersion, serverVerson);
		if (![dbVersion isEqualToString:serverVerson]) {
			dbVersion = serverVerson;
			[self onStartDownload];
			
		}

	}
	
	    
	NSArray *recodNodes  = [rssParser nodesForXPath:@"//schoolScore" error:nil];
	
    for (CXMLElement *resultElement in recodNodes) {
		
		NSMutableDictionary *dataItem = [[NSMutableDictionary alloc] init];
		
		for(int i = 0; i < [[resultElement children] count]; i++) {
			[dataItem setValue:[[[resultElement children] objectAtIndex:i] stringValue] forKey:[[[resultElement children] objectAtIndex:i] name]];
			NSLog(@"**%@",[[[resultElement children] objectAtIndex:i] stringValue] );
			
		}
		Record *record = [[Record alloc] init];
		record.ID = idText.text;
		record.term  = [dataItem objectForKey:@"rterm"];
		record.lang  = [[dataItem objectForKey:@"rlang"] doubleValue];
		record.math  = [[dataItem objectForKey:@"rmath"] doubleValue];
		record.fore  = [[dataItem objectForKey:@"rfore"] doubleValue];
		record.inqu1 = [[dataItem objectForKey:@"rinqu1"] doubleValue];
		record.inqu2 = [[dataItem objectForKey:@"rinqu2"] doubleValue];
		[record addRecord];
		
		NSLog(@" dic = %@", dataItem);
	}
	
	NSArray *testNodes  = [rssParser nodesForXPath:@"//mockTest" error:nil];
	
    for (CXMLElement *resultElement in testNodes) {
		
		NSMutableDictionary *dataItem = [[NSMutableDictionary alloc] init];
		
		for(int i = 0; i < [[resultElement children] count]; i++) {
			[dataItem setValue:[[[resultElement children] objectAtIndex:i] stringValue] forKey:[[[resultElement children] objectAtIndex:i] name]];
			NSLog(@"**%@",[[[resultElement children] objectAtIndex:i] stringValue] );
			
		}
		NSLog(@" dic = %@", dataItem);
		Record *record = [[Record alloc] init];
		record.ID = idText.text;
		record.seq   = [self term2seq:[dataItem objectForKey:@"gseq"]];
		record.lang  = [[dataItem objectForKey:@"glang"] doubleValue];
		record.math  = [[dataItem objectForKey:@"gmath"] doubleValue];
		record.fore  = [[dataItem objectForKey:@"gfore"] doubleValue];
		record.inqu1 = [[dataItem objectForKey:@"ginqu1"] doubleValue];
		record.inqu2 = [[dataItem objectForKey:@"ginqu2"] doubleValue];
		[record addGrade];
	}
	MainViewController *mainViewController = [[MainViewController alloc] init];
	mainViewController.ID = idText.text;
	mainViewController.isPremium = isPremium;
	[self.navigationController pushViewController:mainViewController animated:YES];

}

- (int)term2seq:(NSString*)val {
	if([val isEqualToString:@"1-1"]) return 1;
	else if([val isEqualToString:@"1-2"]) return 2;
	else if([val isEqualToString:@"2-1"]) return 3;
	else if([val isEqualToString:@"2-2"]) return 4;
	else if([val isEqualToString:@"3-1"]) return 5;
	else if([val isEqualToString:@"3-2"]) return 6;
	else if([val isEqualToString:@"3-3"]) return 7;
	else if([val isEqualToString:@"3-4"]) return 8;
	else return 0;
}


@end
