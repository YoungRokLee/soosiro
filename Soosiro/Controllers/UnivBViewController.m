//
//  UnivViewControllerViewController.m
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 6. 16..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import "UnivBViewController.h"
#import "RecResultViewController.h"
#import "User.h"
#import "TypeBViewController.h"
#import "UnivInfo.h"
#import "Title.h"
#import "ADPop.h"

@interface UnivBViewController (){
	UIActivityIndicatorView *indicator;
	NSArray *adArray;
	NSTimer *timer;
	NSMutableArray *flist;
	int selectedRow;
}
@end

@implementation UnivBViewController
@synthesize tableView=_tableView;
@synthesize list;
@synthesize area;
@synthesize menu;
@synthesize type;
@synthesize tclass;
@synthesize unit;
@synthesize series03;
@synthesize series02;
@synthesize series01;
@synthesize mf;
@synthesize imageAD;
@synthesize labelMainTitle;
@synthesize ID;

- (BOOL)isExist:(NSString *)univName {
	BOOL result = NO;
	for (int i=0; i<list.count; i++) {
		NSDictionary *dic = [list objectAtIndex:i];
		NSString *name = [dic objectForKey:@"U_NM"];
		if ([name isEqualToString:univName]) {
			result = YES;
			break;
		}
	}
	return result;
}

- (void)initView
{
	[self.tableView setFrame:CGRectMake(0, 44, 320, 367)];
	list = [NSMutableArray array];
	NSMutableArray *grades= [Record gradeArrayWithEmail:ID];
	NSNumber* avg = [Record avgValueWithID:ID];
	NSArray *array =[UnivInfo univInfoWithType:type avg:avg series03:series03 series02:series02 series01:series01 mf:mf];	
	
	for (int i=0; i<array.count; i++) {
		NSDictionary *dic = [array objectAtIndex:i];
		NSLog(@" %@ %@ %@ %@ %@ %@ %f ", [dic objectForKey:@"U_CODE"], [dic objectForKey:@"U_NM"],[dic objectForKey:@"T_NM"],[dic objectForKey:@"U_UNIT"],[dic objectForKey:@"U_PROVISO"],[dic objectForKey:@"TU_NM"],[[dic objectForKey:@"U_JUST"] doubleValue]);
	}
	
	for (int i=0;i<array.count;i++){
		NSDictionary *dic = [array objectAtIndex:i];
		NSLog(@"%@  %@  %@   --%@--  %@  %@ %@ ", [dic objectForKey:@"U_NM"], [dic objectForKey:@"TU_NM"], [dic objectForKey:@"U_FIXNUM"],[dic objectForKey:@"U_PROVISO"], [dic objectForKey:@"TU_SEQ"], [dic objectForKey:@"TU_CODE"], grades);
		
		if (![self chekProvisoWithGrades:grades proviso:[dic objectForKey:@"U_PROVISO"]] || [self isExist:[dic objectForKey:@"U_NM"]]) continue;
		[list addObject:dic];
		
		if (list.count>=10) break;
	}
	[self.tableView reloadData];
	[indicator stopAnimating];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	UIImage *image1 = [UIImage imageNamed:@"iphone_ad_1.png"];
	UIImage *image2 = [UIImage imageNamed:@"iphone_ad_2.png"];
	UIImage *image3 = [UIImage imageNamed:@"iphone_ad_3.png"];
	adArray = [NSArray arrayWithObjects:image1, image2, image3, nil];
	selectedRow = -1;
	timer= [NSTimer scheduledTimerWithTimeInterval:15 target:self selector:@selector(onTimer) userInfo:nil repeats:YES];
	[self onTimer];
	
	[self.tableView setBackgroundColor:[UIColor clearColor]];
	labelMainTitle.text = [Title mainTilteWithMenu:menu];
	
	flist = [NSMutableArray arrayWithObjects:
			 [NSDictionary dictionaryWithObjectsAndKeys:@"추천전형 도전!", @"content", @"master",@"type", [NSNumber numberWithInt:0], @"order",nil],
			 [NSDictionary dictionaryWithObjectsAndKeys:@"추천전형 소신!", @"content", @"master",@"type", [NSNumber numberWithInt:1], @"order",nil],
			 [NSDictionary dictionaryWithObjectsAndKeys:@"추천전형 안정!", @"content", @"master",@"type", [NSNumber numberWithInt:2], @"order",nil],
			nil];

	
	indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 64.0f, 64.0f)];
	[indicator setCenter:self.view.center];
    [indicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
    [self.view addSubview : indicator];	
	
	indicator.hidden= FALSE;
	[indicator startAnimating];
		
	[self performSelector:@selector(initView) withObject:nil afterDelay:0];
	
}

- (void)onTimer{
	int index = arc4random()%3;
	imageAD.image = [adArray objectAtIndex:index];
	imageAD.tag = index;
}

- (void)viewDidUnload
{
	[self setTableView:nil];
	[self setImageAD:nil];
	[self setLabelMainTitle:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)actionBack:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return flist.count;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
	}
	
	NSDictionary *dic =[flist objectAtIndex:indexPath.row];
	if ([[dic objectForKey:@"type"] isEqualToString:@"master"]) {
		cell.textLabel.text = [dic objectForKey:@"content"];
		cell.contentView.backgroundColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"list_bar_yellow_title.png"]];
		cell.indentationLevel = 0;
	} else {
		cell.textLabel.text = [dic objectForKey:@"content"];
		cell.contentView.backgroundColor = [UIColor clearColor];
		cell.indentationLevel = 1;
	}

	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	
	
	NSDictionary *dic = [flist objectAtIndex:indexPath.row];
	if ([[dic objectForKey:@"type"] isEqual:@"master"]) {
		NSMutableArray *array = [NSMutableArray array];
		
		for (NSDictionary *dic in flist){
			if (![[dic objectForKey:@"type"] isEqual:@"master"]) {
				[array addObject:dic];
			}
		}
		
		for (id object in array) {
			[flist removeObject:object];
		}
		
		if (selectedRow != indexPath.row) {
			int count;
			
			int order = [[dic objectForKey:@"order"] intValue];
			
			switch (order) {
				case 0:{
					if (list.count<3) count = list.count;
					else count = 3;
					for (int i=0; i<count; i++) {
						NSDictionary *dic2 = [list objectAtIndex:i];
						NSString *content =[NSString stringWithFormat:@"%@", [dic2 objectForKey:@"U_NM"]];
						NSDictionary *dic1 = [NSDictionary dictionaryWithObjectsAndKeys:content, @"content",@"detail",@"type",[dic2 objectForKey:@"U_CODE"], @"ucode", [dic2 objectForKey:@"U_NM"], @"uname",[dic2 objectForKey:@"T_NM"], @"t_nm" , [dic2 objectForKey:@"U_UNIT"], @"uunit" , nil];
						[flist insertObject:dic1 atIndex:order+i+1];
					}
					break;	
				}
				case 1:{
					if (list.count<6) count = list.count;
					else count = 6;
					for (int i=3; i<count; i++) {
						NSDictionary *dic2 = [list objectAtIndex:i];
						NSString *content =[NSString stringWithFormat:@"%@", [dic2 objectForKey:@"U_NM"]];
						NSDictionary *dic1 = [NSDictionary dictionaryWithObjectsAndKeys:content, @"content",@"detail",@"type", [dic2 objectForKey:@"U_CODE"], @"ucode", [dic2 objectForKey:@"U_NM"], @"uname",[dic2 objectForKey:@"T_NM"], @"t_nm" , [dic2 objectForKey:@"U_UNIT"], @"uunit" ,nil];
						[flist insertObject:dic1 atIndex:order+i-3+1];
					}
					break;	
				}
				case 2:{
					if (list.count<10) count = list.count;
					else count = 10;
					for (int i=6; i<count; i++) {
						NSDictionary *dic2 = [list objectAtIndex:i];
						NSString *content =[NSString stringWithFormat:@"%@", [dic2 objectForKey:@"U_NM"]];
						NSDictionary *dic1 = [NSDictionary dictionaryWithObjectsAndKeys:content, @"content",@"detail",@"type",[dic2 objectForKey:@"U_CODE"], @"ucode", [dic2 objectForKey:@"U_NM"], @"uname",[dic2 objectForKey:@"T_NM"], @"t_nm" , [dic2 objectForKey:@"U_UNIT"], @"uunit" ,nil];
						[flist insertObject:dic1 atIndex:order+i-6+1];
					}
					break;	
				}
							default:
					break;
			}
			
			selectedRow = indexPath.row;
			[tableView reloadData];
		}else {
			selectedRow = -1;
			[tableView reloadData];
		}
	} 
	else {
		RecResultViewController *viewController = [[RecResultViewController alloc] init];
		viewController.type	= type;
		NSDictionary *dic = [flist objectAtIndex:indexPath.row];
		viewController.univ = [dic objectForKey:@"uname"];
		viewController.univCode = [dic objectForKey:@"ucode"];
		
		//viewController.unit = unit;
		viewController.ID = ID;
		
		viewController.series03 = series03;
		viewController.series02 = series02;
		viewController.series01 = series01;
		[self.navigationController pushViewController:viewController animated:YES];
	}
}

- (NSDictionary *)findWithArea:(NSString*)area1 array:(NSArray*)arr {
for (NSDictionary *dic in arr) {
	if ([[dic objectForKey:@"area"] isEqualToString:[area1 lowercaseString]])
		return dic;
}
return nil;
}

- (BOOL)chekProvisoWithGrades:(NSArray*)grades proviso:(NSString*)proviso{
	NSDictionary *userDic = [User userInfoWithID:ID];
	NSString *hCate = [userDic objectForKey:@"P_Cate"];
	
	BOOL isSuccess=YES;
	double study;
	double study1=[[grades objectAtIndex:3] doubleValue];
	double study2=[[grades objectAtIndex:4] doubleValue];
	
	
	if ([proviso isEqualToString:@""]) return isSuccess;
	
	NSString *a = [proviso substringWithRange:NSMakeRange(0, 1)];
	NSString *b = [proviso substringWithRange:NSMakeRange(1, 1)];
	NSString *c = [proviso substringWithRange:NSMakeRange(2, 1)];
	NSString *d = [proviso substringWithRange:NSMakeRange(3, 1)];
	int scount =  [[proviso substringWithRange:NSMakeRange(4, 1)] intValue];
	NSString *gradeType = [proviso substringWithRange:NSMakeRange(5, 1)];
	int acount =  [[proviso substringWithRange:NSMakeRange(7, 1)] intValue];
	NSString *gradeType1 =[proviso substringWithRange:NSMakeRange(8, 1)]; 
	int calcResult =  [[proviso substringWithRange:NSMakeRange(9, 2)] intValue];
	
	NSString *r1 = [proviso substringWithRange:NSMakeRange(12, 1)];
	NSString *r2 = [proviso substringWithRange:NSMakeRange(13, 1)];
	NSString *r3 = [proviso substringWithRange:NSMakeRange(14, 1)];
	
	if (scount==0) {
		study = 0;
	}
	if (scount==1) {
		if (study1<=study2) study = study1;
		else study = study2;
	} else if (scount ==2) {
		if ([gradeType isEqualToString:@"p"]) {
			study = (study1+study2)/2;
		} else {
			if (study1<=study2) study = study2;
			else study = study1;
		}
	}
	
	NSMutableArray *arrGrades = [NSMutableArray array];
	if (![a isEqualToString:@"n"]) [arrGrades addObject: [NSDictionary dictionaryWithObjectsAndKeys: [grades objectAtIndex:0],@"value", @"a", @"area", nil]];
	if (![b isEqualToString:@"n"]) [arrGrades addObject:[NSDictionary dictionaryWithObjectsAndKeys: [grades objectAtIndex:1],@"value", @"b", @"area", nil]];
	if (![c isEqualToString:@"n"]) [arrGrades addObject:[NSDictionary dictionaryWithObjectsAndKeys: [grades objectAtIndex:2],@"value", @"c", @"area", nil]];
	if (![d isEqualToString:@"n"]) [arrGrades addObject:[NSDictionary dictionaryWithObjectsAndKeys: [NSNumber numberWithDouble:study],@"value", @"d", @"area", nil]];
	
	NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"value" ascending:YES];
	NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
	NSMutableArray *sortedArray = [NSMutableArray arrayWithArray:[arrGrades sortedArrayUsingDescriptors:sortDescriptors]];
	
	
	if ([hCate isEqualToString:@"문과"] &&([b isEqualToString:@"B"] || [d isEqualToString:@"D"] )) return NO;
	
	if ([gradeType1 isEqualToString:@"g"]) {
		int passCount=0;
		for (int i = 0; i<sortedArray.count; i++) {
			if(calcResult >= [[[sortedArray objectAtIndex:i] objectForKey:@"value"] doubleValue]) passCount+=1;
		}
		if (passCount<acount) isSuccess = NO;				 
		
		if (![r1 isEqualToString:@"n"] && [r2 isEqualToString:@"n"]) {
			NSDictionary *dic=[self findWithArea:r1 array:sortedArray];
			if ([[dic objectForKey:@"value"] doubleValue]>calcResult) isSuccess = NO;
		} else if(![r1 isEqualToString:@"n"] && ![r2 isEqualToString:@"n"]) {
			if ([r3 isEqualToString:@"1"]) {
				NSDictionary *dic1=[self findWithArea:r1 array:sortedArray];
				NSDictionary *dic2=[self findWithArea:r2 array:sortedArray];
				if (!([[dic1 objectForKey:@"value"] doubleValue]<=calcResult || [[dic2 objectForKey:@"value"] doubleValue]<=calcResult)) isSuccess = NO;
			} else 	if ([r3 isEqualToString:@"2"]) {
				NSDictionary *dic1=[self findWithArea:r1 array:sortedArray];
				NSDictionary *dic2=[self findWithArea:r2 array:sortedArray];
				if (!([[dic1 objectForKey:@"value"] doubleValue]<=calcResult && [[dic2 objectForKey:@"value"] doubleValue]<=calcResult)) isSuccess = NO;
			}			 
		}
	} else if([gradeType1 isEqualToString:@"p"]) {
		if ([r1 isEqualToString:@"n"]) {
			double sum=0;
			for (int i=0; i<acount; i++) {
				sum = sum + [[[sortedArray objectAtIndex:i] objectForKey:@"value"] doubleValue];
			}
			double avg = sum/acount;
			if (avg>calcResult) isSuccess = NO;
		} if (![r1 isEqualToString:@"n"] && [r2 isEqualToString:@"n"]){
			NSDictionary *dic=[self findWithArea:r1 array:sortedArray]; 
			[sortedArray removeObject:dic];
			double sum=0;
			for (int i=0; i<acount-1; i++) {
				sum = sum + [[[sortedArray objectAtIndex:i] objectForKey:@"value"] doubleValue];
			}
			sum = sum+[[dic objectForKey:@"value"] doubleValue];
			double avg = sum/acount;
			if (avg>calcResult) isSuccess = NO;
		} if (![r1 isEqualToString:@"n"] && ![r2 isEqualToString:@"n"]){		
			if ([r3 isEqualToString:@"1"]) {
				BOOL tempCheck = NO;
				
				NSMutableArray *temp = [sortedArray copy];
				
				NSDictionary *dic=[self findWithArea:r1 array:temp]; 
				[temp removeObject:dic];
				double sum=0;
				for (int i=0; i<acount-1; i++) {
					sum = sum + [[[temp objectAtIndex:i] objectForKey:@"value"] doubleValue];
				}
				sum = sum+[[dic objectForKey:@"value"] doubleValue];
				double avg = sum/acount;
				if (avg<=calcResult) tempCheck = YES;
				
				if (!tempCheck) {
					NSMutableArray *temp = [sortedArray copy];
					
					NSDictionary *dic=[self findWithArea:r2 array:temp]; 
					[temp removeObject:dic];
					double sum=0;
					for (int i=0; i<acount-1; i++) {
						sum = sum + [[[temp objectAtIndex:i] objectForKey:@"value"] doubleValue];
					}
					sum = sum+[[dic objectForKey:@"value"] doubleValue];
					avg = sum/acount;
					if (avg<=calcResult) tempCheck = YES;
				}
				if (!tempCheck) isSuccess = NO;
			} else 	if ([r3 isEqualToString:@"2"]) {
				NSDictionary *dic=[self findWithArea:r1 array:sortedArray]; 
				[sortedArray removeObject:dic];
				double sum=[[dic objectForKey:@"value"] doubleValue];
				
				dic=[self findWithArea:r2 array:sortedArray]; 
				[sortedArray removeObject:dic];
				sum=sum+[[dic objectForKey:@"value"] doubleValue];
				
				for (int i=0; i<acount-2; i++) {
					sum = sum + [[[sortedArray objectAtIndex:i] objectForKey:@"value"] doubleValue];
				}
				double avg = sum/acount;
				if (avg>calcResult) isSuccess = NO;
			}	
		}
	} else if([gradeType1 isEqualToString:@"s"]) {
		if ([r1 isEqualToString:@"n"]) {
			double sum=0;
			for (int i=0; i<acount; i++) {
				sum = sum + [[[sortedArray objectAtIndex:i] objectForKey:@"value"] doubleValue];
			}
			if (sum>calcResult) isSuccess = NO;
		} if (![r1 isEqualToString:@"n"] && [r2 isEqualToString:@"n"]){
			NSDictionary *dic=[self findWithArea:r1 array:sortedArray]; 
			[sortedArray removeObject:dic];
			double sum=0;
			for (int i=0; i<acount-1; i++) {
				sum = sum + [[[sortedArray objectAtIndex:i] objectForKey:@"value"] doubleValue];
			}
			sum = sum+[[dic objectForKey:@"value"] doubleValue];
			if (sum>calcResult) isSuccess = NO;
		} if (![r1 isEqualToString:@"n"] && ![r2 isEqualToString:@"n"]){		
			if ([r3 isEqualToString:@"1"]) {
				BOOL tempCheck = NO;
				NSMutableArray *temp = [sortedArray copy];
				NSDictionary *dic=[self findWithArea:r1 array:temp]; 
				[temp removeObject:dic];
				double sum=0;
				for (int i=0; i<acount-1; i++) {
					sum = sum + [[[temp objectAtIndex:i] objectForKey:@"value"] doubleValue];
				}
				sum = sum+[[dic objectForKey:@"value"] doubleValue];
				
				if (sum<=calcResult) tempCheck = YES;
				
				if (!tempCheck) {
					NSMutableArray *temp = [sortedArray copy];
					
					NSDictionary *dic=[self findWithArea:r2 array:temp]; 
					[temp removeObject:dic];
					sum=0;
					for (int i=0; i<acount-1; i++) {
						sum = sum + [[[temp objectAtIndex:i] objectForKey:@"value"] doubleValue];
					}
					sum = sum+[[dic objectForKey:@"value"] doubleValue];
					if (sum<=calcResult) tempCheck = YES;
				}
				if (!tempCheck) isSuccess = NO;
			} else 	if ([r3 isEqualToString:@"2"]) {
				NSDictionary *dic=[self findWithArea:r1 array:sortedArray]; 
				[sortedArray removeObject:dic];
				double sum=[[dic objectForKey:@"value"] doubleValue];
				dic=[self findWithArea:r2 array:sortedArray]; 
				[sortedArray removeObject:dic];
				sum=sum+[[dic objectForKey:@"value"] doubleValue];
				
				for (int i=0; i<acount-2; i++) {
					sum = sum + [[[sortedArray objectAtIndex:i] objectForKey:@"value"] doubleValue];
				}
				if (sum>calcResult) isSuccess = NO;
			}	
		}
		
	}
	return isSuccess;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	UITouch *touch = [touches anyObject];
	if (touch.view == imageAD ) {
		ADPop *adPop = [[ADPop alloc] initWithFrame:CGRectMake(0, 0, 320, 460)];
		[self.view addSubview:adPop];
		adPop.index = touch.view.tag;
	}
}

@end
