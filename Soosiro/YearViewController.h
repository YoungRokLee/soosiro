//
//  YearViewController.h
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 7. 2..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YearViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *labelTitle;
@property (nonatomic) int menu;
@property (nonatomic) int style;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSString *ID;
@property (strong, nonatomic) IBOutlet UILabel *labelMainTitle;
- (IBAction)actionBack:(id)sender;
- (IBAction)actionNext:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *imageAD;


@end
