//
//  UnivInfo.h
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 6. 16..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UnivInfo : NSObject

+ (NSArray *)univInfoWithType:(NSString*)type tclass:(NSString*)tclass avg:(NSNumber*)avg series03:(NSString*)series03 series02:(NSString*)series02 series01:(NSString*)series01 mf:(NSString *)mf;//

+ (NSArray *)univInfoWithMenu:(int)menu area:(int)area unit:(NSString*)unit type:(NSString *)type;//

+ (NSMutableArray *)univInfoWithMenu:(int)menu area:(int)area unit:(NSString*)unit series03:(NSString*)series03 series02:(NSString*)series02 series01:(NSString*)series01 type:(NSString *)type tclass:(NSString*)tclass mf:(NSString *)mf;//

+ (NSArray *)series02InfoWithMenu:(int)menu univ:(NSString *)univ seriese01:(NSString *)series01 type:(NSString *)type tclass:(NSString*)tclass mf:(NSString *)mf;//

+ (NSMutableArray *)series03InfoWithMenu:(int)menu univ:(NSString *)univ seriese02:(NSString *)series02 type:(NSString *)type mf:(NSString*)mf;//
//+ (NSMutableArray *)series03InfoWithMenu:(int)menu univ:(NSString *)univ seriese02:(NSString *)series02 series01:(NSString*)series01 type:(NSString *)type mf:(NSString *)mf;//
+ (NSMutableArray *)series03InfoWithMenu:(int)menu univ:(NSString *)univ seriese02:(NSString *)series02 series01:(NSString*)series01 type:(NSString *)type tclass:(NSString *)tclass mf:(NSString *)mf;
+ (NSArray *)unitInfoWithMenu:(int)menu univ:(NSString *)univ series03:(NSString *)series03 series02:(NSString *)series02 series01:(NSString *)series01 type:(NSString *)type mf:(NSString*)mf;//
+ (NSArray *)unitInfoWithMenu:(int)menu univ:(NSString *)univ series02:(NSString *)series02 series01:(NSString *)series01 type:(NSString *)type mf:(NSString*)mf;//
+ (NSMutableArray *)tclassInfoWithMenu:(int)menu univ:(NSString *)univ series03:(NSString *)series03 series02:(NSString *)series02 series01:(NSString *)series01;//

+ (NSArray *)tnameInfoWithMenu:(int)menu univ:(NSString *)univ tclass:(NSString *)tclass;//
+ (NSDictionary *)typeInfoWithType:(NSString*)type;//
+ (NSDictionary *)typeDescriptionWithUniv:(NSString*)univ unit:(NSString*)unit type:(NSString*)type;//
+ (NSArray *)typesInfoWithTypes:(NSArray *)types avg:(NSNumber*)avg  unit:(NSString*)unit ;//
+ (NSArray *)typesInfoWithTypes:(NSArray*)types avg:(NSNumber*)avg series03:(NSString*)series03 series02:(NSString*)series02 series01:(NSString*)series01 mf:(NSString *)mf;//
+ (NSString *)tuCodeWithUniv:(NSString*)univ unit:(NSString*)unit type:(NSString*)type;//
+ (NSArray *)typeDetailWithTUCode:(NSString *)tuCode ;//
+ (NSDictionary *)typeDetailWithUniv:(NSString*)univ type:(NSString*)type  type1:(NSString*)type1;//
+ (NSArray *)typesDescriptionWithUniv:(NSString*)univ unit:(NSString*)unit type:(NSString*)type; //

+ (NSArray *)typesDescriptionWithUniv:(NSString*)univ avg:(NSNumber*)avg series03:(NSString*)series03 series02:(NSString*)series02 series01:(NSString*)series01 type:(NSString*)type unit:(NSString*)unit;//

+ (NSArray *)typesDescriptionWithMenu:(int)menu univ:(NSString*)univ type:(NSString*)type series03:(NSString*)series03 series02:(NSString*)series02 series01:(NSString*)series01;//

+ (NSArray *)typesDescriptionWithUniv:(NSString*)univ type:(NSString*)type;//

+ (NSMutableArray *)univInfoWithMenu:(int)menu area:(int)area unit:(NSString*)unit series03:(NSString*)series03 series02:(NSString*)series02 series01:(NSString*)series01 type:(NSString *)type mf:(NSString *)mf search:(NSString*)search;//

+ (NSArray *)typeRankWithTypes:(NSArray*)types;//
+ (NSArray *)typeRankWithArray:(NSArray*)types avg:(NSNumber*)avg mf:(NSString *)mf hCate:(NSString*)hCate grades:(NSArray*)grades ID:(NSString*)ID;
+ (NSArray *)typeRankWithArray:(NSArray*)types avg:(NSNumber*)avg series03:(NSString*)series03 series02:(NSString*)series02 series01:(NSString*)series01 mf:(NSString *)mf hCate:(NSString*)hCate grades:(NSArray*)grades ID:(NSString*)ID;//
+ (NSArray *)tuNameWithUniv:(NSString*)univ series03:(NSString*)series03 series02:(NSString*)series02 series01:(NSString*)series01;//
+ (NSArray *)tuNameWithUniv:(NSString*)univ;//
+ (NSString *)provisoWithCode:(NSString*)code;//
+ (NSArray *)univInfoWithType:(NSString*)type avg:(NSNumber*)avg series03:(NSString*)series03 series02:(NSString*)series02 series01:(NSString*)series01 mf:(NSString *)mf;//

//+ (NSArray *)typesDescriptionWithUniv:(NSString*)univ type:(NSString*)type series03:(NSString*)series03 series02:(NSString*)series02 series01:(NSString*)series01;

+ (NSArray *)lcodeWithMenu:(int)menu type:(NSString *)type tclass:(NSString*)tclass series03:(NSString*)series03 series02:(NSString*)series02 series01:(NSString*)series01;//
+ (NSArray *)typesInfoWithType:(NSString*)type avg:(NSNumber*)avg series03:(NSString*)series03 series02:(NSString*)series02 series01:(NSString*)series01 mf:(NSString *)mf;//
@end
