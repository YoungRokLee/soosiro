//
//  UnivInfo.m
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 6. 16..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import "UnivInfo.h"
#import <sqlite3.h>
#import "Proviso.h"
@implementation UnivInfo

+ (NSArray *)univInfoWithMenu:(int)menu area:(int)area unit:(NSString*)unit type:(NSString *)type{
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"mysql.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	NSMutableArray *result = [NSMutableArray array];
	sqlite3_stmt *statement;

	char *sql;
	switch (menu) {
		case 1:{
			switch (area) {
				case 0:{
					sql ="Select distinct i.U_code, i.U_Nm from '05_UInfo' i, '04_Univ' u where i.U_code=u.U_code and  u.L_code  = 'L01' and i.T_Nm = ? order by i.U_Nm";
					break;
				}
				case 1:{
					sql ="Select distinct i.U_code, i.U_Nm from '05_UInfo' i, '04_Univ' u where i.U_code=u.U_code and  u.L_code in ('L03', 'L08') and i.T_Nm = ? order by i.U_Nm";
					break;
				}
				case 2:{
					sql ="Select distinct i.U_code, i.U_Nm from '05_UInfo' i, '04_Univ' u where i.U_code=u.U_code and  u.L_code = 'L15' and i.T_Nm = ? order by i.U_Nm";
					
					break;
				}
				case 3:{
					sql ="Select distinct i.U_code, i.U_Nm from '05_UInfo' i, '04_Univ' u where i.U_code=u.U_code and  u.L_code in ('L04', 'L09', 'L10') and i.T_Nm = ? order by i.U_Nm";
					break;
				}
				case 4:{
					sql ="Select distinct i.U_code, i.U_Nm from '05_UInfo' i, '04_Univ' u where i.U_code=u.U_code and  u.L_code in ('L02', 'L05', 'L07', 'L11', 'L12') and i.T_Nm = ? order by i.U_Nm";
					break;
				}
				case 5:{
					sql ="Select distinct i.U_code, i.U_Nm from '05_UInfo' i, '04_Univ' u where i.U_code=u.U_code and  u.L_code in ('L06', 'L13', 'L14', 'L16') and i.T_Nm = ? order by i.U_Nm";
					break;
				}
				default:{
					sql ="Select distinct i.U_code, i.U_Nm from '05_UInfo' i, '04_Univ' u where i.U_code=u.U_code and  u.L_code  = 'L01' and i.T_Nm = ? order by i.U_Nm";
					break;
				}
			}

			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,   1, [type UTF8String],	-1, SQLITE_TRANSIENT);
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"code",
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)], @"name",
										 nil];
					[result addObject:dic];
				}
			}
			break;
		}
		case 2:{
			switch (area) {
				case 0:{
					sql ="Select distinct i.U_code, i.U_Nm from '05_UInfo' i, '04_Univ' u where i.U_code=u.U_code and  u.L_code  = 'L01' and i.U_unit = ? order by i.U_Nm";
					break;
				}
				case 1:{
					sql ="Select distinct i.U_code, i.U_Nm from '05_UInfo' i, '04_Univ' u where i.U_code=u.U_code and  u.L_code in ('L03', 'L08') and i.U_unit = ? order by i.U_Nm";
					break;
				}
				case 2:{
					sql ="Select distinct i.U_code, i.U_Nm from '05_UInfo' i,  '04_Univ' u where i.U_code=u.U_code and  u.L_code = 'L15' and i.U_unit = ? order by i.U_Nm";
					
					break;
				}
				case 3:{
					sql ="Select distinct i.U_code, i.U_Nm from '05_UInfo' i,  '04_Univ' u where i.U_code=u.U_code and  u.L_code in ('L04', 'L09', 'L10') and i.U_unit = ? order by i.U_Nm";
					break;
				}
				case 4:{
					sql ="Select distinct i.U_code, i.U_Nm from '05_UInfo' i,  '04_Univ' u where i.U_code=u.U_code and  u.L_code in ('L02', 'L05', 'L07', 'L11', 'L12') and i.U_unit = ? order by i.U_Nm";
					break;
				}
				case 5:{
					sql ="Select distinct i.U_code, i.U_Nm from '05_UInfo' i,  '04_Univ' u where i.U_code=u.U_code and  u.L_code in ('L06', 'L13', 'L14', 'L16') and i.U_unit = ? order by i.U_Nm";
					break;
				}
				default:{
					sql ="Select distinct i.U_code, i.U_Nm from '05_UInfo' i,  '04_Univ' u where i.U_code=u.U_code and  u.L_code  = 'L01' and i.U_unit = ? order by i.U_Nm";
					break;
				}
			}
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,   1, [unit UTF8String],	-1, SQLITE_TRANSIENT);
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"code",
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)], @"name",
										 nil];
					[result addObject:dic];
				}
			}
			break;
		}
		case 4:{
//			sql ="Select distinct i.U_code, i.U_Nm from '05_UInfo' i, '04_Univ' u where i.U_code=u.U_code and i.U_unit = ? and i.T_Nm = ? order by i.U_Nm";
			sql ="SELECT distinct i.u_code, i.u_nm FROM '05_UInfo' i, '04_Univ' u where u.u_code = i.u_code and i.u_unit=? and  i.T_Nm=? order by u.U_RAN asc";
			
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,   1, [unit UTF8String],	-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   2, [type UTF8String],	-1, SQLITE_TRANSIENT);
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"code",
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)], @"name",
										 nil];
					[result addObject:dic];
				}
			}
			break;
		}
		default:{
			switch (area) {
				case 0:{
					sql ="Select distinct i.U_code, i.U_Nm from '05_UInfo' i,  '04_Univ' u where i.U_code=u.U_code and  u.L_code  = 'L01' order by i.U_Nm";
					break;
				}
				case 1:{
					sql ="Select distinct i.U_code, i.U_Nm from '05_UInfo' i,  '04_Univ' u where i.U_code=u.U_code and  u.L_code in ('L03', 'L08') order by i.U_Nm";
					break;
				}
				case 2:{
					sql ="Select distinct i.U_code, i.U_Nm from '05_UInfo' i,  '04_Univ' u where i.U_code=u.U_code and  u.L_code  = 'L15' order by i.U_Nm";
					break;
				}
				case 3:{
					sql ="Select distinct i.U_code, i.U_Nm from '05_UInfo' i,  '04_Univ' u where i.U_code=u.U_code and  u.L_code in ('L04', 'L09', 'L10') order by i.U_Nm";
					break;
				}
				case 4:{
					sql ="Select distinct i.U_code, i.U_Nm from '05_UInfo' i,  '04_Univ' u where i.U_code=u.U_code and  u.L_code in ('L02', 'L05', 'L07', 'L11', 'L12') order by i.U_Nm";
					break;
				}
				case 5:{
					sql ="Select distinct i.U_code, i.U_Nm from '05_UInfo' i,  '04_Univ' u where i.U_code=u.U_code and  u.L_code in ('L06', 'L13', 'L14', 'L16')  order by i.U_Nm";
					break;
				}
				default:{
					sql ="Select distinct i.U_code, i.U_Nm from '05_UInfo' i,  '04_Univ' u where i.U_code=u.U_code and  u.L_code  = 'L01' order by i.U_Nm";
					break;
				}
			}
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"code",
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)], @"name",
										 nil];
					[result addObject:dic];
				}
			}
			break;
		}
		sqlite3_finalize(statement);
		sqlite3_close(database);

	}
	return result;
}

+ (NSString *)areaCode:(int)area {
    NSString *localCode=@"";
    if (area==0) localCode = @"('L01')";
	else if (area==1) localCode = @"('L03', 'L08')";
	else if (area==2) localCode = @"('L15')";
	else if (area==3) localCode = @"('L04', 'L09', 'L10')";
	else if (area==4) localCode = @"('L02', 'L05', 'L07', 'L11', 'L12')";
	else if (area==5) localCode = @"('L06', 'L13', 'L14', 'L16')";
    return localCode;
}

+ (NSMutableArray *)univInfoWithMenu:(int)menu area:(int)area unit:(NSString*)unit series03:(NSString*)series03 series02:(NSString*)series02 series01:(NSString*)series01 type:(NSString *)type tclass:(NSString*)tclass mf:(NSString *)mf{
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"mysql.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	
	NSLog(@" %@ %@ %@ ", series03, series02, series01);
	NSString *localCode = [self areaCode:area];
	NSMutableArray *result = [NSMutableArray array];
	sqlite3_stmt *statement;
	
	switch (menu) {
		case 0:{
			NSString *query = [NSString stringWithFormat:
							   @"Select distinct i.U_code, i.U_Nm from '05_UInfo' i, '04_Univ' u where i.U_code=u.U_code and  u.L_code in %@ Order by i.U_Nm", localCode];
			const char *sql=[query UTF8String];
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"code",
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)], @"name",
										 nil];
					[result addObject:dic];
				}
			}
			break;
		}
		case 1:{
			NSString *query = [NSString stringWithFormat:
							   @"Select distinct i.U_code, i.U_Nm from '05_UInfo' i, '04_Univ' u where i.U_code=u.U_code and  u.L_code in %@ and i.T_Nm = ? and i.T_Class = ? order by i.U_Nm ", localCode];
			const char *sql=[query UTF8String];
						
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,   1, [type UTF8String],	-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   2, [tclass UTF8String],	-1, SQLITE_TRANSIENT);
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"code",
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)], @"name",
										 nil];
					[result addObject:dic];
				}
			}
			break;
		}
		case 2:{
			NSString *query = [NSString stringWithFormat:
							   @"Select distinct i.U_code, i.U_Nm from '05_UInfo' i, '04_Univ' u where i.U_code=u.U_code and  u.L_code in %@ and i.u_ser03=? and i.u_ser02=? and i.u_ser01=? order by i.U_Nm ", localCode];
			
			NSLog(@"%@", query);
			const char *sql=[query UTF8String];
			
						if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
							sqlite3_bind_text(statement,   1, [series03 UTF8String],	-1, SQLITE_TRANSIENT);
							sqlite3_bind_text(statement,   2, [series02 UTF8String],	-1, SQLITE_TRANSIENT);
							sqlite3_bind_text(statement,   3, [series01 UTF8String],	-1, SQLITE_TRANSIENT);
							
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"code",
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)], @"name",
										 nil];
					[result addObject:dic];
				}
			}
			break;
		}
		case 4:{
			char *sql;
			if ([mf isEqualToString:@"M"])
				sql ="SELECT distinct i.u_code, i.u_nm FROM '05_UInfo' i, '04_Univ' u where u.u_code = i.u_code and i.u_ser03=? and i.u_ser02=? and i.u_ser01=? and  i.T_Nm=? and u.u_af='A' order by u.U_RAN asc";
			else 
				sql ="SELECT distinct i.u_code, i.u_nm FROM '05_UInfo' i, '04_Univ' u where u.u_code = i.u_code and i.u_ser03=? and i.u_ser02=? and i.u_ser01=? and  i.T_Nm=? order by u.U_RAN asc";
			
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,   1, [series03 UTF8String],	-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   2, [series02 UTF8String],	-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   3, [series01 UTF8String],	-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   4, [type UTF8String],	-1, SQLITE_TRANSIENT);
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"code",
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)], @"name",
										 nil];
					[result addObject:dic];
				}
			}
			break;
		}
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	return result;
}

+ (NSMutableArray *)univInfoWithMenu:(int)menu area:(int)area unit:(NSString*)unit series03:(NSString*)series03 series02:(NSString*)series02 series01:(NSString*)series01 type:(NSString *)type mf:(NSString *)mf search:(NSString*)search{
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"mysql.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	NSMutableString *searchWord = [NSMutableString stringWithString:@"'%"];
	[searchWord appendString:search];
	[searchWord appendString:@"%'"];
	NSLog(@"searchword = %@", searchWord);
	
	NSString *localCode = [self areaCode:area];
	NSMutableArray *result = [NSMutableArray array];
	sqlite3_stmt *statement;
	
	switch (menu) {
		case 0:{
			NSString *query = [NSString stringWithFormat:
							   @"Select distinct i.U_code, i.U_Nm from '05_UInfo' i, '04_Univ' u where i.U_code=u.U_code and  u.L_code in %@ and i.U_Nm like %@ Order by i.U_Nm", localCode, searchWord];
			const char *sql=[query UTF8String];
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"code",
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)], @"name",
										 nil];
					[result addObject:dic];
				}
			}
			break;
		}
		case 1:{
			NSString *query = [NSString stringWithFormat:
							   @"Select distinct i.U_code, i.U_Nm from '05_UInfo' i, '04_Univ' u where i.U_code=u.U_code and  u.L_code in %@ and i.U_Nm like %@ and i.T_Nm = ? Order by i.U_Nm", localCode, searchWord];
			const char *sql=[query UTF8String];
			
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,   1, [type UTF8String],	-1, SQLITE_TRANSIENT);
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"code",
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)], @"name",
										 nil];
					[result addObject:dic];
				}
			}
			break;
		}
		case 2:{
			NSString *query = [NSString stringWithFormat:
							   @"Select distinct i.U_code, i.U_Nm from '05_UInfo' i, '04_Univ' u where i.U_code=u.U_code and  u.L_code in %@ and i.u_ser03=? and i.u_ser02=? and i.u_ser01=? and i.U_Nm like %@  Order by i.U_Nm", localCode, searchWord];
			const char *sql=[query UTF8String];
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,   1, [series03 UTF8String],	-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   2, [series02 UTF8String],	-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   3, [series01 UTF8String],	-1, SQLITE_TRANSIENT);
				
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"code",
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)], @"name",
										 nil];
					[result addObject:dic];
				}
			}
			break;
		}
		case 4:{
			
			NSString *query;
			if ([mf isEqualToString:@"M"])
				query= [NSString stringWithFormat:
							   @"SELECT distinct i.u_code, i.u_nm FROM '05_UInfo' i, '04_Univ' u where u.u_code = i.u_code and i.u_ser03=? and i.u_ser02=? and i.u_ser01=? and  i.T_Nm=? and u.u_af='A' and i.U_Nm like %@ order by u.U_RAN asc",  searchWord];
			else query= [NSString stringWithFormat:
						 @"SELECT distinct i.u_code, i.u_nm FROM '05_UInfo' i, '04_Univ' u where u.u_code = i.u_code and i.u_ser03=? and i.u_ser02=? and i.u_ser01=? and  i.T_Nm=? and i.U_Nm like %@ order by u.U_RAN asc", searchWord];
			
			NSLog(@"============%@", query);
			const char *sql=[query UTF8String];
			
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,   1, [series03 UTF8String],	-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   2, [series02 UTF8String],	-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   3, [series01 UTF8String],	-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   4, [type UTF8String],	-1, SQLITE_TRANSIENT);
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"code",
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)], @"name",
										 nil];
					[result addObject:dic];
				}
			}
			break;
		}
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	return result;
}


+ (NSArray *)series02InfoWithMenu:(int)menu univ:(NSString *)univ seriese01:(NSString *)series01 type:(NSString *)type tclass:(NSString*)tclass mf:(NSString *)mf{
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"mysql.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	
	NSMutableArray *result = [NSMutableArray array];
	sqlite3_stmt *statement;
	
	switch (menu) {
		case 0:{
			char *sql ="Select distinct U_ser02 from '05_UInfo' where U_code=? and U_ser01=? ";
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,   1, [univ UTF8String],	-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   2, [series01 UTF8String],-1, SQLITE_TRANSIENT);
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSString *series = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
					[result addObject:series];
				}
			}
			break;
		}
		case 1:{
			char *sql ="Select distinct U_ser02 from '05_UInfo' where U_code=? and U_ser01=? and T_Nm =? and T_Class =?";
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,   1, [univ UTF8String],	-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   2, [series01 UTF8String],-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   3, [type UTF8String],-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   4, [tclass UTF8String],-1, SQLITE_TRANSIENT);
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSString *series = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
					[result addObject:series];
				}
			}
			break;
		}
		case 2:{
			char *sql ="Select distinct U_ser02 from '05_UInfo' where U_ser01=?";
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,  1, [series01 UTF8String],-1, SQLITE_TRANSIENT);
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSString *series = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
					[result addObject:series];
				}
			}
			break;
		}
		case 4:{
			char *sql;
			if ([mf isEqualToString:@"M"])
				sql ="Select distinct U_ser02 from '05_UInfo' i, '04_Univ' u where i.U_Code = u.U_Code and T_Nm=? and U_ser01=? and u.u_af='A'";
			else 
				sql ="Select distinct U_ser02 from '05_UInfo' where T_Nm=? and U_ser01=? ";
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,   1, [type UTF8String],	-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   2, [series01 UTF8String],-1, SQLITE_TRANSIENT);
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSString *series = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
					[result addObject:series];
				}
			}
			break;
		}
		case 5:{
			NSString *query;
			const char *sql;
			if ([mf isEqualToString:@"M"])
				query = [NSString stringWithFormat:
							   @"Select distinct U_ser02 from '05_UInfo' i, '04_Univ' u where i.U_Code = u.U_Code and T_Nm in (%@) and U_ser01=? and u.u_af='A'", type];
			else 
				query = [NSString stringWithFormat:
						 @"Select distinct U_ser02 from '05_UInfo' i, '04_Univ' u where i.U_Code = u.U_Code and T_Nm in (%@) and U_ser01=? ", type];
			sql=[query UTF8String];
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,   1, [series01 UTF8String],-1, SQLITE_TRANSIENT);
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSString *series = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
					[result addObject:series];
				}
			}
			break;
		}

		default:{
			char *sql ="Select distinct U_ser02 from '05_UInfo' where U_ser01=?";
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,  1, [series01 UTF8String],-1, SQLITE_TRANSIENT);
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSString *series = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
					[result addObject:series];
				}
			}
			break;

		}
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);

	return result;
}


+ (NSMutableArray *)series03InfoWithMenu:(int)menu univ:(NSString *)univ seriese02:(NSString *)series02 series01:(NSString*)series01 type:(NSString *)type tclass:(NSString *)tclass mf:(NSString *)mf{
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"mysql.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	
	int i=0;
	NSMutableArray *result = [NSMutableArray array];
	sqlite3_stmt *statement;
	switch (menu) {
		case 0:{
			char *sql ="Select  distinct U_ser03 from '05_UInfo' where U_code=? and U_ser02=? and U_ser01=? ";
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,   1, [univ UTF8String],	-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   2, [series02 UTF8String],-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   3, [series01 UTF8String],-1, SQLITE_TRANSIENT);
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"content",
										 @"master", @"type",
										 [NSNumber numberWithInt:i],@"order",
										 nil];
					i++;
					[result addObject:dic];
				}
			}
			break;
		}
		case 1:{
			char *sql ="Select  distinct U_ser03 from '05_UInfo' where U_code=? and U_ser02=? and U_ser01=? and T_Nm=? and T_Class=?";
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,   1, [univ UTF8String],	-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   2, [series02 UTF8String],-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   3, [series01 UTF8String],-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   4, [type UTF8String],-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   5, [tclass UTF8String],-1, SQLITE_TRANSIENT);
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"content",
										 @"master", @"type",
										 [NSNumber numberWithInt:i],@"order",
										 nil];
					i++;
					[result addObject:dic];
				}
			}
			break;
		}
		case 2:{
			char *sql ="Select distinct U_ser03 from '05_UInfo' where U_ser02=? and U_ser01=? ";
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,   1, [series02 UTF8String],-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   2, [series01 UTF8String],-1, SQLITE_TRANSIENT);
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"content",
										 @"master", @"type",
										 [NSNumber numberWithInt:i],@"order",
										 nil];
					i++;
					[result addObject:dic];
				}
			}
			break;
		}
		case 3:{
			char *sql;
			if ([mf isEqualToString:@"M"])
				sql ="Select distinct U_ser03 from '05_UInfo' i, '04_Univ' u where i.U_Code = u.U_Code and U_ser02=? and U_ser01=? and u.u_af='A'";
			else 
				sql ="Select distinct U_ser03 from '05_UInfo' where U_ser02=? and U_ser01=?";
			
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,   1, [series02 UTF8String],-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   2, [series01 UTF8String],	-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   3, [type UTF8String],	-1, SQLITE_TRANSIENT);
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"content",
										 @"master", @"type",
										 [NSNumber numberWithInt:i],@"order",
										 nil];
					i++;
					[result addObject:dic];
				}
			}
			break;
		}

		case 4:{
			char *sql;
			if ([mf isEqualToString:@"M"])
				sql ="Select distinct U_ser03 from '05_UInfo' i, '04_Univ' u where i.U_Code = u.U_Code and U_ser02=? and U_ser01=? and T_Nm=?  and u.u_af='A'";
			else 
				sql ="Select distinct U_ser03 from '05_UInfo' where U_ser02=? and U_ser01=? and T_Nm=?";
			
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,   1, [series02 UTF8String],-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   2, [series01 UTF8String],	-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   3, [type UTF8String],	-1, SQLITE_TRANSIENT);
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"content",
										 @"master", @"type",
										 [NSNumber numberWithInt:i],@"order",
										 nil];
					i++;
					[result addObject:dic];
				}
			}
			break;
		}
		case 5:{
			
			NSString *query;
			const char *sql;
			if ([mf isEqualToString:@"M"])
				query = [NSString stringWithFormat:
						 @"Select distinct U_ser03 from '05_UInfo' i, '04_Univ' u where i.U_Code = u.U_Code and U_ser02=? and U_ser01=? and T_Nm in (%@) and u.u_af='A'", type];
			
			else 
				query = [NSString stringWithFormat:
						 @"Select distinct U_ser03 from '05_UInfo' i, '04_Univ' u where i.U_Code = u.U_Code and U_ser02=? and U_ser01=? and T_Nm in (%@)", type];
			NSLog(@"%@", query);
			sql=[query UTF8String];
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,   1, [series02 UTF8String],-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   2, [series01 UTF8String],	-1, SQLITE_TRANSIENT);
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"content",
										 @"master", @"type",
										 [NSNumber numberWithInt:i],@"order",
										 nil];
					i++;
					[result addObject:dic];
				}
			}
			break;
		}


		default:{
			char *sql ="Select distinct U_ser03 from '05_UInfo' where U_ser02=?";
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,   1, [series02 UTF8String],-1, SQLITE_TRANSIENT);
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"content",
										 @"master", @"type",
										 [NSNumber numberWithInt:i],@"order",
										 nil];
					i++;
					[result addObject:dic];
				}
			}
			break;
		}
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);

	return result;
}


+ (NSMutableArray *)series03InfoWithMenu:(int)menu univ:(NSString *)univ seriese02:(NSString *)series02 type:(NSString *)type mf:(NSString *)mf{
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"mysql.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	
	int i=0;
	NSMutableArray *result = [NSMutableArray array];
	sqlite3_stmt *statement;
	switch (menu) {
		case 0:{
			char *sql ="Select  distinct U_ser03 from '05_UInfo' where U_code=? and U_ser02=?  ";
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,   1, [univ UTF8String],	-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   2, [series02 UTF8String],-1, SQLITE_TRANSIENT);
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"content",
										 @"master", @"type",
										 [NSNumber numberWithInt:i],@"order",
										 nil];
					i++;
					[result addObject:dic];
				}
			}
			break;
		}
		case 1:{
			char *sql ="Select  distinct U_ser03 from '05_UInfo' where U_code=? and U_ser02=? and T_Nm=? ";
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,   1, [univ UTF8String],	-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   2, [series02 UTF8String],-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   3, [type UTF8String],-1, SQLITE_TRANSIENT);
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"content",
										 @"master", @"type",
										 [NSNumber numberWithInt:i],@"order",
										 nil];
					i++;
					[result addObject:dic];
				}
			}
			break;
		}
		case 2:{
			char *sql ="Select distinct U_ser03 from '05_UInfo' where U_ser02=?";
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,   1, [series02 UTF8String],-1, SQLITE_TRANSIENT);
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"content",
										 @"master", @"type",
										 [NSNumber numberWithInt:i],@"order",
										 nil];
					i++;
					[result addObject:dic];
				}
			}
			break;
		}
		case 4:{
			char *sql;
			if ([mf isEqualToString:@"M"])
				sql ="Select distinct U_ser03 from '05_UInfo' i, '04_Univ' u where i.U_Code = u.U_Code and T_Nm=? and U_ser02=? and u.u_af='A'";
			else 
				sql ="Select distinct U_ser03 from '05_UInfo' where U_ser02=? and T_Nm=?";
			
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,   1, [series02 UTF8String],-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   2, [type UTF8String],	-1, SQLITE_TRANSIENT);
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"content",
										 @"master", @"type",
										 [NSNumber numberWithInt:i],@"order",
										 nil];
					i++;
					[result addObject:dic];
				}
			}
			break;
		}
			
		default:{
			char *sql ="Select distinct U_ser03 from '05_UInfo' where U_ser02=?";
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,   1, [series02 UTF8String],-1, SQLITE_TRANSIENT);
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"content",
										 @"master", @"type",
										 [NSNumber numberWithInt:i],@"order",
										 nil];
					i++;
					[result addObject:dic];
				}
			}
			break;
		}
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	
	return result;
}

+ (NSArray *)unitInfoWithMenu:(int)menu univ:(NSString *)univ seriese03:(NSString *)series03 type:(NSString *)type mf:(NSString*)mf{
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"mysql.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	
	NSMutableArray *result = [NSMutableArray array];
	sqlite3_stmt *statement;
	
	switch (menu) {
		case 0:{
			char *sql ="Select distinct U_unit from '05_UInfo' where U_code=? and U_ser03=?  ";
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,   1, [univ UTF8String],	-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   2, [series03 UTF8String],-1, SQLITE_TRANSIENT);
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"content",
										 @"detail", @"type",
										 nil];
					
					[result addObject:dic];
				}
			}
			break;
		}
		case 1:{
			char *sql ="Select distinct U_unit from '05_UInfo' where U_code=? and U_ser03=? and T_Nm=?";
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,   1, [univ UTF8String],	-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   2, [series03 UTF8String],-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   3, [type UTF8String],-1, SQLITE_TRANSIENT);
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"content",
										 @"detail", @"type",
										 nil];
					
					[result addObject:dic];
				}
			}
			break;
		}
		case 2:{
			char *sql ="Select distinct U_unit from '05_UInfo' where U_ser03=?  ";
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,   1, [series03 UTF8String],-1, SQLITE_TRANSIENT);
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"content",
										 @"detail", @"type",
										 nil];
					
					[result addObject:dic];
				}
			}
			break;
		}
		case 4:{
			char *sql;
			if ([mf isEqualToString:@"M"])
				sql ="Select distinct U_unit from from '05_UInfo' i, '04_Univ' u where i.U_Code = u.U_Code and T_Nm=? and U_ser03=? and u.u_af='A'";
			else 
				sql ="Select distinct U_unit from '05_UInfo' where U_ser03=?  and T_Nm=? ";
			
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,   1, [series03 UTF8String],-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   2, [type UTF8String],-1, SQLITE_TRANSIENT);
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"content",
										 @"detail", @"type",
										 nil];
					
					[result addObject:dic];
				}
			}
			break;
		}

		default:{
			char *sql ="Select distinct U_unit from '05_UInfo' where U_ser03=? ";
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,   1, [series03 UTF8String],-1, SQLITE_TRANSIENT);
				
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"content",
										 @"detail", @"type",
										 nil];
					
					[result addObject:dic];
				}
			}
			break;
		}
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);

	return result;
}

+ (NSArray *)unitInfoWithMenu:(int)menu univ:(NSString *)univ series03:(NSString *)series03 series02:(NSString *)series02 series01:(NSString *)series01 type:(NSString *)type mf:(NSString*)mf{
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"mysql.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	
	NSMutableArray *result = [NSMutableArray array];
	sqlite3_stmt *statement;
	
	switch (menu) {
		case 0:{
			char *sql ="Select distinct U_unit from '05_UInfo' where U_code=? and U_ser03=? and U_ser02=? and U_ser01=?  ";
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,   1, [univ UTF8String],	-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   2, [series03 UTF8String],-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   3, [series02 UTF8String],-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   4, [series01 UTF8String],-1, SQLITE_TRANSIENT);
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"content",
										 @"detail", @"type",
										 nil];
					
					[result addObject:dic];
				}
			}
			break;
		}
		case 1:{
			char *sql ="Select distinct U_unit from '05_UInfo' where U_code=? and U_ser03=? and U_ser02=? and U_ser01=?  and T_Nm=?";
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,   1, [univ UTF8String],	-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   2, [series03 UTF8String],-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   3, [series02 UTF8String],-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   4, [series01 UTF8String],-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   5, [type UTF8String],-1, SQLITE_TRANSIENT);
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"content",
										 @"detail", @"type",
										 nil];
					
					[result addObject:dic];
				}
			}
			break;
		}
		case 2:{
			char *sql ="Select distinct U_unit from '05_UInfo' where U_ser03=? and U_ser02=? and U_ser01=?   ";
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,   1, [series03 UTF8String],-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   2, [series02 UTF8String],-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   3, [series01 UTF8String],-1, SQLITE_TRANSIENT);
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"content",
										 @"detail", @"type",
										 nil];
					
					[result addObject:dic];
				}
			}
			break;
		}
		case 3:{
			char *sql;
			if ([mf isEqualToString:@"M"])
				sql ="Select distinct U_unit from '05_UInfo' i, '04_Univ' u where i.U_Code = u.U_Code and U_ser03=? and U_ser02=? and U_ser01=? and u.u_af='A'";
			else 
				sql ="Select distinct U_unit from '05_UInfo' where U_ser03=? and U_ser02=? and U_ser01=?  ";
			
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,   1, [series03 UTF8String],-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   2, [series02 UTF8String],-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   3, [series01 UTF8String],-1, SQLITE_TRANSIENT);
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"content",
										 @"detail", @"type",
										 nil];
					
					[result addObject:dic];
				}
			}
			break;
		}

		case 4:{
			char *sql;
			if ([mf isEqualToString:@"M"])
				sql ="Select distinct U_unit from '05_UInfo' i, '04_Univ' u where i.U_Code = u.U_Code and T_Nm=? and U_ser03=? and U_ser02=? and U_ser01=? and u.u_af='A'";
			else 
				sql ="Select distinct U_unit from '05_UInfo' where T_Nm=? and U_ser03=? and U_ser02=? and U_ser01=?  ";
			
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,   1, [type UTF8String],-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   2, [series03 UTF8String],-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   3, [series02 UTF8String],-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   4, [series01 UTF8String],-1, SQLITE_TRANSIENT);
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"content",
										 @"detail", @"type",
										 nil];
					
					[result addObject:dic];
				}
			}
			break;
		}
		case 5:{
			
			NSString *query;
			const char *sql;
			if ([mf isEqualToString:@"M"])
				query = [NSString stringWithFormat:
						 @"Select distinct U_unit from '05_UInfo' i, '04_Univ' u where i.U_Code = u.U_Code and U_ser03=? and U_ser02=? and U_ser01=? and T_Nm in (%@) and u.u_af='A'", type];
			
			else 
				query = [NSString stringWithFormat:
						 @"Select distinct U_unit from '05_UInfo' i, '04_Univ' u where i.U_Code = u.U_Code  U_ser03=? and and U_ser02=? and U_ser01=? and T_Nm in (%@)", type];
			NSLog(@"%@", query);

			sql = [query UTF8String];
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,   1, [series03 UTF8String],-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   2, [series02 UTF8String],-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   3, [series01 UTF8String],-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   4, [type UTF8String],-1, SQLITE_TRANSIENT);
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"content",
										 @"detail", @"type",
										 nil];
					
					[result addObject:dic];
				}
			}
			break;
		}
	
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	
	return result;
}

+ (NSArray *)unitInfoWithMenu:(int)menu univ:(NSString *)univ series02:(NSString *)series02 series01:(NSString *)series01 type:(NSString *)type mf:(NSString*)mf{
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"mysql.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	
	NSMutableArray *result = [NSMutableArray array];
	sqlite3_stmt *statement;
	
	switch (menu) {
		case 0:{
			char *sql ="Select distinct U_unit from '05_UInfo' where U_code=? and U_ser03=?   ";
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,   1, [univ UTF8String],	-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   2, [series02 UTF8String],-1, SQLITE_TRANSIENT);
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"content",
										 @"detail", @"type",
										 nil];
					
					[result addObject:dic];
				}
			}
			break;
		}
		case 1:{
			char *sql ="Select distinct U_unit from '05_UInfo' where U_code=? and U_ser02=? and T_Nm=?";
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,   1, [univ UTF8String],	-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   2, [series02 UTF8String],-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   3, [type UTF8String],-1, SQLITE_TRANSIENT);
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"content",
										 @"detail", @"type",
										 nil];
					
					[result addObject:dic];
				}
			}
			break;
		}
		case 2:{
			char *sql ="Select distinct U_unit from '05_UInfo' where U_ser02=?  ";
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,   1, [series02 UTF8String],-1, SQLITE_TRANSIENT);
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"content",
										 @"detail", @"type",
										 nil];
					
					[result addObject:dic];
				}
			}
			break;
		}
		case 4:{
			char *sql;
			if ([mf isEqualToString:@"M"])
				sql ="Select distinct U_unit from from '05_UInfo' i, '04_Univ' u where i.U_Code = u.U_Code and T_Nm=? and U_ser02=? and U_ser01=? and u.u_af='A'";
			else 
				sql ="Select distinct U_unit from '05_UInfo' where T_Nm=? and U_ser02=? and U_ser01=?  ";
			
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,   1, [type UTF8String],-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   2, [series02 UTF8String],-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   3, [series01 UTF8String],-1, SQLITE_TRANSIENT);
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"content",
										 @"detail", @"type",
										 nil];
					
					[result addObject:dic];
				}
			}
			break;
		}
			
		default:{
			char *sql ="Select distinct U_unit from '05_UInfo' where U_ser02=? ";
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,   1, [series02 UTF8String],-1, SQLITE_TRANSIENT);
				
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"content",
										 @"detail", @"type",
										 nil];
					
					[result addObject:dic];
				}
			}
			break;
		}
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	
	return result;
}
+ (NSMutableArray *)tclassInfoWithMenu:(int)menu univ:(NSString *)univ series03:(NSString *)series03 series02:(NSString *)series02 series01:(NSString *)series01 {
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"mysql.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	
	int i=0;
	NSMutableArray *result = [NSMutableArray array];
	sqlite3_stmt *statement;
	char *sql;
	switch (menu) {
		case 0:{
			sql ="Select distinct T_class from '05_UInfo' where U_code=?";
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,   1, [univ UTF8String],	-1, SQLITE_TRANSIENT);
				
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"content",
										 @"master", @"type",
										 [NSNumber numberWithInt:i],@"order",
										 nil];
					i++;
					[result addObject:dic];
				}
			}
			break;
		}
		case 1:{
			sql ="Select distinct T_class from '05_UInfo'";
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"content",
										 @"master", @"type",
										 [NSNumber numberWithInt:i],@"order",
										 nil];
					i++;
					[result addObject:dic];
				}
			}
			break;
		}
		case 2:{
			sql ="Select distinct T_class from '05_UInfo' where U_code=? and U_ser03=? and U_ser02=? and U_ser01=?";
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,   1, [univ UTF8String],	-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   2, [series03 UTF8String],-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   3, [series02 UTF8String],-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   4, [series01 UTF8String],-1,SQLITE_TRANSIENT); 
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"content",
										 @"master", @"type",
										 [NSNumber numberWithInt:i],@"order",
										 nil];
					i++;
					[result addObject:dic];
				}
			}
			break;	
		}
		default:
			break;
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	
	return result;
}

+ (NSArray *)tnameInfoWithMenu:(int)menu univ:(NSString *)univ tclass:(NSString *)tclass {
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"mysql.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	
	
	NSMutableArray *result = [NSMutableArray array];
	sqlite3_stmt *statement;
	char *sql;
	switch (menu) {
		case 0:{
			sql ="Select distinct T_Nm, T_Code from '05_UInfo' where U_code=? and T_class=?";
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,   1, [univ UTF8String],	-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   2, [tclass UTF8String],-1, SQLITE_TRANSIENT);
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"content",
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)], @"tcode",
										 @"detail", @"type",
										 nil];
					[result addObject:dic];
				}
			}

			break;
		}
		case 1:{
			sql ="Select distinct T_Nm from '05_UInfo' where T_class=?";
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,   1, [tclass UTF8String],-1, SQLITE_TRANSIENT);
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"content",
										 @"detail", @"type",
										 nil];
					[result addObject:dic];
				}
			}
			break;
		}
		case 2:{
			sql ="Select distinct T_Nm from '05_UInfo' where U_code=? and T_class=?";
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,   1, [univ UTF8String],	-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   2, [tclass UTF8String],-1, SQLITE_TRANSIENT);
				while (sqlite3_step(statement)==SQLITE_ROW) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"content",
										 @"detail", @"type",
										 nil];
					[result addObject:dic];
				}
			}
			break;	
		}
		default:
			break;
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);

	return result;
}

+ (NSDictionary *)typeInfoWithType:(NSString*)type {
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"mysql.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	
	sqlite3_stmt *statement;
	NSDictionary *result;
	NSString *cata;
	char *sql ="SELECT T_intro, cata, prep FROM '08_tyin' where T_Nm =?";
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_text(statement,   1, [type UTF8String],	-1, SQLITE_TRANSIENT);
		while (sqlite3_step(statement)==SQLITE_ROW) {
						
			if(sqlite3_column_type(statement, 1)!= SQLITE_NULL) cata = [NSString	stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
			else cata = @"";

			result = [NSDictionary dictionaryWithObjectsAndKeys:
					  [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"T_intro",
					  cata, @"cata",
					  [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)], @"prep",
					  nil];
		}	
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);

	return result;
}

+ (NSArray *)typeDetailWithTUCode:(NSString *)tuCode {
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"mysql.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	NSString *ratio;
	sqlite3_stmt *statement;
	NSMutableArray *result=[[NSMutableArray alloc] init];
	const char *sql = "SELECT U_NM, TU_NM, TU_STEP, TU_RATIO FROM '34_TUESS' where TU_CODE = ? Order by TU_STEP";
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_text(statement,   1, [tuCode UTF8String],	-1, SQLITE_TRANSIENT);
		while (sqlite3_step(statement)==SQLITE_ROW) 
		{
			if(sqlite3_column_type(statement, 3)!= SQLITE_NULL) ratio = [NSString	stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)];
			else ratio = @"";
			
			NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
								 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"U_NM",
								 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)], @"TU_NM",
								 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)], @"TU_STEP",
								 [NSString stringWithString:ratio], @"TU_RATIO",
								nil];
			[result addObject:dic];

		}
	}	
	sqlite3_finalize(statement);
	sqlite3_close(database);
	return result;
}

+ (NSString *)arrayofTUCodeWithUniv:(NSString*)univ unit:(NSString*)unit type:(NSString*)type {
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"mysql.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	
	sqlite3_stmt *statement;
	NSString *result;
	char *sql ="SELECT TU_CODE FROM '05_UInfo' where U_code =? and U_unit=? and T_Nm=?";
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_text(statement,   1, [univ UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,   2, [unit UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,   3, [type UTF8String],	-1, SQLITE_TRANSIENT);
		while (sqlite3_step(statement)==SQLITE_ROW) {
			result=[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
		}	
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	return result;

}

+ (NSString *)tuCodeWithUniv:(NSString*)univ unit:(NSString*)unit type:(NSString*)type {
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"mysql.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	
	sqlite3_stmt *statement;
	NSMutableString *result = [NSMutableString string];
	char *sql ="SELECT TU_CODE FROM '05_UInfo' where U_code =? and U_unit=? and T_Nm=?";
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_text(statement,   1, [univ UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,   2, [unit UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,   3, [type UTF8String],	-1, SQLITE_TRANSIENT);
		while (sqlite3_step(statement)==SQLITE_ROW) {
			result=[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
		}	
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	return result;
	
}

+ (NSString *)provisoWithCode:(NSString*)code{
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"mysql.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	
	sqlite3_stmt *statement;
	NSMutableString *result = [NSMutableString string];
	char *sql ="SELECT U_MEA FROM '33_uprov' where U_PROVISO =?";
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_text(statement,   1, [code UTF8String],	-1, SQLITE_TRANSIENT);
		while (sqlite3_step(statement)==SQLITE_ROW) {
			result=[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
		}	
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	return result;
	
}



+ (NSArray *)typesDescriptionWithUniv:(NSString*)univ unit:(NSString*)unit type:(NSString*)type {
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"mysql.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	
	sqlite3_stmt *statement;
	NSMutableArray *result=[NSMutableArray array];
	NSString *proviso = [NSMutableString string];
	char *sql ="SELECT U_NM, TU_NM, T_NM, U_FIXNUM, TU_SEQ, TU_CODE, U_PROVISO FROM '05_UInfo' where U_code =? and U_unit=? and T_Nm=?";
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_text(statement,   1, [univ UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,   2, [unit UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,   3, [type UTF8String],	-1, SQLITE_TRANSIENT);
		while (sqlite3_step(statement)==SQLITE_ROW) {
			
			if(sqlite3_column_type(statement, 6)!= SQLITE_NULL) proviso = [NSString	stringWithUTF8String:(char *)sqlite3_column_text(statement, 6)];
			else proviso = @"";
			
			NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
								 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"U_NM",
								 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)], @"TU_NM",
								 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)], @"T_NM",
								 [NSNumber numberWithInt:sqlite3_column_int(statement, 3)], @"U_FIXNUM",
								 [NSNumber numberWithInt:sqlite3_column_int(statement, 4)], @"TU_SEQ",
								 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 5)], @"TU_CODE",
								 proviso, @"U_PROVISO",
								 nil];
			[result addObject:dic];
		}	
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	return result;
}

+ (NSArray *)typesDescriptionWithUniv:(NSString*)univ avg:(NSNumber*)avg series03:(NSString*)series03 series02:(NSString*)series02 series01:(NSString*)series01 type:(NSString*)type unit:(NSString*)unit{
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"mysql.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	
	sqlite3_stmt *statement;
	NSMutableArray *result=[NSMutableArray array];
	NSString *proviso;
	NSString *u_note;
	NSString *u_mea;
	
	if (unit != nil) {
		char *sql ="SELECT U_NM, U_UNIT, TU_NM, T_NM, U_FIXNUM, TU_SEQ, TU_CODE, U_PROVISO, T_CODE, U_JUST, U_NOTE, U_MEA FROM '05_UInfo' where U_code =? and U_JUST >=? and U_Ser03=? and U_Ser02=? and U_Ser01=? and T_Nm=? and U_UNIT=? ";
		
		
		if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
			sqlite3_bind_text(statement,   1, [univ UTF8String],	-1, SQLITE_TRANSIENT);
			sqlite3_bind_double(statement, 2, [avg doubleValue]);
			sqlite3_bind_text(statement,   3, [series03 UTF8String],	-1, SQLITE_TRANSIENT);
			sqlite3_bind_text(statement,   4, [series02 UTF8String],	-1, SQLITE_TRANSIENT);
			sqlite3_bind_text(statement,   5, [series01 UTF8String],	-1, SQLITE_TRANSIENT);
			sqlite3_bind_text(statement,   6, [type UTF8String],	-1, SQLITE_TRANSIENT);
			sqlite3_bind_text(statement,   7, [unit UTF8String],	-1, SQLITE_TRANSIENT);
			while (sqlite3_step(statement)==SQLITE_ROW) {
				
				if(sqlite3_column_type(statement, 7)!= SQLITE_NULL) proviso = [NSString	stringWithUTF8String:(char *)sqlite3_column_text(statement, 7)];
				else proviso = @"";
				if(sqlite3_column_type(statement, 10)!= SQLITE_NULL) u_note = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 10)];
				else u_note = @"";
				if(sqlite3_column_type(statement, 11)!= SQLITE_NULL) u_mea = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 11)];
				else u_mea = @"";
				
				NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
									 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"U_NM",
									 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)], @"U_UNIT",
									 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)], @"TU_NM",
									 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)], @"T_NM",
									 [NSNumber numberWithInt:sqlite3_column_int(statement, 4)], @"U_FIXNUM",
									 [NSNumber numberWithInt:sqlite3_column_int(statement, 5)], @"TU_SEQ",
									 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 6)], @"TU_CODE",
									 proviso, @"U_PROVISO",
									 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 8)], @"T_CODE",
									 [NSNumber numberWithDouble:sqlite3_column_double(statement, 9)], @"U_JUST",
									 u_note, @"U_NOTE",
									 u_mea, @"U_MEA",
									 nil];
				[result addObject:dic];
			}	
		}

	} else {
		char *sql ="SELECT U_NM, U_UNIT, TU_NM, T_NM, U_FIXNUM, TU_SEQ, TU_CODE, U_PROVISO, T_CODE, U_JUST, U_NOTE, U_MEA FROM '05_UInfo' where U_code =? and U_JUST >=? and U_Ser03=? and U_Ser02=? and U_Ser01=? and T_Nm=? order by U_UNIT";
		
		
		if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
			sqlite3_bind_text(statement,   1, [univ UTF8String],	-1, SQLITE_TRANSIENT);
			sqlite3_bind_double(statement, 2, [avg doubleValue]);
			sqlite3_bind_text(statement,   3, [series03 UTF8String],	-1, SQLITE_TRANSIENT);
			sqlite3_bind_text(statement,   4, [series02 UTF8String],	-1, SQLITE_TRANSIENT);
			sqlite3_bind_text(statement,   5, [series01 UTF8String],	-1, SQLITE_TRANSIENT);
			sqlite3_bind_text(statement,   6, [type UTF8String],	-1, SQLITE_TRANSIENT);
			while (sqlite3_step(statement)==SQLITE_ROW) {
				
				if(sqlite3_column_type(statement, 7)!= SQLITE_NULL) proviso = [NSString	stringWithUTF8String:(char *)sqlite3_column_text(statement, 7)];
				else proviso = @"";
				if(sqlite3_column_type(statement, 10)!= SQLITE_NULL) u_note = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 10)];
				else u_note = @"";
				if(sqlite3_column_type(statement, 11)!= SQLITE_NULL) u_mea = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 11)];
				else u_mea = @"";
				NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
									 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"U_NM",
									 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)], @"U_UNIT",
									 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)], @"TU_NM",
									 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)], @"T_NM",
									 [NSNumber numberWithInt:sqlite3_column_int(statement, 4)], @"U_FIXNUM",
									 [NSNumber numberWithInt:sqlite3_column_int(statement, 5)], @"TU_SEQ",
									 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 6)], @"TU_CODE",
									 proviso, @"U_PROVISO",
									 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 8)], @"T_CODE",
									 [NSNumber numberWithDouble:sqlite3_column_double(statement, 9)], @"U_JUST",
									 u_note, @"U_NOTE",
									 u_mea, @"U_MEA",
									 nil];
				[result addObject:dic];
			}	
		}
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	return result;
}


+ (NSArray *)tuNameWithUniv:(NSString*)univ series03:(NSString*)series03 series02:(NSString*)series02 series01:(NSString*)series01 {
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"mysql.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	
	sqlite3_stmt *statement;
	NSMutableArray *result=[NSMutableArray array];
	char *sql ="SELECT TU_NM, T_NM FROM '05_UInfo' where U_code =? and U_Ser03=? and U_Ser02=? and U_Ser01=?";
	
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_text(statement,   1, [univ UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,   2, [series03 UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,   3, [series02 UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,   4, [series01 UTF8String],	-1, SQLITE_TRANSIENT);

		while (sqlite3_step(statement)==SQLITE_ROW) {
			NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
								 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"TU_NM",
								 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)], @"T_NM",
								nil];
			[result addObject:dic];
		}	
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	return result;
}

+ (NSArray *)tuNameWithUniv:(NSString*)univ  {
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"mysql.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	
	sqlite3_stmt *statement;
	NSMutableArray *result=[NSMutableArray array];
	char *sql ="SELECT DISTINCT TU_NM, T_NM FROM '05_UInfo' where U_code =? ";
	
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_text(statement,   1, [univ UTF8String],	-1, SQLITE_TRANSIENT);
			
		while (sqlite3_step(statement)==SQLITE_ROW) {
			NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
								 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"TU_NM",
								 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)], @"T_NM",
								 nil];
			[result addObject:dic];
		}	
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	return result;
}

+ (NSArray *)typesDescriptionWithUniv:(NSString*)univ type:(NSString*)type{
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"mysql.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	
	sqlite3_stmt *statement;
	NSMutableArray *result=[NSMutableArray array];
	NSString *u_note;
	char *sql ="SELECT distinct U_NM, TU_NM, T_NM, TU_CODE, T_CODE, U_NOTE, TU_SEQ,  sum(U_FIXNUM) FROM '05_UInfo' where U_code =? and TU_Nm=? group by  U_NM, TU_NM, T_NM,TU_CODE,T_CODE, TU_SEQ";
	
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_text(statement,   1, [univ UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,   2, [type UTF8String],	-1, SQLITE_TRANSIENT);
		while (sqlite3_step(statement)==SQLITE_ROW) {
			if(sqlite3_column_type(statement, 5)!= SQLITE_NULL) u_note = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 5)];
			else u_note = @"";
			NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
								 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"U_NM",
								 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)], @"TU_NM",
								 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)], @"T_NM",
								 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)], @"TU_CODE",
								 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 4)], @"T_CODE",
								 u_note, @"U_NOTE",
								 [NSNumber numberWithInt:sqlite3_column_int(statement, 6)], @"TU_SEQ",
								 [NSNumber numberWithInt:sqlite3_column_int(statement, 7)], @"U_FIXNUM",
								 nil];
			[result addObject:dic];
		}	
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	return result;
}


+ (NSArray *)typesDescriptionWithMenu:(int)menu univ:(NSString*)univ type:(NSString*)type series03:(NSString*)series03 series02:(NSString*)series02 series01:(NSString*)series01{
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"mysql.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	
	sqlite3_stmt *statement;
	NSMutableArray *result=[NSMutableArray array];
	NSString *u_note;
	NSString *proviso;
	char *sql;
	if (menu==2) {
		sql ="SELECT U_NM, TU_NM, T_NM, TU_CODE, T_CODE, U_NOTE, TU_SEQ,  U_FIXNUM, U_UNIT, U_PROVISO FROM '05_UInfo' where U_code =? and TU_Nm=? and U_Ser03=? and U_Ser02=? and U_Ser01=?";
	} else {
	
		sql ="SELECT U_NM, TU_NM, T_NM, TU_CODE, T_CODE, U_NOTE, TU_SEQ,  U_FIXNUM, U_UNIT, U_PROVISO FROM '05_UInfo' where U_code =? and T_Nm=? and U_Ser03=? and U_Ser02=? and U_Ser01=?";
	}
	
	
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_text(statement,   1, [univ UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,   2, [type UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,   3, [series03 UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,   4, [series02 UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,   5, [series01 UTF8String],	-1, SQLITE_TRANSIENT);
		while (sqlite3_step(statement)==SQLITE_ROW) {
			if(sqlite3_column_type(statement, 5)!= SQLITE_NULL) u_note = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 5)];
			else u_note = @"";
			if(sqlite3_column_type(statement, 9)!= SQLITE_NULL) proviso = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 9)];
			else proviso = @"";
			NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
								 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"U_NM",
								 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)], @"TU_NM",
								 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)], @"T_NM",
								 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)], @"TU_CODE",
								 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 4)], @"T_CODE",
								 u_note, @"U_NOTE",
								 [NSNumber numberWithInt:sqlite3_column_int(statement, 6)], @"TU_SEQ",
								 [NSNumber numberWithInt:sqlite3_column_int(statement, 7)], @"U_FIXNUM",
								 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 8)], @"U_UNIT",
								proviso, @"U_PROVISO",
								 nil];
			[result addObject:dic];
		}	
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	return result;
}


+ (NSDictionary *)typeDescriptionWithUniv:(NSString*)univ unit:(NSString*)unit type:(NSString*)type {
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"mysql.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	
	sqlite3_stmt *statement;
	NSDictionary *result;
	char *sql ="SELECT U_NM, TU_NM, T_NM, U_FIXNUM, TU_SEQ, TU_CODE FROM '05_UInfo' where U_code =? and U_unit=? and T_Nm=?";
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_text(statement,   1, [univ UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,   2, [unit UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,   3, [type UTF8String],	-1, SQLITE_TRANSIENT);
		while (sqlite3_step(statement)==SQLITE_ROW) {
			result = [NSDictionary dictionaryWithObjectsAndKeys:
					  [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"U_NM",
					  [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)], @"TU_NM",
					  [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)], @"T_NM",
					  [NSNumber numberWithInt:sqlite3_column_int(statement, 3)], @"U_FIXNUM",
					  [NSNumber numberWithInt:sqlite3_column_int(statement, 4)], @"TU_SEQ",
					  [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 5)], @"TU_CODE",
					  nil];
			
		}	
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	return result;
}


+ (NSDictionary *)typeDetailWithUniv:(NSString*)univ type:(NSString*)type type1:(NSString*)type1{
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"mysql.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	
	NSString *query = [NSString stringWithFormat:@"SELECT %@_SUM, %@_POSS, %@_TYPE, %@_SUB, %@_CHAR From '%@' where U_CODE = ?", type1, type1, type1, type1, type1, type];
					   
	NSString *sum;
	NSString *poss;
	NSString *typ;
	NSString *sub;
	NSString *chr;
	
	sqlite3_stmt *statement;
	NSDictionary *result;
	const char *sql = [query UTF8String];
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_text(statement,   1, [univ UTF8String],	-1, SQLITE_TRANSIENT);
		while (sqlite3_step(statement)==SQLITE_ROW) {
			sum = [NSString	stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
			
			if(sqlite3_column_type(statement, 1)!= SQLITE_NULL) poss = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
			else poss = @"";
			if(sqlite3_column_type(statement, 2)!= SQLITE_NULL) typ  = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)];
			else typ = @"";
			if(sqlite3_column_type(statement, 3)!= SQLITE_NULL) sub  = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)];
			else sub = @"";
			if(sqlite3_column_type(statement, 4)!= SQLITE_NULL) chr  = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 4)];
			else chr = @"";
			result = [NSDictionary dictionaryWithObjectsAndKeys:
								 sum, @"SUM",
								 poss, @"POSS",
								 typ, @"TYPE",
								 sub, @"SUB",
								 chr, @"CHAR",
								 nil];
			
		}	
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	return result;
}

+ (NSArray *)typesInfoWithTypes:(NSArray*)types avg:(NSNumber*)avg series03:(NSString*)series03 series02:(NSString*)series02 series01:(NSString*)series01 mf:(NSString *)mf{
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"mysql.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	
	sqlite3_stmt *statement;
	NSString *proviso;
	
	NSMutableArray *result=[[NSMutableArray alloc] init];
	
	for (int i=0; i<types.count; i++) {
		char *sql;
		if ([mf isEqualToString:@"M"])
			sql = "SELECT distinct i.U_NM, i.T_NM, i.U_UNIT, i.U_PROVISO, i.U_JUST, i.TU_NM , i.U_CODE FROM '05_uinfo' i, '04_univ' u where i.u_code=u.u_code and i.T_Nm = ? and i.U_JUST >= ?  and U_Ser03=? and U_Ser02=? and U_Ser01=? and u.u_af='A' order by i.u_code, i.u_just limit 12";
		else 
			sql = "SELECT distinct i.U_NM, i.T_NM, i.U_UNIT, i.U_PROVISO, i.U_JUST, i.TU_NM , i.U_CODE FROM '05_uinfo' i, '04_univ' u where i.u_code=u.u_code and i.T_Nm = ? and i.U_JUST >= ?  and U_Ser03=? and U_Ser02=? and U_Ser01=? order by i.u_code, i.u_just limit 12";;

		if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
			
			sqlite3_bind_text(statement,   1, [[[types objectAtIndex:i] objectForKey:@"Type"] UTF8String],	-1, SQLITE_TRANSIENT);
			sqlite3_bind_double(statement, 2, [avg doubleValue]);
			sqlite3_bind_text(statement,   3, [series03 UTF8String],	-1, SQLITE_TRANSIENT);
			sqlite3_bind_text(statement,   4, [series02 UTF8String],	-1, SQLITE_TRANSIENT);
			sqlite3_bind_text(statement,   5, [series01 UTF8String],	-1, SQLITE_TRANSIENT);
//			NSLog (@"%@, %f, %@, %@, %@", [types objectAtIndex:i], [avg doubleValue], series03, series02, series01); 
			
			while (sqlite3_step(statement)==SQLITE_ROW) 
			{
				if(sqlite3_column_type(statement, 3)!= SQLITE_NULL) proviso = [NSString	stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)];
				else proviso = @"";
				
				NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
									 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"U_NM",
									 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)], @"T_NM",
									 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)], @"U_UNIT",
									 
									 [NSString stringWithString:proviso], @"U_PROVISO",
									 [NSNumber numberWithDouble:sqlite3_column_double(statement, 4)], @"U_JUST",
									 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 5)], @"TU_NM",
									 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 6)], @"U_CODE",
									 nil];
				[result addObject:dic];
				
			}
		}	
		
		
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	
	NSSortDescriptor *sortDescriptor;
	sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"U_CODE" ascending:YES];
	NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
	return sortDescriptors = [result sortedArrayUsingDescriptors:sortDescriptors];
}

+ (NSArray *)typesInfoWithType:(NSString*)type avg:(NSNumber*)avg series03:(NSString*)series03 series02:(NSString*)series02 series01:(NSString*)series01 mf:(NSString *)mf{
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"mysql.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	
	sqlite3_stmt *statement;
	NSString *proviso;
	
	NSMutableArray *result=[[NSMutableArray alloc] init];
	
	
	char *sql;
	if ([mf isEqualToString:@"M"])
		sql = "SELECT distinct i.U_NM, i.T_NM, i.U_UNIT, i.U_PROVISO, i.U_JUST, i.TU_NM , i.U_CODE FROM '05_uinfo' i, '04_univ' u where i.u_code=u.u_code and i.T_Nm = ? and i.U_JUST >= ?  and U_Ser03=? and U_Ser02=? and U_Ser01=? and u.u_af='A' order by i.u_code, i.u_just";
	else 
		sql = "SELECT distinct i.U_NM, i.T_NM, i.U_UNIT, i.U_PROVISO, i.U_JUST, i.TU_NM , i.U_CODE FROM '05_uinfo' i, '04_univ' u where i.u_code=u.u_code and i.T_Nm = ? and i.U_JUST >= ?  and U_Ser03=? and U_Ser02=? and U_Ser01=? order by i.u_code, i.u_just ";
	
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		
		sqlite3_bind_text(statement,   1, [type UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_double(statement, 2, [avg doubleValue]);
		sqlite3_bind_text(statement,   3, [series03 UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,   4, [series02 UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,   5, [series01 UTF8String],	-1, SQLITE_TRANSIENT);

		while (sqlite3_step(statement)==SQLITE_ROW) 
		{
			if(sqlite3_column_type(statement, 3)!= SQLITE_NULL) proviso = [NSString	stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)];
			else proviso = @"";
			
			NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
								 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"U_NM",
								 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)], @"T_NM",
								 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)], @"U_UNIT",
								 
								 [NSString stringWithString:proviso], @"U_PROVISO",
								 [NSNumber numberWithDouble:sqlite3_column_double(statement, 4)], @"U_JUST",
								 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 5)], @"TU_NM",
								 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 6)], @"U_CODE",
								 nil];
			[result addObject:dic];
			
		}
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	
	return result;
}

+ (NSArray *)univInfoWithType:(NSString*)type tclass:(NSString*)tclass avg:(NSNumber*)avg series03:(NSString*)series03 series02:(NSString*)series02 series01:(NSString*)series01 mf:(NSString *)mf{
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"mysql.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	
	sqlite3_stmt *statement;
	NSString *proviso;
	
	NSMutableArray *result=[[NSMutableArray alloc] init];
	
	
	char *sql;
	if ([mf isEqualToString:@"M"])
		sql = "SELECT i.U_NM, i.T_NM, i.U_UNIT, i.U_PROVISO, i.U_JUST, i.TU_NM, i.U_CODE FROM '05_uinfo' i, '04_univ' u where i.u_code=u.u_code and i.T_Nm = ? and i.T_Class = ? and i.U_JUST >= ?  and U_Ser03=? and U_Ser02=? and U_Ser01=? and u.u_af='A' order by i.u_code, i.u_just";
	else 
		sql = "SELECT i.U_NM, i.T_NM, i.U_UNIT, i.U_PROVISO, i.U_JUST, i.TU_NM, i.U_CODE FROM '05_uinfo' i, '04_univ' u where i.u_code=u.u_code and i.T_Nm = ? and i.T_Class = ? and i.U_JUST >= ?  and U_Ser03=? and U_Ser02=? and U_Ser01=? order by  i.u_code,  i.u_just ";;
	
	
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		
		sqlite3_bind_text(statement,   1, [type UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,   2, [tclass UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_double(statement, 3, [avg doubleValue]);
		sqlite3_bind_text(statement,   4, [series03 UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,   5, [series02 UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,   6, [series01 UTF8String],	-1, SQLITE_TRANSIENT);
		
		while (sqlite3_step(statement)==SQLITE_ROW) 
		{
			if(sqlite3_column_type(statement, 3)!= SQLITE_NULL) proviso = [NSString	stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)];
			else proviso = @"";
			
			NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
								 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"U_NM",
								 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)], @"T_NM",
								 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)], @"U_UNIT",
								 
								 [NSString stringWithString:proviso], @"U_PROVISO",
								 [NSNumber numberWithDouble:sqlite3_column_double(statement, 4)], @"U_JUST",
								 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 5)], @"TU_NM",
								 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 6)], @"U_CODE",
								 nil];
			[result addObject:dic];
			
		}
	}
		
	sqlite3_finalize(statement);
	sqlite3_close(database);
	
	NSSortDescriptor *sortDescriptor;
	sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"U_CODE" ascending:YES];
	NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
	return sortDescriptors = [result sortedArrayUsingDescriptors:sortDescriptors];
}

+ (NSArray *)univInfoWithType:(NSString*)type avg:(NSNumber*)avg series03:(NSString*)series03 series02:(NSString*)series02 series01:(NSString*)series01 mf:(NSString *)mf{
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"mysql.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	
	sqlite3_stmt *statement;
	NSString *proviso;
	
	NSMutableArray *result=[[NSMutableArray alloc] init];
	
	
	char *sql;
	if ([mf isEqualToString:@"M"])
		sql = "SELECT i.U_NM, i.T_NM, i.U_UNIT, i.U_PROVISO, i.U_JUST, i.TU_NM, i.U_CODE FROM '05_uinfo' i, '04_univ' u where i.u_code=u.u_code and i.T_Nm = ? and i.U_JUST >= ?  and U_Ser03=? and U_Ser02=? and U_Ser01=? and u.u_af='A' order by i.u_code, i.u_just";
	else 
		sql = "SELECT i.U_NM, i.T_NM, i.U_UNIT, i.U_PROVISO, i.U_JUST, i.TU_NM, i.U_CODE FROM '05_uinfo' i, '04_univ' u where i.u_code=u.u_code and i.T_Nm = ? and i.U_JUST >= ?  and U_Ser03=? and U_Ser02=? and U_Ser01=? order by  i.u_code,  i.u_just ";;
	
	
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		
		sqlite3_bind_text(statement,   1, [type UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_double(statement, 2, [avg doubleValue]);
		sqlite3_bind_text(statement,   3, [series03 UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,   4, [series02 UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,   5, [series01 UTF8String],	-1, SQLITE_TRANSIENT);
		NSLog (@"%@, %f, %@, %@, %@", type , [avg doubleValue], series03, series02, series01); 
		
		while (sqlite3_step(statement)==SQLITE_ROW) 
		{
			if(sqlite3_column_type(statement, 3)!= SQLITE_NULL) proviso = [NSString	stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)];
			else proviso = @"";
			
			NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
								 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"U_NM",
								 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)], @"T_NM",
								 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)], @"U_UNIT",
								 
								 [NSString stringWithString:proviso], @"U_PROVISO",
								 [NSNumber numberWithDouble:sqlite3_column_double(statement, 4)], @"U_JUST",
								 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 5)], @"TU_NM",
								 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 6)], @"U_CODE",
								 nil];
			[result addObject:dic];
			
		}
	}
	
	sqlite3_finalize(statement);
	sqlite3_close(database);
	
	
	NSSortDescriptor *sortDescriptorUCode = [[NSSortDescriptor alloc] initWithKey:@"U_CODE" ascending:YES];
	NSSortDescriptor *sortDescriptorUJust = [[NSSortDescriptor alloc] initWithKey:@"U_JUST" ascending:YES];
	NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptorUCode, sortDescriptorUJust,nil];
	return sortDescriptors = [result sortedArrayUsingDescriptors:sortDescriptors];
}

+ (NSArray *)typesInfoWithTypes:(NSArray*)types avg:(NSNumber*)avg unit:(NSString*)unit {
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"mysql.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	
	sqlite3_stmt *statement;
	NSString *proviso;
	
	NSMutableArray *result=[[NSMutableArray alloc] init];
	
	for (int i=0; i<types.count; i++) {
		const char *sql = "SELECT U_NM, T_NM, U_UNIT, U_PROVISO, U_JUST, TU_NM FROM '05_uinfo' where T_Nm = ? and U_JUST >= ? and U_UNIT = ? order by u_just limit 12";
		if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
			
			sqlite3_bind_text(statement,   1, [[types objectAtIndex:i] UTF8String],	-1, SQLITE_TRANSIENT);
			sqlite3_bind_double(statement, 2, [avg doubleValue]);
			sqlite3_bind_text(statement,   3, [unit UTF8String],	-1, SQLITE_TRANSIENT);
			
			while (sqlite3_step(statement)==SQLITE_ROW) 
			{
				if(sqlite3_column_type(statement, 3)!= SQLITE_NULL) proviso = [NSString	stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)];
				else proviso = @"";
				
				NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
									 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"U_NM",
									 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)], @"T_NM",
									 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)], @"U_UNIT",
									 
									 [NSString stringWithString:proviso], @"U_PROVISO",
									 [NSNumber numberWithDouble:sqlite3_column_double(statement, 4)], @"U_JUST",
									 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 5)], @"TU_NM",
									 nil];
				[result addObject:dic];
				
			}
		}	
		
		
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	
	NSSortDescriptor *sortDescriptor;
	sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"U_JUST" ascending:YES];
	NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
	return sortDescriptors = [result sortedArrayUsingDescriptors:sortDescriptors];
}


+ (NSArray *)typeRankWithTypes:(NSArray*)types  {
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"mysql.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	
	sqlite3_stmt *statement;
	NSString *proviso;
	
	NSMutableArray *result=[[NSMutableArray alloc] init];
	
	for (int i=0; i<types.count; i++) {
		const char *sql = "Select distinct u_ran from '05_uinfo' i, '04_univ' u where i.u_code=u.u_code and t_nm =? order by u.u_ran limit 1";
		
		if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
			sqlite3_bind_text(statement,   1, [[types objectAtIndex:i] UTF8String],	-1, SQLITE_TRANSIENT);
			while (sqlite3_step(statement)==SQLITE_ROW) 
			{
				if(sqlite3_column_type(statement, 3)!= SQLITE_NULL) proviso = [NSString	stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)];
				else proviso = @"";
				
				NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
									  [NSNumber numberWithInt:sqlite3_column_int(statement, 0)], @"Rank",
									  [types objectAtIndex:i],@"Type",
									  nil];
			 
									 
				[result addObject:dic];
				
			}
		}	
		
		
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	
	NSSortDescriptor *sortDescriptor;
	sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Rank" ascending:YES];
	NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
	return sortDescriptors = [result sortedArrayUsingDescriptors:sortDescriptors];
}

+ (NSArray *)typeRankWithArray:(NSArray*)types avg:(NSNumber*)avg mf:(NSString *)mf hCate:(NSString*)hCate grades:(NSArray*)grades ID:(NSString*)ID{
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"mysql.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	
	
	sqlite3_stmt *statement;
	NSString *proviso;
	NSMutableArray *result=[[NSMutableArray alloc] init];
	char *sql;
	for (int i=0; i<types.count; i++) {
		
		if ([mf isEqualToString:@"M"])
			sql ="Select  u_ran, u_just, u_proviso from '05_uinfo' i, '04_univ' u where i.u_code=u.u_code and t_nm =? and i.u_just>=? and u.u_af='A' order by u.u_ran, i.u_just ";
		else 
			sql ="Select  u_ran, u_just, u_proviso from '05_uinfo' i, '04_univ' u where i.u_code=u.u_code and t_nm =? and i.u_just>=? order by u.u_ran, i.u_just ";
		
		
		if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
			sqlite3_bind_text(statement,   1, [[types objectAtIndex:i] UTF8String],	-1, SQLITE_TRANSIENT);
			sqlite3_bind_double(statement, 2, [avg doubleValue]);
			NSLog(@"%@ %F", [types objectAtIndex:i], [avg doubleValue]);
			while (sqlite3_step(statement)==SQLITE_ROW) 
			{
				
				if(sqlite3_column_type(statement, 2)!= SQLITE_NULL) proviso = [NSString	stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)];
				else proviso = @"";
				
				NSLog(@"+++++==========%d %f %@  %@", sqlite3_column_int(statement, 0), sqlite3_column_double(statement, 1),[types objectAtIndex:i], proviso);
				BOOL isPass = [Proviso isPassWithID:ID grades:grades proviso:proviso hCate:hCate];
				if (isPass) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSNumber numberWithInt:sqlite3_column_int(statement, 0)], @"Rank",
										 [NSNumber numberWithDouble:sqlite3_column_double(statement, 1)], @"Just",
										 [types objectAtIndex:i],@"Type",
										 nil];
					
					NSLog(@"==========%d %f %@  %@", [[dic objectForKey:@"Rank"] intValue], [[dic objectForKey:@"Just"] doubleValue],[dic objectForKey:@"Type"], proviso);
					
					[result addObject:dic];
					break;
				}

			}
		}	
		
		
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);

	NSSortDescriptor *sortDescriptorRank = [[NSSortDescriptor alloc] initWithKey:@"Rank" ascending:YES];
	NSSortDescriptor *sortDescriptorJust = [[NSSortDescriptor alloc] initWithKey:@"Just" ascending:YES];
	NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptorRank, sortDescriptorJust, nil];
	sortDescriptors = [result sortedArrayUsingDescriptors:sortDescriptors];

	return sortDescriptors;
}

+ (NSArray *)typeRankWithArray:(NSArray*)types avg:(NSNumber*)avg series03:(NSString*)series03 series02:(NSString*)series02 series01:(NSString*)series01 mf:(NSString *)mf hCate:(NSString*)hCate grades:(NSArray*)grades ID:(NSString*)ID{
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"mysql.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	
	sqlite3_stmt *statement;
	NSString *proviso;	
	NSMutableArray *result=[[NSMutableArray alloc] init];
	char *sql;
	for (int i=0; i<types.count; i++) {
		
		if ([mf isEqualToString:@"M"])
			sql ="Select  u_ran, u_just, u_proviso from '05_uinfo' i, '04_univ' u where i.u_code=u.u_code and t_nm =? and i.u_just>=? and i.u_ser03=? and i.u_ser02=? and i.u_ser01=? and u.u_af='A' order by u.u_ran, i.u_just ";
		else 
			sql ="Select  u_ran, u_just, u_proviso from '05_uinfo' i, '04_univ' u where i.u_code=u.u_code and t_nm =? and i.u_just>=? and i.u_ser03=? and i.u_ser02=? and i.u_ser01=? order by u.u_ran, i.u_just";
		if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
			sqlite3_bind_text(statement,   1, [[types objectAtIndex:i] UTF8String],	-1, SQLITE_TRANSIENT);
			sqlite3_bind_double(statement, 2, [avg doubleValue]);
			sqlite3_bind_text(statement,   3, [series03 UTF8String],	-1, SQLITE_TRANSIENT);
			sqlite3_bind_text(statement,   4, [series02 UTF8String],	-1, SQLITE_TRANSIENT);
			sqlite3_bind_text(statement,   5, [series01 UTF8String],	-1, SQLITE_TRANSIENT);
			while (sqlite3_step(statement)==SQLITE_ROW) 
			{
				if(sqlite3_column_type(statement, 2)!= SQLITE_NULL) proviso = [NSString	stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)];
				else proviso = @"";
				
				NSLog(@"+++++==========%d %f %@  %@", sqlite3_column_int(statement, 0), sqlite3_column_double(statement, 1),[types objectAtIndex:i], proviso);
				BOOL isPass = [Proviso isPassWithID:ID grades:grades proviso:proviso hCate:hCate];
				if (isPass) {
					NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
										 [NSNumber numberWithInt:sqlite3_column_int(statement, 0)], @"Rank",
										 [NSNumber numberWithDouble:sqlite3_column_double(statement, 1)], @"Just",
										 [types objectAtIndex:i],@"Type",
										 nil];
					
					NSLog(@"==========%d %f %@  %@", [[dic objectForKey:@"Rank"] intValue], [[dic objectForKey:@"Just"] doubleValue],[dic objectForKey:@"Type"], proviso);
					 
					[result addObject:dic];
					break;
				}
				
			}
		}	
		
		
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	
	NSSortDescriptor *sortDescriptorRank = [[NSSortDescriptor alloc] initWithKey:@"Rank" ascending:YES];
	NSSortDescriptor *sortDescriptorJust = [[NSSortDescriptor alloc] initWithKey:@"Just" ascending:YES];
	NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptorRank, sortDescriptorJust, nil];
	return sortDescriptors = [result sortedArrayUsingDescriptors:sortDescriptors];
}


+ (NSArray *)lcodeWithMenu:(int)menu type:(NSString *)type tclass:(NSString*)tclass series03:(NSString*)series03 series02:(NSString*)series02 series01:(NSString*)series01{
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"mysql.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	
	sqlite3_stmt *statement;
	NSMutableArray *result=[[NSMutableArray alloc] init];
	
	switch (menu) {
		case 0:{
			const char *sql = "select distinct L_CODE From '05_uinfo' i, '04_univ' u where i.u_code = u.u_code order by l_code ";
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
			while (sqlite3_step(statement)==SQLITE_ROW) 
				{
					[result addObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)]];
				}
			}	
			break;
		}
		case 1:{
			const char *sql = "select distinct L_CODE From '05_uinfo' i, '04_univ' u where i.u_code = u.u_code and t_nm =? and i.t_class = ? order by l_code ";
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,   1, [type UTF8String],	    -1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   2, [tclass UTF8String],	    -1, SQLITE_TRANSIENT);
				while (sqlite3_step(statement)==SQLITE_ROW) 
				{
					[result addObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)]];
				}
			}	
			break;
		}
		case 2:{
			const char *sql = "select distinct L_CODE From '05_uinfo' i, '04_univ' u where i.u_code = u.u_code and U_Ser03=? and U_Ser02=? and U_Ser01=? order by l_code ";
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_bind_text(statement,   1, [series03 UTF8String],	-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   2, [series02 UTF8String],	-1, SQLITE_TRANSIENT);
				sqlite3_bind_text(statement,   3, [series01 UTF8String],	-1, SQLITE_TRANSIENT);
				while (sqlite3_step(statement)==SQLITE_ROW) 
				{
					[result addObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)]];
				}
			}	
			break;
		}
		default:{
			const char *sql = "select distinct L_CODE From '05_uinfo' i, '04_univ' u where i.u_code = u.u_code order by l_code ";
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
				while (sqlite3_step(statement)==SQLITE_ROW) 
				{
					[result addObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)]];
				}
			}	
			break;
		}
	}
			
	sqlite3_finalize(statement);
	sqlite3_close(database);
	return result;
}

@end
