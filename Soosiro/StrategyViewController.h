//
//  StrategyViewController.h
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 7. 15..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StrategyViewController : UIViewController
@property (strong, nonatomic) NSString *ID;
@property (strong, nonatomic) NSString *unit;
@property (strong, nonatomic) NSString *series03;
@property (strong, nonatomic) NSString *series02;
@property (strong, nonatomic) NSString *series01;
@property (strong, nonatomic) NSString *category;
@property (strong, nonatomic) NSString *mf;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UILabel *lableMainTitle;
@property (nonatomic) int menu;
@property (strong, nonatomic) IBOutlet UILabel *labelTitle;
@property (strong, nonatomic) IBOutlet UIImageView *imageAD;
- (IBAction)actionBack:(id)sender;
- (IBAction)actionHome:(id)sender;
@end
