//
//  SpecialityCell.h
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 6. 20..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpecialityCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *labelInfo;
@property (strong, nonatomic) IBOutlet UIButton *btnGold;
@property (strong, nonatomic) IBOutlet UIButton *btnSilver;
@property (strong, nonatomic) IBOutlet UIButton *btnCupper;
@property (strong, nonatomic) IBOutlet UIButton *btnCamp;
- (IBAction)actionSelectMedal:(id)sender;
- (IBAction)actionTouchMedal:(id)sender;
- (IBAction)actionTouchCamp:(id)sender;
- (IBAction)actionSelectCamp:(id)sender;

@end
