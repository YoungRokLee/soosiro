//
//  SpecialityCell.m
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 6. 20..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import "SpecialityCell.h"

@implementation SpecialityCell
@synthesize labelInfo;
@synthesize btnGold;
@synthesize btnSilver;
@synthesize btnCupper;
@synthesize btnCamp;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)actionSelectMedal:(id)sender {
	UIButton *btn = (UIButton *)sender;
	btn.highlighted = !btn.highlighted;
}

- (IBAction)actionTouchMedal:(id)sender {

}

- (IBAction)actionTouchCamp:(id)sender {
}

- (IBAction)actionSelectCamp:(id)sender {
	UIButton *btn = (UIButton *)sender;
	btn.highlighted = !btn.highlighted;
}
@end
