//
//  ADPop.h
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 7. 29..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ADPop : UIView{
	UILabel *title;
	UITextView *text;
	UILabel *url;
}

@property (nonatomic) int index;
@end
