//
//  Proviso.h
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 7. 29..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Proviso : NSObject
+ (BOOL)isPassWithID:(NSString *)ID grades:(NSArray*)grades proviso:(NSString*)proviso hCate:(NSString*)hCate;
+ (NSArray *)arrayOfPossibleType:(NSString *)user;
@end
