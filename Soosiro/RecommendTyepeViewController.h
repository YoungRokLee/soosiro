//
//  RecommendTyepeViewController.h
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 7. 14..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecommendTyepeViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>
@property (strong, nonatomic) NSString *ID;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UILabel *labelName;
@property (strong, nonatomic) IBOutlet UILabel *lableMainTitle;
@property (nonatomic) int menu;

- (IBAction)goBack:(id)sender;
@end
