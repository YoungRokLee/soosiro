//
//  Proviso.m
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 7. 29..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import "Proviso.h"
#import "User.h"
#import "Record.h"

@implementation Proviso
+ (BOOL)isSociety:(NSDictionary *)dic {
	BOOL result = NO;
	for (int i=16; i<30; i++) {
		NSString *key = [NSString stringWithFormat:@"P_INFO%2d", i];
		NSString *value = [dic objectForKey:key];
		if (value == @"Y") {
			result = YES;
			break;
		}
	}
	return result;
}

+ (BOOL)isChance:(NSDictionary *)dic {
	BOOL result = NO;
	for (int i=11; i<16; i++) {
		NSString *key = [NSString stringWithFormat:@"P_INFO%2d", i];
		NSString *value = [dic objectForKey:key];
		if (value == @"Y") {
			result = YES;
			break;
		}
	}
	return result;
}


+ (NSArray *)arrayOfPossibleType:(NSString *)user
{
	NSDictionary *userDic = [User userInfoWithID:user];
	NSDictionary *dic = [User infoWithID:user];
    NSString *hClass = [userDic objectForKey:@"H_Class"];
	NSString *hCate = [userDic objectForKey:@"P_Cate"];
	
	NSMutableArray *result  = [NSMutableArray array];
	
	if ([hClass isEqualToString:@"HZ"]) {
		[result addObject:@"검정고시자 특별전형"];
		[result addObject:@"논술중심전형"];
		[result addObject:@"적성고사중심전형"];
		return result;
	}

	
	if ([[dic objectForKey:@"P_INFO01"] isEqualToString:@"Y"]) [result addObject:@"입학사정관전형"];
	if ([[dic objectForKey:@"P_INFO02"] isEqualToString:@"Y"]) [result addObject:@"어학특기전형"];
	if ([hCate isEqualToString:@"문과"]) {
		if ([[dic objectForKey:@"P_INFO30"] isEqualToString:@"Y"]) [result addObject:@"백일장특별전형"];
	} else if ([hCate isEqualToString:@"이과"]) {
		if ([hClass isEqualToString:@"HA"]|| [hClass isEqualToString:@"HC"]) [result addObject:@"수학과학특기전형"];
		if (![hClass isEqualToString:@"HA"] && ![hClass isEqualToString:@"HC"] && [[dic objectForKey:@"P_INFO03"]  isEqualToString: @"Y"]) [result addObject:@"수학과학특기전형"];
	} 
	
	if ([[dic objectForKey:@"P_INFO05"] isEqualToString:@"Y"]) [result addObject:@"리더십특별전형"];
	if ([[dic objectForKey:@"P_INFO07"] isEqualToString:@"Y"]) [result addObject:@"학교추천특별전형"];
	if ([[dic objectForKey:@"P_INFO06"] isEqualToString:@"Y"]) [result addObject:@"종교추천특별전형"];
	if ([[dic objectForKey:@"P_INFO04"] isEqualToString:@"Y"]) [result addObject:@"농어촌특별전형"];
	if ([[dic objectForKey:@"P_INFO09"] isEqualToString:@"Y"]) [result addObject:@"특수교육대상자특별전형"];
	if ([hClass isEqualToString:@"HJ"] || [hClass isEqualToString:@"HG"]) [result addObject:@"특성화고졸업자특별전형"];
	if ([self isSociety:dic]) [result addObject:@"사회기여자특별전형"];
	if ([self isChance:dic]) [result addObject:@"기회균등특별전형"];
	if ([hClass isEqualToString:@"HZ"]) [result addObject:@"검정고시자특별전형"];
	if ([hClass isEqualToString:@"HD"]) [result addObject:@"대안학교특별전형"];
	if ([[dic objectForKey:@"P_INFO10"] isEqualToString:@"Y"]) [result addObject:@"지역배려특별전형"];
	if ([[dic objectForKey:@"P_INFO08"] isEqualToString:@"Y"]) 
		[result addObject:@"특성화고출신재직자특별전형"];
	[result addObject:@"학생부중심전형"];
	
	if ([hCate isEqualToString:@"문과"]) {
		if ([hClass isEqualToString:@"HB"]) [result addObject:@"논술중심전형"];
		if (![hClass isEqualToString:@"HB"] && [Record langWithEmail:user] <=3 ) [result addObject:@"논술중심전형"];
	} else if ([hCate  isEqualToString:@"이과"]) {
		if ([hClass isEqualToString:@"HF"]) [result addObject:@"논술중심전형"];
		if (![hClass isEqualToString:@"HF"] && [Record mathWithEmail:user] <=3 ) [result addObject:@"논술중심전형"];
	} 
	[result addObject:@"적성고사중심전형"];
	[result addObject:@"서류중심전형"];
	return result;
//	NSNumber *avg = [Record avgValueWithID:user];
//	NSLog(@"-----%f", [avg doubleValue]);
//	return [UnivInfo typeRankWithArray:result avg:avg];
}


+ (NSDictionary *)findWithArea:(NSString*)area array:(NSArray*)array {
	for (NSDictionary *dic in array) {
		if ([[dic objectForKey:@"area"] isEqualToString:[area lowercaseString]])
			return dic;
	}
	return nil;
}

+ (BOOL)isPassWithID:(NSString *)ID grades:(NSArray*)grades proviso:(NSString*)proviso hCate:(NSString*)hCate{
	
	BOOL isSuccess=YES;
	double study;
	double study1=[[grades objectAtIndex:3] doubleValue];
	double study2=[[grades objectAtIndex:4] doubleValue];
	
	
	if ([proviso isEqualToString:@""]) return isSuccess;
	
	NSString *a = [proviso substringWithRange:NSMakeRange(0, 1)];
	NSString *b = [proviso substringWithRange:NSMakeRange(1, 1)];
	NSString *c = [proviso substringWithRange:NSMakeRange(2, 1)];
	NSString *d = [proviso substringWithRange:NSMakeRange(3, 1)];
	int scount =  [[proviso substringWithRange:NSMakeRange(4, 1)] intValue];
	NSString *gradeType = [proviso substringWithRange:NSMakeRange(5, 1)];
	int acount =  [[proviso substringWithRange:NSMakeRange(7, 1)] intValue];
	NSString *gradeType1 =[proviso substringWithRange:NSMakeRange(8, 1)]; 
	int calcResult =  [[proviso substringWithRange:NSMakeRange(9, 2)] intValue];
	
	NSString *r1 = [proviso substringWithRange:NSMakeRange(12, 1)];
	NSString *r2 = [proviso substringWithRange:NSMakeRange(13, 1)];
	NSString *r3 = [proviso substringWithRange:NSMakeRange(14, 1)];
	
	if (scount==0) {
		study = 0;
	}
	if (scount==1) {
		if (study1<=study2) study = study1;
		else study = study2;
	} else if (scount ==2) {
		if ([gradeType isEqualToString:@"p"]) {
			study = (study1+study2)/2;
		} else {
			if (study1<=study2) study = study2;
			else study = study1;
		}
	}
	
	NSMutableArray *arrGrades = [NSMutableArray array];
	if (![a isEqualToString:@"n"]) [arrGrades addObject: [NSDictionary dictionaryWithObjectsAndKeys: [grades objectAtIndex:0],@"value", @"a", @"area", nil]];
	if (![b isEqualToString:@"n"]) [arrGrades addObject:[NSDictionary dictionaryWithObjectsAndKeys: [grades objectAtIndex:1],@"value", @"b", @"area", nil]];
	if (![c isEqualToString:@"n"]) [arrGrades addObject:[NSDictionary dictionaryWithObjectsAndKeys: [grades objectAtIndex:2],@"value", @"c", @"area", nil]];
	if (![d isEqualToString:@"n"]) [arrGrades addObject:[NSDictionary dictionaryWithObjectsAndKeys: [NSNumber numberWithDouble:study],@"value", @"d", @"area", nil]];
	
	NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"value" ascending:YES];
	NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
	NSMutableArray *sortedArray = [NSMutableArray arrayWithArray:[arrGrades sortedArrayUsingDescriptors:sortDescriptors]];
	
	
	if ([hCate isEqualToString:@"문과"] &&([b isEqualToString:@"B"] || [d isEqualToString:@"D"] )) return NO;
	
	if ([gradeType1 isEqualToString:@"g"]) {
		int passCount=0;
		for (int i = 0; i<sortedArray.count; i++) {
			if(calcResult >= [[[sortedArray objectAtIndex:i] objectForKey:@"value"] doubleValue]) passCount+=1;
		}
		if (passCount<acount) isSuccess = NO;				 
		
		if (![r1 isEqualToString:@"n"] && [r2 isEqualToString:@"n"]) {
			NSDictionary *dic=[self findWithArea:r1 array:sortedArray];
			if ([[dic objectForKey:@"value"] doubleValue]>calcResult) isSuccess = NO;
		} else if(![r1 isEqualToString:@"n"] && ![r2 isEqualToString:@"n"]) {
			if ([r3 isEqualToString:@"1"]) {
				NSDictionary *dic1=[self findWithArea:r1 array:sortedArray];
				NSDictionary *dic2=[self findWithArea:r2 array:sortedArray];
				if (!([[dic1 objectForKey:@"value"] doubleValue]<=calcResult || [[dic2 objectForKey:@"value"] doubleValue]<=calcResult)) isSuccess = NO;
			} else 	if ([r3 isEqualToString:@"2"]) {
				NSDictionary *dic1=[self findWithArea:r1 array:sortedArray];
				NSDictionary *dic2=[self findWithArea:r2 array:sortedArray];
				if (!([[dic1 objectForKey:@"value"] doubleValue]<=calcResult && [[dic2 objectForKey:@"value"] doubleValue]<=calcResult)) isSuccess = NO;
			}			 
		}
	} else if([gradeType1 isEqualToString:@"p"]) {
		if ([r1 isEqualToString:@"n"]) {
			double sum=0;
			for (int i=0; i<acount; i++) {
				sum = sum + [[[sortedArray objectAtIndex:i] objectForKey:@"value"] doubleValue];
			}
			double avg = sum/acount;
			if (avg>calcResult) isSuccess = NO;
		} if (![r1 isEqualToString:@"n"] && [r2 isEqualToString:@"n"]){
			NSDictionary *dic=[self findWithArea:r1 array:sortedArray]; 
			[sortedArray removeObject:dic];
			double sum=0;
			for (int i=0; i<acount-1; i++) {
				sum = sum + [[[sortedArray objectAtIndex:i] objectForKey:@"value"] doubleValue];
			}
			sum = sum+[[dic objectForKey:@"value"] doubleValue];
			double avg = sum/acount;
			if (avg>calcResult) isSuccess = NO;
		} if (![r1 isEqualToString:@"n"] && ![r2 isEqualToString:@"n"]){		
			if ([r3 isEqualToString:@"1"]) {
				BOOL tempCheck = NO;
				
				NSMutableArray *temp = [sortedArray copy];
				
				NSDictionary *dic=[self findWithArea:r1 array:temp]; 
				[temp removeObject:dic];
				double sum=0;
				for (int i=0; i<acount-1; i++) {
					sum = sum + [[[temp objectAtIndex:i] objectForKey:@"value"] doubleValue];
				}
				sum = sum+[[dic objectForKey:@"value"] doubleValue];
				double avg = sum/acount;
				if (avg<=calcResult) tempCheck = YES;
				
				if (!tempCheck) {
					NSMutableArray *temp = [sortedArray copy];
					
					NSDictionary *dic=[self findWithArea:r2 array:temp]; 
					[temp removeObject:dic];
					double sum=0;
					for (int i=0; i<acount-1; i++) {
						sum = sum + [[[temp objectAtIndex:i] objectForKey:@"value"] doubleValue];
					}
					sum = sum+[[dic objectForKey:@"value"] doubleValue];
					avg = sum/acount;
					if (avg<=calcResult) tempCheck = YES;
				}
				if (!tempCheck) isSuccess = NO;
			} else 	if ([r3 isEqualToString:@"2"]) {
				NSDictionary *dic=[self findWithArea:r1 array:sortedArray]; 
				[sortedArray removeObject:dic];
				double sum=[[dic objectForKey:@"value"] doubleValue];
				
				dic=[self findWithArea:r2 array:sortedArray]; 
				[sortedArray removeObject:dic];
				sum=sum+[[dic objectForKey:@"value"] doubleValue];
				
				for (int i=0; i<acount-2; i++) {
					sum = sum + [[[sortedArray objectAtIndex:i] objectForKey:@"value"] doubleValue];
				}
				double avg = sum/acount;
				if (avg>calcResult) isSuccess = NO;
			}	
		}
	} else if([gradeType1 isEqualToString:@"s"]) {
		if ([r1 isEqualToString:@"n"]) {
			double sum=0;
			for (int i=0; i<acount; i++) {
				sum = sum + [[[sortedArray objectAtIndex:i] objectForKey:@"value"] doubleValue];
			}
			if (sum>calcResult) isSuccess = NO;
		} if (![r1 isEqualToString:@"n"] && [r2 isEqualToString:@"n"]){
			NSDictionary *dic=[self findWithArea:r1 array:sortedArray]; 
			[sortedArray removeObject:dic];
			double sum=0;
			for (int i=0; i<acount-1; i++) {
				sum = sum + [[[sortedArray objectAtIndex:i] objectForKey:@"value"] doubleValue];
			}
			sum = sum+[[dic objectForKey:@"value"] doubleValue];
			if (sum>calcResult) isSuccess = NO;
		} if (![r1 isEqualToString:@"n"] && ![r2 isEqualToString:@"n"]){		
			if ([r3 isEqualToString:@"1"]) {
				BOOL tempCheck = NO;
				NSMutableArray *temp = [sortedArray copy];
				NSDictionary *dic=[self findWithArea:r1 array:temp]; 
				[temp removeObject:dic];
				double sum=0;
				for (int i=0; i<acount-1; i++) {
					sum = sum + [[[temp objectAtIndex:i] objectForKey:@"value"] doubleValue];
				}
				sum = sum+[[dic objectForKey:@"value"] doubleValue];
				
				if (sum<=calcResult) tempCheck = YES;
				
				if (!tempCheck) {
					NSMutableArray *temp = [sortedArray copy];
					
					NSDictionary *dic=[self findWithArea:r2 array:temp]; 
					[temp removeObject:dic];
					sum=0;
					for (int i=0; i<acount-1; i++) {
						sum = sum + [[[temp objectAtIndex:i] objectForKey:@"value"] doubleValue];
					}
					sum = sum+[[dic objectForKey:@"value"] doubleValue];
					if (sum<=calcResult) tempCheck = YES;
				}
				if (!tempCheck) isSuccess = NO;
			} else 	if ([r3 isEqualToString:@"2"]) {
				NSDictionary *dic=[self findWithArea:r1 array:sortedArray]; 
				[sortedArray removeObject:dic];
				double sum=[[dic objectForKey:@"value"] doubleValue];
				dic=[self findWithArea:r2 array:sortedArray]; 
				[sortedArray removeObject:dic];
				sum=sum+[[dic objectForKey:@"value"] doubleValue];
				
				for (int i=0; i<acount-2; i++) {
					sum = sum + [[[sortedArray objectAtIndex:i] objectForKey:@"value"] doubleValue];
				}
				if (sum>calcResult) isSuccess = NO;
			}	
		}
		
	}
	return isSuccess;
}

@end
