//
//  YearViewController.m
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 7. 2..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import "YearViewController.h"
#import "Record.h"
#import "GradeViewController.h"
#import "User.h"
@interface YearViewController (){
	NSArray *list;
	NSArray *slist;
	NSArray *adArray;
	NSTimer *timer;
	int syear;
	int range;
}

@end

@implementation YearViewController
@synthesize imageAD;
@synthesize labelTitle;
@synthesize style;
@synthesize tableView=_tableView;
@synthesize menu;
@synthesize ID;
@synthesize labelMainTitle;
- (void)viewDidLoad
{
    [super viewDidLoad];
	labelMainTitle.text = [Title mainTilteWithMenu:menu];
	UIImage *image1 = [UIImage imageNamed:@"iphone_ad_1.png"];
	UIImage *image2 = [UIImage imageNamed:@"iphone_ad_2.png"];
	UIImage *image3 = [UIImage imageNamed:@"iphone_ad_3.png"];
	adArray = [NSArray arrayWithObjects:image1, image2, image3, nil];
	
	timer= [NSTimer scheduledTimerWithTimeInterval:15 target:self selector:@selector(onTimer) userInfo:nil repeats:YES];
	
	[self onTimer];

	syear = [User userYear:ID];

	if (syear<1) {
		range = -1;
	} else if (syear == 1) {
		range = 1;
	} else if (syear == 2) {
		range = 3;
	} else {
		range = 7;
	} 
	
	[self viewWillAppear:YES];	
	[self.tableView reloadData];
	
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	if(style==0) {
		list = [NSArray arrayWithObjects:@"1학년 1학기", @"1학년 2학기", @"2학년 1학기", @"2학년 2학기", @"3학년 1학기", @"3학년 2학기", nil];
		slist = [Record listOfRecordWithEmail:ID];
	} else {
		list = [NSArray arrayWithObjects:@"1학년 1회차", @"1학년 2회차", @"2학년 1회차", @"2학년 2회차", @"3학년 1회차", @"3학년 2회차",@"3학년 3회차", @"3학년 4회차", nil];
		slist = [Record listOfGradeWithEmail:ID];
	}
	[self.tableView reloadData];
}
- (void)onTimer{
	int index = arc4random()%3;
	imageAD.image = [adArray objectAtIndex:index];
	imageAD.tag = index;
}

- (void)viewDidUnload
{
    [self setLabelTitle:nil];
    [self setLabelMainTitle:nil];
    [self setImageAD:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)setStyle:(int)value{
	style = value;
	if(style==0) {
		slist = [Record listOfRecordWithEmail:ID];
	} else {
		
		slist = [Record listOfGradeWithEmail:ID];
		
	}
	[self.tableView reloadData];
}

- (IBAction)actionBack:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)actionNext:(id)sender {
//	GradeViewController *viewController =[[GradeViewController alloc] init];
//	viewController.style = 0;
//	[self.navigationController pushViewController:viewController animated:YES];	
}
	
#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return list.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
	}
	NSString *rterm = [list objectAtIndex:indexPath.row];
	cell.textLabel.text = rterm;
	cell.textLabel.backgroundColor =[UIColor clearColor];
	cell.accessoryType = UITableViewCellAccessoryNone;
	if (style ==0) {
		for (int i=0; i<slist.count; i++) {
			NSString *term =[slist objectAtIndex:i];
			NSLog(@"%@   %@", term , rterm);
			if ([term isEqualToString:rterm]) {
				cell.accessoryType = UITableViewCellAccessoryCheckmark;
				break;
			}
		}
	} else {
		for (int i=0; i<slist.count; i++) {
			NSNumber *number =[slist objectAtIndex:i];
			if ([number intValue]==indexPath.row+1) {
				cell.accessoryType = UITableViewCellAccessoryCheckmark;
				break;
			}
		}
	}
	
	
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	
	if (indexPath.row>range) {
		cell.textLabel.alpha = 0.4f;
	}
	return cell;
}
	 
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.row>range) {
		return;
	}
	GradeViewController *viewController =[[GradeViewController alloc] init];
	viewController.ID = ID;
	viewController.menu = menu;
	
	viewController.style = style;
	viewController.seq = indexPath.row;
	viewController.title =  [list objectAtIndex:indexPath.row]; 
	[self.navigationController pushViewController:viewController animated:YES];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	UITouch *touch = [touches anyObject];
	if (touch.view == imageAD ) {
		
		ADPop *adPop = [[ADPop alloc] initWithFrame:CGRectMake(0, 0, 320, 460)];
		[self.view addSubview:adPop];
		adPop.index = touch.view.tag;
	}
}

@end
