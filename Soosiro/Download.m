//
//  Download.m
//  SampleDownload
//
//  Created by Myounggyu JANG on 11. 1. 16..
//  Copyright 2011 귀여운 아들. All rights reserved.
//

#import "Download.h"

#define NUMBER(X) [NSNumber numberWithFloat:X]

static Download *sharedInstance = nil;

@implementation Download
@synthesize response;
@synthesize data;
@synthesize delegate;
@synthesize urlString;
@synthesize urlconnection;
@synthesize isDownloading;

- (void) start
{
	self.isDownloading = NO;
	
	NSURL * url = [NSURL URLWithString:self.urlString]; //String을 URL형식으로 변경
	NSLog(@"url %@", url);
	if(!url)
	{
		NSString * reason = [NSString stringWithFormat:@"Could not create URL from string %@", self.urlString];
		[sharedInstance.delegate dataDownloadFailed:reason];
		return;
	}
	
	NSMutableURLRequest * theRequest = [NSMutableURLRequest requestWithURL:url];  //URL 접속 초기화
	NSLog(@"request %@", theRequest);
	if(!theRequest) 
	{
		NSString * reason = [NSString stringWithFormat:@"Could not create URL request from string %@", self.urlString];
		[sharedInstance.delegate dataDownloadFailed:reason];
	}
	
	self.urlconnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
	if(!self.urlconnection)
	{
		NSString *reason = [NSString stringWithFormat:@"URL connection failed for string %@", self.urlString];
		[sharedInstance.delegate dataDownloadFailed:reason];
	}
	
	self.isDownloading = YES;
	
	self.data = [NSMutableData data]; //수신할 데이터의 공간을 마련
	self.response = nil;
	
	
	//NSURLConnection를 thread로 동작 시킵니다.
	[self.urlconnection scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
}



- (void) cleanup
{
	self.data = nil;
	self.response = nil;
	self.urlconnection = nil;
	self.urlString = nil;
	self.isDownloading = NO;
}

//서버 응답 성공 후
- (void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)aResponse
{
	//store the response information 
	// 받을 데이터 타입을 미리 확인하는 부분입니다.
	// UTF8인지, 이미지인지, EUC-KR인지 확인합니다.
	self.response = aResponse;
	
	NSLog(@"[aResponse expectedContentLength] : %d",(int)[aResponse expectedContentLength]);
	//check for bad connection
	if([aResponse expectedContentLength] < 0 )
	{
		NSString *reason = [NSString stringWithFormat:@"Invalid URL [%@]", self.urlString];
		[sharedInstance.delegate dataDownloadFailed:reason];
		[connection cancel];
		[self cleanup];
		return;   
	}
	
	if([aResponse suggestedFilename])  //파일명
	{
		NSLog(@"[aResponse suggestedFilename] %@",[aResponse suggestedFilename]);
		[sharedInstance.delegate didReceiveFilename:[aResponse suggestedFilename]];
	
	}
	
}


//데이터가 완료 될때 까지 데이터를 받아 옵니다.
- (void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)theData 
{
	[self.data appendData:theData]; //받아온 데아타를 추가합니다.
	if(self.response) { //진행 상황을 계산합니다.
		float expectedLength = [self.response expectedContentLength];
		float currentLength = self.data.length;
		float percent = currentLength / expectedLength;
		[sharedInstance.delegate dataDownloadAtPercent:NUMBER(percent)];
	}
}

//다운로드 완료
- (void) connectionDidFinishLoading:(NSURLConnection *)connection
{
	// finished downloading the data, cleaning up
	self.response = nil;
	
	//Delegate is repsonsible for releasing data
	if(sharedInstance.delegate) {
	
		NSData * theData = [self.data retain];
		NSLog(@"완료");
		[sharedInstance.delegate didReceiveData:theData];	
	}

	//스레드가 정지합니다.
	[self.urlconnection unscheduleFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
	[self cleanup];
}

//서버 접속 실패 (http://포함해야함)
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error 
{
	self.isDownloading = NO;
	[sharedInstance.delegate dataDownloadFailed:@"Failed Connection"];
	[sharedInstance.delegate dismissActionSheet];
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	[self cleanup];
}

//Download 클래스를 초기화한다.
+ (Download *)sharedInstance
{
	if (!sharedInstance) {
		sharedInstance = [[self alloc] init];
	}
	return sharedInstance;
}

+ (void) download:(NSString *)aURLString
{
	if(sharedInstance.isDownloading) {
		[sharedInstance.delegate dataDownloadFailed:@"이미 다운로드 중입니다."];
		return;
	}
	sharedInstance.urlString = aURLString;
	[sharedInstance start];
}

+ (void) cancel
{
	if(sharedInstance.isDownloading)[sharedInstance.urlconnection cancel];
}
@end
