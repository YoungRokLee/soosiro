//
//  User.h
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 7. 4..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface User : NSObject
@property (strong, nonatomic) NSString *P_ID;
@property (strong, nonatomic) NSString *P_Nm;
@property (strong, nonatomic) NSString *P_PWD;
@property (strong, nonatomic) NSString *P_MF;
@property (strong, nonatomic) NSString *L_code;
@property (strong, nonatomic) NSString *H_Nm;
@property (strong, nonatomic) NSString *H_Class;
@property (nonatomic) int P_Syear;
@property (strong, nonatomic) NSString *P_Cate;
@property (strong, nonatomic) NSArray *info;
+ (NSArray *)schoolList:(NSString*)L_Code;
+ (NSMutableDictionary *)userInfoWithID:(NSString *)ID;
+ (NSDictionary *)infoWithID:(NSString *)ID;
+ (NSString *)userMF:(NSString*)ID;
+ (int)userYear:(NSString*)ID;
+ (NSArray *)localLists;
+ (NSArray *)hClassLists;
+ (int)userYear:(NSString*)ID;
+ (void)updateInfoLang:(NSString *)ID value:(NSString*)value;
+ (void)updateInfoMath:(NSString *)ID value:(NSString*)value;
- (void)add;
- (void)addInfo;

@end
