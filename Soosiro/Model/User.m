//
//  User.m
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 7. 4..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import "User.h"
#import <sqlite3.h>

@implementation User
@synthesize P_ID;
@synthesize P_Nm;
@synthesize P_PWD;
@synthesize P_MF;
@synthesize L_code;
@synthesize H_Nm;
@synthesize H_Class;
@synthesize P_Syear;
@synthesize P_Cate;
@synthesize info;


+ (int)userYear:(NSString*)ID{
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"soosiro.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return -1;
	}	
	
	int result;
	sqlite3_stmt *statement;
	char *sql ="Select P_SYEAR from '00_User1' where P_ID= ? ";
	
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_text(statement,   1, [ID UTF8String],	-1, SQLITE_TRANSIENT);
		while (sqlite3_step(statement)==SQLITE_ROW) {
			result=sqlite3_column_int(statement, 0);
		}
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	return result;
}


+ (NSString *)userMF:(NSString*)ID{
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"soosiro.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	
	NSString *result;
	sqlite3_stmt *statement;
	char *sql ="Select P_MF from '00_User1' where P_ID= ? ";
	
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_text(statement,   1, [ID UTF8String],	-1, SQLITE_TRANSIENT);
		while (sqlite3_step(statement)==SQLITE_ROW) {
			result=[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement,0)];
		}
		
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	return result;
}


+ (NSMutableDictionary *)userInfoWithID:(NSString*)ID{
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"soosiro.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	
	NSMutableDictionary *dic;
	sqlite3_stmt *statement;
	char *sql ="Select P_Nm, P_MF, L_code, H_Nm, H_Class, P_Syear, P_Cate from '00_User1' where P_ID= ? ";
	
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_text(statement,   1, [ID UTF8String],	-1, SQLITE_TRANSIENT);
		while (sqlite3_step(statement)==SQLITE_ROW) {
			dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:
				   [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"P_Nm",
				   [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)], @"P_MF",
				   [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)], @"L_code",
				   [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)], @"H_Nm",
				   [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 4)], @"H_Class",
					[NSNumber numberWithInt:sqlite3_column_int(statement, 5)], @"P_Syear",
				   [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 6)], @"P_Cate",
				   
				   nil];
		}
	
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	return dic;
}

-(void)add {
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"soosiro.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"error: %s", sqlite3_errmsg(database));
		return;
	}
	
	sqlite3_stmt *statement;
	
	char *sql = "INSERT OR REPLACE INTO '00_User1'(P_ID, P_Nm, P_MF, L_code, H_Nm, H_Class, P_Syear, P_Cate, P_PWD) VALUES(?,?,?,?,?,?,?,?,?)";
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_text(statement, 1, [P_ID	 UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement, 2, [P_Nm	 UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement, 3, [P_MF	 UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement, 4, [L_code  UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement, 5, [H_Nm	 UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement, 6, [H_Class UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_int (statement, 7, P_Syear);
		sqlite3_bind_text(statement, 8, [P_Cate UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement, 9, [P_PWD UTF8String],	-1, SQLITE_TRANSIENT);
		if (sqlite3_step(statement) != SQLITE_DONE) {
			NSLog(@"error: %s", sqlite3_errmsg(database));
			
		}
	} else NSLog(@"error: %s", sqlite3_errmsg(database));
	sqlite3_finalize(statement);
	sqlite3_close(database);
}

+ (void)updateInfoLang:(NSString *)ID value:(NSString*)value{
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"soosiro.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"error: %s", sqlite3_errmsg(database));
		return ;
	}	
	sqlite3_stmt *statement;
	char *sql ="Update '00_User2' set  P_info02=? where P_ID=?";
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_text(statement,   1, [value UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,   2, [ID UTF8String],	-1, SQLITE_TRANSIENT);
		if (sqlite3_step(statement) != SQLITE_DONE) {
			NSLog(@"error: %s", sqlite3_errmsg(database));
		}
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	
	
}

+ (void)updateInfoMath:(NSString *)ID  value:(NSString*)value{
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"soosiro.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"error: %s", sqlite3_errmsg(database));
		return ;
	}	
	sqlite3_stmt *statement;
	char *sql ="Update '00_User2' set  P_info03=? where P_ID=?";
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_text(statement,   1, [value UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,   2, [ID UTF8String],	-1, SQLITE_TRANSIENT);
		if (sqlite3_step(statement) != SQLITE_DONE) {
			NSLog(@"error: %s", sqlite3_errmsg(database));
		}
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	
	
}



+ (NSDictionary *)infoWithID:(NSString *)ID {
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"soosiro.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"error: %s", sqlite3_errmsg(database));
		return nil;
	}	
	
	
	sqlite3_stmt *statement;
	NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
	char *sql ="Select P_info01,  P_info02,  P_info03,  P_info04,  P_info05,  P_info06,  P_info07,  P_info08,  P_info09,  P_info10,  P_info11,  P_info12,  P_info13,  P_info14,  P_info15,  P_info16,  P_info17,  P_info18,  P_info19,  P_info20,  P_info21,  P_info22,  P_info23,  P_info24,  P_info25,  P_info26,  P_info27,  P_info28,  P_info29,  P_info30 FROM '00_User2' where P_ID=?";
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_text(statement,   1, [ID UTF8String],	-1, SQLITE_TRANSIENT);
		while (sqlite3_step(statement)==SQLITE_ROW) {
			for (int i=0; i<30; i++) {
				[dic setValue:[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, i)] forKey: [NSString stringWithUTF8String:(char *)sqlite3_column_name(statement, i)]];
			}

		}
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	return dic;

}

- (void)addInfo
{
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"soosiro.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"error: %s", sqlite3_errmsg(database));
		return;
	}
	
	sqlite3_stmt *statement;
	
	char *sql = "INSERT OR REPLACE INTO '00_User2'(P_ID,  P_info01,  P_info02,  P_info03,  P_info04,  P_info05,  P_info06,  P_info07,  P_info08,  P_info09,  P_info10,  P_info11,  P_info12,  P_info13,  P_info14,  P_info15,  P_info16,  P_info17,  P_info18,  P_info19,  P_info20,  P_info21,  P_info22,  P_info23,  P_info24,  P_info25,  P_info26,  P_info27,  P_info28,  P_info29,  P_info30 ) VALUES(?,?,?,?,?,?,?,?,?,? ,?,?,?,?,?,?,?,?,?,?, ?,?,?,?,?,?,?,?,?,?, ?)";
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_text(statement, 1, [P_ID	 UTF8String],	-1, SQLITE_TRANSIENT);
		
		sqlite3_bind_text(statement, 2, [[info objectAtIndex:0]	 UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement, 3, [[info objectAtIndex:1]	 UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement, 4, [[info objectAtIndex:2]	 UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement, 5, [[info objectAtIndex:3]	 UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement, 6, [[info objectAtIndex:4]	 UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement, 7, [[info objectAtIndex:5]	 UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement, 8, [[info objectAtIndex:6]	 UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement, 9, [[info objectAtIndex:7]	 UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,10, [[info objectAtIndex:8]	 UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,11, [[info objectAtIndex:9]	 UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,12, [[info objectAtIndex:10] UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,13, [[info objectAtIndex:11] UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,14, [[info objectAtIndex:12] UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,15, [[info objectAtIndex:13] UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,16, [[info objectAtIndex:14] UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,17, [[info objectAtIndex:15] UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,18, [[info objectAtIndex:16] UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,19, [[info objectAtIndex:17] UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,20, [[info objectAtIndex:18] UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,21, [[info objectAtIndex:19] UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,22, [[info objectAtIndex:20] UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,23, [[info objectAtIndex:21] UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,24, [[info objectAtIndex:22] UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,25, [[info objectAtIndex:23] UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,26, [[info objectAtIndex:24] UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,27, [[info objectAtIndex:25] UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,28, [[info objectAtIndex:26] UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,29, [[info objectAtIndex:27] UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,30, [[info objectAtIndex:28] UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,31, [[info objectAtIndex:29] UTF8String],	-1, SQLITE_TRANSIENT);

		if (sqlite3_step(statement) != SQLITE_DONE) {
			NSLog(@"error11: %s", sqlite3_errmsg(database));
		}
	} else NSLog(@"error22: %s", sqlite3_errmsg(database));
	sqlite3_finalize(statement);
	sqlite3_close(database);
}

+ (NSArray *)localLists{
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"mysql.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	
	NSMutableArray *result = [NSMutableArray array];
	sqlite3_stmt *statement;
	char *sql ="Select L_CODE, L_NM from '01_Local'";
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		while (sqlite3_step(statement)==SQLITE_ROW) {
			NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
					  [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"L_CODE",
					  [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)], @"L_NM",
					  nil];
			[result addObject:dic];
			
		}
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	return result;
}

+ (NSArray *)schoolList:(NSString*)L_Code{
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"mysql.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	
	NSMutableArray *result = [NSMutableArray array];
	sqlite3_stmt *statement;
	char *sql ="Select H_CODE, H_NM from '02_HSchool' where L_Code = ? order by H_NM";
	
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_text(statement, 1, [L_Code UTF8String],	-1, SQLITE_TRANSIENT);
		while (sqlite3_step(statement)==SQLITE_ROW) {
			NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
								 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"H_CODE",
								 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)], @"H_NM",
								 nil];
			[result addObject:dic];
			
		}
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	return result;
}


+ (NSArray *)hClassLists{
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"mysql.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	
	NSMutableArray *result = [NSMutableArray array];
	sqlite3_stmt *statement;
	char *sql ="Select H_CLASS, H_CLASSCODE from '03_HCLASS'";
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		while (sqlite3_step(statement)==SQLITE_ROW) {
			NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
								 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"H_CLASS",
								 [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)], @"H_CLASSCODE",
								 nil];
			[result addObject:dic];
			
		}
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	return result;
}



@end
