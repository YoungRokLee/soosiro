//
//  Special.m
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 7. 21..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import "Special.h"
#import <sqlite3.h>
@implementation Special

@synthesize P_ID;
@synthesize SHSCHOOL;
@synthesize INOLIMP;
@synthesize MOLIMP;
@synthesize POLIMP;
@synthesize COLIMP;
@synthesize OOLIMP;
@synthesize EOLIMP;
@synthesize AOLIMP;
@synthesize NOLIMP;
@synthesize ROBOT;

+ (BOOL)chkSpecialWithID:(NSString*)ID {
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"soosiro.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return NO;
	}
	
	sqlite3_stmt *statement;
	BOOL exist=NO;
	char *sql = "SELECT SHSCHOOL, INOLIMP,MOLIMP,POLIMP,COLIMP,OOLIMP,EOLIMP,AOLIMP,NOLIMP,ROBOT From '00_User2_2' where P_ID=? ";
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_text(statement, 1, [ID UTF8String], -1, SQLITE_TRANSIENT);
		while (sqlite3_step(statement)==SQLITE_ROW) {
			for (int i=0; i<10; i++) {
				NSString *state = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, i)];			
				if ([state isEqualToString:@"Y"]) {
					exist = YES;
					break;
				}
			} 
		}
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	return exist;
}

+ (void)removeSpecialWithID:(NSString*)ID {
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"soosiro.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"error: %s", sqlite3_errmsg(database));
		return;
	}
	
	sqlite3_stmt *statement;
	char *sql = "Delete from '00_User2_2' Where P_ID = ?";
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_text  (statement, 1, [ID UTF8String],	-1, SQLITE_TRANSIENT);
		if (sqlite3_step(statement) != SQLITE_DONE) {
			NSLog(@"error: %s", sqlite3_errmsg(database));
		}
	} else {
		NSLog(@"Delete Specital  Prepare error");
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
}

-(void)addSpecial {
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"soosiro.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"error: %s", sqlite3_errmsg(database));
		return;
	}
	sqlite3_stmt *statement;
	char *sql = "INSERT OR REPLACE INTO '00_User2_2'(P_ID, SHSCHOOL, INOLIMP, MOLIMP, POLIMP, COLIMP, OOLIMP, EOLIMP, AOLIMP, NOLIMP, ROBOT) VALUES(?,?,?,?,?,?,?,?,?,?,?)";
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_text(statement, 1,  [P_ID UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement, 2, [SHSCHOOL UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement, 3, [INOLIMP UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement, 4, [MOLIMP UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement, 5, [POLIMP UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement, 6, [COLIMP UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement, 7, [OOLIMP UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement, 8, [EOLIMP UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement, 9, [AOLIMP UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement, 10, [NOLIMP UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement, 11, [ROBOT UTF8String],	-1, SQLITE_TRANSIENT);
			if (sqlite3_step(statement) != SQLITE_DONE) {
			NSLog(@"error: %s", sqlite3_errmsg(database));
		}
	} else {
		NSLog(@"addLanguage Insert Prepare error");
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
}

+ (NSArray *)specialRecordWithEmail:(NSString*)P_ID{
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"soosiro.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"error: %s", sqlite3_errmsg(database));
		return nil;
	}	
	
	NSMutableArray *result = [NSMutableArray array];
	sqlite3_stmt *statement;
	char *sql ="Select SHSCHOOL, INOLIMP, MOLIMP, POLIMP, COLIMP, OOLIMP, EOLIMP, AOLIMP, NOLIMP, ROBOT from '00_User2_2' where P_ID=?";
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_text(statement,   1, [P_ID UTF8String],	-1, SQLITE_TRANSIENT);
		while (sqlite3_step(statement)==SQLITE_ROW) {
			for (int i=0; i<10; i++) {
				NSMutableDictionary *dic=[NSMutableDictionary dictionaryWithObjectsAndKeys:[NSString stringWithUTF8String:(char *)sqlite3_column_name(statement, i)], @"content", [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, i)], @"grade", nil];
				[result addObject:dic];
			}
			
		}
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	return result;
}

@end
