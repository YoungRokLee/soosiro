//
//  Test.m
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 7. 4..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import "Test.h"
#import <sqlite3.h>

@implementation Test
@synthesize lang;
@synthesize math;
@synthesize fore;
@synthesize inqu1;
@synthesize inqu2;

-(void)addRecord {
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"soosiro.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return;
	}
	
	sqlite3_stmt *statement;
	BOOL isExist=NO;
	char *sql = "SELECT count(*) From 'Test_User3' ";
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		while (sqlite3_step(statement)==SQLITE_ROW) {
			if(sqlite3_column_int(statement, 0)>0) isExist = YES; 
		}
	}
	if (!isExist)	{
		char *sql = "INSERT INTO 'Test_User3' (TR_lang, TR_math, TR_fore, TR_inqu1, TR_inqu2) VALUES(?,?,?,?,?)";
		if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
			sqlite3_bind_double(statement, 1, lang);
			sqlite3_bind_double(statement, 2, math);
			sqlite3_bind_double(statement, 3, fore);
			sqlite3_bind_double(statement, 4, inqu1);
			sqlite3_bind_double(statement, 5, inqu2);
			if (sqlite3_step(statement) != SQLITE_DONE) {
				NSLog(@"Error");
			}
		}
	}
	else {
		char *sql = "UPDATE 'Test_User3' set TR_lang=?, TR_math=?, TR_fore=?, TR_inqu1=?, TR_inqu2=? ";
		if(sqlite3_prepare_v2	(database, sql, -1, &statement, NULL) == SQLITE_OK) {
			sqlite3_bind_double(statement, 1, lang);
			sqlite3_bind_double(statement, 2, math);
			sqlite3_bind_double(statement, 3, fore);
			sqlite3_bind_double(statement, 4, inqu1);
			sqlite3_bind_double(statement, 5, inqu2);
			if (sqlite3_step(statement) != SQLITE_DONE) {
				NSLog(@"Error");
			}
		}
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
}

-(void)addGrade {
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"soosiro.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return;
	}
	
	sqlite3_stmt *statement;
	BOOL isExist=NO;
	char *sql = "SELECT count(*) From 'Test_User4' ";
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		while (sqlite3_step(statement)==SQLITE_ROW) {
			if(sqlite3_column_int(statement, 0)>0) isExist = YES; 
		}
	}
	if (!isExist)	{
		char *sql = "INSERT INTO 'Test_User4' (TG_lang, TG_math, TG_fore, TG_inqu1, TG_inqu2) VALUES(?,?,?,?,?)";
		if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
			sqlite3_bind_double(statement, 1, lang);
			sqlite3_bind_double(statement, 2, math);
			sqlite3_bind_double(statement, 3, fore);
			sqlite3_bind_double(statement, 4, inqu1);
			sqlite3_bind_double(statement, 5, inqu2);
			if (sqlite3_step(statement) != SQLITE_DONE) {
				NSLog(@"Error");
			}
		}
	}
	else {
		char *sql = "UPDATE 'Test_User4' set TG_lang=?, TG_math=?, TG_fore=?, TG_inqu1=?, TG_inqu2=? ";
		if(sqlite3_prepare_v2	(database, sql, -1, &statement, NULL) == SQLITE_OK) {
			sqlite3_bind_double(statement, 1, lang);
			sqlite3_bind_double(statement, 2, math);
			sqlite3_bind_double(statement, 3, fore);
			sqlite3_bind_double(statement, 4, inqu1);
			sqlite3_bind_double(statement, 5, inqu2);
			if (sqlite3_step(statement) != SQLITE_DONE) {
				NSLog(@"Error");
			}
		}
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
}


+ (NSNumber*)avgValue {
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"soosiro.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	
	NSNumber *result = [[NSNumber alloc] init];
	sqlite3_stmt *statement;
	char *sql ="select (avg(tr_lang)+ avg(tr_math)+avg(tr_fore)+( (avg(tr_inqu1)+avg(tr_inqu2))/2 ) )/4 from 'Test_user3'";
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		while (sqlite3_step(statement)==SQLITE_ROW) {
			result=[NSNumber numberWithDouble:sqlite3_column_double(statement, 0)];
		}
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	return result;
	
}


+ (NSArray *)record {
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"soosiro.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	
	NSMutableArray *result = [NSMutableArray array];
	sqlite3_stmt *statement;
	char *sql ="Select TR_lang, TR_math, TR_fore, TR_inqu1, TR_inqu2 from 'Test_User3' ";
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		while (sqlite3_step(statement)==SQLITE_ROW) {
			result=[NSMutableArray arrayWithObjects:
					[NSMutableDictionary dictionaryWithObjectsAndKeys:@"언어", @"content", [NSNumber numberWithDouble:sqlite3_column_double(statement, 0)], @"grade", nil],
					[NSMutableDictionary dictionaryWithObjectsAndKeys:@"수리", @"content", [NSNumber numberWithDouble:sqlite3_column_double(statement, 1)], @"grade", nil],
					[NSMutableDictionary dictionaryWithObjectsAndKeys:@"외국어", @"content", [NSNumber numberWithDouble:sqlite3_column_double(statement, 2)], @"grade", nil],
					[NSMutableDictionary dictionaryWithObjectsAndKeys:@"탐구-1", @"content", [NSNumber numberWithDouble:sqlite3_column_double(statement, 3)], @"grade", nil],
					[NSMutableDictionary dictionaryWithObjectsAndKeys:@"탐구-2", @"content", [NSNumber numberWithDouble:sqlite3_column_double(statement, 4)], @"grade", nil],
					nil];
		}
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);

	return result;
}

+ (NSArray *)grade {
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"soosiro.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	
	NSMutableArray *result = [NSMutableArray array];
	sqlite3_stmt *statement;
	char *sql ="Select TG_lang, TG_math, TG_fore, TG_inqu1, TG_inqu2 from 'Test_User4'";
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		while (sqlite3_step(statement)==SQLITE_ROW) {
			result=[NSMutableArray arrayWithObjects:
					[NSMutableDictionary dictionaryWithObjectsAndKeys:@"언어", @"content", [NSNumber numberWithInt:sqlite3_column_int(statement, 0)], @"grade", nil],
					[NSMutableDictionary dictionaryWithObjectsAndKeys:@"수리", @"content", [NSNumber numberWithInt:sqlite3_column_int(statement, 1)], @"grade", nil],
					[NSMutableDictionary dictionaryWithObjectsAndKeys:@"외국어", @"content", [NSNumber numberWithInt:sqlite3_column_int(statement, 2)], @"grade", nil],
					[NSMutableDictionary dictionaryWithObjectsAndKeys:@"탐구-1", @"content", [NSNumber numberWithInt:sqlite3_column_int(statement, 3)], @"grade", nil],
					[NSMutableDictionary dictionaryWithObjectsAndKeys:@"탐구-2", @"content", [NSNumber numberWithInt:sqlite3_column_int(statement, 4)], @"grade", nil],
					nil];
		}
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);

	return result;
}


+ (NSMutableArray *)gradeArray{
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"soosiro.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	NSMutableArray *result = [NSMutableArray array];
	sqlite3_stmt *statement;
	char *sql ="Select TG_lang, TG_math, TG_fore, TG_inqu1, TG_inqu2 from 'test_User4' ";
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		
		while (sqlite3_step(statement)==SQLITE_ROW) {
			[result addObject:[NSNumber numberWithInt:sqlite3_column_int(statement, 0)]];
			[result addObject:[NSNumber numberWithInt:sqlite3_column_int(statement, 1)]];
			[result addObject:[NSNumber numberWithInt:sqlite3_column_int(statement, 2)]];
			[result addObject:[NSNumber numberWithInt:sqlite3_column_int(statement, 3)]];
			[result addObject:[NSNumber numberWithInt:sqlite3_column_int(statement, 4)]];
		}
	}
	
	NSLog(@"after %@", result);
	sqlite3_finalize(statement);
	sqlite3_close(database);
	return result;
}

+ (BOOL)chkGrade {
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"soosiro.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return NO;
	}
	
	sqlite3_stmt *statement;
	BOOL exist=NO;
	char *sql = "SELECT count(*) From 'test_User4' where TG_lang+TG_math+TG_fore+TG_inqu1+ TG_inqu2>0 ";
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		while (sqlite3_step(statement)==SQLITE_ROW) {
			if(sqlite3_column_int(statement, 0)>0) exist = YES; 
		}
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	return exist;
}

+ (BOOL)chkRecord {
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"soosiro.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return NO;
	}
	
	sqlite3_stmt *statement;
	BOOL exist=NO;
	char *sql = "SELECT count(*) From 'test_User3' where TR_lang+TR_math+TR_fore+TR_inqu1+ TR_inqu2>0 ";
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		while (sqlite3_step(statement)==SQLITE_ROW) {
			if(sqlite3_column_int(statement, 0)>0) exist = YES; 
		}
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	return exist;
}


@end
