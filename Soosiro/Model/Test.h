//
//  Test.h
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 7. 4..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Test : NSObject
@property (nonatomic) double lang;
@property (nonatomic) double math;
@property (nonatomic) double fore;
@property (nonatomic) double inqu1;
@property (nonatomic) double inqu2;
+ (NSArray *)record;
+ (NSArray *)grade;
+ (NSNumber*)avgValue;
+ (BOOL)chkGrade;
+ (BOOL)chkRecord;
- (void)addRecord;
- (void)addGrade;
+ (NSMutableArray *)gradeArray;
@end
