//
//  Record.h
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 7. 3..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Record : NSObject
@property (strong, nonatomic) NSString *ID;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *term;
@property (nonatomic) int seq;
@property (nonatomic) double lang;
@property (nonatomic) double math;
@property (nonatomic) double fore;
@property (nonatomic) double inqu1;
@property (nonatomic) double inqu2;
+ (BOOL)chkGradeWithID:(NSString*)ID;
+ (BOOL)chkRecordWithID:(NSString*)ID;
+ (NSArray *)listOfRecordWithEmail:(NSString *)email;
+ (NSArray *)listOfGradeWithEmail:(NSString *)email;
+ (NSArray *)recordWithEmail:(NSString*)email term:(NSString *)term;
+ (NSArray *)gradeWithEmail:(NSString*)email seq:(int)seq ;
+ (NSInteger)langWithEmail:(NSString*)ID;
+ (NSInteger)mathWithEmail:(NSString*)ID;
+ (NSNumber*)avgValueWithID:(NSString*)ID;
+ (NSMutableArray *)gradeArrayWithEmail:(NSString*)ID;
-(void)addRecord;
-(void)addGrade;
+ (BOOL)chkRecordWithID:(NSString*)ID term:(NSString*)term;
+ (BOOL)chkGradeWithID:(NSString*)ID seq:(int)seq;

@end
