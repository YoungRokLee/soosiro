//
//  Special.h
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 7. 21..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Special : NSObject
@property (strong, nonatomic) NSString *P_ID;
@property (strong, nonatomic) NSString *SHSCHOOL;
@property (strong, nonatomic) NSString *INOLIMP;
@property (strong, nonatomic) NSString *MOLIMP;
@property (strong, nonatomic) NSString *POLIMP;
@property (strong, nonatomic) NSString *COLIMP;
@property (strong, nonatomic) NSString *EOLIMP;
@property (strong, nonatomic) NSString *AOLIMP;
@property (strong, nonatomic) NSString *NOLIMP;
@property (strong, nonatomic) NSString *ROBOT;
@property (strong, nonatomic) NSString *OOLIMP;
+ (BOOL)chkSpecialWithID:(NSString*)ID;
+ (void)removeSpecialWithID:(NSString*)ID ;
- (void)addSpecial;
+ (NSArray *)specialRecordWithEmail:(NSString*)P_ID;
@end
