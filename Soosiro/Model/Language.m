//
//  Language.m
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 7. 5..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import "Language.h"
#import <sqlite3.h>
@implementation Language
@synthesize P_ID;

@synthesize ENG_TEST;
@synthesize ENG_SCORE;
@synthesize CHI;
@synthesize DEU;
@synthesize FRA;
@synthesize JAP;
@synthesize RUS;
@synthesize SPA;
@synthesize HAN;

+ (void)removeLanguageWithID:(NSString*)ID {
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"soosiro.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"error: %s", sqlite3_errmsg(database));
		return;
	}
	
	sqlite3_stmt *statement;
	char *sql = "Delete from '00_User2_1' Where P_ID = ?";
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_text  (statement, 1, [ID UTF8String],	-1, SQLITE_TRANSIENT);
			if (sqlite3_step(statement) != SQLITE_DONE) {
			NSLog(@"error: %s", sqlite3_errmsg(database));
		}
	} else {
		NSLog(@"Delete Language  Prepare error");
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
}


+ (BOOL)chkEngWithID:(NSString*)ID {
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"soosiro.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return NO;
	}
	
	sqlite3_stmt *statement;
	BOOL exist=NO;
	char *sql = "SELECT ENG_SCORE From '00_User2_1' where P_ID=? ";
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_text(statement, 1, [ID UTF8String], -1, SQLITE_TRANSIENT);
		while (sqlite3_step(statement)==SQLITE_ROW) {
			if(sqlite3_column_int(statement, 0)>0) exist = YES; 
		}
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	return exist;
}


+ (BOOL)chkLangWithID:(NSString*)ID {
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"soosiro.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return NO;
	}
	
	sqlite3_stmt *statement;
	BOOL exist=NO;
	char *sql = "SELECT ENG_SCORE+CHI+DEU+FRA+ JAP+RUS+SPA+HAN From '00_User2_1' where P_ID=? ";
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_text(statement, 1, [ID UTF8String], -1, SQLITE_TRANSIENT);
		while (sqlite3_step(statement)==SQLITE_ROW) {
			if(sqlite3_column_int(statement, 0)>0) exist = YES; 
		}
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	return exist;
}

-(void)addLanguage {
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"soosiro.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"error: %s", sqlite3_errmsg(database));
		return;
	}
	
	sqlite3_stmt *statement;
	char *sql = "INSERT OR REPLACE INTO '00_User2_1'(P_ID, ENG_TEST, ENG_SCORE, CHI, DEU, FRA, JAP, RUS, SPA, HAN) VALUES(?,?,?,?,?,?,?,?,?,?)";
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_text  (statement, 1, [P_ID UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text  (statement, 2, [ENG_TEST UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_int   (statement, 3, ENG_SCORE);
		sqlite3_bind_int   (statement, 4, CHI);
		sqlite3_bind_int   (statement, 5, DEU);
		sqlite3_bind_int   (statement, 6, FRA);
		sqlite3_bind_int   (statement, 7, JAP);
		sqlite3_bind_int   (statement, 8, RUS);
		sqlite3_bind_int   (statement, 9, SPA);
		sqlite3_bind_int   (statement, 10, HAN);
		if (sqlite3_step(statement) != SQLITE_DONE) {
			NSLog(@"error: %s", sqlite3_errmsg(database));
		}
	} else {
		NSLog(@"addLanguage Insert Prepare error");
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
}


+ (NSArray *)languageRecordWithEmail:(NSString*)P_ID{
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"soosiro.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	
	NSMutableArray *result;
	sqlite3_stmt *statement;
	char *sql ="Select CHI, DEU, FRA, JAP, RUS, SPA, HAN from '00_User2_1' where P_ID=?";
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_text(statement,   1, [P_ID UTF8String],	-1, SQLITE_TRANSIENT);
		while (sqlite3_step(statement)==SQLITE_ROW) {
			
			result = [NSMutableArray arrayWithObjects:
						[NSMutableDictionary dictionaryWithObjectsAndKeys:@"영어", @"content", [NSNumber numberWithInt:0], @"grade", nil],
						[NSMutableDictionary dictionaryWithObjectsAndKeys:@"중국어", @"content", [NSNumber numberWithInt:sqlite3_column_int(statement, 0)], @"grade", nil],
						[NSMutableDictionary dictionaryWithObjectsAndKeys:@"독일어", @"content", [NSNumber numberWithInt:sqlite3_column_int(statement, 1)], @"grade", nil],
						[NSMutableDictionary dictionaryWithObjectsAndKeys:@"프랑스어", @"content", [NSNumber numberWithInt:sqlite3_column_int(statement, 2)], @"grade", nil],
						[NSMutableDictionary dictionaryWithObjectsAndKeys:@"일본어", @"content", [NSNumber numberWithInt:sqlite3_column_int(statement, 3)], @"grade", nil],
						[NSMutableDictionary dictionaryWithObjectsAndKeys:@"러시아어", @"content", [NSNumber numberWithInt:sqlite3_column_int(statement, 4)], @"grade", nil],
						[NSMutableDictionary dictionaryWithObjectsAndKeys:@"스페인어", @"content", [NSNumber numberWithInt:sqlite3_column_int(statement, 5)], @"grade", nil],
						[NSMutableDictionary dictionaryWithObjectsAndKeys:@"한자", @"content", [NSNumber numberWithInt:sqlite3_column_int(statement, 6)], @"grade", nil],
					  nil];

		}
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	return result;
}

+ (NSMutableDictionary *)englishRecordWithEmail:(NSString*)P_ID{
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"soosiro.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"error: %s", sqlite3_errmsg(database));
		return nil;
	}	
	
	NSMutableDictionary *result;
	sqlite3_stmt *statement;
	char *sql ="Select ENG_TEST, ENG_SCORE From '00_User2_1' where P_ID=? and ENG_TEST is not null";
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_text(statement,   1, [P_ID UTF8String],	-1, SQLITE_TRANSIENT);
		while (sqlite3_step(statement)==SQLITE_ROW) {
			result=[NSMutableDictionary dictionaryWithObjectsAndKeys:[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)], @"title", [NSNumber numberWithInt:sqlite3_column_int(statement, 1)], @"score", nil];
		}
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	return result;
}

@end
