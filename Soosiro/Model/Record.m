//
//  Record.m
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 7. 3..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import "Record.h"
#import <sqlite3.h>

@implementation Record
@synthesize ID;
@synthesize name;
@synthesize seq;
@synthesize lang;
@synthesize math;
@synthesize fore;
@synthesize inqu1;
@synthesize inqu2;
@synthesize term;

+ (BOOL)chkRecordWithID:(NSString*)ID term:(NSString*)term {
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"soosiro.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return NO;
	}
	
	sqlite3_stmt *statement;
	BOOL exist=NO;
	char *sql = "SELECT count(*) From '00_User3' where P_ID=? and R_TERM = ? and R_lang+R_math+R_fore+R_inqu1+R_inqu2>0 ";
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_text(statement, 1, [ID UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement, 2, [term UTF8String], -1, SQLITE_TRANSIENT);
		while (sqlite3_step(statement)==SQLITE_ROW) {
			if(sqlite3_column_int(statement, 0)>0) exist = YES; 
		}
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	return exist;
}

+ (BOOL)chkGradeWithID:(NSString*)ID seq:(int)seq{
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"soosiro.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return NO;
	}
	
	sqlite3_stmt *statement;
	BOOL exist=NO;
	char *sql = "SELECT count(*) From '00_User4' where P_ID=? and G_SEQ = ? and G_lang+G_math+G_fore+G_inqu1+G_inqu2>0 ";
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_text(statement, 1, [ID UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_int (statement, 2, seq);
		while (sqlite3_step(statement)==SQLITE_ROW) {
			if(sqlite3_column_int(statement, 0)>0) exist = YES; 
		}
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	return exist;
}


+ (BOOL)chkGradeWithID:(NSString*)ID {
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"soosiro.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return NO;
	}
	
	sqlite3_stmt *statement;
	BOOL exist=NO;
	char *sql = "SELECT count(*) From '00_User3' where P_ID=? ";
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_text(statement, 1, [ID UTF8String], -1, SQLITE_TRANSIENT);
		while (sqlite3_step(statement)==SQLITE_ROW) {
			if(sqlite3_column_int(statement, 0)>0) exist = YES; 
		}
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	return exist;
}

+ (BOOL)chkRecordWithID:(NSString*)ID {
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"soosiro.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return NO;
	}
	
	sqlite3_stmt *statement;
	BOOL exist=NO;
	char *sql = "SELECT count(*) From '00_User4' where P_ID=? ";
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_text(statement, 1, [ID UTF8String], -1, SQLITE_TRANSIENT);
		while (sqlite3_step(statement)==SQLITE_ROW) {
			if(sqlite3_column_int(statement, 0)>0) exist = YES; 
		}
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	return exist;
}

	
	
-(void)addRecord {
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"soosiro.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return;
	}
	
	sqlite3_stmt *statement;
	BOOL exist=NO;
	char *sql = "SELECT count(*) From '00_User3' where P_ID=? and R_term=?";
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_text(statement, 1, [ID UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement, 2, [term UTF8String], -1, SQLITE_TRANSIENT);
		while (sqlite3_step(statement)==SQLITE_ROW) {
			if(sqlite3_column_int(statement, 0)>0) exist = YES; 
		}
	}
	if (!exist)	{
		char *sql = "INSERT INTO '00_User3'(P_ID,R_term, R_lang, R_math, R_fore, R_inqu1, R_inqu2) VALUES(?,?,?,?,?,?,?)";
		if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
			sqlite3_bind_text  (statement, 1, [ID UTF8String],	-1, SQLITE_TRANSIENT);
			sqlite3_bind_text  (statement, 2, [term UTF8String],	-1, SQLITE_TRANSIENT);
			sqlite3_bind_double(statement, 3, lang);
			sqlite3_bind_double(statement, 4, math);
			sqlite3_bind_double(statement, 5, fore);
			sqlite3_bind_double(statement, 6, inqu1);
			sqlite3_bind_double(statement, 7, inqu2);
			if (sqlite3_step(statement) != SQLITE_DONE) {
				NSLog(@"Error");
			}
		}
	}
	else {
		char *sql = "UPDATE '00_User3' set R_lang=?, R_math=?, R_fore=?, R_inqu1=?, R_inqu2=? where P_ID=? and R_term=? ";
		if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
			sqlite3_bind_text  (statement, 6, [ID UTF8String],	-1, SQLITE_TRANSIENT);
			sqlite3_bind_text  (statement, 7, [term UTF8String],	-1, SQLITE_TRANSIENT);
			sqlite3_bind_double(statement, 1, lang);
			sqlite3_bind_double(statement, 2, math);
			sqlite3_bind_double(statement, 3, fore);
			sqlite3_bind_double(statement, 4, inqu1);
			sqlite3_bind_double(statement, 5, inqu2);
			if (sqlite3_step(statement) != SQLITE_DONE) {
				NSLog(@"Error");
			}
		}
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
}

-(void)addGrade {
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"soosiro.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return;
	}
	
	sqlite3_stmt *statement;
	BOOL exist=NO;
	char *sql = "SELECT count(*) From '00_User4' where P_ID=? and G_seq=?";
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_text(statement, 1, [ID UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_int (statement, 2, seq);
		while (sqlite3_step(statement)==SQLITE_ROW) {
			if(sqlite3_column_int(statement, 0)>0) exist = YES; 
		}
	}
	if (!exist)	{
		char *sql = "INSERT INTO '00_User4'(P_ID, G_seq, G_lang, G_math, G_fore, G_inqu1, G_inqu2) VALUES(?,?,?,?,?,?,?)";
		if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
			sqlite3_bind_text  (statement, 1, [ID UTF8String],	-1, SQLITE_TRANSIENT);
			sqlite3_bind_int   (statement, 2, seq);
			sqlite3_bind_double(statement, 3, lang);
			sqlite3_bind_double(statement, 4, math);
			sqlite3_bind_double(statement, 5, fore);
			sqlite3_bind_double(statement, 6, inqu1);
			sqlite3_bind_double(statement, 7, inqu2);
			if (sqlite3_step(statement) != SQLITE_DONE) {
				NSLog(@"Error");
			}
		}
	}
	else {
		char *sql = "UPDATE '00_User4' set G_lang=?, G_math=?, G_fore=?, G_inqu1=?, G_inqu2=? where P_ID=? and G_seq=? ";
		if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
			sqlite3_bind_text  (statement, 6, [ID UTF8String],	-1, SQLITE_TRANSIENT);
			sqlite3_bind_int   (statement, 7, seq);
			sqlite3_bind_double(statement, 1, lang);
			sqlite3_bind_double(statement, 2, math);
			sqlite3_bind_double(statement, 3, fore);
			sqlite3_bind_double(statement, 4, inqu1);
			sqlite3_bind_double(statement, 5, inqu2);
			if (sqlite3_step(statement) != SQLITE_DONE) {
				NSLog(@"Error");
			}
		}
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
}

+ (NSArray *)listOfRecordWithEmail:(NSString*)ID {
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"soosiro.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	
	NSMutableArray *result = [NSMutableArray array];
	sqlite3_stmt *statement;
	char *sql ="Select R_term from '00_User3' where P_ID=? and R_lang+R_math+R_fore+R_inqu1+R_inqu2>0";
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_text(statement,   1, [ID UTF8String],	-1, SQLITE_TRANSIENT);
		while (sqlite3_step(statement)==SQLITE_ROW) {
			NSString *term = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
			NSString *rterm = [NSString stringWithFormat:@"%@학년 %@학기", [term substringWithRange:NSMakeRange(0, 1)],[term substringWithRange:NSMakeRange(2, 1)]];
			[result addObject:rterm];
		}
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	return result;
}

+ (NSArray *)recordWithEmail:(NSString*)ID term:(NSString *)term {
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"soosiro.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	
	NSMutableArray *result = [NSMutableArray array];
	sqlite3_stmt *statement;
	char *sql ="Select R_lang, R_math, R_fore, R_inqu1, R_inqu2 from '00_User3' where P_ID=? and R_term=?";
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_text(statement,   1, [ID UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(statement,   2, [term UTF8String],	-1, SQLITE_TRANSIENT);
		while (sqlite3_step(statement)==SQLITE_ROW) {
			result=[NSMutableArray arrayWithObjects:
			 [NSMutableDictionary dictionaryWithObjectsAndKeys:@"언어", @"content", [NSNumber numberWithInt:sqlite3_column_int(statement, 0)], @"grade", nil],
			 [NSMutableDictionary dictionaryWithObjectsAndKeys:@"수리", @"content", [NSNumber numberWithInt:sqlite3_column_int(statement, 1)], @"grade", nil],
			 [NSMutableDictionary dictionaryWithObjectsAndKeys:@"외국어", @"content", [NSNumber numberWithInt:sqlite3_column_int(statement, 2)], @"grade", nil],
			 [NSMutableDictionary dictionaryWithObjectsAndKeys:@"탐구-1", @"content", [NSNumber numberWithInt:sqlite3_column_int(statement, 3)], @"grade", nil],
			 [NSMutableDictionary dictionaryWithObjectsAndKeys:@"탐구-2", @"content", [NSNumber numberWithInt:sqlite3_column_int(statement, 4)], @"grade", nil],
			 nil];
		}
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	return result;
}

+ (NSNumber*)avgValueWithID:(NSString*)ID {
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"soosiro.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	
	NSNumber *result = [[NSNumber alloc] init];
	sqlite3_stmt *statement;
	char *sql ="select (avg(r_lang)+ avg(r_math)+avg(r_fore)+   (avg(r_inqu1)+avg(r_inqu2))/2     )     /4 from '00_user3' where P_ID==? and r_lang+r_math+r_fore+r_inqu1+r_inqu2 <>0 ";
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_text(statement,   1, [ID UTF8String],	-1, SQLITE_TRANSIENT);
		while (sqlite3_step(statement)==SQLITE_ROW) {
			result=[NSNumber numberWithDouble:sqlite3_column_double(statement, 0)];
		}
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	return result;

}

+ (NSArray *)listOfGradeWithEmail:(NSString*)ID {
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"soosiro.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	

	NSMutableArray *result = [NSMutableArray array];
	sqlite3_stmt *statement;
	char *sql ="Select G_seq from '00_User4' where P_ID=? and G_lang+G_math+G_fore+G_inqu1+G_inqu2>0";
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_text(statement,   1, [ID UTF8String],	-1, SQLITE_TRANSIENT);
		while (sqlite3_step(statement)==SQLITE_ROW) {
			int seq = sqlite3_column_int(statement, 0);
			[result addObject:[NSNumber numberWithInt:seq]];
		}
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	return result;
}

+ (NSArray *)gradeWithEmail:(NSString*)ID seq:(int)seq {
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"soosiro.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	
	NSMutableArray *result = [NSMutableArray array];
	sqlite3_stmt *statement;
	char *sql ="Select G_lang, G_math, G_fore, G_inqu1, G_inqu2 from '00_User4' where P_ID=? and G_seq=? ";
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_text(statement,   1, [ID UTF8String],	-1, SQLITE_TRANSIENT);
		sqlite3_bind_int (statement,   2, seq);
		while (sqlite3_step(statement)==SQLITE_ROW) {
			result=[NSMutableArray arrayWithObjects:
					[NSMutableDictionary dictionaryWithObjectsAndKeys:@"언어", @"content", [NSNumber numberWithInt:sqlite3_column_int(statement, 0)], @"grade", nil],
					[NSMutableDictionary dictionaryWithObjectsAndKeys:@"수리", @"content", [NSNumber numberWithInt:sqlite3_column_int(statement, 1)], @"grade", nil],
					[NSMutableDictionary dictionaryWithObjectsAndKeys:@"외국어", @"content", [NSNumber numberWithInt:sqlite3_column_int(statement, 2)], @"grade", nil],
					[NSMutableDictionary dictionaryWithObjectsAndKeys:@"탐구-1", @"content", [NSNumber numberWithInt:sqlite3_column_int(statement, 3)], @"grade", nil],
					[NSMutableDictionary dictionaryWithObjectsAndKeys:@"탐구-2", @"content", [NSNumber numberWithInt:sqlite3_column_int(statement, 4)], @"grade", nil],
					nil];
		}
	}
	sqlite3_finalize(statement);
	sqlite3_close(database);
	return result;
}

+ (NSMutableArray *)gradeArrayWithEmail:(NSString*)ID{
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"soosiro.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return nil;
	}	
	NSMutableArray *result = [NSMutableArray array];
	sqlite3_stmt *statement;
	char *sql ="Select G_lang, G_math, G_fore, G_inqu1, G_inqu2 from '00_User4' where P_ID=? and G_lang+G_math+G_fore+G_inqu1+G_inqu2>0 order by G_SEQ desc limit 1";
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_text(statement,   1, [ID UTF8String],	-1, SQLITE_TRANSIENT);
		
		while (sqlite3_step(statement)==SQLITE_ROW) {
			[result addObject:[NSNumber numberWithInt:sqlite3_column_int(statement, 0)]];
			[result addObject:[NSNumber numberWithInt:sqlite3_column_int(statement, 1)]];
			[result addObject:[NSNumber numberWithInt:sqlite3_column_int(statement, 2)]];
			[result addObject:[NSNumber numberWithInt:sqlite3_column_int(statement, 3)]];
			[result addObject:[NSNumber numberWithInt:sqlite3_column_int(statement, 4)]];
		}
	}
	//NSLog(@"before %@", result);
	//[result sortUsingSelector:@selector(compare:)];
	NSLog(@"after %@", result);
	sqlite3_finalize(statement);
	sqlite3_close(database);
	return result;
}



+ (NSInteger )langWithEmail:(NSString*)ID{
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"soosiro.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return 0;
	}	
	
	NSNumber *result = [[NSNumber alloc] init];
	sqlite3_stmt *statement;
	char *sql ="Select G_lang from '00_User4' where P_ID=? order by G_Seq desc limit 1";
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_text(statement,   1, [ID UTF8String],	-1, SQLITE_TRANSIENT);
	
		while (sqlite3_step(statement)==SQLITE_ROW) {
			result = [NSNumber numberWithInt:sqlite3_column_int(statement, 0)] ;
		}
	}
	
	sqlite3_finalize(statement);
	sqlite3_close(database);
	return [result intValue];
}

+ (NSInteger )mathWithEmail:(NSString*)ID{
	sqlite3 *database;
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"soosiro.sqlite"];
	if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
		sqlite3_close(database);
		NSLog(@"Error");
		return 0;
	}	
	
	NSNumber *result = [[NSNumber alloc] init];
	sqlite3_stmt *statement;
	char *sql ="Select G_math from '00_User4' where P_ID=? order by G_Seq desc limit 1";
	if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_text(statement,   1, [ID UTF8String],	-1, SQLITE_TRANSIENT);
		
		while (sqlite3_step(statement)==SQLITE_ROW) {
			result = [NSNumber numberWithInt:sqlite3_column_int(statement, 0)] ;
		}
	}
	
	sqlite3_finalize(statement);
	sqlite3_close(database);
	return [result intValue];
}


@end
