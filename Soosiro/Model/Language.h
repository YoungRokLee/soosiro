//
//  Language.h
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 7. 5..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Language : NSObject
@property (strong, nonatomic) NSString *P_ID;
@property (strong, nonatomic) NSString *ENG_TEST;
@property (nonatomic) int ENG_SCORE;
@property (nonatomic) int CHI;
@property (nonatomic) int DEU;
@property (nonatomic) int FRA;
@property (nonatomic) int JAP;
@property (nonatomic) int RUS;
@property (nonatomic) int SPA;
@property (nonatomic) int HAN;
+ (BOOL)chkLangWithID:(NSString*)ID;
+ (BOOL)chkEngWithID:(NSString*)ID;
+ (NSArray *)languageRecordWithEmail:(NSString*)P_ID;
+ (NSMutableDictionary *)englishRecordWithEmail:(NSString*)P_ID;
+ (void)removeLanguageWithID:(NSString*)ID;
- (void)addLanguage;

@end
