//
//  StrategyViewController.m
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 7. 15..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import "StrategyViewController.h"
#import "User.h"
#import "Test.h"
#import "Proviso.h"
#import "RecResultViewController.h"
#import "SimpleServiceViewController.h"
@interface StrategyViewController (){
	NSMutableArray *list;
	NSMutableArray *list1;
	UIActivityIndicatorView *indicator;
	NSArray *adArray;
	NSTimer *timer;
	NSMutableArray *flist;
	int selectedRow;
}


@end

@implementation StrategyViewController
@synthesize ID;
@synthesize category;
@synthesize tableView=_tableView;
@synthesize lableMainTitle;
@synthesize unit;
@synthesize mf;
@synthesize series03;
@synthesize series02;
@synthesize series01;
@synthesize menu;
@synthesize labelTitle;
@synthesize imageAD;

- (void)viewDidLoad
{
    [super viewDidLoad];
	selectedRow=-1;
	lableMainTitle.text = [Title mainTilteWithMenu:menu];
	if (menu==3) labelTitle.text = @"추천대학리스트";
	UIImage *image1 = [UIImage imageNamed:@"iphone_ad_1.png"];
	UIImage *image2 = [UIImage imageNamed:@"iphone_ad_2.png"];
	UIImage *image3 = [UIImage imageNamed:@"iphone_ad_3.png"];
	adArray = [NSArray arrayWithObjects:image1, image2, image3, nil];
	
	flist = [NSMutableArray arrayWithObjects:
			 [NSDictionary dictionaryWithObjectsAndKeys:@"추천전형-1 도전", @"content", @"master",@"type", [NSNumber numberWithInt:0], @"order",nil],
			 [NSDictionary dictionaryWithObjectsAndKeys:@"추천전형-1 소신", @"content", @"master",@"type", [NSNumber numberWithInt:1], @"order",nil],
			 [NSDictionary dictionaryWithObjectsAndKeys:@"추천전형-1 안정", @"content", @"master",@"type", [NSNumber numberWithInt:2], @"order",nil],
			 [NSDictionary dictionaryWithObjectsAndKeys:@"추천전형-2 도전", @"content", @"master",@"type", [NSNumber numberWithInt:3], @"order",nil],
			 [NSDictionary dictionaryWithObjectsAndKeys:@"추천전형-2 소신", @"content", @"master",@"type", [NSNumber numberWithInt:4], @"order",nil],
			 [NSDictionary dictionaryWithObjectsAndKeys:@"추천전형-2 안정", @"content", @"master",@"type", [NSNumber numberWithInt:5], @"order",nil],
			 nil];

	
	timer= [NSTimer scheduledTimerWithTimeInterval:15 target:self selector:@selector(onTimer) userInfo:nil repeats:YES];
	
	[self onTimer];

	indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 64.0f, 64.0f)];
	[indicator setCenter:self.view.center];
    [indicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
    [self.view addSubview : indicator];
    [indicator startAnimating];
	
	[self performSelector:@selector(CalcProviso) withObject:nil afterDelay:0];
}

- (void)onTimer{
	int index = arc4random()%3;
	imageAD.image = [adArray objectAtIndex:index];
	imageAD.tag = index;
}


- (void)viewDidUnload
{
	[self setTableView:nil];
	[self setLableMainTitle:nil];
    [self setImageAD:nil];
	[self setLabelTitle:nil];
    [super viewDidUnload];
}
- (BOOL)isExist1:(NSDictionary *)dic {
	BOOL result = NO;
	for (int i=0; i<list1.count; i++) {
		NSDictionary *dict = [list1 objectAtIndex:i];
		NSString *uname = [dict objectForKey:@"U_NM"];
	    if ([uname isEqualToString:[dic objectForKey:@"U_NM"]]){
			NSString *tunm = [dict objectForKey:@"T_NM"];
			if ([tunm isEqualToString:[dic objectForKey:@"T_NM"]]){
				result = YES;
				break;
			}
	    }
	}
	return result;
}

- (BOOL)isExist:(NSDictionary *)dic {
	BOOL result = NO;
	for (int i=0; i<list.count; i++) {
		NSDictionary *dict = [list objectAtIndex:i];
		NSString *uname = [dict objectForKey:@"U_NM"];
	    if ([uname isEqualToString:[dic objectForKey:@"U_NM"]]){
		   NSString *tunm = [dict objectForKey:@"T_NM"];
			if ([tunm isEqualToString:[dic objectForKey:@"T_NM"]]){
				result = YES;
				break;
			}
	    }
	}
	return result;
}

- (void)CalcProviso
{
    NSDictionary *userDic = [User userInfoWithID:ID];
	NSString *hCate = [userDic objectForKey:@"P_Cate"];	
	mf = [userDic objectForKey:@"P_MF"];				
	NSArray *types = [Proviso arrayOfPossibleType:ID];
	for (int i=0; i<types.count; i++) {
		NSLog(@"===================%@  ", [types objectAtIndex:i]);
	}
	
	NSMutableArray *grades;
	NSNumber *avg;
	if (menu==3) {
		avg = [Test avgValue];
		grades= [Test gradeArray];
	} else {
		avg = [Record avgValueWithID:ID];
		grades= [Record gradeArrayWithEmail:ID];
	}
	NSLog (@"avg = %f", [avg doubleValue]);
	
	NSArray *sortedArreay = [UnivInfo typeRankWithArray:types avg:avg series03:series03 series02:series02 series01:series01 mf:mf hCate:hCate grades:grades ID:ID];
	
	for (int i=0; i<sortedArreay.count; i++) {
		NSLog(@"==%@ %f %@ ", [[sortedArreay objectAtIndex:i] objectForKey:@"Rank"],[[[sortedArreay objectAtIndex:i] objectForKey:@"Just"] doubleValue], [[sortedArreay objectAtIndex:i] objectForKey:@"Type"]);
	}
	
	list = [[NSMutableArray alloc] init];
	list1 = [[NSMutableArray alloc] init];	
	int typecount;
	if (sortedArreay.count<2) 
		typecount=sortedArreay.count;
	else typecount = 2;
	
	for (int i=0; i<2; i++) {
		NSDictionary *dic = [sortedArreay objectAtIndex:i];
		NSString *atype = [dic objectForKey:@"Type"];
		
		NSArray *listFinal = [UnivInfo typesInfoWithType:atype avg:avg series03:series03 series02:series02 series01:series01 mf:mf];
		for (int k=0; k<listFinal.count; k++) {
			NSDictionary *dica = [listFinal objectAtIndex:k];
				NSLog(@" %@ %@ ", [dica objectForKey:@"U_NM"], [dica objectForKey:@"TU_NM"]);
		}

		
		
		for (int j=0; j<listFinal.count; j++) {
			
			NSDictionary *dic = [listFinal objectAtIndex:j];
			if (i==0){
				if ([self isExist:dic]) continue;
			} else if (i==1){
				if ([self isExist1:dic]) continue;
			}
			
			NSString *proviso = [dic objectForKey:@"U_PROVISO"];
			NSLog(@" %@ %@ %@", [dic objectForKey:@"U_NM"], [dic objectForKey:@"TU_NM"],  [dic objectForKey:@"U_PROVISO"]);
			BOOL isPass = [Proviso isPassWithID:ID grades:grades proviso:proviso hCate:hCate];
			
			if (isPass) {
				NSLog(@"Pass");
				
				if (i==0) [list addObject:dic]; 
				else if (i==1) [list1 addObject:dic]; 
				
				if (menu ==3){
					if (list.count>=2){
						[self.tableView reloadData];
						[indicator stopAnimating];
						return;
					}
				} else {
					if (list.count>=16) {
						
//						[self.tableView reloadData];
//						[indicator stopAnimating];
//						return;
					}
				}
				
				if (i==0 && list.count==10) break;
				if (i==1 && list1.count==6) {
					[self.tableView reloadData];
					[indicator stopAnimating];
					break;
				}
			}
		}
	}
	

			 
	[self.tableView reloadData];
	[indicator stopAnimating];
}


- (IBAction)actionBack:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)actionHome:(id)sender {
	[self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (menu == 5) return [flist count];
	else return [list count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
	}
	 
	
	if (menu==3) {
		NSDictionary *dic =[list objectAtIndex:indexPath.row];
		cell.textLabel.text = [NSString stringWithFormat:@"%@ - %@", [dic objectForKey:@"U_NM"], [dic objectForKey:@"U_UNIT"]];
	} else {
		NSDictionary *dic =[flist objectAtIndex:indexPath.row];
		if ([[dic objectForKey:@"type"] isEqualToString:@"master"]) {
			cell.textLabel.text = [dic objectForKey:@"content"];
			cell.contentView.backgroundColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"list_bar_yellow_title.png"]];
			cell.indentationLevel = 0;
		} else {
			cell.textLabel.text = [dic objectForKey:@"content"];
			cell.contentView.backgroundColor = [UIColor clearColor];
			cell.indentationLevel = 1;
		}
	}
	
	cell.textLabel.backgroundColor =[UIColor clearColor];
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (menu==3) {
		RecResultViewController *viewController = [[RecResultViewController alloc] init];
		
		NSDictionary *dic = [list objectAtIndex:indexPath.row];
		viewController.type	= [dic objectForKey:@"T_NM"];
		viewController.univ = [dic objectForKey:@"U_NM"];
		viewController.menu = menu;
		viewController.univCode = [dic objectForKey:@"U_CODE"];
		viewController.ID = ID;
		viewController.series03 = series03;
		viewController.series02 = series02;
		viewController.series01 = series01;
		[self.navigationController pushViewController:viewController animated:YES];
	} else {
		NSDictionary *dic = [flist objectAtIndex:indexPath.row];
		if ([[dic objectForKey:@"type"] isEqual:@"master"]) {
			NSMutableArray *array = [NSMutableArray array];
			
			for (NSDictionary *dic in flist){
				if (![[dic objectForKey:@"type"] isEqual:@"master"]) {
					[array addObject:dic];
				}
			}
			
			for (id object in array) {
				[flist removeObject:object];
			}
			
			if (selectedRow != indexPath.row) {
				int count;

				int order = [[dic objectForKey:@"order"] intValue];
				
				switch (order) {
					case 0:{
						if (list.count<3) count = list.count;
						else count = 3;
						for (int i=0; i<count; i++) {
							NSDictionary *dic2 = [list objectAtIndex:i];
							NSString *content =[NSString stringWithFormat:@"%@", [dic2 objectForKey:@"U_NM"]];
							NSDictionary *dic1 = [NSDictionary dictionaryWithObjectsAndKeys:content, @"content",@"detail",@"type",[dic2 objectForKey:@"U_CODE"], @"ucode", [dic2 objectForKey:@"U_NM"], @"uname",[dic2 objectForKey:@"T_NM"], @"t_nm" , [dic2 objectForKey:@"U_UNIT"], @"uunit" , nil];
							[flist insertObject:dic1 atIndex:order+i+1];
						}
						break;	
					}
					case 1:{
						if (list.count<6) count = list.count;
						else count = 6;
						for (int i=3; i<count; i++) {
							NSDictionary *dic2 = [list objectAtIndex:i];
							NSString *content =[NSString stringWithFormat:@"%@", [dic2 objectForKey:@"U_NM"]];
							NSDictionary *dic1 = [NSDictionary dictionaryWithObjectsAndKeys:content, @"content",@"detail",@"type", [dic2 objectForKey:@"U_CODE"], @"ucode", [dic2 objectForKey:@"U_NM"], @"uname",[dic2 objectForKey:@"T_NM"], @"t_nm" , [dic2 objectForKey:@"U_UNIT"], @"uunit" ,nil];
							[flist insertObject:dic1 atIndex:order+i-3+1];
						}
						break;	
					}
					case 2:{
						if (list.count<10) count = list.count;
						else count = 10;
						for (int i=6; i<count; i++) {
							NSDictionary *dic2 = [list objectAtIndex:i];
							NSString *content =[NSString stringWithFormat:@"%@", [dic2 objectForKey:@"U_NM"]];
							NSDictionary *dic1 = [NSDictionary dictionaryWithObjectsAndKeys:content, @"content",@"detail",@"type",[dic2 objectForKey:@"U_CODE"], @"ucode", [dic2 objectForKey:@"U_NM"], @"uname",[dic2 objectForKey:@"T_NM"], @"t_nm" , [dic2 objectForKey:@"U_UNIT"], @"uunit" ,nil];
							[flist insertObject:dic1 atIndex:order+i-6+1];
						}
						break;	
					}
					case 3:{
						if (list1.count<2) count = list1.count;
						else count = 2;
						for (int i=0; i<count; i++) {
							NSDictionary *dic2 = [list1 objectAtIndex:i];
							NSString *content =[NSString stringWithFormat:@"%@", [dic2 objectForKey:@"U_NM"]];
							NSDictionary *dic1 = [NSDictionary dictionaryWithObjectsAndKeys:content, @"content",@"detail",@"type",[dic2 objectForKey:@"U_CODE"], @"ucode", [dic2 objectForKey:@"U_NM"], @"uname",[dic2 objectForKey:@"T_NM"], @"t_nm" , [dic2 objectForKey:@"U_UNIT"], @"uunit" ,nil];
							[flist insertObject:dic1 atIndex:order+i+1];
						}
						break;	
					}
					case 4:{
						if (list1.count<4) count = list1.count;
						else count = 4;
						for (int i=2; i<count; i++) {
							NSDictionary *dic2 = [list1 objectAtIndex:i];
							NSString *content =[NSString stringWithFormat:@"%@", [dic2 objectForKey:@"U_NM"]];
							NSDictionary *dic1 = [NSDictionary dictionaryWithObjectsAndKeys:content, @"content",@"detail",@"type",[dic2 objectForKey:@"U_CODE"], @"ucode", [dic2 objectForKey:@"U_NM"], @"uname",[dic2 objectForKey:@"T_NM"], @"t_nm" , [dic2 objectForKey:@"U_UNIT"], @"uunit" ,nil];
							[flist insertObject:dic1 atIndex:order+i-2+1];
						}
						break;	
					}
					case 5:{
						if (list1.count<6) count = list1.count;
						else count = 6;
						for (int i=4; i<count; i++) {
							NSDictionary *dic2 = [list1 objectAtIndex:i];
							NSString *content =[NSString stringWithFormat:@"%@", [dic2 objectForKey:@"U_NM"]];
							NSDictionary *dic1 = [NSDictionary dictionaryWithObjectsAndKeys:content, @"content",@"detail",@"type",[dic2 objectForKey:@"U_CODE"], @"ucode", [dic2 objectForKey:@"U_NM"], @"uname",[dic2 objectForKey:@"T_NM"], @"t_nm", [dic2 objectForKey:@"U_UNIT"], @"uunit" , nil];
							[flist insertObject:dic1 atIndex:order+i-4+1];
						}
						break;	
					}
					default:
						break;
				}

				selectedRow = indexPath.row;
				[tableView reloadData];
			}else {
				selectedRow = -1;
				[tableView reloadData];
			}
		} 
		else {
			RecResultViewController *viewController = [[RecResultViewController alloc] init];
			
			NSDictionary *dic = [flist objectAtIndex:indexPath.row];
			viewController.type	= [dic objectForKey:@"t_nm"];
			viewController.univ = [dic objectForKey:@"uname"];
			viewController.univCode = [dic objectForKey:@"ucode"];
			viewController.ID = ID;
			
			viewController.series03 = series03;
			viewController.series02 = series02;
			viewController.series01 = series01;
			[self.navigationController pushViewController:viewController animated:YES];
		}
	}
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	UITouch *touch = [touches anyObject];
	if (touch.view == imageAD ) {
		
		ADPop *adPop = [[ADPop alloc] initWithFrame:CGRectMake(0, 0, 320, 460)];
		[self.view addSubview:adPop];
		adPop.index = touch.view.tag;
	}
}


@end
