//
//  AppDelegate.m
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 6. 6..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import "AppDelegate.h"

#import "LoginViewController.h"

@implementation AppDelegate

@synthesize window = _window;
@synthesize navigationController = _navigationController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.

	LoginViewController *loginViewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
	self.navigationController = [[UINavigationController alloc] initWithRootViewController:loginViewController];
	self.window.rootViewController = self.navigationController;
    [self.window makeKeyAndVisible];
	[self copyDatabase];
	[NSThread sleepForTimeInterval:2];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
	/*
	 Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
	 Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
	 */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
	/*
	 Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
	 If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
	 */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
	/*
	 Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
	 */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
	/*
	 Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
	 */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
	/*
	 Called when the application is about to terminate.
	 Save data if appropriate.
	 See also applicationDidEnterBackground:.
	 */
}

- (NSString *)getDBPath {
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDir =[paths objectAtIndex:0];
	return [documentsDir stringByAppendingFormat:@"/mysql.sqlite"];
	
}

- (NSString *)getDBPath1 {
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDir =[paths objectAtIndex:0];
	return [documentsDir stringByAppendingFormat:@"/soosiro.sqlite"];
	
}

- (void)copyDatabase {
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSError *error;
	NSString *dbPath = [self getDBPath];
	BOOL success = [fileManager fileExistsAtPath:dbPath];
	if(!success) {
		NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingFormat:@"/mysql.sqlite"];
		NSLog(@"%@  %@", defaultDBPath, dbPath);
		success = [fileManager copyItemAtPath:defaultDBPath toPath:dbPath error:&error];
	}
	
	dbPath = [self getDBPath1];
	success = [fileManager fileExistsAtPath:dbPath];
	if(!success) {
		NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingFormat:@"/soosiro.sqlite"];
		NSLog(@"%@  %@", defaultDBPath, dbPath);
		success = [fileManager copyItemAtPath:defaultDBPath toPath:dbPath error:&error];
	}
}

@end
