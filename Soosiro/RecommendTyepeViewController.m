//
//  RecommendTyepeViewController.m
//  Soosiro
//
//  Created by Yeongrok Lee on 12. 7. 14..
//  Copyright (c) 2012년 Happy Potion. All rights reserved.
//

#import "RecommendTyepeViewController.h"
#import "User.h"
#import "Record.h"
#import "SeriesViewController.h"
#import "Proviso.h"
@interface RecommendTyepeViewController (){
	UIActivityIndicatorView *indicator;
}

@end

@implementation RecommendTyepeViewController{
	NSString *selectedType;
	NSMutableArray *list;
	NSArray *sortedList;
	NSString *mf;
}
@synthesize ID;
@synthesize tableView=_tableView;
@synthesize labelName;
@synthesize lableMainTitle;
@synthesize menu;


- (BOOL)isSociety:(NSDictionary *)dic {
	BOOL result = NO;
	for (int i=16; i<30; i++) {
		NSString *key = [NSString stringWithFormat:@"P_INFO%2d", i];
		NSString *value = [dic objectForKey:key];
		if (value == @"Y") {
			result = YES;
			break;
		}
	}
	return result;
}

- (BOOL)isChance:(NSDictionary *)dic {
	BOOL result = NO;
	for (int i=11; i<16; i++) {
		NSString *key = [NSString stringWithFormat:@"P_INFO%2d", i];
		NSString *value = [dic objectForKey:key];
		if (value == @"Y") {
			result = YES;
			break;
		}
	}
	return result;
}

- (void)initView
{
	NSDictionary *userDic = [User userInfoWithID:ID];
	NSString *hCate = [userDic objectForKey:@"P_Cate"];	
	mf = [userDic objectForKey:@"P_MF"];				
	NSNumber *avg = [Record avgValueWithID:ID];
	NSMutableArray *grades= [Record gradeArrayWithEmail:ID];
   	NSArray *types = [Proviso arrayOfPossibleType:ID];
	sortedList =  [UnivInfo typeRankWithArray:types avg:avg  mf:mf  hCate:hCate grades:grades ID:ID];

   for (int i = 0; i<sortedList.count; i++) {
   	NSLog(@"%d %f %@", [[[sortedList objectAtIndex:i] objectForKey:@"Rank"] intValue], [[[sortedList objectAtIndex:i] objectForKey:@"Just"] doubleValue] , [[sortedList objectAtIndex:i] objectForKey:@"Type"]);
   }
	
	[self.tableView reloadData];     
	[indicator stopAnimating];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	lableMainTitle.text = [Title mainTilteWithMenu:menu];
	NSDictionary *userDic = [User userInfoWithID:ID];
	labelName.text = [NSString stringWithFormat:@"%@님에게",[userDic objectForKey:@"P_Nm"]];
	indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 64.0f, 64.0f)];
	[indicator setCenter:self.view.center];
    [indicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
    [self.view addSubview : indicator];	
	
	indicator.hidden= FALSE;
	[indicator startAnimating];
	
	[self performSelector:@selector(initView) withObject:nil afterDelay:0];

	
}

- (void)viewDidUnload
{
	[self setTableView:nil];
    [self setLabelName:nil];
    [self setLableMainTitle:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)goBack:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	//return list.count;
	return sortedList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
		
	}
	
	//cell.textLabel.text = [list objectAtIndex:indexPath.row]; 
	cell.textLabel.text = [[sortedList objectAtIndex:indexPath.row] objectForKey:@"Type"]; 
	cell.textLabel.backgroundColor =[UIColor clearColor];
	
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	SeriesViewController *viewController = [[SeriesViewController alloc] init];
	viewController.ID = ID;
	viewController.menu = 4;
	viewController.type = [[sortedList objectAtIndex:indexPath.row] objectForKey:@"Type"];
	viewController.mf = mf; 
	
	[self.navigationController pushViewController:viewController animated:YES];
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	
}
@end
